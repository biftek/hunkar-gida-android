package com.biftekyazilim.hunkargida.view;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.listener.OnResultRowSelected;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.ReflectionUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.vo.CariAdresVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;

public class CariAdreslerDialog {

	private Dialog popupDialog;

	private Context context;
	private TableHeaderVO[] tableHeaderList;
	private TableLayout tblSearchResult;
	private Button btnCancel;
	private String[] tableColNameList;
	private String[] objPropertyNameList;
	private String cariKodu;
	private OnResultRowSelected<CariAdresVO> rowSelected;
	
	public CariAdreslerDialog(Context context, String cariKodu) {
		this.cariKodu = cariKodu;
		this.context = context;
		init();
		
	}
	
	
	private void init() {
		
		popupDialog = new Dialog(context);
		popupDialog.setContentView(R.layout.dialog_cari_adresler);
		popupDialog.setTitle("Cari Adresleri");
		popupDialog.setCancelable(true);
		
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		lp.copyFrom(popupDialog.getWindow().getAttributes());
		lp.width = 800;
		lp.height = WindowManager.LayoutParams.MATCH_PARENT;
		popupDialog.getWindow().setAttributes(lp);
		
		tblSearchResult = (TableLayout)popupDialog.findViewById(R.id.tblSearchResult);
		btnCancel = (Button)popupDialog.findViewById(R.id.btnCancel);
		
		TableRow headerRow = (TableRow)popupDialog.findViewById(R.id.headerRow);
		
		tableColNameList = new String[]{
				"Cadde",
				"Sokak",
				"Posta Kodu",
				"İlçe",
				"İl",
				"Telefon",
				"Fax"
		};
		
		objPropertyNameList = new String[]{
				"adr_cadde",
				"adr_sokak",
				"adr_posta_kodu",
				"adr_ilce",
				"adr_il",
				"tel1",
				"fax"
		};
		
		tableHeaderList = new TableHeaderVO[tableColNameList.length];
		int weight = 1000 / tableColNameList.length;
		for (int i = 0; i < tableColNameList.length; i++) {
			tableHeaderList[i] = new TableHeaderVO(tableColNameList[i], weight);
		}
		
		TableLayoutUtil.setHeader(context, headerRow, tableHeaderList);
		
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				popupDialog.dismiss();
			}
		});
	}
	public void setOnRowSelectedListener(OnResultRowSelected<CariAdresVO> rowSelected) {
		this.rowSelected = rowSelected;
	}
	private void loadCariAdresler() {
		final ProgressDialog dialog = ProgressDialog.show(context, "", 
                "Yükleniyor. Bekleyiniz...", true);
		
		RestBO.getInstance(context).getService(GetService.class).getCariAdresler(cariKodu, new Callback<List<CariAdresVO>>() {
			
			@Override
			public void success(List<CariAdresVO> result, Response arg1) {
				
				if(result.size() == 0) {
					Toast.makeText(context, "Bu cariye ait adres bulunmamaktadır.", Toast.LENGTH_LONG).show();
				} else {
					fillResultTable(result);
					dialog.dismiss();
					popupDialog.show();
				}
				
			}
			
			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(context, err.getMessage(), Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}
		});
		
		
	}
	
	private void fillResultTable(List<CariAdresVO> result) {
		tblSearchResult.removeAllViews();
		int rowCounter = 0;
		for (CariAdresVO item : result) {
			
			TableRow tRow = new TableRow(context);
			tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tRow.setMinimumHeight(65);
			tRow.setTag(item);
			tRow.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					CariAdresVO it = (CariAdresVO) arg0.getTag();
					if(rowSelected != null) {
						rowSelected.onSelected(it);
					}
					popupDialog.dismiss();
				}
			});
			TextView[] tvList = new TextView[tableColNameList.length];
			
			for (int i = 0; i < tvList.length; i++) {
				tvList[i] = new TextView(context);
			}
			
			int col = 0;
			for (TextView textView : tvList) {
				
				int weight = tableHeaderList[col].getWeight();
				if(rowCounter%2 == 0) {
					textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
				} else {
					textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
				}
				textView.setGravity(Gravity.CENTER);
				textView.setTextColor(Color.BLACK);
				
				TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,weight);
				params.column = col;
				textView.setLayoutParams(params);
				col++;

				tRow.addView(textView);
			}

			
			rowCounter++;
			
			for (int i = 0; i < tvList.length; i++) {
				
				String textVal = ReflectionUtil.getValue(objPropertyNameList[i], item) + "";
				
				tvList[i].setText(textVal);
			}
			
			tblSearchResult.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
		}
		
	}
	
	
	public void show() {
		loadCariAdresler();
	}
}
