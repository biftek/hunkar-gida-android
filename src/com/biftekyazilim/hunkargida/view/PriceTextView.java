package com.biftekyazilim.hunkargida.view;

import com.biftekyazilim.hunkargida.listener.OnCurrencyEditTextFocusListener;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;

import android.content.Context;
import android.widget.TextView;

public class PriceTextView extends TextView {

	public PriceTextView(Context context) {
		super(context);
		
		this.setOnFocusChangeListener(new OnCurrencyEditTextFocusListener());
	}

	public void setText(double num) {
		this.setText(NumberFormatUtil.formatPrice(num));
	}
}
