package com.biftekyazilim.hunkargida.view;

import android.content.Context;

import com.biftekyazilim.hunkargida.listener.OnCurrencyEditTextFocusListener;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;

public class PriceEditText extends NumberEditText {

	public PriceEditText(Context context) {
		super(context);
		
		this.setOnFocusChangeListener(new OnCurrencyEditTextFocusListener());
	}

	@Override
	public void setText(double num) {
		this.setText(NumberFormatUtil.formatPrice(num));
	}
}
