package com.biftekyazilim.hunkargida.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.biftekyazilim.hunkargida.util.UIUtil;

public class GenericEditText extends EditText {

	public GenericEditText(Context context) {
		super(context);
		init();
	}
	
	public GenericEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	
	public GenericEditText(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	private void init() {
		this.setOnKeyListener(new OnKeyListener() {
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// If the event is a key-down event on the "enter" button
				if ((event.getAction() == KeyEvent.ACTION_DOWN)
						&& (keyCode == KeyEvent.KEYCODE_ENTER)) {
					
					UIUtil.hideKeyboard(getContext(), v);
					
					return true;
				}
				return false;
			}
		});
	}
}
