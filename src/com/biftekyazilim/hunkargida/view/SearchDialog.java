package com.biftekyazilim.hunkargida.view;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.graphics.Color;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.enm.SearchService;
import com.biftekyazilim.hunkargida.listener.OnResultRowSelected;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.ReflectionUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.SiparisSearchVO;
import com.biftekyazilim.hunkargida.vo.StokSearchVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;
import com.biftekyazilim.hunkargida.vo.TeklifSearchVO;

public class SearchDialog<T> {

	
	
	private Dialog popupDialog;

	private Context context;
	private Button btnSearchStartEditText;
	private Button btnSearchStartRadio;
	private RadioButton rbtnChoose1;
	private RadioButton rbtnChoose2;
	private EditText etKeyword;

	private TextView txtKeyword1;
	private TextView txtKeyword2;
	private EditText etKeyword1;
	private EditText etKeyword2;
	private NumberEditText etKeywordNumber1;
	private NumberEditText etKeywordNumber2;
	private TableLayout tblSearchResult;
	private Button btnCancel;
	private LinearLayout llEditTextInputContainer;
	private LinearLayout llRadioInputContainer;
	
	private TableHeaderVO[] tableHeaderList;
	private OnResultRowSelected<T> rowSelected;
	
	private String[] inputNameValueList;
	private String[] tableColNameList;
	private String[] objPropertyNameList;
	private SearchService searchService;
	
	private boolean isMultipleChoice;
	
	public static List<?> resultList;
	
	private String title;
	
	private List displayList;
	
	public SearchDialog(Context context, String title, SearchService searchService, boolean isMultipleChoice, String[] inputNameValueList, String[] tableColNameList, String[] objPropertyNameList, OnResultRowSelected<T> rowSelected) {

		this.title = title;
		this.context = context;
		this.rowSelected = rowSelected;
		this.inputNameValueList = inputNameValueList;
		this.tableColNameList = tableColNameList;
		this.searchService = searchService;
		this.objPropertyNameList = objPropertyNameList;
		this.isMultipleChoice = isMultipleChoice;
		
		init();
		initHand();
	}
	
	private void init() {
		
		popupDialog = new Dialog(context);
		popupDialog.setContentView(R.layout.dialog_search);
		popupDialog.setTitle(title);
		popupDialog.setCancelable(true);
		
		btnSearchStartEditText = (Button)popupDialog.findViewById(R.id.btnSearchStartEditText);
		btnSearchStartRadio = (Button)popupDialog.findViewById(R.id.btnSearchStartRadio);
		rbtnChoose1 = (RadioButton)popupDialog.findViewById(R.id.rbtnChoose1);
		rbtnChoose2 = (RadioButton)popupDialog.findViewById(R.id.rbtnChoose2);
		etKeyword = (EditText)popupDialog.findViewById(R.id.etKeyword);
		etKeyword1 = (EditText)popupDialog.findViewById(R.id.etKeyword1);
		etKeyword2 = (EditText)popupDialog.findViewById(R.id.etKeyword2);
		etKeywordNumber1 = (NumberEditText)popupDialog.findViewById(R.id.etKeywordNumber1);
		etKeywordNumber2 = (NumberEditText)popupDialog.findViewById(R.id.etKeywordNumber2);
		txtKeyword1 = (TextView)popupDialog.findViewById(R.id.txtKeyword1);
		txtKeyword2 = (TextView)popupDialog.findViewById(R.id.txtKeyword2);
		tblSearchResult = (TableLayout)popupDialog.findViewById(R.id.tblSearchResult);
		btnCancel = (Button)popupDialog.findViewById(R.id.btnCancel);
		llEditTextInputContainer = (LinearLayout)popupDialog.findViewById(R.id.llEditTextInputContainer);
		llRadioInputContainer = (LinearLayout)popupDialog.findViewById(R.id.llRadioInputContainer);
		
		if(isMultipleChoice) {
			llRadioInputContainer.setVisibility(View.VISIBLE);
			llEditTextInputContainer.setVisibility(View.GONE);
			
			rbtnChoose1.setText(inputNameValueList[0]);
			rbtnChoose1.setTag(inputNameValueList[1]);
			rbtnChoose2.setText(inputNameValueList[2]);
			rbtnChoose2.setTag(inputNameValueList[3]);
			
		} else {
			llRadioInputContainer.setVisibility(View.GONE);
			llEditTextInputContainer.setVisibility(View.VISIBLE);
			
			txtKeyword1.setText(inputNameValueList[0]);
			txtKeyword2.setText(inputNameValueList[1]);
		}
		
		TableRow headerRow = (TableRow)popupDialog.findViewById(R.id.headerRow);
		
		
		
		tableHeaderList = new TableHeaderVO[tableColNameList.length];
		int weight = 1000 / tableColNameList.length;
		for (int i = 0; i < tableColNameList.length; i++) {
			tableHeaderList[i] = new TableHeaderVO(tableColNameList[i], weight);
		}
		
		TableLayoutUtil.setHeader(context, headerRow, tableHeaderList, new OnClickListener() {
			
			@SuppressWarnings("unchecked")
			@Override
			public void onClick(View vw) {
				if(displayList == null || displayList.size() == 0) {
					return;
				}
				int col = (int)vw.getTag();
				final boolean desc = col < 0 ? true : false;
				vw.setTag(-col);
				col = Math.abs(col)-1;
				
				final String propertyName = objPropertyNameList[col];
				Collections.sort(displayList, new Comparator<T>() {

					@Override
					public int compare(T arg0, T arg1) {
						
						String val1 = ReflectionUtil.getValue(propertyName, arg0) + "";
						String val2 = ReflectionUtil.getValue(propertyName, arg1) + "";
						return (desc ? -1 : 1) * val1.compareTo(val2);
					}
				});
				
				fillResultTable(displayList);
			}
		});
		
	}
	
	private void initHand() {
		
		btnSearchStartRadio.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				

				EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
				EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
				
				UIUtil.hideKeyboard(context, etKeyword.hasFocus() ? etKeyword : (etKeyword1.hasFocus() ? etKeyword1 : etKeyword2));
				
				if(etKeyword.getText().toString().trim().length() == 0) {
					/*new AlertDialog.Builder(context)
				    .setTitle(R.string.app_name)
				    .setMessage("En az 1 karakter girmeniz gerekmektedir.")
				    .setPositiveButton(android.R.string.ok, null)
				    .setIcon(android.R.drawable.ic_dialog_alert)
				     .show();*/
					return;
				}
				
				doSearch(rbtnChoose1.isChecked(), etKeyword.getText().toString());
			}
		});
		
		btnSearchStartEditText.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				

				EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
				EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
				
				UIUtil.hideKeyboard(context, etKeyword.hasFocus() ? etKeyword : (etKeyword1.hasFocus() ? etKeyword1 : etKeyword2));
				
				if(etKeyword1.getText().toString().trim().length() == 0 && etKeyword2.getText().toString().trim().length() == 0) {
					/*new AlertDialog.Builder(context)
				    .setTitle(R.string.app_name)
				    .setMessage("En az 1 karakter girmeniz gerekmektedir.")
				    .setPositiveButton(android.R.string.ok, null)
				    .setIcon(android.R.drawable.ic_dialog_alert)
				     .show();*/
					return;
				}
				
				doSearch(etKeyword1.getText()+"", etKeyword2.getText()+"");
			}
		});
		
		popupDialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface arg0) {
				

				EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
				EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
				
				UIUtil.hideKeyboard(context, etKeyword.hasFocus() ? etKeyword : (etKeyword1.hasFocus() ? etKeyword1 : etKeyword2));
				popupDialog.dismiss();
			}
		});
		
		btnCancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
				EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
				
				UIUtil.hideKeyboard(context, etKeyword.hasFocus() ? etKeyword : (etKeyword1.hasFocus() ? etKeyword1 : etKeyword2));
				popupDialog.dismiss();
			}
		});
	}
	
	public void setEditTextNumber(int index) {
		
		switch (index % 2) {
		case 0:
			
			etKeyword1.setVisibility(View.GONE);
			etKeywordNumber1.setVisibility(View.VISIBLE);
			
			break;
		case 1:
			
			etKeyword2.setVisibility(View.GONE);
			etKeywordNumber2.setVisibility(View.VISIBLE);
			
			break;

		default:
			break;
		}
		
	}
	
	private void doSearch(String keyword1, String keyword2) { 
		

		EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
		EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
		
		String evrakSiraNo = etKeyword2.getText() + "";
		String evrakSeriNo = etKeyword1.getText() + "";
		tblSearchResult.removeAllViews();
		final ProgressDialog dialog = ProgressDialog.show(context, "", 
                "Yükleniyor. Bekleyiniz...", true);
		
		
		 if(searchService == SearchService.TEKLIF) {
				
				Callback<List<TeklifSearchVO>> callback = new Callback<List<TeklifSearchVO>>() {
					
					@Override
					public void success(List<TeklifSearchVO> result, Response arg1) {
						resultList = result;
						
						/*List<TeklifVO> clearedTeklifList = new ArrayList<>(result);
						for (TeklifVO teklifVO : result) {
							
							TeklifVO removeTeklif = null;
							int counter = 0;
							for (TeklifVO teklifVO2 : clearedTeklifList) {
								
								if(teklifVO2.getTkl_fileid() == teklifVO.getTkl_fileid()) {
									counter++;
								}
								if(counter > 1) {
									removeTeklif = teklifVO2;
									break;
								}
							}
							if(removeTeklif != null) {
								clearedTeklifList.remove(removeTeklif);
							}
						}*/

						displayList = result;
						fillResultTable((List<T>) displayList);
						dialog.dismiss();
					}
					
					@Override
					public void failure(RetrofitError err) {
						Toast.makeText(context, err.getMessage(), Toast.LENGTH_LONG).show();
						err.printStackTrace();
						dialog.dismiss();
					}
				};
				
				if(evrakSiraNo.trim().length() == 0) {
					RestBO.getInstance(context).getService(GetService.class).searchTeklif(evrakSeriNo, callback);
				} else {
					RestBO.getInstance(context).getService(GetService.class).searchTeklif(evrakSeriNo, evrakSiraNo, callback);
				}
			} else if (searchService == SearchService.SIPARIS) {
				
				Callback<List<SiparisSearchVO>> callback = new Callback<List<SiparisSearchVO>>() {

					@Override
					public void failure(RetrofitError err) {
						Toast.makeText(context, err.getMessage(), Toast.LENGTH_LONG).show();
						err.printStackTrace();
						dialog.dismiss();
					}

					@Override
					public void success(List<SiparisSearchVO> result, Response arg1) {
						
						resultList = result;
						
						/*List<SiparisVO> clearedList = new ArrayList<>(result);
						for (SiparisVO siparis : result) {
							
							SiparisVO removeSiparis = null;
							int counter = 0;
							for (SiparisVO siparis2 : clearedList) {
								
								if(siparis2.getSip_fileid() == siparis.getSip_fileid()) {
									counter++;
								}
								if(counter > 1) {
									removeSiparis = siparis2;
									break;
								}
							}
							if(removeSiparis != null) {
								clearedList.remove(removeSiparis);
							}
						}*/

						displayList = result;
						fillResultTable((List<T>) displayList);
						dialog.dismiss();
						
					}

				};
				if(evrakSiraNo.trim().length() == 0) {
					RestBO.getInstance(context).getService(GetService.class).searchSiparis(evrakSeriNo, callback);
				} else {
					RestBO.getInstance(context).getService(GetService.class).searchSiparis(evrakSeriNo, evrakSiraNo, callback);
				}
				
			}
		 
	}
	private void doSearch(boolean isChoose1, String keyword) {
		
		tblSearchResult.removeAllViews();
		final ProgressDialog dialog = ProgressDialog.show(context, "", 
                "Yükleniyor. Bekleyiniz...", true);
		
		
		
		if(searchService == SearchService.STOK) {
			
			String searchBy = (String) (isChoose1 ? rbtnChoose1.getTag() : rbtnChoose2.getTag());
			RestBO.getInstance(context).getService(GetService.class).searchStok(searchBy, keyword, new Callback<List<StokSearchVO>>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(context, err.getMessage(), Toast.LENGTH_LONG).show();
					err.printStackTrace();
					dialog.dismiss();
				}

				@SuppressWarnings("unchecked")
				@Override
				public void success(List<StokSearchVO> result, Response arg1) {
					resultList = result;
					displayList = result;
					fillResultTable((List<T>) result);
					dialog.dismiss();
				}
			});
			
		} else if (searchService == SearchService.CARI) {
			
			String searchBy = (String) (isChoose1 ? rbtnChoose1.getTag() : rbtnChoose2.getTag());
			RestBO.getInstance(context).getService(GetService.class).searchCari(searchBy, keyword, new Callback<List<CariSummaryVO>>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(context, err.getMessage(), Toast.LENGTH_LONG).show();
					err.printStackTrace();
					dialog.dismiss();
				}

				@SuppressWarnings("unchecked")
				@Override
				public void success(List<CariSummaryVO> result, Response arg1) {
					resultList = result;
					displayList = result;
					fillResultTable((List<T>) result);
					dialog.dismiss();
				}
			});
			
		}
		
	}
	
	private void fillResultTable(List<T> result) {
		
		EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
		EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
		
		UIUtil.hideKeyboard(context, etKeyword.hasFocus() ? etKeyword : (etKeyword1.hasFocus() ? etKeyword1 : etKeyword2));
		
		
		tblSearchResult.removeAllViews();
		int rowCounter = 0;
		for (T item : result) {
			
			TableRow tRow = new TableRow(context);
			tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tRow.setMinimumHeight(45);
			tRow.setTag(item);
			tRow.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					@SuppressWarnings("unchecked")
					T it = (T) arg0.getTag();
					if(rowSelected != null) {
						rowSelected.onSelected(it);
					}
					popupDialog.dismiss();
				}
			});
			
			TextView[] tvList = new TextView[tableColNameList.length];
			
			for (int i = 0; i < tvList.length; i++) {
				tvList[i] = new TextView(context);
			}
			
			int col = 0;
			for (TextView textView : tvList) {
				
				int weight = tableHeaderList[col].getWeight();
				if(rowCounter%2 == 0) {
					textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
				} else {
					textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
				}
				textView.setGravity(Gravity.CENTER);
				textView.setTextColor(Color.BLACK);
				
				TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,weight);
				params.column = col;
				textView.setLayoutParams(params);
				col++;

				tRow.addView(textView);
			}

			
			rowCounter++;
			
			for (int i = 0; i < tvList.length; i++) {
				
				String textVal = ReflectionUtil.getValue(objPropertyNameList[i], item) + "";
				
				tvList[i].setText(textVal);
			}
			
			tblSearchResult.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
		}
		
	}
	
	
	public void show() {
		show(false);
	}
	
	public void show(boolean startSearch) {
		popupDialog.show();
		if(startSearch) {
			
			EditText etKeyword1 = SearchDialog.this.etKeyword1.getVisibility() == View.GONE ? etKeywordNumber1 : SearchDialog.this.etKeyword1;
			EditText etKeyword2 = SearchDialog.this.etKeyword2.getVisibility() == View.GONE ? etKeywordNumber2 : SearchDialog.this.etKeyword2;
			
			UIUtil.hideKeyboard(context, etKeyword.hasFocus() ? etKeyword : (etKeyword1.hasFocus() ? etKeyword1 : etKeyword2));
			
			if(etKeyword1.getText().toString().trim().length() == 0 && etKeyword2.getText().toString().trim().length() == 0) {
				/*new AlertDialog.Builder(context)
			    .setTitle(R.string.app_name)
			    .setMessage("En az 1 karakter girmeniz gerekmektedir.")
			    .setPositiveButton(android.R.string.ok, null)
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();*/
				return;
			}
			
			doSearch(etKeyword1.getText()+"", etKeyword2.getText()+"");
			
		}
	}
	
	public void setTextEditText1(String txt) {
		if(etKeywordNumber1.getVisibility() == View.VISIBLE) {
			etKeywordNumber1.setText(txt);
		} else {
			etKeyword1.setText(txt);
		}
		
	}
	public void setTextEditText2(String txt) {
		if(etKeywordNumber2.getVisibility() == View.VISIBLE) {
			etKeywordNumber2.setText(txt);
		} else {
			etKeyword2.setText(txt);
		}
	}
}
