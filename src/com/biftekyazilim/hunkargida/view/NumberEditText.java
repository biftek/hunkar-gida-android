package com.biftekyazilim.hunkargida.view;

import android.content.Context;
import android.text.Editable;
import android.text.InputType;
import android.text.SpannableStringBuilder;
import android.util.AttributeSet;

import com.biftekyazilim.hunkargida.listener.OnPointedNumberEditTextFocusListener;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;

public class NumberEditText extends GenericEditText {

	public NumberEditText(Context context, AttributeSet attr) {
		super(context, attr);
		this.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
	}
	public NumberEditText(Context context) {
		super(context);
		this.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
		this.setOnFocusChangeListener(new OnPointedNumberEditTextFocusListener());
	}

	public void setText(double num) {
		this.setText(NumberFormatUtil.formatPointedNumber(num));
	}
	
	/*@Override
	public Editable getText() {
		Editable edt = super.getText();
		
		return new SpannableStringBuilder(edt.toString().replaceAll(",", ""));
		
	}*/
}
