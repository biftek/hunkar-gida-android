package com.biftekyazilim.hunkargida.fragment;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.MainActivity;
import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.enm.FragmentType;
import com.biftekyazilim.hunkargida.enm.SearchService;
import com.biftekyazilim.hunkargida.listener.OnResultRowSelected;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.view.CariAdreslerDialog;
import com.biftekyazilim.hunkargida.view.SearchDialog;
import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.CariVO;

public class CariKartFragment extends ArgumentableFragment {

	private CariVO cari;

	private Button btnCHKodu;
	private Button btnCHIsmi;
	
	private TextView tvKodu;
	private Button btnAdresler;
	private Button btnCariHesap;
	private Button btnCariStok;
	private Button btnSiparisler;
	private Button btnSiparisVer;
	private Button btnTeklifVer;
	private TextView tvUnvani;
	private TextView tvUnvani2;
	private TextView tvHareketTipi;
	private TextView tvCariBagTipi;
	private TextView tvBagOrtFirmaNo;
	private TextView tvStokAlimTipi;
	private TextView tvStokSatimTipi;
	private TextView tvDovizCinsi1;
	private TextView tvMuhasebeKodu1;
	private TextView tvDovizCinsi2;
	private TextView tvMuhasebeKodu2;
	private TextView tvDovizCinsi3;
	private TextView tvMuhasebeKodu3;
	private TextView tvMuhKodArtikeli;
	private TextView tvAnaCariKodu;
	private TextView tvAnaCariUnvani;
	private TextView tvTemsilciKodu;
	private TextView tvTemsilciAdi;
	private TextView tvGrupKodu;
	private TextView tvGrupAdi;
	private TextView tvSektorKodu;
	private TextView tvSektorAdi;
	private TextView tvBolgeKodu;
	private TextView tvBolgeAdi;
	private TextView tvVergiDairesi;
	private TextView tvFaturaAdresNo;
	private TextView tvVergiNo;
	private TextView tvSevkAdresNo;
	private TextView tvBasitVergi;
	private TextView tvWebAdresi;
	private TextView tvYetkiliCep;
	private TextView tvEmailAdresi;
	private TextView tvKayitTarih;

	public CariKartFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_carikart, container,
				false);

		getActivity().setTitle("Cari Kart");
		
		btnCHKodu = (Button) rootView.findViewById(R.id.btnCHKodu);
		btnCHIsmi = (Button) rootView.findViewById(R.id.btnCHIsmi);
		
		tvKodu = (TextView) rootView.findViewById(R.id.tvKodu);
		btnAdresler = (Button) rootView.findViewById(R.id.btnAdresler);
		btnCariHesap = (Button) rootView.findViewById(R.id.btnCariHesap);
		btnCariStok = (Button) rootView.findViewById(R.id.btnCariStok);
		btnSiparisler = (Button) rootView.findViewById(R.id.btnSiparisler);
		btnSiparisVer = (Button) rootView.findViewById(R.id.btnSiparisVer);
		btnTeklifVer = (Button) rootView.findViewById(R.id.btnTeklifVer);
		tvUnvani = (TextView) rootView.findViewById(R.id.tvUnvani);
		tvUnvani2 = (TextView) rootView.findViewById(R.id.tvUnvani2);
		tvHareketTipi = (TextView) rootView.findViewById(R.id.tvHareketTipi);
		tvCariBagTipi = (TextView) rootView.findViewById(R.id.tvCariBagTipi);
		tvBagOrtFirmaNo = (TextView) rootView.findViewById(R.id.tvBagOrtFirmaNo);
		tvStokAlimTipi = (TextView) rootView.findViewById(R.id.tvStokAlimTipi);
		tvStokSatimTipi = (TextView) rootView.findViewById(R.id.tvStokSatimTipi);
		tvDovizCinsi1 = (TextView) rootView.findViewById(R.id.tvDovizCinsi1);
		tvMuhasebeKodu1 = (TextView) rootView.findViewById(R.id.tvMuhasebeKodu1);
		tvDovizCinsi2 = (TextView) rootView.findViewById(R.id.tvDovizCinsi2);
		tvMuhasebeKodu2 = (TextView) rootView.findViewById(R.id.tvMuhasebeKodu2);
		tvDovizCinsi3 = (TextView) rootView.findViewById(R.id.tvDovizCinsi3);
		tvMuhasebeKodu3 = (TextView) rootView.findViewById(R.id.tvMuhasebeKodu3);
		tvMuhKodArtikeli = (TextView) rootView.findViewById(R.id.tvMuhKodArtikeli);
		tvAnaCariKodu = (TextView) rootView.findViewById(R.id.tvAnaCariKodu);
		tvAnaCariUnvani = (TextView) rootView.findViewById(R.id.tvAnaCariUnvani);
		tvTemsilciKodu = (TextView) rootView.findViewById(R.id.tvTemsilciKodu);
		tvTemsilciAdi = (TextView) rootView.findViewById(R.id.tvTemsilciAdi);
		tvGrupKodu = (TextView) rootView.findViewById(R.id.tvGrupKodu);
		tvGrupAdi = (TextView) rootView.findViewById(R.id.tvGrupAdi);
		tvSektorKodu = (TextView) rootView.findViewById(R.id.tvSektorKodu);
		tvSektorAdi = (TextView) rootView.findViewById(R.id.tvSektorAdi);
		tvBolgeKodu = (TextView) rootView.findViewById(R.id.tvBolgeKodu);
		tvBolgeAdi = (TextView) rootView.findViewById(R.id.tvBolgeAdi);
		tvVergiDairesi = (TextView) rootView.findViewById(R.id.tvVergiDairesi);
		tvFaturaAdresNo = (TextView) rootView.findViewById(R.id.tvFaturaAdresNo);
		tvVergiNo = (TextView) rootView.findViewById(R.id.tvVergiNo);
		tvSevkAdresNo = (TextView) rootView.findViewById(R.id.tvSevkAdresNo);
		tvBasitVergi = (TextView) rootView.findViewById(R.id.tvBasitVergi);
		tvWebAdresi = (TextView) rootView.findViewById(R.id.tvWebAdresi);
		tvYetkiliCep = (TextView) rootView.findViewById(R.id.tvYetkiliCep);
		tvEmailAdresi = (TextView) rootView.findViewById(R.id.tvEmailAdresi);
		tvKayitTarih = (TextView) rootView.findViewById(R.id.tvKayitTarih);
		
		btnAdresler.setEnabled(false);
		btnCariHesap.setEnabled(false);
		btnCariStok.setEnabled(false);
		btnSiparisler.setEnabled(false);
		btnTeklifVer.setEnabled(false);
		btnSiparisVer.setEnabled(false);
		
		initHand();

		/*int cariRecNo = getArguments().getInt("cariRecNo");

		if(cari != null && cariRecNo == cari.getCari_RECno()) {
			fillCari();
		} else {
			loadCari(cariRecNo);
		}*/
		
		if(cari != null) {
			fillCari();
		}
		
		return rootView;
	}

	@Override
	public void onResume() {
		super.onResume();
		
		if(MainActivity.FRAGMENT_CHANGE) {
			MainActivity.FRAGMENT_CHANGE = false;
			reset();
		} else if(cari != null) {
			fillCari();
		}
	}
	
	private void initHand() {

		final SearchDialog<CariSummaryVO> dialog = new SearchDialog<>(
				getActivity(), "Cari Arama", 
				SearchService.CARI, true, 
				new String[] {
					"Cari İsmi",
					"cariIsmi",
					"Cari Kodu",
					"cariKodu"
				}, new String[] {
					"Cari Kodu",
					"Cari Ünvanı"
				}, new String[]{
					"cari_kod",
					"cari_unvan1"
				}, new OnResultRowSelected<CariSummaryVO>() {

					@Override
					public void onSelected(CariSummaryVO obj) {
						reset();
						loadCari(obj.getCari_RECno());
						
					}
				});
		
		btnCHIsmi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {

				dialog.show();
			}
		});
		
		btnCHKodu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				dialog.show();
			}
		});
		
		btnCariHesap.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				displayCariHesap(cari.getCari_kod());
			}
		});
		btnCariStok.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				displayCariStok(cari.getCari_kod());
			}
		});
		btnSiparisler.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				displaySiparisler(cari.getCari_kod());
			}
		});
		btnSiparisVer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				displaySiparisVer(cari);
			}
		});
		btnTeklifVer.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				displayTeklifVer(cari);
			}
		});
		btnAdresler.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				
				CariAdreslerDialog dialog = new CariAdreslerDialog(getActivity(), cari.getCari_kod());
				dialog.show();
				
			}
		});
		
	}

	private void reset() {
		tvKodu.setText("");
		tvUnvani.setText("");
		tvUnvani2.setText("");
		tvHareketTipi.setText("");
		tvCariBagTipi.setText("");
		tvBagOrtFirmaNo.setText("");
		tvStokAlimTipi.setText("");
		tvStokSatimTipi.setText("");
		tvDovizCinsi1.setText("");
		tvMuhasebeKodu1.setText("");
		tvDovizCinsi2.setText("");
		tvMuhasebeKodu2.setText("");
		tvDovizCinsi3.setText("");
		tvMuhasebeKodu3.setText("");
		tvMuhKodArtikeli.setText("");
		tvAnaCariKodu.setText("");
		tvAnaCariUnvani.setText("");
		tvTemsilciKodu.setText("");
		tvTemsilciAdi.setText("");
		tvGrupKodu.setText("");
		tvGrupAdi.setText("");
		tvSektorKodu.setText("");
		tvSektorAdi.setText("");
		tvBolgeKodu.setText("");
		tvBolgeAdi.setText("");
		tvVergiDairesi.setText("");
		tvFaturaAdresNo.setText("");
		tvVergiNo.setText("");
		tvSevkAdresNo.setText("");
		tvBasitVergi.setText("");
		tvWebAdresi.setText("");
		tvYetkiliCep.setText("");
		tvEmailAdresi.setText("");
		tvKayitTarih.setText("");
	}
	
	private void loadCari(int cariRecNo) {
		final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
				"Yükleniyor. Bekleyiniz...", true);
		RestBO.getInstance(getActivity()).getService(GetService.class)
				.getCari(cariRecNo, new Callback<CariVO>() {

					@Override
					public void failure(RetrofitError err) {
						Toast.makeText(getActivity(), err.getMessage(),
								Toast.LENGTH_LONG).show();
						err.printStackTrace();
						dialog.dismiss();
					}

					@Override
					public void success(CariVO arg0, Response arg1) {

						if (arg0  == null) {
							dialog.dismiss();
							return;
						}
						cari = arg0;

						
						fillCari();
						dialog.dismiss();
						
					}
				});

	}
	
	private void fillCari() {
		
		tvKodu.setText(cari.getCari_kod());
		tvUnvani.setText(cari.getCari_unvan1());
		tvUnvani2.setText(cari.getCari_unvan2());
		tvHareketTipi.setText(cari.getCari_hareket_tipi() +"");
		tvCariBagTipi.setText(cari.getCari_baglanti_tipi() + "");
		tvBagOrtFirmaNo.setText(cari.getCari_BagliOrtaklisa_Firma() + "");
		tvStokAlimTipi.setText(cari.getCari_stok_alim_cinsi() + "");
		tvStokSatimTipi.setText(cari.getCari_stok_satim_cinsi() + "");
		tvDovizCinsi1.setText(cari.getCari_doviz_cinsi() + "");
		tvMuhasebeKodu1.setText(cari.getCari_muh_kod());
		tvDovizCinsi2.setText(cari.getCari_doviz_cinsi1() + "");
		tvMuhasebeKodu2.setText(cari.getCari_muh_kod1());
		tvDovizCinsi3.setText(cari.getCari_doviz_cinsi2() + "");
		tvMuhasebeKodu3.setText(cari.getCari_muh_kod2());
		tvMuhKodArtikeli.setText(cari.getCari_muhartikeli());
		tvAnaCariKodu.setText(cari.getCari_Ana_cari_kodu());
		//tvAnaCariUnvani.setText(cari.getCari_a);
		tvTemsilciKodu.setText(cari.getCari_temsilci_kodu());
		//tvTemsilciAdi.setText(cari.getCari_te);
		tvGrupKodu.setText(cari.getCari_grup_kodu());
		//tvGrupAdi.setText(cari.getCari_);
		tvSektorKodu.setText(cari.getCari_sektor_kodu());
		//tvSektorAdi.setText(cari);
		tvBolgeKodu.setText(cari.getCari_bolge_kodu());
		//tvBolgeAdi.setText(cari);
		tvVergiDairesi.setText(cari.getCari_vdaire_adi());
		tvFaturaAdresNo.setText(cari.getCari_fatura_adres_no() + "");
		tvVergiNo.setText(cari.getCari_vdaire_no());
		tvSevkAdresNo.setText(cari.getCari_sevk_adres_no() + "");
		tvBasitVergi.setText(cari.getCari_BUV_tabi_fl() == 0 ? "Hayır" : "Evet");
		tvWebAdresi.setText(cari.getCari_wwwadresi());
		tvYetkiliCep.setText(cari.getCari_CepTel());
		tvEmailAdresi.setText(cari.getCari_EMail());
		tvKayitTarih.setText(cari.getCari_kaydagiristarihi().getFormattedDate());
		
		btnAdresler.setEnabled(true);
		btnCariHesap.setEnabled(true);
		btnCariStok.setEnabled(true);
		btnSiparisler.setEnabled(true);
		btnTeklifVer.setEnabled(true);
		btnSiparisVer.setEnabled(true);
	}
	
	private void displayCariHesap(String cariKod) {
		UIUtil.hideKeyboard(getActivity());
		MainActivity mainActivity = (MainActivity) getActivity();
		Bundle arguments = new Bundle();
		arguments.putString("cariKod", cariKod);
		mainActivity.displayFragment(FragmentType.CARI_HESAP, arguments);
		
	}
	
	private void displayCariStok(String cariKod) {
		
		MainActivity mainActivity = (MainActivity) getActivity();
		Bundle arguments = new Bundle();
		arguments.putString("cariKod", cariKod);
		mainActivity.displayFragment(FragmentType.CARI_STOK, arguments);
		
	}
	private void displaySiparisler(String cariKod) {
		
		MainActivity mainActivity = (MainActivity) getActivity();
		Bundle arguments = new Bundle();
		arguments.putString("cariKod", cariKod);
		mainActivity.displayFragment(FragmentType.SIPARISLERIM, arguments);
		
	}
	private void displaySiparisVer(CariVO cari) {
		CariSummaryVO crs = new CariSummaryVO();
		crs.setCari_kod(cari.getCari_kod());
		crs.setCari_unvan1(cari.getCari_unvan1());
		crs.setCari_odemeplan_no(cari.getCari_odemeplan_no());
		crs.setCari_temsilci_kodu(cari.getCari_temsilci_kodu());
		SiparisVerFragment.setCariSummary(crs);
		
		MainActivity mainActivity = (MainActivity) getActivity();
		Bundle arguments = new Bundle();
		mainActivity.displayFragment(FragmentType.SIPARIS_VER, arguments);
		
	}
	
	private void displayTeklifVer(CariVO cari) {
		CariSummaryVO crs = new CariSummaryVO();
		crs.setCari_kod(cari.getCari_kod());
		crs.setCari_unvan1(cari.getCari_unvan1());
		crs.setCari_odemeplan_no(cari.getCari_odemeplan_no());
		crs.setCari_temsilci_kodu(cari.getCari_temsilci_kodu());
		TeklifVerFragment.setCariSummary(crs);
		
		MainActivity mainActivity = (MainActivity) getActivity();
		Bundle arguments = new Bundle();
		mainActivity.displayFragment(FragmentType.TEKLIF_VER, arguments);
		
	}
}