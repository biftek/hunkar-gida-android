package com.biftekyazilim.hunkargida.fragment;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.MainActivity;
import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.enm.FragmentType;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;
import com.biftekyazilim.hunkargida.util.ReflectionUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;

public class CariAraFragment extends ArgumentableFragment {

	
	private RadioButton rbtnCariKodu;
	private RadioButton rbtnCariIsmi;
	private Button btnSearch;
	private EditText etCariKoduOrIsmi;
	private TableHeaderVO[] tableHeaderList;
	private TableLayout tblContent;
	
	private List<CariSummaryVO> cariList;
	
	private TableRow headerRow;
	
	private boolean isForSelection;
	private static CariSummaryVO selectedCari;
	
	
	public CariAraFragment() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_cariarama,
				container, false);

		getActivity().setTitle("Cari Arama");
		
		rbtnCariKodu = (RadioButton) rootView.findViewById(R.id.rbtnCariKodu);
		rbtnCariIsmi = (RadioButton) rootView.findViewById(R.id.rbtnCariIsmi);
		btnSearch =  (Button) rootView.findViewById(R.id.btnSearch);
		etCariKoduOrIsmi =  (EditText) rootView.findViewById(R.id.etCariKoduOrIsmi);
		tblContent = (TableLayout) rootView.findViewById(R.id.tblContent);
		
		headerRow = (TableRow) rootView.findViewById(R.id.headerRow);
		
		tableHeaderList = new TableHeaderVO[] {
				new TableHeaderVO("CARİ ÜNVANI","cari_unvan1", 170, 12),
				new TableHeaderVO("CARİ ÜNVANI 2","cari_unvan2", 110, 12),
				new TableHeaderVO("CARİ KODU","cari_kod", 100, 12),
				new TableHeaderVO("E-FATURA","cari_efatura_fl", 50, 12),
				new TableHeaderVO("TEMSİLCİ","cari_temsilci_kodu", 100, 12),
				new TableHeaderVO("BAKİYE VE HAREKET SAYISI","bakiye", 60, 12),
				new TableHeaderVO("kk1","kk1", 60, 12),
				new TableHeaderVO("kk2","kk2", 60, 12),
				new TableHeaderVO("kk3","kk3", 60, 12),
				new TableHeaderVO("BAĞLANTI TİPİ","baglanti_tipi", 70, 12),
				new TableHeaderVO("HAREKET TİPİ","hareket_tipi", 160, 12) };
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList, new OnClickListener() {
			
			@Override
			public void onClick(View vw) {
				if(cariList == null || cariList.size() == 0) {
					return;
				}
				int col = (int)vw.getTag();
				final boolean desc = col < 0 ? true : false;
				vw.setTag(-col);
				col = Math.abs(col)-1;
				
				TableHeaderVO th = tableHeaderList[col];
				final String propertyName = th.getObjectPropertyName();
				Collections.sort(cariList, new Comparator<CariSummaryVO>() {

					@Override
					public int compare(CariSummaryVO arg0, CariSummaryVO arg1) {
						
						String val1 = ReflectionUtil.getValue(propertyName, arg0) + "";
						String val2 = ReflectionUtil.getValue(propertyName, arg1) + "";
						return (desc ? -1 : 1) * val1.compareTo(val2);
					}
				});
				
				fillCariTable();
			}
		});
		
		
		initHand();
		
		if(cariList != null) {
			fillCariTable();
		}
		
		
		isForSelection = false;
		
		if(getArguments() != null && getArguments().containsKey("isForSelection") && getArguments().containsKey("cariKod")) {
			isForSelection = getArguments().getBoolean("isForSelection");
		}
		
		if(isForSelection) {
			rbtnCariKodu.setVisibility(View.GONE);
			rbtnCariIsmi.setVisibility(View.GONE);
			btnSearch.setVisibility(View.GONE);
			etCariKoduOrIsmi.setVisibility(View.GONE);
			
			String cariKod = getArguments().getString("cariKod");
			
			doSearch(true, cariKod);
		}
		
		return rootView;
	}
	
	private void initHand() {
		
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				UIUtil.hideKeyboard(getActivity());
				if(etCariKoduOrIsmi.getText().toString().trim().length() == 0) {
					new AlertDialog.Builder(getActivity())
				    .setTitle(R.string.app_name)
				    .setMessage("Cari kodu yada ismi olarak en az 1 karakter girmeniz gerekmektedir.")
				    .setPositiveButton(android.R.string.ok, null)
				    .setIcon(android.R.drawable.ic_dialog_alert)
				     .show();
					return;
				}
				
				doSearch(rbtnCariKodu.isChecked(), etCariKoduOrIsmi.getText().toString());
			}
		});
		
	}
	
	private void doSearch(boolean isCariKod, String criteriaValue) {
		
		tblContent.removeAllViews();
		final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", 
                "Yükleniyor. Bekleyiniz...", true);
		
		String searchBy = isCariKod ? "cariKodu" : "cariIsmi";
		RestBO.getInstance(getActivity()).getService(GetService.class).searchCari(searchBy, criteriaValue, new Callback<List<CariSummaryVO>>() {

			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}

			@Override
			public void success(List<CariSummaryVO> result, Response arg1) {
				
				cariList = result;
				
				fillCariTable();
				
				dialog.dismiss();
			}
		});
		
	}
	
	public static CariSummaryVO getSelectedCari() {
		CariSummaryVO tmpCari = selectedCari;
		selectedCari = null;
		return tmpCari;
	}
	
	public void displayCariKart(int cariRecNo) {
		
		MainActivity mainActivity = (MainActivity) getActivity();
		Bundle arguments = new Bundle();
		arguments.putInt("cariRecNo", cariRecNo);
		mainActivity.displayFragment(FragmentType.CARI_KART, arguments);
		
	}
	
	public void fillCariTable() {
		tblContent.removeAllViews();
		int rowCounter = 0;
		for (CariSummaryVO cari : cariList) {
			
			TableRow tRow = new TableRow(getActivity());
			tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
			tRow.setMinimumHeight(ROW_HEIGHT);
			tRow.setTag(rowCounter);
			
			TextView tvCariUnvan = new TextView(getActivity());
			TextView tvCariUnvan2 = new TextView(getActivity());
			TextView tvCariKodu = new TextView(getActivity());
			TextView tvEfatura = new TextView(getActivity());
			TextView tvTemsilci = new TextView(getActivity());
			TextView tvBakiyeHareket = new TextView(getActivity());
			TextView tvKk1 = new TextView(getActivity());
			TextView tvKk2 = new TextView(getActivity());
			TextView tvKk3 = new TextView(getActivity());
			TextView tvBaglantiTipi = new TextView(getActivity());
			TextView tvHareketTipi = new TextView(getActivity());
			
			TextView[] tvList = new TextView[]{
					tvCariUnvan,
					tvCariUnvan2,
					tvCariKodu,
					tvEfatura,
					tvTemsilci,
					tvBakiyeHareket,
					tvKk1,
					tvKk2,
					tvKk3,
					tvBaglantiTipi,
					tvHareketTipi
			};
			
			
			int col = 0;
			for (TextView textView : tvList) {
				
				int weight = tableHeaderList[col].getWeight();
				if(rowCounter%2 == 0) {
					textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
				} else {
					textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
				}
				textView.setGravity(Gravity.CENTER);
				textView.setTextColor(Color.BLACK);
				textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
				TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,weight);
				params.column = col;
				textView.setLayoutParams(params);
				col++;

				tRow.addView(textView);
				
			}
			
			rowCounter++;
			
			tvCariUnvan.setText(cari.getCari_unvan1());
			tvCariUnvan2.setText(cari.getCari_unvan2());
			tvCariKodu.setText(cari.getCari_kod());
			tvEfatura.setText(cari.getCari_efatura_fl() == 0 ? "YOK" : "VAR");
			//tvTemsilci.setText(asdasd);
			tvBakiyeHareket.setText(NumberFormatUtil.formatPrice(cari.getBakiye()));
			tvKk1.setText(NumberFormatUtil.formatPrice(cari.getKk1()) + "");
			tvKk2.setText(NumberFormatUtil.formatPrice(cari.getKk2()) + "");
			tvKk3.setText(NumberFormatUtil.formatPrice(cari.getKk3()) + "");
			tvBaglantiTipi.setText(cari.getBaglanti_tipi());
			tvHareketTipi.setText(cari.getHareket_tipi());
			
			
			tRow.setClickable(true);
			tRow.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					UIUtil.hideKeyboard(getActivity());
					int cariIndex = (int) arg0.getTag();
					CariSummaryVO cari = cariList.get(cariIndex);
					if(isForSelection) {
						selectedCari = cari;
						MainActivity activity = (MainActivity) getActivity();
						activity.popBackFragment();
					} else {
						displayCariKart(cari.getCari_RECno());
					}
					
				}
			});
			
			tblContent.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
		}
	}

}