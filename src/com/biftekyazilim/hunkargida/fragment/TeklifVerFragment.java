package com.biftekyazilim.hunkargida.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.MainActivity;
import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.bo.StaticDataBO;
import com.biftekyazilim.hunkargida.enm.FragmentType;
import com.biftekyazilim.hunkargida.enm.SearchService;
import com.biftekyazilim.hunkargida.listener.EditTextWatcher;
import com.biftekyazilim.hunkargida.listener.OnDateEditTextFocusListener;
import com.biftekyazilim.hunkargida.listener.OnEditTextChangeListener;
import com.biftekyazilim.hunkargida.listener.OnResultRowSelected;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.service.PostService;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.view.CariAdreslerDialog;
import com.biftekyazilim.hunkargida.view.NumberEditText;
import com.biftekyazilim.hunkargida.view.PriceEditText;
import com.biftekyazilim.hunkargida.view.SearchDialog;
import com.biftekyazilim.hunkargida.vo.CariAdresVO;
import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.IdReturnVO;
import com.biftekyazilim.hunkargida.vo.SiraNoVO;
import com.biftekyazilim.hunkargida.vo.SorumlulukMerkeziVO;
import com.biftekyazilim.hunkargida.vo.StokFiyatVO;
import com.biftekyazilim.hunkargida.vo.StokSearchVO;
import com.biftekyazilim.hunkargida.vo.StokVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;
import com.biftekyazilim.hunkargida.vo.TeklifSearchVO;
import com.biftekyazilim.hunkargida.vo.TeklifVO;

public class TeklifVerFragment extends ArgumentableFragment {

	public final static double[] VERGI = new double[]{
		0.0f,
		0.0f, 
		0.01f, 
		0.08f, 
		0.18f, 
		0.26f
		};
	
	private TableLayout tblTeklifStokUrun;
	private EditText etEvrakSeriNo;
	private EditText etEvrakSiraNo;
	private EditText etBelgeNo;
	private EditText etCHKodu;
	private EditText etCHIsmi;
	private EditText etOdemePlani;
	private EditText etSorumlu;
	private EditText etTeslimTuru;
	private EditText etProje;
	private EditText etSorumluMerkezi;
	private EditText etDoviz;
	private EditText etAdres;
	
	private EditText etBaslamaTarihi;
	private EditText etGecerlilikTarihi;
	private EditText etEvrakTarih;
	private EditText etBelgeTarih;
	
	private TextView etIskontoPerc;
	private TextView etMasrafPerc;
	private TextView etVergiPerc;
	
	private TextView etAraToplam;
	private TextView etIskonto;
	private TextView etMasraf;
	private TextView etVergi;
	private TextView etYekun;
	
	private Button btnCHKodu;
	private Button btnCHIsmi;
	private Button btnDoviz;
	private Button btnOdemePlani;
	private Button btnSorumluMerkezi;
	private Button btnSearch;
	private Button btnYeni;
	private Button btnKaydet;
	private Button btnSil;
	private Button btnSiparisOlustur;
	private Button btnNewLine;
	private Button btnAdres;
	
	private boolean isEditing = true;
	
	private TableHeaderVO[] tableHeaderList;
	
	private Calendar calendar;
	
	private List<TeklifVO> stokUrunList;
	
	private boolean isSorumlulukTwo = false;
	
	private static String[] sorumlulukMerkeziNameList;
	
	private static CariSummaryVO CARI_SUMMARY;
	
	public static CariSummaryVO getCariSummary() {
		return CARI_SUMMARY;
	}
	
	public static void setCariSummary(CariSummaryVO cariSummary) {
		CARI_SUMMARY = cariSummary;
	}
	
	public TeklifVerFragment() {
		stokUrunList = new ArrayList<>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_teklifver,
				container, false);

		tblTeklifStokUrun = (TableLayout) rootView.findViewById(R.id.tblTeklifStokUrun);
		TableRow headerRow = (TableRow)rootView.findViewById(R.id.headerRow);
		
		tableHeaderList = new TableHeaderVO[] {
				new TableHeaderVO("İşlem", 50),
				new TableHeaderVO("Kodu", 150),
				new TableHeaderVO("İsmi", 300),
				new TableHeaderVO("Miktar", 75),
				new TableHeaderVO("Br", 75),
				new TableHeaderVO("Alış Fiyatı", 100),
				new TableHeaderVO("Kar Oranı", 100),
				new TableHeaderVO("Brüt Fiyat", 150) 
		};
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList);
		/*, new OnClickListener() {
			
			@Override
			public void onClick(View vw) {
				if(stokUrunList == null || stokUrunList.size() == 0) {
					return;
				}
				int col = (int)vw.getTag();
				if(col == 1) {
					return;
				}
				final boolean desc = col < 0 ? true : false;
				vw.setTag(-col);
				col = Math.abs(col)-1;
				
				TableHeaderVO th = tableHeaderList[col];
				final String propertyName = th.getObjectPropertyName();
				Collections.sort(stokUrunList, new Comparator<TeklifVO>() {

					@Override
					public int compare(TeklifVO arg0, TeklifVO arg1) {
						
						String val1 = ReflectionUtil.getValue(propertyName, arg0) + "";
						String val2 = ReflectionUtil.getValue(propertyName, arg1) + "";
						return (desc ? -1 : 1) * val1.compareTo(val2);
					}
				});
				
				clearStokUrunTable(false);
				fillStokUrunTable(stokUrunList.get(0).getTkl_fileid());
			}
		}*/
		
		etEvrakSeriNo = (EditText)rootView.findViewById(R.id.etEvrakSeriNo);
		etEvrakSiraNo = (EditText)rootView.findViewById(R.id.etEvrakSiraNo);
		
		etBelgeNo = (EditText)rootView.findViewById(R.id.etBelgeNo);
		etCHKodu = (EditText)rootView.findViewById(R.id.etCHKodu);
		etCHIsmi = (EditText)rootView.findViewById(R.id.etCHIsmi);
		etOdemePlani = (EditText)rootView.findViewById(R.id.etOdemePlani);
		etSorumlu = (EditText)rootView.findViewById(R.id.etSorumlu);
		etTeslimTuru = (EditText)rootView.findViewById(R.id.etTeslimTuru);
		etProje = (EditText)rootView.findViewById(R.id.etProje);
		etSorumluMerkezi = (EditText)rootView.findViewById(R.id.etSorumluMerkezi);
		etDoviz = (EditText)rootView.findViewById(R.id.etDoviz);
		etAdres = (EditText)rootView.findViewById(R.id.etAdres);
		btnAdres = (Button)rootView.findViewById(R.id.btnAdres);

		etBaslamaTarihi = (EditText)rootView.findViewById(R.id.etBaslamaTarihi);
		etGecerlilikTarihi = (EditText)rootView.findViewById(R.id.etGecerlilikTarihi);
		etEvrakTarih = (EditText)rootView.findViewById(R.id.etEvrakTarih);
		etBelgeTarih = (EditText)rootView.findViewById(R.id.etBelgeTarih);
		
		
		btnCHKodu = (Button)rootView.findViewById(R.id.btnCHKodu);
		btnCHIsmi = (Button)rootView.findViewById(R.id.btnCHIsmi);
		btnDoviz = (Button)rootView.findViewById(R.id.btnDoviz);
		btnOdemePlani = (Button)rootView.findViewById(R.id.btnOdemePlani);
		btnSorumluMerkezi = (Button)rootView.findViewById(R.id.btnSorumluMerkezi);
		btnYeni = (Button)rootView.findViewById(R.id.btnYeni);
		btnKaydet = (Button)rootView.findViewById(R.id.btnKaydet);
		btnSil = (Button)rootView.findViewById(R.id.btnSil);
		btnSiparisOlustur = (Button)rootView.findViewById(R.id.btnSiparisOlustur);
		
		btnSearch = (Button)rootView.findViewById(R.id.btnSearch);
		btnNewLine = (Button)rootView.findViewById(R.id.btnNewLine);
		
		etIskontoPerc = (TextView)rootView.findViewById(R.id.etIskontoPerc);
		etMasrafPerc = (TextView)rootView.findViewById(R.id.etMasrafPerc);
		etVergiPerc = (TextView)rootView.findViewById(R.id.etVergiPerc);
			
		etAraToplam = (TextView)rootView.findViewById(R.id.etAraToplam);
		etIskonto = (TextView)rootView.findViewById(R.id.etIskonto);
		etMasraf = (TextView)rootView.findViewById(R.id.etMasraf);
		etVergi = (TextView)rootView.findViewById(R.id.etVergi);
		etYekun = (TextView)rootView.findViewById(R.id.etYekun);
		
		etCHKodu.setEnabled(false);
		etCHIsmi.setEnabled(false);
		etAdres.setEnabled(false);
		//etEvrakSiraNo.setEnabled(false);
		//etEvrakSeriNo.setEnabled(false);
		
		calendar = Calendar.getInstance(new Locale("tr"));
		initHand();
		
		setEditing(true);
		
		etSorumluMerkezi.setText("1");
		etSorumluMerkezi.setEnabled(false);
		etDoviz.setText("TL");
		etDoviz.setEnabled(false);
		etOdemePlani.setEnabled(false);
		
		
		sorumlulukMerkeziNameList = new String[StaticDataBO.getInstance(getActivity()).getSorumlulukMerkeziList().size()];
		int j = 0;
		for (SorumlulukMerkeziVO srm : StaticDataBO.getInstance(getActivity()).getSorumlulukMerkeziList()) {
			
			sorumlulukMerkeziNameList[j] = srm.getMsg_S_0870();
			j++;
		}
		

		if(CARI_SUMMARY != null) {
			fillFieldsRelatedCari();
		}
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if(MainActivity.FRAGMENT_CHANGE) {
			MainActivity.FRAGMENT_CHANGE = false;
			setEditing(true);
		}
	}
	
	private void initHand() {
		
		btnYeni.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				new AlertDialog.Builder(getActivity())
			    .setTitle(R.string.app_name)
			    .setMessage("Yeni bir teklif açmak istediğinize emin misiniz?")
			    .setCancelable(true)
			    .setNegativeButton(android.R.string.cancel, null)
			    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			    	@Override
			    	public void onClick(DialogInterface arg0, int arg1) {
			    		setEditing(true);
			    	}
			    })
			    .setIcon(android.R.drawable.ic_dialog_alert)
			    .show();
			}
		});
		
		btnSorumluMerkezi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				new AlertDialog.Builder(getActivity())
		        .setSingleChoiceItems(sorumlulukMerkeziNameList, 0, null)
		        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int whichButton) {
		                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
		                SorumlulukMerkeziVO sorumlulukMerkezi = StaticDataBO.getInstance(getActivity()).getSorumlulukMerkeziList().get(selectedPosition);
		                etSorumluMerkezi.setText(sorumlulukMerkezi.getMsg_S_0078());
		                etSorumluMerkezi.setTag(sorumlulukMerkezi.getMsg_S_0088());
		                isSorumlulukTwo = sorumlulukMerkezi.getMsg_S_0088() == 2;
		                calculateTotals();
		                dialog.dismiss();
		            }
		        })
		        .setCancelable(true)
		        .show();
			}
		});

		btnSiparisOlustur.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				MainActivity mainActivity = (MainActivity) getActivity();
				SiparisVerFragment.teklifStokUrunList = stokUrunList;
				mainActivity.displayFragment(FragmentType.SIPARIS_VER, null);
				
			}
		});
		
		btnKaydet.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				if(stokUrunList.size() == 0) {
					Toast.makeText(getActivity(), "Stoktan en az 1 ürün eklemeniz gerekmektedir.", Toast.LENGTH_LONG).show();
					return;
				}
				EditText[] reqFields = new EditText[]{
						
						etEvrakSeriNo,
						etEvrakSiraNo,

						etBelgeNo,
						etCHKodu,
						etCHIsmi,
						etOdemePlani,

						etBaslamaTarihi,
						etGecerlilikTarihi,
						etEvrakTarih,
						etBelgeTarih
						
				};
				for (int i = 0; i < reqFields.length; i++) {
					if(reqFields[i].getText().toString().trim().length() == 0) {
						Toast.makeText(getActivity(), "Lütfen gerekli bütün alanları doldurunuz.", Toast.LENGTH_LONG).show();
						reqFields[i].requestFocus();
						return;
					}
				}
				
				final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", 
		                "Yükleniyor. Bekleyiniz...", true);
				
				saveTeklif(0, dialog);
				
			}
		});
		
		btnSil.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				new AlertDialog.Builder(getActivity())
			    .setTitle(R.string.app_name)
			    .setMessage("Teklif kaydınız silinecek. Devam etmek istiyor musunuz?")
			    .setCancelable(true)
			    .setNegativeButton(android.R.string.cancel, null)
			    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						String siraNo = etEvrakSiraNo.getText().toString();
						String seriNo = etEvrakSeriNo.getText().toString();
						
						final ProgressDialog dia = ProgressDialog.show(getActivity(), "", 
				                "İşlem yapılıyor. Bekleyiniz...", true);
						RestBO.getInstance(getActivity()).getService(PostService.class).deleteTeklif(seriNo, siraNo, new Callback<IdReturnVO>() {
							
							@Override
							public void success(IdReturnVO arg0, Response arg1) {
								Toast.makeText(getActivity(), "İşlem tamamlandı.", Toast.LENGTH_LONG).show();
								setEditing(true);
								dia.dismiss();
							}
							
							@Override
							public void failure(RetrofitError err) {
								Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
								err.printStackTrace();
								dia.dismiss();
							}
						});
					}
				})
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();
				
				
			}
		});
		
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				
				SearchDialog<TeklifSearchVO> dialog = new SearchDialog<>(
						getActivity(), "Evrak Arama", 
						SearchService.TEKLIF, false, 
						new String[] {
								"Evrak Seri No",
								"Evrak Sıra No"
							}, new String[] {
							"Evrak Seri No",
							"Evrak Sıra No",
							"Cari Ünvan"
						}, new String[]{
							"tkl_evrakno_seri",
							"tkl_evrakno_sira",
							"cari_unvan1"
						}, new OnResultRowSelected<TeklifSearchVO>() {

							@Override
							public void onSelected(TeklifSearchVO obj) {
								
								final ProgressDialog dia = ProgressDialog.show(getActivity(), "", 
						                "Yükleniyor. Bekleyiniz...", true);
								RestBO.getInstance(getActivity()).getService(GetService.class).getTeklif(obj.getTkl_evrakno_seri(), obj.getTkl_evrakno_sira(), new Callback<List<TeklifVO>>() {
									
									@Override
									public void success(List<TeklifVO> result, Response arg1) {
										if(result.size() == 0) {
											Toast.makeText(getActivity(), "Belirtilen teklif bulunamadı.", Toast.LENGTH_LONG).show();
											dia.dismiss();
											return;
										}
										setEditing(false);
										clearStokUrunTable(true);
										stokUrunList = result;
										fillTeklif(result.get(0));
										fillStokUrunTable(result.get(0).getTkl_fileid());
										dia.dismiss();
									}
									
									@Override
									public void failure(RetrofitError err) {
										Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
										err.printStackTrace();
										dia.dismiss();
									}
								});
								
							}
						});
				
				dialog.setEditTextNumber(1);
				dialog.setTextEditText2(etEvrakSiraNo.getText()+"");
				dialog.setTextEditText1(etEvrakSeriNo.getText()+"");
				dialog.show(true);
				
			}
		});
		
		btnNewLine.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				showStokSearch(-1, true);
				
			}
		});
		
		btnAdres.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String cariKod = etCHKodu.getText().toString();
				if(cariKod.isEmpty()) {
					Toast.makeText(getActivity(), "Öncelikle bir cari seçmeniz gerekmektedir.", Toast.LENGTH_LONG).show();
				} else {
					CariAdreslerDialog dialog = new CariAdreslerDialog(getActivity(), cariKod);
					dialog.setOnRowSelectedListener(new OnResultRowSelected<CariAdresVO>() {
						
						@Override
						public void onSelected(CariAdresVO obj) {
							etAdres.setText(obj.getAdr_cadde() + ", " + obj.getAdr_sokak() + ", " + obj.getAdr_ilce() + ", " + obj.getAdr_il());
							btnAdres.setTag(obj.getAdr_RECno());
						}
					});
					dialog.show();
				}
				
			}
			
		});
		btnCHKodu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				SearchDialog<CariSummaryVO> dialog = new SearchDialog<>(
						getActivity(), "Cari Arama", 
						SearchService.CARI, true, 
						new String[] {
							"Cari İsmi",
							"cariIsmi",
							"Cari Kodu",
							"cariKodu"
						}, new String[] {
							"Cari Kodu",
							"Cari Ünvanı"
						}, new String[]{
							"cari_kod",
							"cari_unvan1"
						}, new OnResultRowSelected<CariSummaryVO>() {

							@Override
							public void onSelected(CariSummaryVO obj) {
								CARI_SUMMARY = obj;
								fillFieldsRelatedCari();
								
							}

						});
				dialog.show();
			}
		});
		
		btnCHIsmi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				SearchDialog<CariSummaryVO> dialog = new SearchDialog<>(
						getActivity(), "Cari Arama", 
						SearchService.CARI, true, 
						new String[] {
							"Cari İsmi",
							"cariIsmi",
							"Cari Kodu",
							"cariKodu"
						}, new String[] {
							"Cari Kodu",
							"Cari Ünvanı"
						}, new String[]{
							"cari_kod",
							"cari_unvan1"
						}, new OnResultRowSelected<CariSummaryVO>() {

							@Override
							public void onSelected(CariSummaryVO obj) {
								CARI_SUMMARY = obj;
								fillFieldsRelatedCari();
							}
						});
				dialog.show();
			}
		});
		
		OnFocusChangeListener dateEditTextListener = new OnDateEditTextFocusListener(getActivity());
		
		etGecerlilikTarihi.setOnFocusChangeListener(dateEditTextListener);
		etBaslamaTarihi.setOnFocusChangeListener(dateEditTextListener);
		etBelgeTarih.setOnFocusChangeListener(dateEditTextListener);
		etEvrakTarih.setOnFocusChangeListener(dateEditTextListener);
		/*TextWatcher textWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//TODO sira numarası çekilecek servisten
				int evrakSiraNo = 4;
				etEvrakSiraNo.setText(evrakSiraNo+"");
			}
		};
		etEvrakSeriNo.addTextChangedListener(textWatcher);*/
		//etEvrakSiraNo.addTextChangedListener(textWatcher);
		etEvrakSeriNo.setImeOptions(EditorInfo.IME_ACTION_DONE);
		etEvrakSeriNo.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH ||
		                actionId == EditorInfo.IME_ACTION_DONE ||
		                event.getAction() == KeyEvent.ACTION_DOWN &&
		                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					
					updateSiraNo(false);
		        }
				return false;
			}
		});
		etEvrakSeriNo.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(final View editText, boolean hasFocus) {
				if(hasFocus && etEvrakSeriNo.getText().toString().trim().length() != 0)
					return;
				updateSiraNo(false);
				
			}
		});
	}
	
	private void fillFieldsRelatedCari() {
		etCHKodu.setText(CARI_SUMMARY.getCari_kod());
		etCHIsmi.setText(CARI_SUMMARY.getCari_unvan1());
		etOdemePlani.setText(CARI_SUMMARY.getCari_odemeplan_no()+"");
		etSorumlu.setText(CARI_SUMMARY.getCari_temsilci_kodu());
		CARI_SUMMARY = null;
	}
	
	private void updateSiraNo(final boolean alsoCallSave) {
		
		if(etEvrakSeriNo.getText().toString().trim().length() != 0) {
			
			final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", 
	                "Yükleniyor. Bekleyiniz...", true);
			
			RestBO.getInstance(getActivity()).getService(GetService.class).getTeklifSiraNo(etEvrakSeriNo.getText().toString(), new Callback<SiraNoVO>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
					err.printStackTrace();
					dialog.dismiss();
				}

				@Override
				public void success(SiraNoVO arg0, Response arg1) {
					
					if(arg0.getSirano() < 1) {
						
						Toast.makeText(getActivity(), "Girdiğiniz seri numarası kayıtlı değildir.", Toast.LENGTH_LONG).show();
						dialog.dismiss();
					} else {
						etEvrakSiraNo.setText(arg0.getSirano()+"");
						etEvrakSiraNo.setTag(etEvrakSeriNo.getText()+"");
						if(alsoCallSave) {
							saveTeklif(0, dialog);
						} else {
							dialog.dismiss();
						}
					}
					
					
				}
				
			});
			
		}
		
	}
	
	private String getEditTextDateFormatted(EditText et) {
		
		String[] spl = et.getText().toString().split("/");
		String tarih = spl[2] + spl[1] + spl[0];
		
		return tarih;
	}

	
	public void setEditing(boolean editing) {
		clearStokUrunTable(true);
		
		etBelgeNo.setEnabled(editing);
		etSorumlu.setEnabled(editing);
		etTeslimTuru.setEnabled(editing);
		etProje.setEnabled(editing);
		
		etEvrakSeriNo.setEnabled(editing);
		etEvrakSiraNo.setEnabled(editing);
		etBaslamaTarihi.setEnabled(editing);
		etGecerlilikTarihi.setEnabled(editing);
		etEvrakTarih.setEnabled(editing);
		etBelgeTarih.setEnabled(editing);
		
		btnCHIsmi.setEnabled(editing);
		btnCHKodu.setEnabled(editing);
		btnAdres.setEnabled(editing);
		btnDoviz.setEnabled(editing);
		btnOdemePlani.setEnabled(editing);
		btnSorumluMerkezi.setEnabled(editing);
		btnNewLine.setVisibility(editing ? View.VISIBLE : View.GONE);
		
		//btnYeni.setEnabled(true);
		btnKaydet.setEnabled(editing);
		btnSil.setEnabled(!editing);
		btnSiparisOlustur.setEnabled(!editing);
		
		etEvrakSeriNo.setText("");
		etEvrakSiraNo.setText("");
		etBelgeNo.setText("");
		etCHKodu.setText("");
		etAdres.setText("");
		etCHIsmi.setText("");
		etOdemePlani.setText("");
		etSorumlu.setText("");
		etTeslimTuru.setText("");
		etProje.setText("");
		
		etBaslamaTarihi.setText("");
		etGecerlilikTarihi.setText("");
		etEvrakTarih.setText("");
		etBelgeTarih.setText("");
		
		
		etIskontoPerc.setText("0.0");
		etMasrafPerc.setText("0.0");
		etVergiPerc.setText("0.0");
			
		etAraToplam.setText("0.0");
		etIskonto.setText("0.0");
		etMasraf.setText("0.0");
		etVergi.setText("0.0");
		etYekun.setText("0.0");
		
		isEditing = editing;
	}
	
	private void fillTeklif(TeklifVO teklif) {
		
		etEvrakSiraNo.setText(teklif.getTkl_evrakno_sira()+"");
		etEvrakSeriNo.setText(teklif.getTkl_evrakno_seri());
		etBelgeNo.setText(teklif.getTkl_belge_no());
		etCHKodu.setText(teklif.getTkl_cari_kod());
		etCHIsmi.setText(teklif.getCari_unvan1());
		etOdemePlani.setText(teklif.getTkl_Odeme_Plani()+"");
		etSorumlu.setText(teklif.getTkl_Sorumlu_Kod());
		etTeslimTuru.setText(teklif.getTKL_TESLIMTURU());
		etProje.setText(teklif.getTkl_ProjeKodu());
		//SorumlulukMerkeziVO sormrkz = StaticDataBO.getInstance(getActivity()).getSorumlulukMerkezi(teklif.getTkl_cari_sormerk());
		//etSorumluMerkezi.setText(sormrkz == null ? teklif.getTkl_cari_sormerk() : sormrkz.getMsg_S_0870());
		
		//etDoviz.setText("TL");//teklif.getTkl_doviz_cins() + "");
		etBaslamaTarihi.setText(teklif.getTkl_baslangic_tarihi().getFormattedDate());
		etGecerlilikTarihi.setText(teklif.getTkl_Gecerlilik_Sures().getFormattedDate());
		etEvrakTarih.setText(teklif.getTkl_evrak_tarihi().getFormattedDate());
		etBelgeTarih.setText(teklif.getTkl_belge_tarih().getFormattedDate());
		
		
		etIskontoPerc.setText(teklif.getTkl_iskonto1() + "");
		etMasrafPerc.setText(teklif.getTkl_masraf1()+ "");
		etVergiPerc.setText(teklif.getTkl_vergi() + "");
		
	}
	
	private void fillStokUrunTable(int teklifFileId) {
		
		for (TeklifVO tklf : stokUrunList) {
			if(teklifFileId == tklf.getTkl_fileid()) {
				addStokUrunRow(tklf);
			}
		}
		
		calculateTotals();
	}
	
	private int rowIndex = 0;
	private void addStokUrunRow(TeklifVO tklf) {
		
		TableRow tRow = generateStokUrunTableRow(tklf, rowIndex);
		tblTeklifStokUrun.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
		rowIndex++;
	}
	
	private void insertStokUrunRow(TeklifVO tklf, int rowind) {
		
		TableRow tRow = generateStokUrunTableRow(tklf, rowind);
		tblTeklifStokUrun.addView(tRow, rowind, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
		rowIndex++;
	}
	
	private void clearStokUrunTable(boolean alsoList) {
		if(alsoList)
			stokUrunList.clear();
		tblTeklifStokUrun.removeAllViews();
		rowIndex = 0;
	}
	
	private void removeStokUrunTableRow(int rIndex) {
		
		stokUrunList.remove(rIndex);
		
		tblTeklifStokUrun.removeViewAt(rIndex);
		rowIndex--;
		for(int i = 0; i < tblTeklifStokUrun.getChildCount(); i++){
		    TableRow tRow = (TableRow) tblTeklifStokUrun.getChildAt(i);
		    
		    int background = -1;
			if(i%2 == 0) {
				background = R.drawable.table_cell_cloud_bg;
			} else {
				background = R.drawable.table_cell_asbestos_bg;
			}
			
			tRow.setBackgroundResource(background);
			
			for(int j = 0; j < tRow.getChildCount(); j++){
				View vw = tRow.getChildAt(i);
				vw.setTag(i);
			}
		}
	}
	
	private TableRow generateStokUrunTableRow(TeklifVO tklf, int row) {
		
		TableRow tRow = new TableRow(getActivity());
		
		int background = -1;
		if(row%2 == 0) {
			background = R.drawable.table_cell_cloud_bg;
		} else {
			background = R.drawable.table_cell_asbestos_bg;
		}
		
		tRow.setBackgroundResource(background);
		
		ImageButton btnRemoveRow = new ImageButton(getActivity());
		btnRemoveRow.setImageResource(R.drawable.ic_delete);
		btnRemoveRow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Integer rIndex = (Integer) arg0.getTag();
				removeStokUrunTableRow(rIndex);
			}
		});
		btnRemoveRow.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
		
		Button btnKodu = new Button(getActivity());
		/*btnKodu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View arg0) {
				
				int rIndex = (Integer)arg0.getTag();
				showStokSearch(rIndex, false);
				
			}
		});*/
		Button btnIsmi = new Button(getActivity());
		NumberEditText txtMiktar = new NumberEditText(getActivity());
		EditText txtBr = new EditText(getActivity());
		PriceEditText txtAlisFiyati = new PriceEditText(getActivity());
		NumberEditText txtKarOrani = new NumberEditText(getActivity());
		PriceEditText txtBrutFiyati = new PriceEditText(getActivity());
		txtBr.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View arg0, boolean arg1) {
				EditText txt = (EditText) arg0;
				if(txt.getText().toString().equals("[Birim]")) {
					txt.setText("");
				}
			}
		});
		
		btnKodu.setText(tklf.getSto_kod() == null ? "[Kodu]" : tklf.getSto_kod());
		btnIsmi.setText(tklf.getSto_isim() == null ? "[Stok İsmi]" : tklf.getSto_isim());
		
		txtMiktar.setText((double) (tklf.getTkl_miktar() == -1 ? 0f : tklf.getTkl_miktar()));
		txtBr.setText(tklf.getSto_birim1_ad() == null ? "[Birim]" : tklf.getSto_birim1_ad());
		txtAlisFiyati.setText((double) (tklf.getTkl_Alisfiyati() == -1 ? 0f : tklf.getTkl_Alisfiyati()));
		txtKarOrani.setText((double) (tklf.getTkl_karorani() == -1 ? 0f : tklf.getTkl_karorani()));
		txtBrutFiyati.setText((double) (tklf.getTkl_Brut_fiyat() == -1 ? 0f : tklf.getTkl_Brut_fiyat()));
		
		txtBrutFiyati.setEnabled(false);
		txtBr.setEnabled(false);
		
		EditTextWatcher.addChangeListener(txtMiktar, new OnEditTextChangeListener() {
			
			@Override
			public void onTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void beforeTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void afterTextChanged(EditText et, Editable arg0) {
				if(arg0.toString().trim().length() == 0 || stokUrunList.size() == 0)
					return;
				
				int rIndex = (Integer)et.getTag();
				
				TeklifVO tkl = stokUrunList.get(rIndex);
				
				double newMiktar = Double.valueOf(arg0.toString());
				double newBrutFiyat = newMiktar * tkl.getTkl_Alisfiyati() * ((tkl.getTkl_karorani()/100) + 1);
				
				tkl.setTkl_Brut_fiyat(newBrutFiyat);
				tkl.setTkl_miktar(newMiktar);
				
				TableRow tRow = (TableRow) tblTeklifStokUrun.getChildAt(rIndex);
				PriceEditText txtBrutF = (PriceEditText) tRow.getChildAt(7);
				txtBrutF.setText(newBrutFiyat);
				
				calculateTotals();
			}
		});
		
		EditTextWatcher.addChangeListener(txtAlisFiyati, new OnEditTextChangeListener() {
			
			@Override
			public void onTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void beforeTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void afterTextChanged(EditText et, Editable arg0) {
				if(arg0.toString().trim().length() == 0 || stokUrunList.size() == 0)
					return;
				
				int rIndex = (Integer)et.getTag();
				
				TeklifVO tkl = stokUrunList.get(rIndex);
				
				double newAlisFiyat = 0;
				try {
					newAlisFiyat = Double.valueOf(arg0.toString());
				} catch (NumberFormatException e) {
					return;
				}
				double newBrutFiyat = newAlisFiyat * tkl.getTkl_miktar() * ((tkl.getTkl_karorani()/100) + 1);
				
				tkl.setTkl_Brut_fiyat(newBrutFiyat);
				tkl.setTkl_Alisfiyati(newAlisFiyat);
				
				TableRow tRow = (TableRow) tblTeklifStokUrun.getChildAt(rIndex);
				PriceEditText txtBrutF = (PriceEditText) tRow.getChildAt(7);
				txtBrutF.setText(newBrutFiyat);
				
				calculateTotals();
			}
		});
		
		EditTextWatcher.addChangeListener(txtKarOrani, new OnEditTextChangeListener() {
			
			@Override
			public void onTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void beforeTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void afterTextChanged(EditText et, Editable arg0) {
				if(arg0.toString().trim().length() == 0 || stokUrunList.size() == 0)
					return;
				
				int rIndex = (Integer)et.getTag();
				
				TeklifVO tkl = stokUrunList.get(rIndex);
				
				double newKarOrani = 0;
				try {
					newKarOrani = Double.valueOf(arg0.toString());
				} catch (NumberFormatException e) {
					return;
				}
				double newBrutFiyat = tkl.getTkl_Alisfiyati() * tkl.getTkl_miktar() * ((newKarOrani/100) + 1);
				
				tkl.setTkl_Brut_fiyat(newBrutFiyat);
				tkl.setTkl_karorani(newKarOrani);
				
				TableRow tRow = (TableRow) tblTeklifStokUrun.getChildAt(rIndex);
				PriceEditText txtBrutF = (PriceEditText) tRow.getChildAt(7);
				txtBrutF.setText(newBrutFiyat);
				
				calculateTotals();
			}
		});
		/*
		if(tklf.getTkl_RECno() != -1) {
			btnKodu.setText(tklf.getSto_kod());
			btnIsmi.setText(tklf.getSto_isim());
			
			txtMiktar.setText(tklf.getTkl_miktar());
			txtBr.setText(tklf.getSto_birim1_ad());
			txtAlisFiyati.setText(tklf.getTkl_Alisfiyati());
			txtKarOrani.setText(tklf.getTkl_karorani()+"");
			txtBrutFiyati.setText(tklf.getTkl_Brut_fiyat());
		} else {
			
		}*/
		
		if(!isEditing)
			btnRemoveRow.setTag(false);
		View[] vwList = new View[]{
				btnRemoveRow,
				btnKodu,
				btnIsmi,
				txtMiktar,
				txtBr,
				txtAlisFiyati,
				txtKarOrani,
				txtBrutFiyati
		};
		
		int col = 0;
		for (View vw : vwList) {
			
			int weight = tableHeaderList[col].getWeight();
			
			int height = TableRow.LayoutParams.MATCH_PARENT;
			if(col == 0) {
				height = 45;
			}
			TableRow.LayoutParams params = new TableRow.LayoutParams(0, height,weight);
			params.column = col;
			vw.setLayoutParams(params);
			
			vw.setPadding(3, 3, 3, 3);
			
			if(col != 0) {
				vw.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
				
				int[][] states = new int[][] {
					    new int[] { android.R.attr.state_enabled}, // enabled
					    new int[] {-android.R.attr.state_enabled}, // disabled
					    new int[] {-android.R.attr.state_checked}, // unchecked
					    new int[] { android.R.attr.state_pressed}  // pressed
					};

					int[] colors = new int[] {
					    Color.BLACK,
					    Color.BLACK,
					    Color.BLACK,
					    Color.BLACK
					};

				if(vw instanceof Button) {
					((Button)vw).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
					((Button)vw).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
					((Button)vw).setTextColor(new ColorStateList(states, colors));
				} else {
					((EditText)vw).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
					((EditText)vw).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
					((EditText)vw).setTextColor(new ColorStateList(states, colors));
				}
			}
			
			vw.setEnabled(isEditing);
			int visibility = View.VISIBLE;
			if(vw.getTag() instanceof Boolean) {
				visibility = View.INVISIBLE;
			}
			vw.setTag(row);
			tRow.addView(vw);
			vw.setVisibility(visibility);
			
			col++;
		}
		txtBr.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		txtMiktar.setGravity(Gravity.CENTER);
		
		return tRow;
	}
	
	
	private void showStokSearch(final int rIndex, final boolean isNewStok) {
		
		if(etCHIsmi.getText().length() == 0 || etCHKodu.getText().length() == 0) {
			Toast.makeText(getActivity(), "Öncelikle bir cari seçmelisiniz.", Toast.LENGTH_LONG).show();
			return;
		}
		
		SearchDialog<StokSearchVO> dialog = new SearchDialog<>(getActivity(), "Stok Arama", SearchService.STOK, true, new String[] {
			"Stok İsmi",
			"stokIsmi",
			"Stok Kodu",
			"stokKodu"
		}, new String[] {
			"Stok Kodu",
			"Stok İsmi",
		}, new String[] {
			"msg_S_0870",
			"msg_S_0078"
		}, new OnResultRowSelected<StokSearchVO>() {
			
			@Override
			public void onSelected(StokSearchVO obj) {
				
				
				final ProgressDialog dia = ProgressDialog.show(getActivity(), "", 
		                "Yükleniyor. Bekleyiniz...", true);
				RestBO.getInstance(getActivity()).getService(GetService.class).getStok(obj.getMsg_S_0088(), new Callback<List<StokVO>>() {
					
					@Override
					public void success(List<StokVO> arg0, Response arg1) {
						if(arg0.size() == 0) {
							Toast.makeText(getActivity(), "Stok detayları bulunamadı.", Toast.LENGTH_LONG).show();
							dia.dismiss();
							return;
						}
						
						final StokVO stok = arg0.get(0);
						
						RestBO.getInstance(getActivity()).getService(GetService.class).getStokFiyat(stok.getSto_kod(), etCHKodu.getText().toString(), new Callback<StokFiyatVO>() {
							
							@Override
							public void success(StokFiyatVO arg0, Response arg1) {
								
								TeklifVO teklif = new TeklifVO();
								teklif.setSto_kod(stok.getSto_kod());
								teklif.setSto_isim(stok.getSto_isim());
								teklif.setSto_birim1_ad(stok.getSto_birim1_ad());
								teklif.setTkl_vergi_pntr(stok.getSto_toptan_vergi());
								Log.d("Stok Detail", stok.getSto_toptan_vergi() + "");
								if(arg0 == null) {
									teklif.setTkl_Alisfiyati(0);
								} else {
									teklif.setTkl_Alisfiyati(arg0.getFiyat());
								}
								
								
								if(isNewStok) {
									stokUrunList.add(teklif);
									addStokUrunRow(teklif);
								} else {
									removeStokUrunTableRow(rIndex);
									stokUrunList.remove(rIndex);
									insertStokUrunRow(teklif, rIndex);
									
								}
								calculateTotals();
								dia.dismiss();
							}
							
							@Override
							public void failure(RetrofitError err) {
								Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
								err.printStackTrace();
								dia.dismiss();

							}
						});
						
						
					}
					
					@Override
					public void failure(RetrofitError err) {
						Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
						err.printStackTrace();
						dia.dismiss();
					}
				});
				
			}
		});
		dialog.show();
		
	}

	
	private void calculateTotals() {
		
		
		double araToplam = 0;
		double iskonto = 0;
		double iskontoPerc = 0;
		double masraf = 0;
		double masrafPerc = 0;
		double vergiPerc = 0;
		double vergi = 0;
		double yekun = 0;
		for (TeklifVO tklf : stokUrunList) {
			
			araToplam += tklf.getTkl_Brut_fiyat();
			int vergiIndex = isSorumlulukTwo ? 0 : tklf.getTkl_vergi_pntr();
			if(vergiIndex < VERGI.length && vergiIndex != -1) {
				vergiPerc += VERGI[vergiIndex];
				vergi +=  tklf.getTkl_Brut_fiyat() * VERGI[vergiIndex];
			}
			
		}
		
		vergiPerc = ((double)vergiPerc / (double)stokUrunList.size())*100.0; 
		yekun += (vergi + araToplam);
		
		
		etIskontoPerc.setText(NumberFormatUtil.formatPointedNumber(iskontoPerc));
		etVergiPerc.setText(NumberFormatUtil.formatPointedNumber(vergiPerc));
		etMasrafPerc.setText(NumberFormatUtil.formatPointedNumber(masrafPerc));
		
		
		etAraToplam.setText(NumberFormatUtil.formatPrice(araToplam));
		etIskonto.setText(NumberFormatUtil.formatPrice(iskonto));
		etVergi.setText(NumberFormatUtil.formatPrice(vergi));
		etYekun.setText(NumberFormatUtil.formatPrice(yekun));
		etMasraf.setText(NumberFormatUtil.formatPrice(masraf));
		
	}
	
	
	private void saveTeklif(final int tkl_satirno, final ProgressDialog dialog) {
		
		if(stokUrunList.size() <= tkl_satirno) {
			
			Toast.makeText(getActivity(), "Kaydedildi.", Toast.LENGTH_LONG).show();
			UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
			setEditing(true);
			dialog.dismiss();
			
			return;
		}
		
		TeklifVO tk = stokUrunList.get(tkl_satirno);
		
		String tkl_create_date = DateFormat.format("yyyyMMdd hh:mm:ss", new Date()) + "";
		int user = 100;
		int tkl_Odeme_Plani = -52;
		String tkl_cari_sormerk = etSorumluMerkezi.getText()+"";
		String tkl_stok_sormerk = etSorumluMerkezi.getText()+"";
		
		String tkl_stok_kod = tk.getSto_kod();
		double tkl_Alisfiyati = tk.getTkl_Alisfiyati() == -1 ? 0 : tk.getTkl_Alisfiyati();
		double tkl_karorani = tk.getTkl_karorani() == -1 ? 0 : tk.getTkl_karorani();
		double tkl_miktar = tk.getTkl_miktar() == -1 ? 0 : tk.getTkl_miktar();
		double tkl_alt_doviz_kur = tk.getTkl_alt_doviz_kur() == -1 ? 0 : tk.getTkl_alt_doviz_kur();
		int tkl_vergi_pntr = isSorumlulukTwo ? 0 : tk.getTkl_vergi_pntr();
		String tkl_Sorumlu_Kod = etSorumlu.getText() + "";

		RestBO.getInstance(getActivity()).getService(PostService.class).saveTeklif(
				user, 
				tkl_create_date, 
				user, 
				tkl_create_date, tkl_stok_kod, 
				etCHKodu.getText()+"", 
				etEvrakSeriNo.getText()+"", 
				Integer.valueOf(etEvrakSiraNo.getText()+""), 
				getEditTextDateFormatted(etEvrakTarih),
				tkl_satirno, 
				etBelgeNo.getText()+"", 
				getEditTextDateFormatted(etBelgeTarih),
				getEditTextDateFormatted(etBaslamaTarihi),
				getEditTextDateFormatted(etGecerlilikTarihi),
				tkl_Odeme_Plani, 
				tkl_Alisfiyati, 
				tkl_karorani, 
				tkl_miktar, 
				tkl_alt_doviz_kur, 
				tkl_vergi_pntr, 
				tkl_Sorumlu_Kod, 
				tkl_cari_sormerk, 
				tkl_stok_sormerk, new Callback<IdReturnVO>() {

					@Override
					public void failure(RetrofitError err) {
						
						Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
						err.printStackTrace();
						
						UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
						dialog.dismiss();
					}

					@Override
					public void success(IdReturnVO arg0, Response arg1) {
						
						
						if(arg0.getError() == 1) {
							Toast.makeText(getActivity(), "İşlem yapılırken servis ile ilgili bir hata oluştu.", Toast.LENGTH_LONG).show();
							UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
							dialog.dismiss();
						} else if(arg0.getId() == 0) {
							
							
							new AlertDialog.Builder(getActivity())
							.setTitle("Hünkar Gıda")
							.setMessage("Bu seri-sıra numaralı teklif zaten kayıtlı. Sıradaki seri numarası ile kaydetmek ister misiniz?")
							.setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									etEvrakSiraNo.requestFocus();
								}
							})
							.setPositiveButton("Evet", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									updateSiraNo(true);
								}
							})
							.setOnCancelListener(new DialogInterface.OnCancelListener() {
								
								@Override
								public void onCancel(DialogInterface arg0) {
									etEvrakSiraNo.requestFocus();
								}
							}).show();
							UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
							
							dialog.dismiss();
						} else {
							
							saveTeklif(tkl_satirno+1, dialog);
							
						}
						
					}
				});
		
	}
	
}