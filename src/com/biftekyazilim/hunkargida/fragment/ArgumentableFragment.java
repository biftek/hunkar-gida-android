package com.biftekyazilim.hunkargida.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;

public class ArgumentableFragment extends Fragment {

	protected static final int ROW_HEIGHT = 60;
	public ArgumentableFragment() {
		 setArguments(new Bundle());
	}
	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d("ArgumentableFragment", "onResume: " + getClass().getCanonicalName());
	}
}
