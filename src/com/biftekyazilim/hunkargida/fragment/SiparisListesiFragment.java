package com.biftekyazilim.hunkargida.fragment;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;
import com.biftekyazilim.hunkargida.util.ReflectionUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.view.PriceTextView;
import com.biftekyazilim.hunkargida.view.PriceTextView;
import com.biftekyazilim.hunkargida.vo.SiparisTakipVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;

public class SiparisListesiFragment extends ArgumentableFragment {
    
	private TableRow headerRow;
	private TableLayout tblContent;
	private TableHeaderVO[] tableHeaderList;
	private List<SiparisTakipVO> siparisTakipList;
	
    public SiparisListesiFragment(){}
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        getActivity().setTitle("Siparişlerim");
		View rootView = inflater.inflate(R.layout.fragment_siparislistesi,
				container, false);

		tblContent = (TableLayout) rootView.findViewById(R.id.tblContent);

		headerRow = (TableRow) rootView.findViewById(R.id.headerRow);

		tableHeaderList = new TableHeaderVO[] { new TableHeaderVO("Stok Kodu","msg_S_0078", 80, 12),
				new TableHeaderVO("Stok İsmi","msg_S_0070", 130, 12),
				new TableHeaderVO("Birimi","msg_S_0010", 40, 12),
				new TableHeaderVO("Cari Ünvanı","msg_S_0201", 130, 12),
				new TableHeaderVO("Cari No","msg_S_0200", 80, 12),
				new TableHeaderVO("Teslim Tarihi","teslim_tarihi", 70, 12),
				new TableHeaderVO("Seri No","msg_S_0243", 40, 12),
				new TableHeaderVO("Sıra No","msg_S_0157", 40, 12),
				new TableHeaderVO("Depo","msg_S_0159", 50, 12),
				new TableHeaderVO("Kalan","msg_S_0463", 60, 12),
				new TableHeaderVO("Birim Fiyat","msg_S_0248", 60, 12),
				new TableHeaderVO("Sipariş Bürüt Tutar","msg_S_0249", 60, 12),
				new TableHeaderVO("Sipariş Net","msg_S_0252", 60, 12),
				new TableHeaderVO("Kalan Sipariş Net","msg_S_0253", 60, 12),
				new TableHeaderVO("Durum","msg_S_0262", 40, 12)};
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList, new OnClickListener() {
			
			@Override
			public void onClick(View vw) {
				if(siparisTakipList == null || siparisTakipList.size() == 0) {
					return;
				}
				int col = (int)vw.getTag();
				if(col == 1) {
					return;
				}
				final boolean desc = col < 0 ? true : false;
				vw.setTag(-col);
				col = Math.abs(col)-1;
				
				TableHeaderVO th = tableHeaderList[col];
				final String propertyName = th.getObjectPropertyName();
				if(propertyName == null)
					return;
				Collections.sort(siparisTakipList, new Comparator<SiparisTakipVO>() {

					@Override
					public int compare(SiparisTakipVO arg0, SiparisTakipVO arg1) {
						
						String val1 = ReflectionUtil.getValue(propertyName, arg0) + "";
						String val2 = ReflectionUtil.getValue(propertyName, arg1) + "";
						return (desc ? -1 : 1) * val1.compareTo(val2);
					}
				});
				fillTable();
			}
		});

		
		
		if(getArguments() != null && getArguments().containsKey("cariKod")) {
			String cariKod = getArguments().getString("cariKod");
			loadCariSiparisList(cariKod);
			getArguments().remove("cariKod");
		} else {
			loadSiparisList();
		}
        return rootView;
    }
    
    private void loadCariSiparisList(String cariKod) {
    	
    	final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
				"Yükleniyor. Bekleyiniz...", true);
    	Log.d("Sale List", "Cari seal list loading");
		RestBO.getInstance(getActivity()).getService(GetService.class).getCariSiparisTakipList(cariKod, new Callback<List<SiparisTakipVO>>() {
			
			@Override
			public void success(List<SiparisTakipVO> result, Response arg1) {
				
				siparisTakipList = result;
				fillTable();
				dialog.dismiss();
			}
			
			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(getActivity(), err.getMessage(),
						Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}
		});
		
    }
    private void loadSiparisList() {
		final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
				"Yükleniyor. Bekleyiniz...", true);
    	Log.d("Sale List", "All seal list loading");
		RestBO.getInstance(getActivity()).getService(GetService.class).getSiparisTakipList(new Callback<List<SiparisTakipVO>>() {
			
			@Override
			public void success(List<SiparisTakipVO> result, Response arg1) {
				
				siparisTakipList = result;
				fillTable();
				dialog.dismiss();
			}
			
			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(getActivity(), err.getMessage(),
						Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}
		});
		
		
	}
    
    
    private void fillTable() {
		tblContent.removeAllViews();
		if(siparisTakipList.size() >0) {
			
			
			double toplamKalan = 0;
			double toplamBirimFiyat = 0;
			double toplamSiparisBurutTutar= 0;
			double toplamSiparisNet = 0;
			double toplamKalanSiparisNet = 0;
			
			
			for (SiparisTakipVO siparisTakip : siparisTakipList) {
				
				TableRow tRow = new TableRow(getActivity());
				tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
				tRow.setMinimumHeight(ROW_HEIGHT);
				
				TextView tvStokKodu = new TextView(getActivity());
				TextView tvStokIsmi = new TextView(getActivity());
				TextView tvBirimi = new TextView(getActivity());
				TextView tvCariUnvan = new TextView(getActivity());
				TextView tvCariNo = new TextView(getActivity());
				TextView tvTeslimTarih = new TextView(getActivity());
				TextView tvSeriNo = new TextView(getActivity());
				TextView tvSiraNo = new TextView(getActivity());
				TextView tvDepo = new TextView(getActivity());
				PriceTextView tvKalan = new PriceTextView(getActivity());
				PriceTextView tvBirimFiyat = new PriceTextView(getActivity());
				PriceTextView tvSiparisBurutTutar = new PriceTextView(getActivity());
				PriceTextView tvSiparisNet = new PriceTextView(getActivity());
				PriceTextView tvKalanSiparisNet = new PriceTextView(getActivity());
				TextView tvDurum = new TextView(getActivity());
				

				tvKalan.setGravity(Gravity.RIGHT);
				tvBirimFiyat.setGravity(Gravity.RIGHT);
				tvSiparisBurutTutar.setGravity(Gravity.RIGHT);
				tvSiparisNet.setGravity(Gravity.RIGHT);
				tvKalanSiparisNet.setGravity(Gravity.RIGHT);
				
				TextView[] tvList = new TextView[]{
						tvStokKodu,
						tvStokIsmi,
						tvBirimi,
						tvCariUnvan,
						tvCariNo,
						tvTeslimTarih,
						tvSeriNo,
						tvSiraNo,
						tvDepo,
						tvKalan,
						tvBirimFiyat,
						tvSiparisBurutTutar,
						tvSiparisNet,
						tvKalanSiparisNet,
						tvDurum
				};
				
				int col = 0;
				for (TextView textView : tvList) {
					
					int weight = tableHeaderList[col].getWeight();
					textView.setBackgroundResource(R.drawable.table_cell_white_bg);
					/*if(rowCounter%2 == 0) {
						textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
					} else {
						textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
					}*/
					textView.setPadding(5, 5, 5, 5);
					textView.setTextColor(Color.BLACK);
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
					TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,weight);
					params.column = col;
					textView.setLayoutParams(params);
					col++;

					tRow.addView(textView);
					
				}
				
				
				tvStokKodu.setText(siparisTakip.getMsg_S_0078());
				tvStokIsmi.setText(siparisTakip.getMsg_S_0070());
				tvBirimi.setText(siparisTakip.getMsg_S_0084());
				tvCariUnvan.setText(siparisTakip.getMsg_S_0201());
				tvCariNo.setText(siparisTakip.getMsg_S_0200());
				tvTeslimTarih.setText(siparisTakip.getTeslim_Tarihi());
				tvSeriNo.setText(siparisTakip.getMsg_S_0243());
				tvSiraNo.setText(siparisTakip.getMsg_S_0157()+"");
				tvDepo.setText(siparisTakip.getMsg_S_0159());
				tvKalan.setText(siparisTakip.getMsg_S_0463());
				tvBirimFiyat.setText(siparisTakip.getMsg_S_0248());
				tvSiparisBurutTutar.setText(siparisTakip.getMsg_S_0249());
				tvSiparisNet.setText(siparisTakip.getMsg_S_0252());
				tvKalanSiparisNet.setText(siparisTakip.getMsg_S_0253());
				tvDurum.setText(siparisTakip.getMsg_S_0262());
				
				
				toplamKalan += siparisTakip.getMsg_S_0463();
				toplamBirimFiyat += siparisTakip.getMsg_S_0248();
				toplamSiparisBurutTutar += siparisTakip.getMsg_S_0249();
				toplamSiparisNet += siparisTakip.getMsg_S_0252();
				toplamKalanSiparisNet += siparisTakip.getMsg_S_0253();
				
				
				
				tblContent.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
			}
			
			
				Object[] toplamList = new Object[]{
					"Toplam",
					toplamKalan,
					toplamBirimFiyat,
					toplamSiparisBurutTutar,
					toplamSiparisNet,
					toplamKalanSiparisNet
				};
				TableRow totalRow = new TableRow(getActivity());
				TextView[] toplamTvList = new TextView[]{
						new TextView(getActivity()), 
						new TextView(getActivity()), 
						new TextView(getActivity()), 
						new TextView(getActivity()), 
						new TextView(getActivity()), 
						new TextView(getActivity()), 
						new TextView(getActivity())
				};
				
				int toplamCol = 8;
				for (int i = 0; i < toplamTvList.length; i++) {
					TextView tv = toplamTvList[i];
					
					tv.setGravity(Gravity.RIGHT);
					//tv.setPadding(5, 5, 5, 5);
					tv.setTextColor(Color.BLACK);
					tv.setTextSize(10);
					if(i == 0) {
						tv.setText(toplamList[i]+"");
					} else if(i < toplamTvList.length - 1)
						tv.setText(NumberFormatUtil.formatPrice((double)toplamList[i]));
					
					int weight = tableHeaderList[toplamCol].getWeight();
					TableRow.LayoutParams params = new TableRow.LayoutParams(0, 25,weight);
					params.column = toplamCol;
					if(i == 0) {
						for (int j = 0; j < toplamCol; j++) {
							params.weight += tableHeaderList[j].getWeight();
						}
					}
					tv.setLayoutParams(params);
					totalRow.addView(tv);
					
					toplamCol++;
				}
				totalRow.setPadding(0, 15, 0, 15);
				tblContent.addView(totalRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
				
		} else {
			Toast.makeText(getActivity(), "Herhangi bir satış bulunamadı.", Toast.LENGTH_LONG).show();
		}
	}
}