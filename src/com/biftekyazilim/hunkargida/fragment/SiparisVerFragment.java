package com.biftekyazilim.hunkargida.fragment;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.MainActivity;
import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.bo.StaticDataBO;
import com.biftekyazilim.hunkargida.enm.SearchService;
import com.biftekyazilim.hunkargida.listener.EditTextWatcher;
import com.biftekyazilim.hunkargida.listener.OnDateEditTextFocusListener;
import com.biftekyazilim.hunkargida.listener.OnEditTextChangeListener;
import com.biftekyazilim.hunkargida.listener.OnResultRowSelected;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.service.PostService;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.view.CariAdreslerDialog;
import com.biftekyazilim.hunkargida.view.NumberEditText;
import com.biftekyazilim.hunkargida.view.PriceEditText;
import com.biftekyazilim.hunkargida.view.SearchDialog;
import com.biftekyazilim.hunkargida.vo.CariAdresVO;
import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.DepoVO;
import com.biftekyazilim.hunkargida.vo.IdReturnVO;
import com.biftekyazilim.hunkargida.vo.SiparisSearchVO;
import com.biftekyazilim.hunkargida.vo.SiparisVO;
import com.biftekyazilim.hunkargida.vo.SiraNoVO;
import com.biftekyazilim.hunkargida.vo.SorumlulukMerkeziVO;
import com.biftekyazilim.hunkargida.vo.StokFiyatVO;
import com.biftekyazilim.hunkargida.vo.StokSearchVO;
import com.biftekyazilim.hunkargida.vo.StokVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;
import com.biftekyazilim.hunkargida.vo.TeklifVO;

public class SiparisVerFragment extends ArgumentableFragment {

	private TableLayout tblSiparisStokUrun;
	private EditText etEvrakSeriNo;
	private EditText etEvrakSiraNo;
	private EditText etBelgeNo;
	private EditText etCHKodu;
	private EditText etCHIsmi;
	private EditText etDoviz;
	private EditText etSaticiKodu;
	private EditText etAdres;
	
	private EditText etProje;
	private EditText etSorumluMerkezi;
	private EditText etOdemePlani;
	private EditText etDepo;
	private EditText etTeslimTuru;
	private EditText etTeslimTarihi;
	
	private EditText etEvrakTarih;
	private EditText etBelgeTarih;
	
	private TextView etIskontoPerc;
	private TextView etMasrafPerc;
	private TextView etKDVPerc;
	private TextView etOTVPerc;
	
	private TextView etAraToplam;
	private TextView etIskonto;
	private TextView etMasraf;
	private TextView etKDV;
	private TextView etOTV;
	private TextView etYekun;
	
	private Button btnCHKodu;
	private Button btnCHIsmi;
	private Button btnDoviz;
	private Button btnOdemePlani;
	private Button btnSorumluMerkezi;
	private Button btnDepo;
	private Button btnYeni;
	private Button btnKaydet;
	private Button btnSil;
	private Button btnSearch;
	private Button btnNewLine;
	private Button btnAdres;
	
	private TableHeaderVO[] tableHeaderList;
	
	private List<SiparisVO> stokUrunList;
	private boolean isEditing = true;
	
	private boolean isSorumlulukTwo = false;
	
	public static List<TeklifVO> teklifStokUrunList;
	
	private static String[] depoNameList;
	private static String[] sorumlulukMerkeziNameList;
	
	
	private static CariSummaryVO CARI_SUMMARY;
	
	public static CariSummaryVO getCariSummary() {
		return CARI_SUMMARY;
	}
	
	public static void setCariSummary(CariSummaryVO cariSummary) {
		CARI_SUMMARY = cariSummary;
	}
	
	public SiparisVerFragment() {
		stokUrunList = new ArrayList<>();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_siparisver,
				container, false);

		tblSiparisStokUrun = (TableLayout) rootView.findViewById(R.id.tblSiparisStokUrun);
		TableRow headerRow = (TableRow)rootView.findViewById(R.id.headerRow);
		
		tableHeaderList = new TableHeaderVO[] {
				new TableHeaderVO("İşlem", 50),
				new TableHeaderVO("Kodu", 125),
				new TableHeaderVO("İsmi", 125),
				new TableHeaderVO("Miktar", 50),
				new TableHeaderVO("Br", 25),
				new TableHeaderVO("Birim Fiyat", 75),
				new TableHeaderVO("Tutarı", 150),
				new TableHeaderVO("Tesl. Tarihi", 75),
				new TableHeaderVO("İsk. 1(%)", 50),
				new TableHeaderVO("İsk. 2(%)", 50),
				new TableHeaderVO("İsk. 3(%)", 50),
				new TableHeaderVO("İsk. 4(%)", 50),
				new TableHeaderVO("Açıklama", 125) };
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList);

		etEvrakSeriNo = (EditText)rootView.findViewById(R.id.etEvrakSeriNo);
		etEvrakSiraNo = (EditText)rootView.findViewById(R.id.etEvrakSiraNo);
		
		etBelgeNo = (EditText)rootView.findViewById(R.id.etBelgeNo);
		etCHKodu = (EditText)rootView.findViewById(R.id.etCHKodu);
		etCHIsmi = (EditText)rootView.findViewById(R.id.etCHIsmi);
		etDoviz = (EditText)rootView.findViewById(R.id.etDoviz);
		etSaticiKodu = (EditText)rootView.findViewById(R.id.etSaticiKodu);
		
		etProje = (EditText)rootView.findViewById(R.id.etProje);
		etSorumluMerkezi = (EditText)rootView.findViewById(R.id.etSorumluMerkezi);
		etOdemePlani = (EditText)rootView.findViewById(R.id.etOdemePlani);
		etDepo = (EditText)rootView.findViewById(R.id.etDepo);
		etTeslimTuru = (EditText)rootView.findViewById(R.id.etTeslimTuru);
		etAdres = (EditText)rootView.findViewById(R.id.etAdres);
		
		etTeslimTarihi = (EditText)rootView.findViewById(R.id.etTeslimTarihi);
		etEvrakTarih = (EditText)rootView.findViewById(R.id.etEvrakTarih);
		etBelgeTarih = (EditText)rootView.findViewById(R.id.etBelgeTarih);
		
		btnCHKodu = (Button)rootView.findViewById(R.id.btnCHKodu);
		btnCHIsmi = (Button)rootView.findViewById(R.id.btnCHIsmi);
		btnDoviz = (Button)rootView.findViewById(R.id.btnDoviz);
		btnOdemePlani = (Button)rootView.findViewById(R.id.btnOdemePlani);
		btnSorumluMerkezi = (Button)rootView.findViewById(R.id.btnSorumluMerkezi);
		btnDepo = (Button)rootView.findViewById(R.id.btnDepo);
		btnYeni = (Button)rootView.findViewById(R.id.btnYeni);
		btnKaydet = (Button)rootView.findViewById(R.id.btnKaydet);
		btnSil = (Button)rootView.findViewById(R.id.btnSil);
		btnAdres = (Button)rootView.findViewById(R.id.btnAdres);
		
		
		btnSearch = (Button)rootView.findViewById(R.id.btnSearch);
		btnNewLine = (Button)rootView.findViewById(R.id.btnNewLine);
		
		etIskontoPerc = (TextView)rootView.findViewById(R.id.etIskontoPerc);
		etMasrafPerc = (TextView)rootView.findViewById(R.id.etMasrafPerc);
		etKDVPerc = (TextView)rootView.findViewById(R.id.etKDVPerc);
		etOTVPerc = (TextView)rootView.findViewById(R.id.etOTVPerc);
			
		etAraToplam = (TextView)rootView.findViewById(R.id.etAraToplam);
		etIskonto = (TextView)rootView.findViewById(R.id.etIskonto);
		etMasraf = (TextView)rootView.findViewById(R.id.etMasraf);
		etKDV = (TextView)rootView.findViewById(R.id.etKDV);
		etOTV = (TextView)rootView.findViewById(R.id.etOTV);
		etYekun = (TextView)rootView.findViewById(R.id.etYekun);
		
		initHand();
		
		etCHKodu.setEnabled(false);
		etCHIsmi.setEnabled(false);
		etAdres.setEnabled(false);
		
		clearStokUrunTable(true);
		setEditing(true);
		
		etSorumluMerkezi.setText("1");
		etSorumluMerkezi.setEnabled(false);
		etDoviz.setText("TL");
		etDoviz.setEnabled(false);
		etOdemePlani.setEnabled(false);
		
		depoNameList = new String[StaticDataBO.getInstance(getActivity()).getDepoList().size()];
		int i = 0;
		for (DepoVO depo : StaticDataBO.getInstance(getActivity()).getDepoList()) {
			
			depoNameList[i] = depo.getDep_adi();
			i++;
		}
		
		sorumlulukMerkeziNameList = new String[StaticDataBO.getInstance(getActivity()).getSorumlulukMerkeziList().size()];
		int j = 0;
		for (SorumlulukMerkeziVO srm : StaticDataBO.getInstance(getActivity()).getSorumlulukMerkeziList()) {
			
			sorumlulukMerkeziNameList[j] = srm.getMsg_S_0870();
			j++;
		}
		
		if(CARI_SUMMARY != null) {
			fillFieldsRelatedCari();
		}
		
		
		return rootView;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		if(MainActivity.FRAGMENT_CHANGE) {
			MainActivity.FRAGMENT_CHANGE = false;
			setEditing(true);
		}
		
		if(teklifStokUrunList != null && teklifStokUrunList.size() > 0) {
			
			
			clearStokUrunTable(true);
			
			SiparisVO sip = null;
			for (TeklifVO teklif : teklifStokUrunList) {
				sip = new SiparisVO();

				sip.setSip_musteri_kod(teklif.getTkl_cari_kod());
				sip.setCari_unvan1(teklif.getCari_unvan1());
				sip.setCari_odemeplan_no(teklif.getTkl_Odeme_Plani());
				sip.setSip_satici_kod(teklif.getTkl_Sorumlu_Kod());
				sip.setSip_teslimturu(teklif.getTKL_TESLIMTURU());
				sip.setSip_projekodu(teklif.getTkl_ProjeKodu());
				sip.setSip_belge_tarih(teklif.getTkl_belge_tarih());
				sip.setSip_create_date(teklif.getTkl_evrak_tarihi());
				
				sip.setSip_iskonto1(teklif.getTkl_iskonto1());
				sip.setSip_iskonto2(teklif.getTkl_iskonto2());
				sip.setSip_iskonto3(teklif.getTkl_iskonto3());
				sip.setSip_iskonto4(teklif.getTkl_iskonto4());
				sip.setSip_iskonto5(teklif.getTkl_iskonto5());
				sip.setSip_iskonto6(teklif.getTkl_iskonto6());
				
				sip.setSip_masraf1(teklif.getTkl_masraf1());
				sip.setSip_masraf2(teklif.getTkl_masraf2());
				sip.setSip_masraf3(teklif.getTkl_masraf3());
				sip.setSip_masraf4(teklif.getTkl_masraf4());
				
				sip.setSip_vergi(teklif.getTkl_vergi());
				sip.setSip_vergi_pntr(teklif.getTkl_vergi_pntr());
				
				sip.setSto_birim1_ad(teklif.getSto_birim1_ad());
				sip.setSto_kod(teklif.getSto_kod());
				sip.setSto_isim(teklif.getSto_isim());
				sip.setSip_b_fiyat(teklif.getTkl_Alisfiyati() * ((teklif.getTkl_karorani()/100) + 1));
				sip.setSip_miktar(teklif.getTkl_miktar());
				sip.setSip_tutar(teklif.getTkl_Brut_fiyat());
				
				sip.setSip_opno(teklif.getTkl_Odeme_Plani());
				
				stokUrunList.add(sip);
			}
			
			
			fillSiparis(sip, false);
			fillStokUrunTable(sip.getSip_fileid());
			teklifStokUrunList = null;
		}
		
	}
	
	private void initHand() {
		
		btnDepo.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				if(depoNameList == null || depoNameList.length == 0) {
					Toast.makeText(getActivity(), "Depo listesi servisten çekilemedi. Uygulamayı yeniden başlatınız.", Toast.LENGTH_LONG).show();
					return;
				}
				new AlertDialog.Builder(getActivity())
		        .setSingleChoiceItems(depoNameList, 0, null)
		        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int whichButton) {
		                dialog.dismiss();
		                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
		                etDepo.setText(depoNameList[selectedPosition]);
		                etDepo.setTag(StaticDataBO.getInstance(getActivity()).getDepoList().get(selectedPosition).getDep_no());//depo no
		            }
		        })
		        .setCancelable(true)
		        .show();
			}
		});
		
		btnSorumluMerkezi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				new AlertDialog.Builder(getActivity())
		        .setSingleChoiceItems(sorumlulukMerkeziNameList, 0, null)
		        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int whichButton) {
		                dialog.dismiss();
		                int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
		                SorumlulukMerkeziVO sorumlulukMerkezi = StaticDataBO.getInstance(getActivity()).getSorumlulukMerkeziList().get(selectedPosition);
		                etSorumluMerkezi.setText(sorumlulukMerkezi.getMsg_S_0078());
		                etSorumluMerkezi.setTag(sorumlulukMerkezi.getMsg_S_0088());
		                isSorumlulukTwo = sorumlulukMerkezi.getMsg_S_0088() == 2;
		                calculateTotals();
		            }
		        })
		        .setCancelable(true)
		        .show();
			}
		});
		
		btnYeni.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				new AlertDialog.Builder(getActivity())
			    .setTitle(R.string.app_name)
			    .setMessage("Yeni bir sipariş açmak istediğinize emin misiniz?")
			    .setCancelable(true)
			    .setNegativeButton(android.R.string.cancel, null)
			    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
			    	@Override
			    	public void onClick(DialogInterface arg0, int arg1) {
			    		setEditing(true);
			    	}
			    })
			    .setIcon(android.R.drawable.ic_dialog_alert)
			    .show();
			}
		});
		
		
		btnKaydet.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				if(stokUrunList.size() == 0) {
					Toast.makeText(getActivity(), "Stoktan en az 1 ürün eklemeniz gerekmektedir.", Toast.LENGTH_LONG).show();
					return;
				}
				EditText[] reqFields = new EditText[]{
						
						etEvrakSeriNo,
						etEvrakSiraNo,

						etCHKodu,
						etCHIsmi,
						etOdemePlani,
						etTeslimTarihi,
						
						etDepo,
						etEvrakTarih,
						etBelgeTarih,
						etAdres
						
				};
				for (int i = 0; i < reqFields.length; i++) {
					if(reqFields[i].getText().toString().trim().length() == 0) {
						Toast.makeText(getActivity(), "Lütfen gerekli bütün alanları doldurunuz.", Toast.LENGTH_LONG).show();
						reqFields[i].requestFocus();
						return;
					}
				}
				
				final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", 
		                "Yükleniyor. Bekleyiniz...", true);
				
				saveSiparis(0, dialog);
				
			}
		});
		
		btnSil.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				new AlertDialog.Builder(getActivity())
			    .setTitle(R.string.app_name)
			    .setMessage("Sipariş kaydınız silinecek. Devam etmek istiyor musunuz?")
			    .setCancelable(true)
			    .setNegativeButton(android.R.string.cancel, null)
			    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int arg1) {
						String siraNo = etEvrakSiraNo.getText().toString();
						String seriNo = etEvrakSeriNo.getText().toString();
						
						final ProgressDialog dia = ProgressDialog.show(getActivity(), "", 
				                "İşlem yapılıyor. Bekleyiniz...", true);
						RestBO.getInstance(getActivity()).getService(PostService.class).deleteSiparis(seriNo, siraNo, new Callback<IdReturnVO>() {
							
							@Override
							public void success(IdReturnVO arg0, Response arg1) {
								Toast.makeText(getActivity(), "İşlem tamamlandı.", Toast.LENGTH_LONG).show();
								setEditing(true);
								dia.dismiss();
							}
							
							@Override
							public void failure(RetrofitError err) {
								Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
								err.printStackTrace();
								dia.dismiss();
							}
						});
					}
				})
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();
				
				
				
			}
		});
		
		btnSearch.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				SearchDialog<SiparisSearchVO> dialog = new SearchDialog<>(
						getActivity(), "Evrak Arama", 
						SearchService.SIPARIS, false, 
						new String[] {
								"Evrak Seri No",
								"Evrak Sıra No"
							}, new String[] {
							"Evrak Seri No",
							"Evrak Sıra No",
							"Cari Ünvan"
						}, new String[]{
							"seri",
							"sira",
							"cari_ismi"
						}, new OnResultRowSelected<SiparisSearchVO>() {

							@Override
							public void onSelected(SiparisSearchVO obj) {
								
								
								final ProgressDialog dia = ProgressDialog.show(getActivity(), "", 
						                "Yükleniyor. Bekleyiniz...", true);
								RestBO.getInstance(getActivity()).getService(GetService.class).getSiparis(obj.getSeri(), obj.getSira(), new Callback<List<SiparisVO>>() {
									
									@Override
									public void success(List<SiparisVO> result, Response arg1) {
										if(result.size() == 0) {
											Toast.makeText(getActivity(), "Belirtilen sipariş bulunamadı.", Toast.LENGTH_LONG).show();
											dia.dismiss();
											return;
										}
										setEditing(false);
										clearStokUrunTable(true);
										stokUrunList = result;
										fillSiparis(result.get(0), true);
										fillStokUrunTable(result.get(0).getSip_fileid());
										dia.dismiss();
									}
									
									@Override
									public void failure(RetrofitError err) {
										Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
										err.printStackTrace();
										dia.dismiss();
									}
								});
								
								
							}
						});

				dialog.setEditTextNumber(1);
				dialog.setTextEditText2(etEvrakSiraNo.getText()+"");
				dialog.setTextEditText1(etEvrakSeriNo.getText()+"");
				dialog.show(true);
				
				
				
			}
		});
		
		btnNewLine.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				showStokSearch(-1, true);
				
			}
		});
		btnAdres.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String cariKod = etCHKodu.getText().toString();
				if(cariKod.isEmpty()) {
					Toast.makeText(getActivity(), "Öncelikle bir cari seçmeniz gerekmektedir.", Toast.LENGTH_LONG).show();
				} else {
					CariAdreslerDialog dialog = new CariAdreslerDialog(getActivity(), cariKod);
					dialog.setOnRowSelectedListener(new OnResultRowSelected<CariAdresVO>() {
						
						@Override
						public void onSelected(CariAdresVO obj) {
							etAdres.setText(obj.getAdr_cadde() + ", " + obj.getAdr_sokak() + ", " + obj.getAdr_ilce() + ", " + obj.getAdr_il());
							btnAdres.setTag(obj.getAdr_adres_no());
						}
					});
					dialog.show();
				}
				
			}
			
		});
		btnCHKodu.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				SearchDialog<CariSummaryVO> dialog = new SearchDialog<>(
						getActivity(), "Cari Arama", 
						SearchService.CARI, true, 
						new String[] {
							"Cari İsmi",
							"cariIsmi",
							"Cari Kodu",
							"cariKodu"
						}, new String[] {
							"Cari Kodu",
							"Cari Ünvanı"
						}, new String[]{
							"cari_kod",
							"cari_unvan1"
						}, new OnResultRowSelected<CariSummaryVO>() {

							@Override
							public void onSelected(CariSummaryVO obj) {
								CARI_SUMMARY = obj;
								fillFieldsRelatedCari();
							}
						});
				dialog.show();
			}
		});
		
		btnCHIsmi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				SearchDialog<CariSummaryVO> dialog = new SearchDialog<>(
						getActivity(), "Cari Arama", 
						SearchService.CARI, true, 
						new String[] {
							"Cari İsmi",
							"cariIsmi",
							"Cari Kodu",
							"cariKodu"
						}, new String[] {
							"Cari Kodu",
							"Cari Ünvanı"
						}, new String[]{
							"cari_kod",
							"cari_unvan1"
						}, new OnResultRowSelected<CariSummaryVO>() {

							@Override
							public void onSelected(CariSummaryVO obj) {
								CARI_SUMMARY = obj;
								fillFieldsRelatedCari();
							}
						});
				dialog.show();
			}
		});
		
		OnFocusChangeListener dateEditTextListener = new OnDateEditTextFocusListener(getActivity());
		
		etBelgeTarih.setOnFocusChangeListener(dateEditTextListener);
		etEvrakTarih.setOnFocusChangeListener(dateEditTextListener);
		etTeslimTarihi.setOnFocusChangeListener(dateEditTextListener);
		/*TextWatcher textWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				//TODO sira numarası çekilecek servisten
				int evrakSiraNo = 4;
				etEvrakSiraNo.setText(evrakSiraNo+"");
			}
		};
		etEvrakSeriNo.addTextChangedListener(textWatcher);*/
		//etEvrakSiraNo.addTextChangedListener(textWatcher);
		etEvrakSeriNo.setImeOptions(EditorInfo.IME_ACTION_DONE);
		etEvrakSeriNo.setOnEditorActionListener(new OnEditorActionListener() {
			
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH ||
		                actionId == EditorInfo.IME_ACTION_DONE ||
		                event.getAction() == KeyEvent.ACTION_DOWN &&
		                event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
					updateSiraNo(false);
		        }
				return false;
			}
		});
		etEvrakSeriNo.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(final View editText, boolean hasFocus) {
				if(hasFocus && etEvrakSeriNo.getText().toString().trim().length() != 0)
					return;
				updateSiraNo(false);
				
			}
		});
	}
	
	private void fillFieldsRelatedCari() {
		etCHKodu.setText(CARI_SUMMARY.getCari_kod());
		etCHIsmi.setText(CARI_SUMMARY.getCari_unvan1());
		etOdemePlani.setText(CARI_SUMMARY.getCari_odemeplan_no()+"");
		etSaticiKodu.setText(CARI_SUMMARY.getCari_temsilci_kodu());
		CARI_SUMMARY = null;
	}
	
	private void updateSiraNo(final boolean alsoCallSave) {
		
		if(etEvrakSeriNo.getText().toString().trim().length() != 0) {
			
			final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", 
	                "Yükleniyor. Bekleyiniz...", true);
			
			RestBO.getInstance(getActivity()).getService(GetService.class).getSiparisSiraNo(etEvrakSeriNo.getText().toString(), new Callback<SiraNoVO>() {

				@Override
				public void failure(RetrofitError err) {
					Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
					err.printStackTrace();
					dialog.dismiss();
				}

				@Override
				public void success(SiraNoVO arg0, Response arg1) {
					
					if(arg0.getSirano() < 1) {
						
						Toast.makeText(getActivity(), "Girdiğiniz seri numarası kayıtlı değildir.", Toast.LENGTH_LONG).show();
						dialog.dismiss();
					} else {
						etEvrakSiraNo.setText(arg0.getSirano()+"");
						etEvrakSiraNo.setTag(etEvrakSeriNo.getText()+"");
						if(alsoCallSave) {
							saveSiparis(0, dialog);
						} else {
							dialog.dismiss();
						}
					}
					
					
				}
				
			});
			
		}
		
	}
	
	private String getEditTextDateFormatted(EditText et) {
		
		String[] spl = et.getText().toString().split("/");
		String tarih = spl[2] + spl[1] + spl[0];
		
		return tarih;
	}
	
	
	public void setEditing(boolean editing) {
		clearStokUrunTable(true);
		
		etBelgeNo.setEnabled(editing);
		etOdemePlani.setEnabled(editing);
		etTeslimTuru.setEnabled(editing);
		etProje.setEnabled(editing);
		etDepo.setEnabled(editing);
		
		etEvrakSeriNo.setEnabled(editing);
		etEvrakSiraNo.setEnabled(editing);
		etEvrakTarih.setEnabled(editing);
		etBelgeTarih.setEnabled(editing);
		etTeslimTarihi.setEnabled(editing);
		
		btnCHIsmi.setEnabled(editing);
		btnCHKodu.setEnabled(editing);
		btnDoviz.setEnabled(editing);
		btnOdemePlani.setEnabled(editing);
		btnSorumluMerkezi.setEnabled(editing);
		btnAdres.setEnabled(editing);
		btnNewLine.setVisibility(editing ? View.VISIBLE : View.GONE);
		btnDepo.setEnabled(editing);
		
		//btnYeni.setEnabled(true);
		btnKaydet.setEnabled(editing);
		btnSil.setEnabled(!editing);
		
		
		etEvrakTarih.setText("");
		etBelgeTarih.setText("");
		
		etEvrakSeriNo.setText("");
		etEvrakSiraNo.setText("");
		etBelgeNo.setText("");
		etCHKodu.setText("");
		etAdres.setText("");
		etCHIsmi.setText("");
		etOdemePlani.setText("");
		etTeslimTuru.setText("");
		etProje.setText("");
		etDepo.setText("");
		etDepo.setTag(null);
		etEvrakTarih.setText("");
		etBelgeTarih.setText("");
		etTeslimTarihi.setText("");
		
		etIskontoPerc.setText("0.0");
		etMasrafPerc.setText("0.0");
		etKDVPerc.setText("0.0");
		etOTVPerc.setText("0.0");
			
		etAraToplam.setText("0.0");
		etIskonto.setText("0.0");
		etMasraf.setText("0.0");
		etKDV.setText("0.0");
		etOTV.setText("0.0");
		etYekun.setText("0.0");
		
		isEditing = editing;
		
	}
	
	private void fillSiparis(SiparisVO siparis, boolean alsoSiraNo) {
		
		if(alsoSiraNo) {
			etEvrakSiraNo.setText(siparis.getSip_evrakno_sira()+"");
			etEvrakSeriNo.setText(siparis.getSip_evrakno_seri());
		}
		etBelgeNo.setText(siparis.getSip_belgeno());
		etCHKodu.setText(siparis.getSip_musteri_kod());
		etCHIsmi.setText(siparis.getCari_unvan1());
		//etDoviz.setText(siparis.getSip_doviz_cinsi()+"");

		etProje.setText(siparis.getSip_projekodu());
		//SorumlulukMerkeziVO sormrkz = StaticDataBO.getInstance(getActivity()).getSorumlulukMerkezi(siparis.getSip_cari_sormerk());
		//etSorumluMerkezi.setText(sormrkz == null ? siparis.getSip_cari_sormerk() : sormrkz.getMsg_S_0870());
		etOdemePlani.setText(siparis.getSip_opno() + "");
		
		DepoVO dep = StaticDataBO.getInstance(getActivity()).getDepo(siparis.getSip_depono());
		etDepo.setText(dep == null ? "" : dep.getDep_adi());
		etSaticiKodu.setText(siparis.getSip_satici_kod());
		etTeslimTuru.setText(siparis.getSip_teslimturu());
		etTeslimTarihi.setText(siparis.getSip_teslim_tarih() == null ? "" : siparis.getSip_teslim_tarih().getFormattedDate());
			
		etEvrakTarih.setText(siparis.getSip_create_date().getFormattedDate());
		etBelgeTarih.setText(siparis.getSip_belge_tarih().getFormattedDate());
			
		etIskontoPerc.setText(siparis.getSip_isk1()+"");
		etMasrafPerc.setText(siparis.getSip_masraf1()+"");
		etKDVPerc.setText(siparis.getSip_vergi()+"");
		etOTVPerc.setText(siparis.getSip_Otv_Vergi()+"");
		
		
		etAdres.setText(siparis.getAdr_cadde() + ", " + siparis.getAdr_sokak() + ", " + siparis.getAdr_ilce() + ", " + siparis.getAdr_il());
		btnAdres.setTag(siparis.getSip_adresno());
	}
	
	private void fillStokUrunTable(int siparisFileId) {
		
		for (SiparisVO sprs : stokUrunList) {
			if(siparisFileId == sprs.getSip_fileid()) {
				addStokUrunRow(sprs);
			}
		}
		calculateTotals();
	}
	
	private int rowIndex = 0;
	private void addStokUrunRow(SiparisVO sprs) {
		
		TableRow tRow = generateStokUrunTableRow(sprs, rowIndex);
		tblSiparisStokUrun.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
		rowIndex++;
	}
	
	private void insertStokUrunRow(SiparisVO sprs, int rowind) {
		
		TableRow tRow = generateStokUrunTableRow(sprs, rowind);
		tblSiparisStokUrun.addView(tRow, rowind, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT));
		rowIndex++;
	}
	
	private void clearStokUrunTable(boolean alsoList) {
		if(alsoList)
			stokUrunList.clear();
		tblSiparisStokUrun.removeAllViews();
		rowIndex = 0;
	}
	
	private void removeStokUrunTableRow(int rIndex) {
		
		stokUrunList.remove(rIndex);
		
		tblSiparisStokUrun.removeViewAt(rIndex);
		rowIndex--;
		for(int i = 0; i < tblSiparisStokUrun.getChildCount(); i++){
		    TableRow tRow = (TableRow) tblSiparisStokUrun.getChildAt(i);
		    
		    int background = -1;
			if(i%2 == 0) {
				background = R.drawable.table_cell_cloud_bg;
			} else {
				background = R.drawable.table_cell_asbestos_bg;
			}
			
			tRow.setBackgroundResource(background);
			
			for(int j = 0; j < tRow.getChildCount(); j++){
				View vw = tRow.getChildAt(i);
				vw.setTag(i);
			}
		}
	}
	
	private TableRow generateStokUrunTableRow(SiparisVO sprs, int row) {
		
		TableRow tRow = new TableRow(getActivity());
		
		int background = -1;
		if(row%2 == 0) {
			background = R.drawable.table_cell_cloud_bg;
		} else {
			background = R.drawable.table_cell_asbestos_bg;
		}
		
		tRow.setBackgroundResource(background);
		
		ImageButton btnRemoveRow = new ImageButton(getActivity());
		btnRemoveRow.setImageResource(R.drawable.ic_delete);
		btnRemoveRow.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				Integer rIndex = (Integer) arg0.getTag();
				removeStokUrunTableRow(rIndex);
			}
		});
		btnRemoveRow.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
		
		Button btnKodu = new Button(getActivity());
		Button btnIsmi = new Button(getActivity());
		/*btnIsmi.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(final View arg0) {
				int rIndex = (Integer)arg0.getTag();
				showStokSearch(rIndex, false);
			}
		});*/
		NumberEditText etMiktar = new NumberEditText(getActivity());
		EditText etBr = new EditText(getActivity());
		PriceEditText etBrFiyat = new PriceEditText(getActivity());
		PriceEditText etTutari = new PriceEditText(getActivity());
		EditText etTeslimTarihi = new EditText(getActivity());
		NumberEditText etIskonto1 = new NumberEditText(getActivity()); etIskonto1.setEnabled(false);
		NumberEditText etIskonto2 = new NumberEditText(getActivity()); etIskonto2.setEnabled(false);
		NumberEditText etIskonto3 = new NumberEditText(getActivity()); etIskonto3.setEnabled(false);
		NumberEditText etIskonto4 = new NumberEditText(getActivity()); etIskonto4.setEnabled(false);
		EditText etAciklama = new EditText(getActivity());
		if(sprs != null) {
			btnKodu.setText(sprs.getSto_kod());
			btnIsmi.setText(sprs.getSto_isim());
			
			etMiktar.setText(NumberFormatUtil.formatPointedNumber(sprs.getSip_miktar()));
			etBr.setText(sprs.getSto_birim1_ad());
			etBrFiyat.setText(sprs.getSip_b_fiyat());
			etTutari.setText(sprs.getSip_tutar());
			etTeslimTarihi.setText(sprs.getSip_teslim_tarih() == null ? "" : sprs.getSip_teslim_tarih().getFormattedDate());
			etIskonto1.setText(sprs.getSip_isk1());
			etIskonto2.setText(sprs.getSip_isk2());
			etIskonto3.setText(sprs.getSip_isk3());
			etIskonto4.setText(sprs.getSip_isk4());
			etAciklama.setText(sprs.getSip_aciklama());
		} else {
			
			btnKodu.setText("[Stok Kodu]");
			btnIsmi.setText("[Stok İsmi]");
			
			etMiktar.setText(0f);
			etBr.setText("[Birim]");
			etBrFiyat.setText(0f);
			etTutari.setText(0f);
			etTeslimTarihi.setText("[Teslim Tarihi]");
			etIskonto1.setText(0);
			etIskonto2.setText(0);
			etIskonto3.setText(0);
			etIskonto4.setText(0);
			etAciklama.setText("[Aciklama]");
			
		}
		
		final EditText[] etIskontoList = new EditText[]{etIskonto1,etIskonto2,etIskonto3,etIskonto4};
		for (EditText editText : etIskontoList) {
			EditTextWatcher.addChangeListener(editText, new OnEditTextChangeListener() {
				
				@Override
				public void onTextChanged(EditText et, CharSequence arg0, int arg1,
						int arg2, int arg3) {
				}
				
				@Override
				public void beforeTextChanged(EditText et, CharSequence arg0, int arg1,
						int arg2, int arg3) {
				}
				
				@Override
				public void afterTextChanged(EditText et, Editable arg0) {
					
					if(arg0.toString().trim().length() == 0 || stokUrunList.size() == 0)
						return;
					
					int rIndex = (Integer)et.getTag();
					
					SiparisVO sprs = stokUrunList.get(rIndex);
					
					
					double tutar = sprs.getSip_miktar() * sprs.getSip_b_fiyat() * ((sprs.getSip_kar_orani()/100) + 1);
					double newTutar = tutar;
					
					for (int a=0; a<etIskontoList.length; a++) {
						EditText editText = etIskontoList[a];
						String strIsk = editText.getText().toString();
						double isk = Double.valueOf(strIsk) / 100;
						
						if(isk != 0) {
							newTutar = newTutar - (newTutar * isk);
						}
					}
					
					
					sprs.setSip_tutar(newTutar);
					
					TableRow tRow = (TableRow) tblSiparisStokUrun.getChildAt(rIndex);
					PriceEditText txtTutar = (PriceEditText) tRow.getChildAt(6);
					txtTutar.setText(newTutar);
					
					calculateTotals();
					
				}
			});
		}
		
		EditTextWatcher.addChangeListener(etMiktar, new OnEditTextChangeListener() {
			
			@Override
			public void onTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void beforeTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void afterTextChanged(EditText et, Editable arg0) {
				if(arg0.toString().trim().length() == 0 || stokUrunList.size() == 0)
					return;
				
				int rIndex = (Integer)et.getTag();
				
				SiparisVO sprs = stokUrunList.get(rIndex);
				
				double newMiktar = Double.valueOf(arg0.toString());
				double newTutar = newMiktar * sprs.getSip_b_fiyat() * ((sprs.getSip_kar_orani()/100) + 1);
				
				sprs.setSip_miktar(newMiktar);
				sprs.setSip_tutar(newTutar);
				
				TableRow tRow = (TableRow) tblSiparisStokUrun.getChildAt(rIndex);
				PriceEditText txtTutar = (PriceEditText) tRow.getChildAt(6);
				txtTutar.setText(newTutar);
				
				calculateTotals();
			}
		});
		
		EditTextWatcher.addChangeListener(etBrFiyat, new OnEditTextChangeListener() {
			
			@Override
			public void onTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void beforeTextChanged(EditText et, CharSequence arg0, int arg1,
					int arg2, int arg3) {
			}
			
			@Override
			public void afterTextChanged(EditText et, Editable arg0) {
				if(arg0.toString().trim().length() == 0 || stokUrunList.size() == 0)
					return;
				
				int rIndex = (Integer)et.getTag();
				
				SiparisVO sprs = stokUrunList.get(rIndex);
				
				double newBrFiyat = 0;
				try {
					newBrFiyat = Double.valueOf(arg0.toString());
				} catch (NumberFormatException e) {
					return;
				}
				double newTutar = sprs.getSip_miktar() * newBrFiyat * ((sprs.getSip_kar_orani()/100) + 1);
				
				sprs.setSip_b_fiyat(newBrFiyat);
				sprs.setSip_tutar(newTutar);
				
				TableRow tRow = (TableRow) tblSiparisStokUrun.getChildAt(rIndex);
				PriceEditText txtTutar = (PriceEditText) tRow.getChildAt(6);
				txtTutar.setText(newTutar);
				
				calculateTotals();
			}
		});

		if(!isEditing)
			btnRemoveRow.setTag(false);
		View[] vwList = new View[]{
				btnRemoveRow,
				btnKodu,
				btnIsmi,
				
				etMiktar,
				etBr,
				etBrFiyat,
				etTutari,
				etTeslimTarihi,
				etIskonto1,
				etIskonto2,
				etIskonto3,
				etIskonto4,
				etAciklama
		};
		
		int col = 0;
		for (View vw : vwList) {
			
			int weight = tableHeaderList[col].getWeight();
			
			int height = TableRow.LayoutParams.MATCH_PARENT;
			if(col == 0) {
				height = 45;
			}
			TableRow.LayoutParams params = new TableRow.LayoutParams(0, height,weight);
			params.column = col;
			vw.setLayoutParams(params);
			
			vw.setPadding(3, 3, 3, 3);
			
			if(col != 0) {
				vw.setBackgroundColor(getActivity().getResources().getColor(android.R.color.transparent));
				
				int[][] states = new int[][] {
					    new int[] { android.R.attr.state_enabled}, // enabled
					    new int[] {-android.R.attr.state_enabled}, // disabled
					    new int[] {-android.R.attr.state_checked}, // unchecked
					    new int[] { android.R.attr.state_pressed}  // pressed
					};

					int[] colors = new int[] {
					    Color.BLACK,
					    Color.BLACK,
					    Color.BLACK,
					    Color.BLACK
					};
					
					if(vw instanceof Button) {
						((Button)vw).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
						((Button)vw).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
						((Button)vw).setTextColor(new ColorStateList(states, colors));
					} else {
						((EditText)vw).setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
						((EditText)vw).setGravity(Gravity.RIGHT | Gravity.CENTER_VERTICAL);
						((EditText)vw).setTextColor(new ColorStateList(states, colors));
					}
			}

			vw.setEnabled(isEditing);
			int visibility = View.VISIBLE;
			if(vw.getTag() instanceof Boolean) {
				visibility = View.INVISIBLE;
			}
			vw.setTag(row);
			tRow.addView(vw);
			vw.setVisibility(visibility);
			
			col++;
		}
		etBr.setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
		etMiktar.setGravity(Gravity.CENTER);
		
		return tRow;
	}
	
	private void showStokSearch(final int rIndex, final boolean isNewStok) {
		
		if(etCHIsmi.getText().length() == 0 || etCHKodu.getText().length() == 0) {
			Toast.makeText(getActivity(), "Öncelikle bir cari seçmelisiniz.", Toast.LENGTH_LONG).show();
			return;
		}
		
		SearchDialog<StokSearchVO> dialog = new SearchDialog<>(getActivity(), "Stok Arama", SearchService.STOK, true, new String[] {
			"Stok İsmi",
			"stokIsmi",
			"Stok Kodu",
			"stokKodu"
		}, new String[] {
			"Stok Kodu",
			"Stok İsmi",
		}, new String[] {
			"msg_S_0870",
			"msg_S_0078"
		}, new OnResultRowSelected<StokSearchVO>() {
			
			@Override
			public void onSelected(StokSearchVO obj) {
				
				final ProgressDialog dia = ProgressDialog.show(getActivity(), "", 
		                "Yükleniyor. Bekleyiniz...", true);
				RestBO.getInstance(getActivity()).getService(GetService.class).getStok(obj.getMsg_S_0088(), new Callback<List<StokVO>>() {
					
					@Override
					public void success(List<StokVO> arg0, Response arg1) {
						if(arg0.size() == 0) {
							dia.dismiss();
							Toast.makeText(getActivity(), "Stok detayları bulunamadı.", Toast.LENGTH_LONG).show();
							return;
						}
						
						final StokVO stok = arg0.get(0);
						
						RestBO.getInstance(getActivity()).getService(GetService.class).getStokFiyat(stok.getSto_kod(), etCHKodu.getText().toString(), new Callback<StokFiyatVO>() {
							
							@Override
							public void success(StokFiyatVO arg0, Response arg1) {
								
								SiparisVO siparis = new SiparisVO();
								siparis.setSto_kod(stok.getSto_kod());
								siparis.setSto_isim(stok.getSto_isim());
								siparis.setSto_birim1_ad(stok.getSto_birim1_ad());
								siparis.setSip_vergi_pntr(stok.getSto_toptan_vergi());
								
								if(arg0 == null) {
									siparis.setSip_b_fiyat(0);
								} else {
									siparis.setSip_b_fiyat(arg0.getFiyat());
								}
								
								if(isNewStok) {
									stokUrunList.add(siparis);
									addStokUrunRow(siparis);
								} else {
									removeStokUrunTableRow(rIndex);
									stokUrunList.remove(rIndex);
									insertStokUrunRow(siparis, rIndex);
									
								}
								calculateTotals();
								dia.dismiss();
								
							}
							
							@Override
							public void failure(RetrofitError err) {
								Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
								err.printStackTrace();
								dia.dismiss();

							}
						});
						
						
					}
					
					@Override
					public void failure(RetrofitError err) {
						Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
						err.printStackTrace();
						dia.dismiss();
					}
				});
				
			}
		});
		dialog.show();
		
	}

	private void calculateTotals() {
		
		
		double araToplam = 0;
		double iskonto = 0;
		double iskontoPerc = 0;
		double masraf = 0;
		double masrafPerc = 0;
		double vergiPerc = 0;
		double vergi = 0;
		double yekun =0;
		double otv = 0;
		
		for (SiparisVO sprs : stokUrunList) {
			
			//araToplam += sprs.getSip_tutar();
			int vergiIndex = isSorumlulukTwo ? 0 : sprs.getSip_vergi_pntr();
			if(vergiIndex < TeklifVerFragment.VERGI.length) {
				vergiPerc += TeklifVerFragment.VERGI[vergiIndex];
				vergi +=  sprs.getSip_tutar() * TeklifVerFragment.VERGI[vergiIndex];
			}
			
			double originalTutar = sprs.getSip_miktar() * sprs.getSip_b_fiyat() * ((sprs.getSip_kar_orani()/100) + 1);
			
			araToplam += originalTutar;
			
			if(originalTutar != sprs.getSip_tutar()) {
				iskonto += (originalTutar - sprs.getSip_tutar());
			}
			
			
		}
		
		vergiPerc = ((double)vergiPerc / (double)stokUrunList.size())*100.0; 
		yekun += (vergi + (araToplam - iskonto));
		
		iskontoPerc = (iskonto*100)/araToplam;
		
		etIskontoPerc.setText(NumberFormatUtil.formatPointedNumber(iskontoPerc) + "");
		etKDVPerc.setText(NumberFormatUtil.formatPointedNumber(vergiPerc) + "");
		etMasrafPerc.setText(NumberFormatUtil.formatPointedNumber(masrafPerc) + "");
		etOTVPerc.setText("0");
		
		etAraToplam.setText(NumberFormatUtil.formatPrice(araToplam));
		etIskonto.setText(NumberFormatUtil.formatPrice(iskonto));
		etKDV.setText(NumberFormatUtil.formatPrice(vergi));
		etOTV.setText(NumberFormatUtil.formatPrice(otv));
		etYekun.setText(NumberFormatUtil.formatPrice(yekun));
		etMasraf.setText(NumberFormatUtil.formatPrice(masraf));
		
	}
	
	private void saveSiparis(final int tkl_satirno, final ProgressDialog dialog) {
		
		if(stokUrunList.size() <= tkl_satirno) {
			
			Toast.makeText(getActivity(), "Kaydedildi.", Toast.LENGTH_LONG).show();
			UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
			setEditing(true);
			dialog.dismiss();
			return;
		}

		String sip_tarih = getEditTextDateFormatted(etEvrakTarih);
		int sip_create_user = 100;
		String sip_satici_kod = etSaticiKodu.getText() + "";
		
		SiparisVO sp = stokUrunList.get(tkl_satirno);
		
		String sip_stok_kod = sp.getSto_kod();
		double sip_b_fiyat = sp.getSip_b_fiyat();
		double sip_miktar = sp.getSip_miktar();
		int sip_birim_pntr = 1;//sp.getSip_birim_pntr();
		int sip_vergi_pntr = isSorumlulukTwo ? 0 : sp.getSip_vergi_pntr();
		int sip_opno = Integer.parseInt(etOdemePlani.getText().toString());
		int sip_depono = (int) etDepo.getTag();
		String sip_stok_sormerk = etSorumluMerkezi.getText()+"";//sp.getSip_stok_sormerk();
		String sip_cari_sormerk = etSorumluMerkezi.getText()+"";
		double sip_alt_doviz_kuru = sp.getSip_alt_doviz_kuru();
		int sip_adresno = (int) btnAdres.getTag();
		
		RestBO.getInstance(getActivity()).getService(PostService.class).saveSiparis(
				tkl_satirno,
				sip_create_user, 
				sip_create_user, sip_tarih, 
				getEditTextDateFormatted(etTeslimTarihi), 
				etEvrakSeriNo.getText()+"", 
				Integer.valueOf(etEvrakSiraNo.getText()+""), 
				etBelgeNo.getText()+"", 
				getEditTextDateFormatted(etBelgeTarih), 
				sip_satici_kod, 
				etCHKodu.getText()+"", 
				sip_stok_kod, 
				sip_b_fiyat, 
				sip_miktar, 
				sip_birim_pntr, 
				sip_vergi_pntr, 
				sip_opno, sip_depono,
				sip_cari_sormerk, 
				sip_stok_sormerk, 
				sip_alt_doviz_kuru, 
				sip_adresno, new Callback<IdReturnVO>() {

					@Override
					public void failure(RetrofitError err) {
						
						Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
						err.printStackTrace();
						
						dialog.dismiss();
						UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
					}

					@Override
					public void success(IdReturnVO arg0, Response arg1) {
						
						
						if(arg0.getError() == 1) {
							Toast.makeText(getActivity(), "İşlem yapılırken servis ile ilgili bir hata oluştu.", Toast.LENGTH_LONG).show();
							dialog.dismiss();
							UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
						} else if(arg0.getId() == 0) {
							
							new AlertDialog.Builder(getActivity())
							.setTitle("Hünkar Gıda")
							.setMessage("Bu seri-sıra numaralı sipariş zaten kayıtlı. Sıradaki seri numarası ile kaydetmek ister misiniz?")
							.setNegativeButton("Hayır", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface dialog, int which) {
									etEvrakSiraNo.requestFocus();
								}
							})
							.setPositiveButton("Tamam", new DialogInterface.OnClickListener() {
								
								@Override
								public void onClick(DialogInterface arg0, int arg1) {
									updateSiraNo(true);
								}
							})
							.setOnCancelListener(new DialogInterface.OnCancelListener() {
								
								@Override
								public void onCancel(DialogInterface arg0) {
									etEvrakSiraNo.requestFocus();
								}
							}).show();
							
							dialog.dismiss();
							UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
							
							dialog.dismiss();
							UIUtil.hideKeyboard(getActivity(), etEvrakSeriNo);
						} else {
							
							saveSiparis(tkl_satirno+1, dialog);
							
						}
						
					}
				});
		
	}
	
}