package com.biftekyazilim.hunkargida.fragment;

import java.text.DecimalFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Currency;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.MainActivity;
import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;
import com.biftekyazilim.hunkargida.util.ReflectionUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.vo.CariHesapVO;
import com.biftekyazilim.hunkargida.vo.CariStokVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;

public class CariStokFragment extends ArgumentableFragment {

	private Button btnBack;
	private TableRow headerRow;
	private TableLayout tblContent;
	private TableHeaderVO[] tableHeaderList;
	private List<CariStokVO> cariStokList;
	
	DecimalFormat fmt = new DecimalFormat();
	public CariStokFragment() {
		fmt.setCurrency(Currency.getInstance("TRY"));
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		getActivity().setTitle("Cari Stok Ekstresi");
		View rootView = inflater.inflate(R.layout.fragment_carihesap,
				container, false);

		btnBack = (Button) rootView.findViewById(R.id.btnBack);
		tblContent = (TableLayout) rootView.findViewById(R.id.tblContent);

		headerRow = (TableRow) rootView.findViewById(R.id.headerRow);

		tableHeaderList = new TableHeaderVO[] { new TableHeaderVO("Tarih", 62, 12),
				new TableHeaderVO("Stok Kodu","getStok_kodu", 93, 12),
				new TableHeaderVO("Stok İsmi","getStok_adi", 194, 12),
				new TableHeaderVO("Evrak Tipi","getEvrak_tipi", 93, 12),
				new TableHeaderVO("Evrak No","seri_sira", 62, 12),
				new TableHeaderVO("Fatura No","fatseri_sira", 62, 12),
				new TableHeaderVO("Tipi","tipi", 62, 12),
				new TableHeaderVO("Cinsi","getHareket_cinsi", 62, 12),
				new TableHeaderVO("Brüt Fiyat","getAna_doviz_brut_birim_fiyati", 62, 12),
				new TableHeaderVO("Net Fiyat","getAna_doviz_net_birim_fiyati", 62, 12),
				new TableHeaderVO("Giren",null, 62, 12),
				new TableHeaderVO("Çıkan","getCikan_miktar", 62, 12),
				new TableHeaderVO("İade Normal","ni", 62, 12) };
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList, new OnClickListener() {
			
			@Override
			public void onClick(View vw) {
				if(cariStokList == null || cariStokList.size() == 0) {
					return;
				}
				int col = (int)vw.getTag();
				if(col == 1) {
					return;
				}
				final boolean desc = col < 0 ? true : false;
				vw.setTag(-col);
				col = Math.abs(col)-1;
				
				TableHeaderVO th = tableHeaderList[col];
				final String propertyName = th.getObjectPropertyName();
				if(propertyName == null)
					return;
				Collections.sort(cariStokList, new Comparator<CariStokVO>() {

					@Override
					public int compare(CariStokVO arg0, CariStokVO arg1) {
						
						String val1 = ReflectionUtil.getValue(propertyName, arg0) + "";
						String val2 = ReflectionUtil.getValue(propertyName, arg1) + "";
						return (desc ? -1 : 1) * val1.compareTo(val2);
					}
				});
				fillTable();
			}
		});

		initHand();

		String cariKod = getArguments().getString("cariKod");
		loadCariHesap(cariKod);
		
		return rootView;
	}
	
	private void initHand() {

		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MainActivity activity = (MainActivity) getActivity();

				activity.popBackFragment();
			}
		});
	}

	private void loadCariHesap(String cariKod) {
		final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
				"Yükleniyor. Bekleyiniz...", true);
		RestBO.getInstance(getActivity()).getService(GetService.class).getCariStokHareket(cariKod, new Callback<List<CariStokVO>>() {
			
			@Override
			public void success(List<CariStokVO> result, Response arg1) {
				
				cariStokList = result;
				
				fillTable();
				dialog.dismiss();
			}
			
			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(getActivity(), err.getMessage(),
						Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}
		});
	}
	
	private void fillTable() {
		tblContent.removeAllViews();
		if(cariStokList.size() >0) {
			
			double cikanToplam = 0.0;
			double girenToplam = 0.0;
			
			for (CariStokVO cariStok : cariStokList) {
				
				TableRow tRow = new TableRow(getActivity());
				tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
				tRow.setMinimumHeight(ROW_HEIGHT);
				
				TextView tvTarih = new TextView(getActivity());
				TextView tvStokKodu = new TextView(getActivity());
				TextView tvStokIsmi = new TextView(getActivity());
				TextView tvEvrakTipi = new TextView(getActivity());
				TextView tvEvrakNo = new TextView(getActivity());
				TextView tvFaturaNo = new TextView(getActivity());
				TextView tvTipi = new TextView(getActivity());
				TextView tvCinsi = new TextView(getActivity());
				TextView tvBrutFiyat = new TextView(getActivity());
				TextView tvNetFiyat = new TextView(getActivity());
				TextView tvGiren = new TextView(getActivity());
				TextView tvCikan = new TextView(getActivity());
				TextView tvIadeNormal = new TextView(getActivity());
				

				tvBrutFiyat.setGravity(Gravity.RIGHT);
				tvNetFiyat.setGravity(Gravity.RIGHT);
				tvGiren.setGravity(Gravity.RIGHT);
				tvCikan.setGravity(Gravity.RIGHT);
				
				TextView[] tvList = new TextView[]{
						tvTarih,
						tvStokKodu,
						tvStokIsmi,
						tvEvrakTipi,
						tvEvrakNo,
						tvFaturaNo,
						tvTipi,
						tvCinsi,
						tvBrutFiyat,
						tvNetFiyat,
						tvGiren,
						tvCikan,
						tvIadeNormal
				};
				
				
				int col = 0;
				for (TextView textView : tvList) {
					
					int weight = tableHeaderList[col].getWeight();
					textView.setBackgroundResource(R.drawable.table_cell_white_bg);
					/*if(rowCounter%2 == 0) {
						textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
					} else {
						textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
					}*/
					textView.setPadding(5, 5, 5, 5);
					textView.setTextColor(Color.BLACK);
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
					TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,weight);
					params.column = col;
					textView.setLayoutParams(params);
					col++;

					tRow.addView(textView);
					
				}
				
				tvTarih.setText(cariStok.getBelge_tarihi().getFormattedDate());
				tvStokKodu.setText(cariStok.getStok_kodu());
				tvStokIsmi.setText(cariStok.getStok_adi());
				tvEvrakTipi.setText(cariStok.getEvrak_tipi());
				tvEvrakNo.setText(cariStok.getSeri() + "-" + cariStok.getSira_no());
				tvFaturaNo.setText(cariStok.getFatseri() + "-" + cariStok.getFatsira_no());
				tvTipi.setText(cariStok.getTipi());
				tvCinsi.setText(cariStok.getHareket_cinsi());
				tvBrutFiyat.setText(NumberFormatUtil.formatPrice(cariStok.getAna_doviz_brut_birim_fiyati()));
				tvNetFiyat.setText(NumberFormatUtil.formatPrice(cariStok.getAna_doviz_net_birim_fiyati()));
				tvGiren.setText("");
				tvCikan.setText(NumberFormatUtil.formatPrice(cariStok.getCikan_miktar()));
				tvIadeNormal.setText(cariStok.getNi());
				//tvTLAlacakBakiye.setText("? TL");
				
				cikanToplam += cariStok.getCikan_miktar();
				
				tblContent.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
			}
			
			
			double[] toplamList = new double[]{
				girenToplam,
				cikanToplam
			};
			TableRow totalRow = new TableRow(getActivity());
			TextView[] toplamTvList = new TextView[]{
					new TextView(getActivity()), //Giren Toplam
					new TextView(getActivity()), //Çıkan Toplam
					new TextView(getActivity()) //temp
			};
			
			int toplamCol = 10;
			for (int i = 0; i < toplamTvList.length; i++) {
				TextView tv = toplamTvList[i];
				
				tv.setGravity(Gravity.RIGHT);
				tv.setPadding(5, 5, 5, 5);
				tv.setTextColor(Color.BLACK);
				
				if(i < toplamTvList.length - 1)
					tv.setText(NumberFormatUtil.formatPrice(toplamList[i]));
				
				
				TableRow.LayoutParams params = new TableRow.LayoutParams(0, 25,tableHeaderList[toplamCol].getWeight());
				params.column = toplamCol;
				if(i == 0) {
					for (int j = 0; j < 10; j++) {
						params.weight += tableHeaderList[j].getWeight();
					}
				}
				tv.setLayoutParams(params);
				totalRow.addView(tv);
				
				toplamCol++;
			}
			totalRow.setPadding(0, 15, 0, 15);
			tblContent.addView(totalRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
			
		}
	}
	
	
}