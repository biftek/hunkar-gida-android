package com.biftekyazilim.hunkargida.fragment;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.MainActivity;
import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.NumberFormatUtil;
import com.biftekyazilim.hunkargida.util.ReflectionUtil;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.vo.CariHesapVO;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;

public class CariHesapFragment extends ArgumentableFragment {

	private Button btnBack;
	private TextView tvBaslik;
	private TableRow headerRow;
	private TableLayout tblContent;
    private TableHeaderVO[] tableHeaderList;

	private List<CariHesapVO> cariHesapList;
	
	public CariHesapFragment() {
		
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_carihesap,
				container, false);

		getActivity().setTitle("Cari Hesap Ekstresi");
		
		btnBack = (Button) rootView.findViewById(R.id.btnBack);
		tblContent = (TableLayout) rootView.findViewById(R.id.tblContent);

		headerRow = (TableRow) rootView.findViewById(R.id.headerRow);
		tvBaslik = (TextView) rootView.findViewById(R.id.tvBaslik);

		tableHeaderList = new TableHeaderVO[] { new TableHeaderVO("Tarih","belge_tarihi", 100, 12),
				new TableHeaderVO("Evrak No","seri_sira", 100, 12),
				new TableHeaderVO("Evrak Cinsi","evrak_tipi", 300, 12),
				new TableHeaderVO("Vade","vade", 100, 12),
				new TableHeaderVO("TL Borç","ana_doviz_borc", 100, 12),
				new TableHeaderVO("TL Alacak","ana_doviz_alacak", 100, 12),
				new TableHeaderVO("TL Borç Bakiye","ana_doviz_borc_bakiye", 100, 12),
				new TableHeaderVO("TL Alacak Bakiye","ana_doviz_alacak_bakiye", 100, 12) };
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList, new OnClickListener() {
			
			@Override
			public void onClick(View vw) {
				if(cariHesapList == null || cariHesapList.size() == 0) {
					return;
				}
				int col = (int)vw.getTag();
				if(col == 1) {
					return;
				}
				final boolean desc = col < 0 ? true : false;
				vw.setTag(-col);
				col = Math.abs(col)-1;
				
				TableHeaderVO th = tableHeaderList[col];
				final String propertyName = th.getObjectPropertyName();
				Collections.sort(cariHesapList, new Comparator<CariHesapVO>() {

					@Override
					public int compare(CariHesapVO arg0, CariHesapVO arg1) {
						
						String val1 = ReflectionUtil.getValue(propertyName, arg0) + "";
						String val2 = ReflectionUtil.getValue(propertyName, arg1) + "";
						return (desc ? -1 : 1) * val1.compareTo(val2);
					}
				});
				
				fillTable();
			}
		});

		initHand();

		String cariKod = getArguments().getString("cariKod");
		loadCariHesap(cariKod);
		
		return rootView;
	}

	private void initHand() {

		btnBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				MainActivity activity = (MainActivity) getActivity();

				activity.popBackFragment();
			}
		});
	}

	private void loadCariHesap(String cariKod) {
		final ProgressDialog dialog = ProgressDialog.show(getActivity(), "",
				"Yükleniyor. Bekleyiniz...", true);
		RestBO.getInstance(getActivity()).getService(GetService.class).getCariHesapHareket(cariKod, new Callback<List<CariHesapVO>>() {
			
			@Override
			public void success(List<CariHesapVO> result, Response arg1) {
				
				
				cariHesapList = result;
				fillTable();
				dialog.dismiss();
			}
			
			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(getActivity(), err.getMessage(),
						Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}
		});
	}
	
	private void fillTable() {
		tblContent.removeAllViews();
		if(cariHesapList.size() >0) {
			
			if(cariHesapList.size() >1) {
				CariHesapVO ch = cariHesapList.get(1);
				tvBaslik.setText(ch.getCari_ismi() + " (Grubu: " + ch.getGrubu() + ")");
			}
			
			double borcToplam = 0.0;
			double alacakToplam = 0.0;
			double borcBakiyeToplam = 0.0;
			double alacakBakiyeToplam = 0.0;
			
			for (CariHesapVO cariHesap : cariHesapList) {
				
				TableRow tRow = new TableRow(getActivity());
				tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
				tRow.setMinimumHeight(ROW_HEIGHT);
				
				TextView tvTarih = new TextView(getActivity());
				TextView tvEvrakNo = new TextView(getActivity());
				TextView tvEvrakCinsi = new TextView(getActivity());
				TextView tvVade = new TextView(getActivity());
				TextView tvTLBorc = new TextView(getActivity());
				TextView tvTLAlacak = new TextView(getActivity());
				TextView tvTLBorcBakiye = new TextView(getActivity());
				TextView tvTLAlacakBakiye = new TextView(getActivity());
				

				tvTarih.setGravity(Gravity.CENTER);
				tvVade.setGravity(Gravity.CENTER);
				
				tvEvrakNo.setGravity(Gravity.LEFT);
				tvEvrakCinsi.setGravity(Gravity.LEFT);
				
				tvTLBorc.setGravity(Gravity.RIGHT);
				tvTLAlacak.setGravity(Gravity.RIGHT);
				tvTLBorcBakiye.setGravity(Gravity.RIGHT);
				tvTLAlacakBakiye.setGravity(Gravity.RIGHT);
				
				TextView[] tvList = new TextView[]{
						tvTarih,
						tvEvrakNo,
						tvEvrakCinsi,
						tvVade,
						tvTLBorc,
						tvTLAlacak,
						tvTLBorcBakiye,
						tvTLAlacakBakiye
				};
				
				
				int col = 0;
				for (TextView textView : tvList) {
					
					int weight = tableHeaderList[col].getWeight();
					textView.setBackgroundResource(R.drawable.table_cell_white_bg);
					/*if(rowCounter%2 == 0) {
						textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
					} else {
						textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
					}*/
					textView.setPadding(5, 5, 5, 5);
					textView.setTextColor(Color.BLACK);
					textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 12);
					TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,weight);
					params.column = col;
					textView.setLayoutParams(params);
					col++;

					tRow.addView(textView);
					
				}
				
				
				tvTarih.setText(cariHesap.getBelge_tarihi().getFormattedDate());
				if(cariHesap.getKayit_no() != 0) {
					tvEvrakNo.setText(cariHesap.getSeri() + "-" + cariHesap.getSira());
					tvEvrakCinsi.setText(cariHesap.getEvrak_tipi());
					
					if(cariHesap.getSeri().equals("HNK")) {
						tvVade.setText(cariHesap.getVade_gun() + " Gün");
					} else {
						tvVade.setText(cariHesap.getVade_tarih() == null ? "" : cariHesap.getVade_tarih().getFormattedDate());
					}
					
				}
				
				tvTLBorc.setText(NumberFormatUtil.formatPrice(cariHesap.getAna_doviz_borc()) );
				tvTLAlacak.setText(NumberFormatUtil.formatPrice(cariHesap.getAna_doviz_alacak()));
				tvTLBorcBakiye.setText(NumberFormatUtil.formatPrice(cariHesap.getAna_doviz_borc_bakiye()));
				tvTLAlacakBakiye.setText(NumberFormatUtil.formatPrice(cariHesap.getAna_doviz_alacak_bakiye()));
				
				borcToplam += cariHesap.getAna_doviz_borc();
				alacakToplam += cariHesap.getAna_doviz_alacak();
				borcBakiyeToplam += cariHesap.getAna_doviz_borc_bakiye();
				alacakBakiyeToplam += cariHesap.getAna_doviz_alacak_bakiye();
				
				//tvTLAlacakBakiye.setText("? TL");
				
				tblContent.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
			}
			
			
			double[] toplamList = new double[]{
				borcToplam,
				alacakToplam,
				borcBakiyeToplam,
				alacakBakiyeToplam
			};
			TableRow totalRow = new TableRow(getActivity());
			TextView[] toplamTvList = new TextView[]{
					new TextView(getActivity()), //TL Borç Toplam
					new TextView(getActivity()), //TL Alacak Toplam
					new TextView(getActivity()), //TL Borç Bakiye Toplam
					new TextView(getActivity()) //TL Alacak Bakiye Toplam
			};
			
			int toplamCol = 4;
			for (int i = 0; i < toplamTvList.length; i++) {
				TextView tv = toplamTvList[i];
				
				tv.setGravity(Gravity.RIGHT);
				tv.setPadding(5, 5, 5, 5);
				tv.setTextColor(Color.BLACK);
				
				tv.setText(NumberFormatUtil.formatPrice(toplamList[i]));
				TableRow.LayoutParams params = new TableRow.LayoutParams(0, 25,tableHeaderList[toplamCol].getWeight());
				params.column = toplamCol;
				if(i == 0) {
					for (int j = 0; j < 4; j++) {
						params.weight += tableHeaderList[j].getWeight();
					}
				}
				tv.setLayoutParams(params);
				totalRow.addView(tv);
				
				toplamCol++;
			}
			totalRow.setPadding(0, 15, 0, 15);
			tblContent.addView(totalRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
			
		}
	}
}