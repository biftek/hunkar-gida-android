package com.biftekyazilim.hunkargida.fragment;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.util.TableLayoutUtil;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;
import com.biftekyazilim.hunkargida.vo.TeklifSummaryVO;

public class TeklifListesiFragment extends ArgumentableFragment {
    
	private TableLayout tblTeklifListContent;
    public TeklifListesiFragment(){}
    private TableHeaderVO[] tableHeaderList;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
  
        View rootView = inflater.inflate(R.layout.fragment_tekliflistesi, container, false);

		tblTeklifListContent = (TableLayout) rootView.findViewById(R.id.tblTeklifListContent);
		TableRow headerRow = (TableRow)rootView.findViewById(R.id.headerRow);
		
		tableHeaderList = new TableHeaderVO[] {
				new TableHeaderVO("EVRAK TARİHİ", 90),
				new TableHeaderVO("EVRAK SERİ NO", 90),
				new TableHeaderVO("EVRAK SIRA NO", 90),
				new TableHeaderVO("CARİ KODU", 100),
				new TableHeaderVO("BAŞLANGIÇ TARİHİ", 115),
				new TableHeaderVO("BİTİŞ SÜRESİ", 90),
				new TableHeaderVO("TUTAR", 70),
				new TableHeaderVO("MİKTAR", 70),
				new TableHeaderVO("TAMAMLANAN MİKTAR", 130),
				new TableHeaderVO("KALAN MİKTAR", 90),
				new TableHeaderVO("DURUM", 65) };
		
		TableLayoutUtil.setHeader(getActivity(), headerRow, tableHeaderList);
		
		fillTable();
		
        return rootView;
    }
    
    private void fillTable() {
    	
    	tblTeklifListContent.removeAllViews();
    	final ProgressDialog dialog = ProgressDialog.show(getActivity(), "", 
                "Yükleniyor. Bekleyiniz...", true);
		RestBO.getInstance(getActivity())
		.getService(GetService.class)
		.getTeklifList(new Callback<List<TeklifSummaryVO>>() {
			
			@Override
			public void success(List<TeklifSummaryVO> result, Response arg1) {
				
				int rowCounter = 0;
				for (TeklifSummaryVO teklifSummary : result) {
					
					TableRow tRow = new TableRow(getActivity());
					tRow.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));
					tRow.setMinimumHeight(ROW_HEIGHT);
					
					TextView tvEvrakTarihi = new TextView(getActivity());
					TextView tvEvrakSeriNo = new TextView(getActivity());
					TextView tvEvrakSiraNo = new TextView(getActivity());
					TextView tvCariKodu = new TextView(getActivity());
					TextView tvBaslangicTarihi = new TextView(getActivity());
					TextView tvBitisSuresi = new TextView(getActivity());
					TextView tvTutar = new TextView(getActivity());
					TextView tvMiktar = new TextView(getActivity());
					TextView tvTamamMiktar = new TextView(getActivity());
					TextView tvKalanMiktar = new TextView(getActivity());
					TextView tvDurum = new TextView(getActivity());
					
					TextView[] tvList = new TextView[]{
							tvEvrakTarihi,
							tvEvrakSeriNo,
							tvEvrakSiraNo,
							tvCariKodu,
							tvBaslangicTarihi,
							tvBitisSuresi,
							tvTutar,
							tvMiktar,
							tvTamamMiktar,
							tvKalanMiktar,
							tvDurum
					};
					
					int col = 0;
					for (TextView textView : tvList) {
						
						int weight = tableHeaderList[col].getWeight();
						if(rowCounter%2 == 0) {
							textView.setBackgroundResource(R.drawable.table_cell_cloud_bg);
						} else {
							textView.setBackgroundResource(R.drawable.table_cell_asbestos_bg);
						}
						textView.setGravity(Gravity.CENTER);
						textView.setTextColor(Color.BLACK);
						
						TableRow.LayoutParams params = new TableRow.LayoutParams(0,  TableRow.LayoutParams.MATCH_PARENT,weight);
						params.column = col;
						textView.setLayoutParams(params);
						col++;

						tRow.addView(textView);
					}
					
					rowCounter++;
					
					tvEvrakTarihi.setText(teklifSummary.getMsg_S_0263().getFormattedDate());
					tvEvrakSeriNo.setText(teklifSummary.getMsg_S_0960());
					tvEvrakSiraNo.setText(teklifSummary.getMsg_S_0213() + "");
					tvCariKodu.setText(teklifSummary.getMsg_S_0200());
					tvBaslangicTarihi.setText(teklifSummary.getMsg_S_1086().getFormattedDate());
					tvBitisSuresi.setText(teklifSummary.getMsg_S_1087().getFormattedDate());
					tvTutar.setText(teklifSummary.getMsg_S_0293()+"");
					tvMiktar.setText(teklifSummary.getMsg_S_0165() + "");
					tvTamamMiktar.setText(teklifSummary.getMsg_S_0456() + "");
					tvKalanMiktar.setText(teklifSummary.getMsg_S_0247() + "");
					tvDurum.setText(teklifSummary.getMsg_S_0262());
					
					tblTeklifListContent.addView(tRow, new TableLayout.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT));
				}
				
				dialog.dismiss();
			}
			
			@Override
			public void failure(RetrofitError err) {
				Toast.makeText(getActivity(), err.getMessage(), Toast.LENGTH_LONG).show();
				err.printStackTrace();
				dialog.dismiss();
			}
		});
	}
}