package com.biftekyazilim.hunkargida.listener;

import android.text.Editable;
import android.widget.EditText;

public interface OnEditTextChangeListener {

	public void onTextChanged(EditText et, CharSequence arg0, int arg1, int arg2, int arg3);
	public void beforeTextChanged(EditText et, CharSequence arg0, int arg1, int arg2,int arg3);
	public void afterTextChanged(EditText et, Editable arg0);
	
}
