package com.biftekyazilim.hunkargida.listener;

import java.util.Calendar;
import java.util.Locale;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.DatePicker;
import android.widget.EditText;

public class OnDateEditTextFocusListener implements OnFocusChangeListener {

	private static Calendar calendar = Calendar.getInstance(new Locale("tr"));
	private Context ctx;
	
	public OnDateEditTextFocusListener(Context ctx) {
		this.ctx = ctx;
	}
	
	@Override
	public void onFocusChange(final View editText, boolean hasFocus) {
		if(hasFocus) {
			int year = calendar.get(Calendar.YEAR);
			int month = calendar.get(Calendar.MONTH);
			int day = calendar.get(Calendar.DAY_OF_MONTH);
			DatePickerDialog datePicker = new DatePickerDialog(ctx, new OnDateSetListener() {
				
				@Override
				public void onDateSet(DatePicker dp, int selectedYear, int selectedMonth, int selectedDay) {
					EditText et = (EditText)editText;
					selectedMonth = selectedMonth == 13 ? 1 : selectedMonth + 1;
					et.setText((selectedDay < 10 ? ("0" + selectedDay) : selectedDay) + "/" + (selectedMonth < 10 ? ("0" + selectedMonth) : selectedMonth) + "/" + selectedYear);
					et.clearFocus();
				}
			}, year, month, day);
			datePicker.setOnCancelListener(new OnCancelListener() {
				
				@Override
				public void onCancel(DialogInterface arg0) {
					editText.clearFocus();
				}
			});
			datePicker.show();
		}
	}

}
