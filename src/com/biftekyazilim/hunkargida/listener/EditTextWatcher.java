package com.biftekyazilim.hunkargida.listener;

import com.biftekyazilim.hunkargida.vo.TeklifVO;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;

public class EditTextWatcher {
	
	public static void addChangeListener(EditText editText, OnEditTextChangeListener changeListener) {
		new EditTextWatcher(editText, changeListener);
	}
	
	private EditText et;
	private OnEditTextChangeListener listener;
	private EditTextWatcher(EditText editText, OnEditTextChangeListener changeListener) {
		this.et = editText;
		
		TextWatcher textWatcher = new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
				listener.onTextChanged(et, arg0, arg1, arg2, arg3);
			}
			
			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
				listener.beforeTextChanged(et, arg0, arg1, arg2, arg3);
			}
			
			@Override
			public void afterTextChanged(Editable arg0) {
				listener.afterTextChanged(et, arg0);
			}
		};
		this.et.addTextChangedListener(textWatcher);
		this.listener = changeListener;
	}

	
	
}
