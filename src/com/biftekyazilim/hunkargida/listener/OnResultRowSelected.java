package com.biftekyazilim.hunkargida.listener;

public interface OnResultRowSelected<T> {
	
	public void onSelected(T obj);
	
}
