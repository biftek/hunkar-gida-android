package com.biftekyazilim.hunkargida.listener;

import com.biftekyazilim.hunkargida.util.NumberFormatUtil;

import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;

public class OnPointedNumberEditTextFocusListener implements OnFocusChangeListener {

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
		EditText txt = (EditText)v;
		if(hasFocus) {
			String str = txt.getText().toString().trim().replace(",", "");
			if(str.equals("0") || str.equals("0.0")) {
				txt.setText("");
			} else {
				txt.setText(str);
			}
		} else {
			String str = txt.getText().toString().trim().replace(",", "");
			if(str.length() == 0) {
				txt.setText(NumberFormatUtil.formatPointedNumber(0));
			} else {
				txt.setText(NumberFormatUtil.formatPointedNumber(Double.valueOf(str)));
			}
		}
	}

}
