package com.biftekyazilim.hunkargida.bo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.biftekyazilim.hunkargida.vo.SorumlulukMerkeziVO;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class SessionBO {

	private static SessionBO instance;

	public static SessionBO getInstance(Context context) {
		if (instance == null) {
			instance = new SessionBO(context);
		} else if (!instance.context.equals(context)) {
			instance.context = context;
		}
		return instance;
	}

	// Shared Preferences
	private SharedPreferences pref;

	// Editor for Shared preferences
	private Editor editor;

	// Context
	private Context context;

	// Shared pref mode
	private int PRIVATE_MODE = 0;
	
	private Gson gson;

	// Sharedpref file name
	private static final String PREF_NAME = "HunkarGidaMikroTablet";

	private SessionBO() {

	}

	private SessionBO(Context context) {
		this.context = context;

		pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		gson = new Gson();
	}


	public void saveObject(String key, Object obj) {
		String json = gson.toJson(obj);
		editor.putString(key, json);
		editor.commit();
	}

	
	public List<?> getObject(String key, Type listType) {
	    String json = pref.getString(key, "");
	    List<?> obj = gson.fromJson(json, listType);
	    return obj;
	}
	
	public <T> T getObject(String key, Class<T> objClass) {
	    String json = pref.getString(key, "");
	    T obj = gson.fromJson(json, objClass);
	    return obj;
	}
	
	public void delete(String key) {
		editor.remove(key);
		editor.commit();
	}
	
	public void clear() {
		editor.clear();
		editor.commit();
	}
	
	public void saveBoolean(String key, boolean val) {
		editor.putBoolean(key, val);
		editor.commit();
	}
	
	public void saveInt(String key, int val) {
		editor.putInt(key, val);
		editor.commit();
	}
	
	public void saveString(String key, String val) {
		editor.putString(key, val);
		editor.commit();
	}
	
	public boolean getBoolean(String key) {
		return pref.getBoolean(key, false);
	}
	
	public int getInt(String key) {
		return pref.getInt(key, -1);
	}
	
	public String getString(String key) {
		return pref.getString(key, "");
	}
	
	
}
