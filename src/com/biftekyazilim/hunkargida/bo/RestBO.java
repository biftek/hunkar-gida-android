package com.biftekyazilim.hunkargida.bo;

import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit.Endpoint;
import retrofit.ErrorHandler;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import android.content.Context;

import com.biftekyazilim.hunkargida.vo.UserVO;
import com.squareup.okhttp.OkHttpClient;

public class RestBO {

	private static RestBO instance;
	private RestAdapter restAdapter;
	private Map<Class<?>, Object> serviceMap;
	private Context context;
	private ExecutorService executorService;
	
	public static RestBO getInstance(Context context) {
		if (instance == null) {
			instance = new RestBO(context);
		} else if (!instance.context.equals(context)) {
			instance.context = context;
		}
		return instance;
	}

	private RestBO() {
		// TODO Auto-generated constructor stub
	}
	private RestBO(Context context) {
		this.context = context;
		serviceMap = new HashMap<Class<?>, Object>();
		
		RequestInterceptor requestInterceptor = new RequestInterceptor()
		{
		    @Override
		    public void intercept(RequestFacade request) {
		    	LoginBO loginBO = LoginBO.getInstance(RestBO.this.context);
		    	if(loginBO.isLoggedIn()) {
		    		UserVO user = loginBO.getLoggedInUser();
		    		if(user != null) {
		    			request.addQueryParam("username", user.getUsername());
				        request.addQueryParam("password", user.getPassword());
		    		}
		    	}
		    }
		};
		
		executorService = Executors.newCachedThreadPool();
		
		OkHttpClient client = new OkHttpClient();
		
		restAdapter = new RestAdapter.Builder().setEndpoint(new Endpoint() {
			
			@Override
			public String getUrl() {
				return "http://78.186.185.146/hunkar/api/index";
			}
			
			@Override
			public String getName() {
				return "Hünkar Gıda - Mikro Muhasebe Yazılımı Web Servisi";
			}
		})
		.setRequestInterceptor(requestInterceptor)
		.setLogLevel(LogLevel.FULL)
		.setErrorHandler(new ErrorHandler() {
			
			@Override
			public Throwable handleError(RetrofitError arg0) {
				if (arg0.isNetworkError()) {
		            if (arg0.getCause() instanceof SocketTimeoutException) {
		                return new Exception("İnternet bağlantınız bulunmamaktadır.");
		            } else {
		            	return new Exception("İnternet bağlantınız bulunmamaktadır.");
		            }
		        }
				return new Exception("Servislerde belirlenemeyen bir hata oluştu.");
			}
		})
		.setClient(new OkClient(client))
		//.setExecutors(executorService, new MainThreadExecutor())
		.build();
		
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getService(Class<T> serviceClass) {
		
		T service = null;
		if(serviceMap.containsKey(serviceClass)) {
			service = (T) serviceMap.get(serviceClass);
		} else {
			service = (T) restAdapter.create(serviceClass);
			serviceMap.put(serviceClass, service);
		}
		
		return (T) service;
	}
	
	public void stopAllRequests() {
		executorService.shutdownNow();
	}
}
