package com.biftekyazilim.hunkargida.bo;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.ProgressDialog;
import android.content.Context;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.service.GetService;
import com.biftekyazilim.hunkargida.vo.DepoVO;
import com.biftekyazilim.hunkargida.vo.SorumlulukMerkeziVO;
import com.google.gson.reflect.TypeToken;

public class StaticDataBO {

	private static final String KEY_SORUMLULUKMERKEZLERI = "SORUMLULUKMERKEZI";
	private static final String KEY_DEPOLISTESI = "DEPOLISTESI";

	private List<SorumlulukMerkeziVO> sorumlulukMerkeziList;
	private List<DepoVO> depoList;

	// Context
	private Context context;
	private static StaticDataBO instance;

	public static StaticDataBO getInstance(Context context) {
		if (instance == null) {
			instance = new StaticDataBO(context);
		} else if (!instance.context.equals(context)) {
			instance.context = context;
		}
		return instance;
	}

	private StaticDataBO() {
		// TODO Auto-generated constructor stub
	}

	private StaticDataBO(Context context) {
		this.context = context;
	}

	public void saveSorumlulukMerkeziList(
			List<SorumlulukMerkeziVO> sorumlulukMerkeziList) {
		SessionBO.getInstance(context).saveObject(KEY_SORUMLULUKMERKEZLERI,
				sorumlulukMerkeziList);
	}

	@SuppressWarnings("unchecked")
	public List<SorumlulukMerkeziVO> getSorumlulukMerkeziList() {
		if (sorumlulukMerkeziList == null) {
			
			Type listType = new TypeToken<ArrayList<SorumlulukMerkeziVO>>() {
		    }.getType();
		    
			sorumlulukMerkeziList = (List<SorumlulukMerkeziVO>) SessionBO.getInstance(context)
					.getObject(KEY_SORUMLULUKMERKEZLERI,listType);
			
			
		}
		return sorumlulukMerkeziList;
	}

	public SorumlulukMerkeziVO getSorumlulukMerkezi(String sorumlulukMerkeziId) {
		for (SorumlulukMerkeziVO sorumlulukMerkezi : sorumlulukMerkeziList) {
			if (sorumlulukMerkezi.getMsg_S_0078().equals(sorumlulukMerkeziId)) {
				return sorumlulukMerkezi;
			}
		}
		return null;
	}

	public void saveDepoList(List<DepoVO> depoList) {
		SessionBO.getInstance(context).saveObject(KEY_DEPOLISTESI, depoList);
	}

	@SuppressWarnings("unchecked")
	public List<DepoVO> getDepoList() {
		if (depoList == null) {
			
			Type listType = new TypeToken<ArrayList<DepoVO>>() {
		    }.getType();
		    
		    depoList = (List<DepoVO>) SessionBO.getInstance(context)
					.getObject(KEY_SORUMLULUKMERKEZLERI,listType);
			
		}
		
		
		return depoList;
	}
	
	public DepoVO getDepo(int depoId) {
		for (DepoVO depo : depoList) {
			if (depo.getDep_no() == depoId) {
				return depo;
			}
		}
		return null;
	}

	public void loadStaticData(final Callback<Boolean> callback) {

		/*if(getDepoList() != null && getSorumlulukMerkeziList() != null) {
			callback.success(true, null);
			return;
		}*/
		
		
		final ProgressDialog dialog = ProgressDialog.show(context, "",
				"Yükleniyor. Bekleyiniz...", true);

		final StaticDataBO statiDataBO = StaticDataBO.getInstance(context);
		RestBO.getInstance(context).getService(
				GetService.class)
				.getSorumlulukMerkeziList(new Callback<List<SorumlulukMerkeziVO>>() {

					@Override
					public void success(List<SorumlulukMerkeziVO> arg0,
							Response arg1) {

						sorumlulukMerkeziList = arg0;
						statiDataBO.saveSorumlulukMerkeziList(arg0);

						RestBO.getInstance(context).getService(
								GetService.class).getDepoList(new Callback<List<DepoVO>>() {

							@Override
							public void success(List<DepoVO> arg0, Response arg1) {
								depoList = arg0;
								statiDataBO.saveDepoList(arg0);
								dialog.dismiss();
								callback.success(true, null);
							}

							@Override
							public void failure(RetrofitError err) {
								Toast.makeText(context, err.getMessage(), Toast.LENGTH_LONG)
										.show();
								err.printStackTrace();
								dialog.dismiss();
								callback.failure(err);
							}
						});

					}

					@Override
					public void failure(RetrofitError err) {
						Toast.makeText(context, err.getMessage(),
								Toast.LENGTH_LONG).show();
						err.printStackTrace();
						callback.failure(err);
					}
				});


	}


}
