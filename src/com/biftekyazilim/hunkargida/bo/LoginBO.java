package com.biftekyazilim.hunkargida.bo;

import android.content.Context;

import com.biftekyazilim.hunkargida.vo.UserVO;

public class LoginBO {
	
	private static LoginBO instance;
	private static final String KEY_LOGGEDINUSER = "LOGGEDINUSER";
	
	private Context context;
	
	public static LoginBO getInstance(Context context) {
		if(instance == null) {
			instance = new LoginBO(context);
		} else if (!instance.context.equals(context)) {
			instance.context = context;
		}
		return instance;
	}
	
	private LoginBO() {
		// TODO Auto-generated constructor stub
	}
	private LoginBO(Context context) {
		this.context = context;
	}

	
	public void saveLogin(UserVO user) {
		SessionBO.getInstance(context).saveObject(KEY_LOGGEDINUSER, user);
	}
	
	public boolean isLoggedIn() {
		return getLoggedInUser() != null;
	}
	
	public void deleteLogin() { //logout
		SessionBO.getInstance(context).clear();
	}
	
	public UserVO getLoggedInUser() {
		return SessionBO.getInstance(context).getObject(KEY_LOGGEDINUSER, UserVO.class);
	}
}
