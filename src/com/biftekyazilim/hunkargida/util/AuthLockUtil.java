package com.biftekyazilim.hunkargida.util;

import java.net.SocketTimeoutException;

import retrofit.Callback;
import retrofit.Endpoint;
import retrofit.ErrorHandler;
import retrofit.RestAdapter;
import retrofit.RestAdapter.LogLevel;
import retrofit.RetrofitError;
import retrofit.client.OkClient;
import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Query;
import android.content.Context;
import android.widget.Toast;

import com.squareup.okhttp.OkHttpClient;

public class AuthLockUtil {

	
	private interface AuthLockService {
		
		@GET("/appauth")
		void checkAuth(@Query("appcode") String appcode, Callback<Boolean> callback);
		
	}
	
	public static void checkAuth(Callback<Boolean> callback) {
		
		OkHttpClient client = new OkHttpClient();
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(new Endpoint() {
			
			@Override
			public String getUrl() {
				return "http://dev.biftek.com.tr/api";
			}
			
			@Override
			public String getName() {
				return "Hünkar Gıda - Mikro Muhasebe Yazılımı Web Servisi";
			}
		})
		.setLogLevel(LogLevel.BASIC)
		.setErrorHandler(new ErrorHandler() {
			
			@Override
			public Throwable handleError(RetrofitError arg0) {
				if (arg0.isNetworkError()) {
		            if (arg0.getCause() instanceof SocketTimeoutException) {
		                return new Exception("İnternet bağlantınız bulunmamaktadır.");
		            } else {
		            	return new Exception("İnternet bağlantınız bulunmamaktadır.");
		            }
		        }
				return new Exception("Servislerde belirlenemeyen bir hata oluştu.");
			}
		})
		.setClient(new OkClient(client))
		.build();
		
		AuthLockService authService = restAdapter.create(AuthLockService.class);
		authService.checkAuth("hunkargida", callback);
	}
}
