package com.biftekyazilim.hunkargida.util;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

public class UIUtil {

	
	public static void hideKeyboard(Activity act) {
		InputMethodManager inputManager = (InputMethodManager) act
	            .getSystemService(Context.INPUT_METHOD_SERVICE);

	    //check if no view has focus:
	    View v=act.getCurrentFocus();
	    if(v==null) {
	    	
	    	act.getWindow().setSoftInputMode(
	    		      WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
	    	
	    } else {
	    	
	    	inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    }
	    
	}
	
	public static void hideKeyboard(Context act, View v) {
		InputMethodManager inputManager = (InputMethodManager) act
	            .getSystemService(Context.INPUT_METHOD_SERVICE);

		inputManager.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	    
	}
}
