package com.biftekyazilim.hunkargida.util;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Locale;

import android.util.Log;

public class ReflectionUtil {

	
	public static Object getValue(String propertyName, Object obj) {
		
		Object val = null;
		Method method = null;
		//Field field;
		try {
			String getMethodName = "get" + propertyName.substring(0, 1).toUpperCase(Locale.ENGLISH) + propertyName.substring(1); 
			method = obj.getClass().getDeclaredMethod(getMethodName);
			//field = obj.getClass().getDeclaredField(propertyName);
			//field.setAccessible(true);
			//val = field.get(obj);
			method.setAccessible(true);
			val = method.invoke(obj, null);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return val;
	}
	
	public static <T> T convertObject(Class<T> targetClass, Object obj, String propertyPrefix, String targetPropertyPrefix) {
		
		
		String failedAtProperty = null;
		try {
			T convertedObj = targetClass.newInstance();
			
			Field[] objFieldList = obj.getClass().getDeclaredFields();
			Field[] targetFieldList = targetClass.getDeclaredFields();
			for (int i = 0; i < objFieldList.length; i++) {
				
				Field objField = objFieldList[i];
				String propertyName = objField.getName();
				if (propertyName.startsWith(propertyPrefix)) {
					propertyName = propertyName.substring(propertyPrefix.length()).toLowerCase(Locale.ENGLISH);
				}
				
				failedAtProperty = propertyName;
				for (int j = 0; j < targetFieldList.length; j++) {
					
					Field targetField = targetFieldList[j];
					String targetPropertyName = targetField.getName();
					if (targetPropertyName.startsWith(targetPropertyPrefix)) {
						targetPropertyName = targetPropertyName.substring(targetPropertyPrefix.length()).toLowerCase(Locale.ENGLISH);
					}
					
					if(targetPropertyName.equals(propertyName)) {
						Class<?> objFieldClass = objField.getType();
						Class<?> targetFieldClass = targetField.getType();
						if(!objFieldClass.equals(targetFieldClass)) {
							Log.e("Reflection", "Type not same for '" + failedAtProperty + "'");
						} else {
							
							objField.setAccessible(true);
							targetField.setAccessible(true);
							targetField.set(convertedObj, getValue(objField.getName(), obj));
						}
						
					}
				}
				
			}
			
			return convertedObj;
		} catch (Exception e) {
			Log.e("Reflection", "Exception at field: " + failedAtProperty);
			e.printStackTrace();
		}
		
		return null;
	}
}
