package com.biftekyazilim.hunkargida.util;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableRow;

import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.vo.TableHeaderVO;

public class TableLayoutUtil {

	public static void setHeader(Context context, TableRow headerRow, TableHeaderVO[] headerList, OnClickListener clickListener) {

		int col = 0;
		for (TableHeaderVO tableHeader : headerList) {

			Button btn = new Button(context);
			btn.setBackgroundResource(R.drawable.table_header_bg);
			btn.setTextColor(Color.WHITE);
			btn.setText(tableHeader.getTitle());
			if(tableHeader.getTextSize() != -1) {
				btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, tableHeader.getTextSize());
			} else {
				btn.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
			}
			btn.setGravity(Gravity.CENTER);
			btn.setTag(col+1);
			if(clickListener != null) {
				btn.setOnClickListener(clickListener);
			}
			TableRow.LayoutParams params = new TableRow.LayoutParams(0, TableRow.LayoutParams.MATCH_PARENT,
					tableHeader.getWeight());
			params.column = col;
			
			btn.setLayoutParams(params);
			col++;

			headerRow.addView(btn);
		}

	}

	public static void setHeader(Context context, TableRow headerRow, TableHeaderVO[] headerList) {
		setHeader(context, headerRow, headerList, null);
	}
}
