package com.biftekyazilim.hunkargida.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

public class NumberFormatUtil {

	private static final NumberFormat fmt = new DecimalFormat("#.###");// NumberFormat.getNumberInstance(Locale.US);
	
	public static String formatPrice(double price) {
		return fmt.format(price) + " TL";
	}
	
	public static String formatPointedNumber(double price) {
		return fmt.format(price);// + "";
	}
}
