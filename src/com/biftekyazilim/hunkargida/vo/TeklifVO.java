package com.biftekyazilim.hunkargida.vo;

public class TeklifVO {
	
	private String cari_unvan1;
	private int tkl_RECno;
	private int tkl_RECid_DBCno;
	private int tkl_RECid_RECno;
	private int tkl_SpecRECno;
	private int tkl_iptal;
	private int tkl_fileid;
	private int tkl_hidden;
	private int tkl_kilitli;
	private int tkl_degisti;
	private int tkl_checksum;
	private int tkl_create_user;
	private TimeAndZoneVO tkl_create_date;
	private int tkl_lastup_user;
	private TimeAndZoneVO tkl_lastup_date;
	private String tkl_special1;
	private String tkl_special2;
	private String tkl_special3;
	private int tkl_firmano;
	private int tkl_subeno;
	private String tkl_stok_kod;
	private String tkl_cari_kod;
	private String tkl_evrakno_seri;
	private int tkl_evrakno_sira;
	private TimeAndZoneVO tkl_evrak_tarihi;
	private int tkl_satirno;
	private String tkl_belge_no;
	private TimeAndZoneVO tkl_belge_tarih;
	private double tkl_asgari_miktar;
	private double tkl_teslimat_suresi;
	private TimeAndZoneVO tkl_baslangic_tarihi;
	private TimeAndZoneVO tkl_Gecerlilik_Sures;
	private double tkl_Brut_fiyat;
	private int tkl_Odeme_Plani;
	private double tkl_Alisfiyati;
	private double tkl_karorani;
	private double tkl_miktar;
	private String tkl_Aciklama;
	private double tkl_doviz_cins;
	private double tkl_doviz_kur;
	private double tkl_alt_doviz_kur;
	private double tkl_iskonto1;
	private double tkl_iskonto2;
	private double tkl_iskonto3;
	private double tkl_iskonto4;
	private double tkl_iskonto5;
	private double tkl_iskonto6;
	private double tkl_masraf1;
	private double tkl_masraf2;
	private double tkl_masraf3;
	private double tkl_masraf4;
	private int tkl_vergi_pntr;
	private double tkl_vergi;
	private double tkl_masraf_vergi_pnt;
	private double tkl_masraf_vergi;
	private double tkl_isk_mas1;
	private double TKL_ISK_MAS2;
	private double TKL_ISK_MAS3;
	private double TKL_ISK_MAS4;
	private double TKL_ISK_MAS5;
	private double TKL_ISK_MAS6;
	private double TKL_ISK_MAS7;
	private double TKL_ISK_MAS8;
	private double TKL_ISK_MAS9;
	private double TKL_ISK_MAS10;
	private double TKL_SAT_ISKMAS1;
	private double TKL_SAT_ISKMAS2;
	private double TKL_SAT_ISKMAS3;
	private double TKL_SAT_ISKMAS4;
	private double TKL_SAT_ISKMAS5;
	private double TKL_SAT_ISKMAS6;
	private double TKL_SAT_ISKMAS7;
	private double TKL_SAT_ISKMAS8;
	private double TKL_SAT_ISKMAS9;
	private double TKL_SAT_ISKMAS10;
	private double TKL_VERGISIZ_FL;
	private double TKL_KAPAT_FL;
	private String TKL_TESLIMTURU;
	private String tkl_ProjeKodu;
	private String tkl_Sorumlu_Kod;
	private double tkl_adres_no;
	private double tkl_yetkili_recid_dbcno;
	private double tkl_yetkili_recid_recno;
	private double tkl_durumu;
	private String tkl_TedarikEdilecekCari;
	private double tkl_fiyat_liste_no;
	private double tkl_Birimfiyati;
	private String tkl_paket_kod;
	private double tkl_teslim_miktar;
	private double tkl_OnaylayanKulNo;
	private double tkl_cagrilabilir_fl;
	private int tkl_harekettipi;
	private String tkl_cari_sormerk;
	private String tkl_stok_sormerk;
	private String tkl_kapatmanedenkod;
	private double sto_RECno;
	private double sto_RECid_DBCno;
	private double sto_RECid_RECno;
	private double sto_SpecRECno;
	private int sto_iptal;
	private double sto_fileid;
	private double sto_hidden;
	private double sto_kilitli;
	private double sto_degisti;
	private int sto_checksum;
	private double sto_create_user;
	private TimeAndZoneVO sto_create_date;
	private double sto_lastup_user;
	private TimeAndZoneVO sto_lastup_date;
	private String sto_special1;
	private String sto_special2;
	private String sto_special3;
	private String sto_kod;
	private String sto_isim;
	private String sto_kisa_ismi;
	private String sto_yabanci_isim;
	private String sto_sat_cari_kod;
	private double sto_cins;
	private double sto_doviz_cinsi;
	private double sto_detay_takip;
	private String sto_birim1_ad;
	private double sto_birim1_katsayi;
	private double sto_birim1_agirlik;
	private double sto_birim1_en;
	private double sto_birim1_boy;
	private double sto_birim1_yukseklik;
	private double sto_birim1_dara;
	private String sto_birim2_ad;
	private double sto_birim2_katsayi;
	private double sto_birim2_agirlik;
	private double sto_birim2_en;
	private double sto_birim2_boy;
	private double sto_birim2_yukseklik;
	private double sto_birim2_dara;
	private String sto_birim3_ad;
	private double sto_birim3_katsayi;
	private double sto_birim3_agirlik;
	private double sto_birim3_en;
	private double sto_birim3_boy;
	private double sto_birim3_yukseklik;
	private double sto_birim3_dara;
	private String sto_birim4_ad;
	private double sto_birim4_katsayi;
	private double sto_birim4_agirlik;
	private double sto_birim4_en;
	private double sto_birim4_boy;
	private double sto_birim4_yukseklik;
	private double sto_birim4_dara;
	private String sto_muh_kod;
	private String sto_muh_Iade_kod;
	private String sto_muh_sat_muh_kod;
	private String sto_muh_satIadmuhkod;
	private String sto_muh_sat_isk_kod;
	private String sto_muh_aIiskmuhkod;
	private String sto_muh_satmalmuhkod;
	private String sto_yurtdisi_satmuhk;
	private String sto_ilavemasmuhkod;
	private String sto_yatirimtesmuhkod;
	private String sto_depsatmuhkod;
	private String sto_depsatmalmuhkod;
	private String sto_bagortsatmuhkod;
	private String sto_bagortsatIadmuhkod;
	private String sto_bagortsatIskmuhkod;
	private String sto_satfiyfarkmuhkod;
	private String sto_yurtdisisatmalmuhkod;
	private String sto_bagortsatmalmuhkod;
	private double sto_karorani;
	private double sto_min_stok;
	private double sto_siparis_stok;
	private double sto_max_stok;
	private double sto_ver_sip_birim;
	private double sto_al_sip_birim;
	private double sto_siparis_sure;
	private double sto_perakende_vergi;
	private double sto_toptan_vergi;
	private String sto_yer_kod;
	private double sto_elk_etk_tipi;
	private double sto_raf_etiketli;
	private double sto_etiket_bas;
	private double sto_satis_dursun;
	private double sto_siparis_dursun;
	private double sto_malkabul_dursun;
	private double sto_malkabul_gun1;
	private double sto_malkabul_gun2;
	private double sto_malkabul_gun3;
	private double sto_malkabul_gun4;
	private double sto_malkabul_gun5;
	private double sto_malkabul_gun6;
	private double sto_malkabul_gun7;
	private double sto_siparis_gun1;
	private double sto_siparis_gun2;
	private double sto_siparis_gun3;
	private double sto_siparis_gun4;
	private double sto_siparis_gun5;
	private double sto_siparis_gun6;
	private double sto_siparis_gun7;
	private double sto_iskon_yapilamaz;
	private double sto_tasfiyede;
	private double sto_alt_grup_no;
	private String sto_kategori_kodu;
	private String sto_urun_sorkod;
	private String sto_altgrup_kod;
	private String sto_anagrup_kod;
	private String sto_uretici_kodu;
	private String sto_sektor_kodu;
	private String sto_reyon_kodu;
	private String sto_muhgrup_kodu;
	private String sto_ambalaj_kodu;
	private String sto_marka_kodu;
	private String sto_beden_kodu;
	private String sto_renk_kodu;
	private String sto_model_kodu;
	private String sto_sezon_kodu;
	private String sto_hammadde_kodu;
	private String sto_prim_kodu;
	private String sto_kalkon_kodu;
	private String sto_paket_kodu;
	private String sto_pozisyonbayrak_kodu;
	private String sto_mkod_artik;
	private double sto_kasa_tarti_fl;
	private double sto_bedenli_takip;
	private double sto_renkDetayli;
	private double sto_miktarondalikli_fl;
	private double sto_pasif_fl;
	private double sto_eksiyedusebilir_fl;
	private String sto_GtipNo;
	private double sto_puan;
	private String sto_komisyon_hzmkodu;
	private double sto_komisyon_orani;
	private double sto_otvuygulama;
	private double sto_otvtutar;
	private double sto_otvliste;
	private double sto_otvbirimi;
	private double sto_prim_orani;
	private double sto_garanti_sure;
	private double sto_garanti_sure_tipi;
	private double sto_iplik_Ne_no;
	private double sto_standartmaliyet;
	private double sto_kanban_kasa_miktari;
	private double sto_oivuygulama;
	private double sto_zraporu_stoku_fl;
	private double sto_maxiskonto_orani;
	private double sto_detay_takibinde_depo_kontrolu_fl;
	private String sto_tamamlayici_kodu;
	private double sto_oto_barkod_acma_sekli;
	private String sto_oto_barkod_kod_yapisi;
	private double sto_KasaIskontoOrani;
	private double sto_KasaIskontoTutari;
	private double sto_gelirpayi;
	private double sto_oivtutar;
	private double sto_oivturu;
	private String sto_giderkodu;
	private double sto_oivvergipntr;
	private double sto_Tevkifat_turu;
	private double sto_SKT_fl;
	private double sto_terazi_SKT;
	private double sto_RafOmru;
	private double sto_KasadaTaksitlenebilir_fl;
	private String sto_ufrsfark_kod;
	private String sto_iade_ufrsfark_kod;
	private String sto_yurticisat_ufrsfark_kod;
	private String sto_satiade_ufrsfark_kod;
	private String sto_satisk_ufrsfark_kod;
	private String sto_alisk_ufrsfark_kod;
	private String sto_satmal_ufrsfark_kod;
	private String sto_yurtdisisat_ufrsfark_kod;
	private String sto_ilavemas_ufrsfark_kod;
	private String sto_yatirimtes_ufrsfark_kod;
	private String sto_depsat_ufrsfark_kod;
	private String sto_depsatmal_ufrsfark_kod;
	private String sto_bagortsat_ufrsfark_kod;
	private String sto_bagortsatiade_ufrsfark_kod;
	private String sto_bagortsatisk_ufrsfark_kod;
	private String sto_satfiyfark_ufrsfark_kod;
	private String sto_yurtdisisatmal_ufrsfark_kod;
	private String sto_bagortsatmal_ufrsfark_kod;
	private String sto_uretimmaliyet_ufrsfark_kod;
	private String sto_uretimkapasite_ufrsfark_kod;
	private String sto_degerdusuklugu_ufrs_kod;
	private double sto_halrusumyudesi;
	private double sto_webe_gonderilecek_fl;
	private double sto_min_stok_belirleme_gun;
	private double sto_sip_stok_belirleme_gun;
	private double sto_max_stok_belirleme_gun;
	private double sto_sev_bel_opr_degerlendime_fl;
	private String tkl_stok_isim; //tmp for conversion
	
	public TeklifVO() {
	}
	public int getTkl_RECno() {
		return tkl_RECno;
	}
	public void setTkl_RECno(int tkl_RECno) {
		this.tkl_RECno = tkl_RECno;
	}
	public int getTkl_RECid_DBCno() {
		return tkl_RECid_DBCno;
	}
	public void setTkl_RECid_DBCno(int tkl_RECid_DBCno) {
		this.tkl_RECid_DBCno = tkl_RECid_DBCno;
	}
	public int getTkl_RECid_RECno() {
		return tkl_RECid_RECno;
	}
	public void setTkl_RECid_RECno(int tkl_RECid_RECno) {
		this.tkl_RECid_RECno = tkl_RECid_RECno;
	}
	public int getTkl_SpecRECno() {
		return tkl_SpecRECno;
	}
	public void setTkl_SpecRECno(int tkl_SpecRECno) {
		this.tkl_SpecRECno = tkl_SpecRECno;
	}
	public int getTkl_iptal() {
		return tkl_iptal;
	}
	public void setTkl_iptal(int tkl_iptal) {
		this.tkl_iptal = tkl_iptal;
	}
	public int getTkl_fileid() {
		return tkl_fileid;
	}
	public void setTkl_fileid(int tkl_fileid) {
		this.tkl_fileid = tkl_fileid;
	}
	public int getTkl_hidden() {
		return tkl_hidden;
	}
	public void setTkl_hidden(int tkl_hidden) {
		this.tkl_hidden = tkl_hidden;
	}
	public int getTkl_kilitli() {
		return tkl_kilitli;
	}
	public void setTkl_kilitli(int tkl_kilitli) {
		this.tkl_kilitli = tkl_kilitli;
	}
	public int getTkl_degisti() {
		return tkl_degisti;
	}
	public void setTkl_degisti(int tkl_degisti) {
		this.tkl_degisti = tkl_degisti;
	}
	public int getTkl_checksum() {
		return tkl_checksum;
	}
	public void setTkl_checksum(int tkl_checksum) {
		this.tkl_checksum = tkl_checksum;
	}
	public int getTkl_create_user() {
		return tkl_create_user;
	}
	public void setTkl_create_user(int tkl_create_user) {
		this.tkl_create_user = tkl_create_user;
	}
	public TimeAndZoneVO getTkl_create_date() {
		return tkl_create_date;
	}
	public void setTkl_create_date(TimeAndZoneVO tkl_create_date) {
		this.tkl_create_date = tkl_create_date;
	}
	public int getTkl_lastup_user() {
		return tkl_lastup_user;
	}
	public void setTkl_lastup_user(int tkl_lastup_user) {
		this.tkl_lastup_user = tkl_lastup_user;
	}
	public TimeAndZoneVO getTkl_lastup_date() {
		return tkl_lastup_date;
	}
	public void setTkl_lastup_date(TimeAndZoneVO tkl_lastup_date) {
		this.tkl_lastup_date = tkl_lastup_date;
	}
	public String getTkl_special1() {
		return tkl_special1;
	}
	public void setTkl_special1(String tkl_special1) {
		this.tkl_special1 = tkl_special1;
	}
	public String getTkl_special2() {
		return tkl_special2;
	}
	public void setTkl_special2(String tkl_special2) {
		this.tkl_special2 = tkl_special2;
	}
	public String getTkl_special3() {
		return tkl_special3;
	}
	public void setTkl_special3(String tkl_special3) {
		this.tkl_special3 = tkl_special3;
	}
	public int getTkl_firmano() {
		return tkl_firmano;
	}
	public void setTkl_firmano(int tkl_firmano) {
		this.tkl_firmano = tkl_firmano;
	}
	public int getTkl_subeno() {
		return tkl_subeno;
	}
	public void setTkl_subeno(int tkl_subeno) {
		this.tkl_subeno = tkl_subeno;
	}
	public String getTkl_stok_kod() {
		return tkl_stok_kod;
	}
	public void setTkl_stok_kod(String tkl_stok_kod) {
		this.tkl_stok_kod = tkl_stok_kod;
	}
	public String getTkl_cari_kod() {
		return tkl_cari_kod;
	}
	public void setTkl_cari_kod(String tkl_cari_kod) {
		this.tkl_cari_kod = tkl_cari_kod;
	}
	public String getTkl_evrakno_seri() {
		return tkl_evrakno_seri;
	}
	public void setTkl_evrakno_seri(String tkl_evrakno_seri) {
		this.tkl_evrakno_seri = tkl_evrakno_seri;
	}
	public int getTkl_evrakno_sira() {
		return tkl_evrakno_sira;
	}
	public void setTkl_evrakno_sira(int tkl_evrakno_sira) {
		this.tkl_evrakno_sira = tkl_evrakno_sira;
	}
	public TimeAndZoneVO getTkl_evrak_tarihi() {
		return tkl_evrak_tarihi;
	}
	public void setTkl_evrak_tarihi(TimeAndZoneVO tkl_evrak_tarihi) {
		this.tkl_evrak_tarihi = tkl_evrak_tarihi;
	}
	public int getTkl_satirno() {
		return tkl_satirno;
	}
	public void setTkl_satirno(int tkl_satirno) {
		this.tkl_satirno = tkl_satirno;
	}
	public String getTkl_belge_no() {
		return tkl_belge_no;
	}
	public void setTkl_belge_no(String tkl_belge_no) {
		this.tkl_belge_no = tkl_belge_no;
	}
	public TimeAndZoneVO getTkl_belge_tarih() {
		return tkl_belge_tarih;
	}
	public void setTkl_belge_tarih(TimeAndZoneVO tkl_belge_tarih) {
		this.tkl_belge_tarih = tkl_belge_tarih;
	}
	public double getTkl_asgari_miktar() {
		return tkl_asgari_miktar;
	}
	public void setTkl_asgari_miktar(double tkl_asgari_miktar) {
		this.tkl_asgari_miktar = tkl_asgari_miktar;
	}
	public double getTkl_teslimat_suresi() {
		return tkl_teslimat_suresi;
	}
	public void setTkl_teslimat_suresi(double tkl_teslimat_suresi) {
		this.tkl_teslimat_suresi = tkl_teslimat_suresi;
	}
	public TimeAndZoneVO getTkl_baslangic_tarihi() {
		return tkl_baslangic_tarihi;
	}
	public void setTkl_baslangic_tarihi(TimeAndZoneVO tkl_baslangic_tarihi) {
		this.tkl_baslangic_tarihi = tkl_baslangic_tarihi;
	}
	public TimeAndZoneVO getTkl_Gecerlilik_Sures() {
		return tkl_Gecerlilik_Sures;
	}
	public void setTkl_Gecerlilik_Sures(TimeAndZoneVO tkl_Gecerlilik_Sures) {
		this.tkl_Gecerlilik_Sures = tkl_Gecerlilik_Sures;
	}
	public double getTkl_Brut_fiyat() {
		return tkl_Brut_fiyat;
	}
	public void setTkl_Brut_fiyat(double tkl_Brut_fiyat) {
		this.tkl_Brut_fiyat = tkl_Brut_fiyat;
	}
	public int getTkl_Odeme_Plani() {
		return tkl_Odeme_Plani;
	}
	public void setTkl_Odeme_Plani(int tkl_Odeme_Plani) {
		this.tkl_Odeme_Plani = tkl_Odeme_Plani;
	}
	public double getTkl_Alisfiyati() {
		return tkl_Alisfiyati;
	}
	public void setTkl_Alisfiyati(double tkl_Alisfiyati) {
		this.tkl_Alisfiyati = tkl_Alisfiyati;
	}
	public double getTkl_karorani() {
		return tkl_karorani;
	}
	public void setTkl_karorani(double tkl_karorani) {
		this.tkl_karorani = tkl_karorani;
	}
	public double getTkl_miktar() {
		return tkl_miktar;
	}
	public void setTkl_miktar(double tkl_miktar) {
		this.tkl_miktar = tkl_miktar;
	}
	public String getTkl_Aciklama() {
		return tkl_Aciklama;
	}
	public void setTkl_Aciklama(String tkl_Aciklama) {
		this.tkl_Aciklama = tkl_Aciklama;
	}
	public double getTkl_doviz_cins() {
		return tkl_doviz_cins;
	}
	public void setTkl_doviz_cins(double tkl_doviz_cins) {
		this.tkl_doviz_cins = tkl_doviz_cins;
	}
	public double getTkl_doviz_kur() {
		return tkl_doviz_kur;
	}
	public void setTkl_doviz_kur(double tkl_doviz_kur) {
		this.tkl_doviz_kur = tkl_doviz_kur;
	}
	public double getTkl_alt_doviz_kur() {
		return tkl_alt_doviz_kur;
	}
	public void setTkl_alt_doviz_kur(double tkl_alt_doviz_kur) {
		this.tkl_alt_doviz_kur = tkl_alt_doviz_kur;
	}
	public double getTkl_iskonto1() {
		return tkl_iskonto1;
	}
	public void setTkl_iskonto1(double tkl_iskonto1) {
		this.tkl_iskonto1 = tkl_iskonto1;
	}
	public double getTkl_iskonto2() {
		return tkl_iskonto2;
	}
	public void setTkl_iskonto2(double tkl_iskonto2) {
		this.tkl_iskonto2 = tkl_iskonto2;
	}
	public double getTkl_iskonto3() {
		return tkl_iskonto3;
	}
	public void setTkl_iskonto3(double tkl_iskonto3) {
		this.tkl_iskonto3 = tkl_iskonto3;
	}
	public double getTkl_iskonto4() {
		return tkl_iskonto4;
	}
	public void setTkl_iskonto4(double tkl_iskonto4) {
		this.tkl_iskonto4 = tkl_iskonto4;
	}
	public double getTkl_iskonto5() {
		return tkl_iskonto5;
	}
	public void setTkl_iskonto5(double tkl_iskonto5) {
		this.tkl_iskonto5 = tkl_iskonto5;
	}
	public double getTkl_iskonto6() {
		return tkl_iskonto6;
	}
	public void setTkl_iskonto6(double tkl_iskonto6) {
		this.tkl_iskonto6 = tkl_iskonto6;
	}
	public double getTkl_masraf1() {
		return tkl_masraf1;
	}
	public void setTkl_masraf1(double tkl_masraf1) {
		this.tkl_masraf1 = tkl_masraf1;
	}
	public double getTkl_masraf2() {
		return tkl_masraf2;
	}
	public void setTkl_masraf2(double tkl_masraf2) {
		this.tkl_masraf2 = tkl_masraf2;
	}
	public double getTkl_masraf3() {
		return tkl_masraf3;
	}
	public void setTkl_masraf3(double tkl_masraf3) {
		this.tkl_masraf3 = tkl_masraf3;
	}
	public double getTkl_masraf4() {
		return tkl_masraf4;
	}
	public void setTkl_masraf4(double tkl_masraf4) {
		this.tkl_masraf4 = tkl_masraf4;
	}
	public int getTkl_vergi_pntr() {
		return tkl_vergi_pntr;
	}
	public void setTkl_vergi_pntr(int tkl_vergi_pntr) {
		this.tkl_vergi_pntr = tkl_vergi_pntr;
	}
	public double getTkl_vergi() {
		return tkl_vergi;
	}
	public void setTkl_vergi(double tkl_vergi) {
		this.tkl_vergi = tkl_vergi;
	}
	public double getTkl_masraf_vergi_pnt() {
		return tkl_masraf_vergi_pnt;
	}
	public void setTkl_masraf_vergi_pnt(double tkl_masraf_vergi_pnt) {
		this.tkl_masraf_vergi_pnt = tkl_masraf_vergi_pnt;
	}
	public double getTkl_masraf_vergi() {
		return tkl_masraf_vergi;
	}
	public void setTkl_masraf_vergi(double tkl_masraf_vergi) {
		this.tkl_masraf_vergi = tkl_masraf_vergi;
	}
	public double getTkl_isk_mas1() {
		return tkl_isk_mas1;
	}
	public void setTkl_isk_mas1(double tkl_isk_mas1) {
		this.tkl_isk_mas1 = tkl_isk_mas1;
	}
	public double getTKL_ISK_MAS2() {
		return TKL_ISK_MAS2;
	}
	public void setTKL_ISK_MAS2(double tKL_ISK_MAS2) {
		TKL_ISK_MAS2 = tKL_ISK_MAS2;
	}
	public double getTKL_ISK_MAS3() {
		return TKL_ISK_MAS3;
	}
	public void setTKL_ISK_MAS3(double tKL_ISK_MAS3) {
		TKL_ISK_MAS3 = tKL_ISK_MAS3;
	}
	public double getTKL_ISK_MAS4() {
		return TKL_ISK_MAS4;
	}
	public void setTKL_ISK_MAS4(double tKL_ISK_MAS4) {
		TKL_ISK_MAS4 = tKL_ISK_MAS4;
	}
	public double getTKL_ISK_MAS5() {
		return TKL_ISK_MAS5;
	}
	public void setTKL_ISK_MAS5(double tKL_ISK_MAS5) {
		TKL_ISK_MAS5 = tKL_ISK_MAS5;
	}
	public double getTKL_ISK_MAS6() {
		return TKL_ISK_MAS6;
	}
	public void setTKL_ISK_MAS6(double tKL_ISK_MAS6) {
		TKL_ISK_MAS6 = tKL_ISK_MAS6;
	}
	public double getTKL_ISK_MAS7() {
		return TKL_ISK_MAS7;
	}
	public void setTKL_ISK_MAS7(double tKL_ISK_MAS7) {
		TKL_ISK_MAS7 = tKL_ISK_MAS7;
	}
	public double getTKL_ISK_MAS8() {
		return TKL_ISK_MAS8;
	}
	public void setTKL_ISK_MAS8(double tKL_ISK_MAS8) {
		TKL_ISK_MAS8 = tKL_ISK_MAS8;
	}
	public double getTKL_ISK_MAS9() {
		return TKL_ISK_MAS9;
	}
	public void setTKL_ISK_MAS9(double tKL_ISK_MAS9) {
		TKL_ISK_MAS9 = tKL_ISK_MAS9;
	}
	public double getTKL_ISK_MAS10() {
		return TKL_ISK_MAS10;
	}
	public void setTKL_ISK_MAS10(double tKL_ISK_MAS10) {
		TKL_ISK_MAS10 = tKL_ISK_MAS10;
	}
	public double getTKL_SAT_ISKMAS1() {
		return TKL_SAT_ISKMAS1;
	}
	public void setTKL_SAT_ISKMAS1(double tKL_SAT_ISKMAS1) {
		TKL_SAT_ISKMAS1 = tKL_SAT_ISKMAS1;
	}
	public double getTKL_SAT_ISKMAS2() {
		return TKL_SAT_ISKMAS2;
	}
	public void setTKL_SAT_ISKMAS2(double tKL_SAT_ISKMAS2) {
		TKL_SAT_ISKMAS2 = tKL_SAT_ISKMAS2;
	}
	public double getTKL_SAT_ISKMAS3() {
		return TKL_SAT_ISKMAS3;
	}
	public void setTKL_SAT_ISKMAS3(double tKL_SAT_ISKMAS3) {
		TKL_SAT_ISKMAS3 = tKL_SAT_ISKMAS3;
	}
	public double getTKL_SAT_ISKMAS4() {
		return TKL_SAT_ISKMAS4;
	}
	public void setTKL_SAT_ISKMAS4(double tKL_SAT_ISKMAS4) {
		TKL_SAT_ISKMAS4 = tKL_SAT_ISKMAS4;
	}
	public double getTKL_SAT_ISKMAS5() {
		return TKL_SAT_ISKMAS5;
	}
	public void setTKL_SAT_ISKMAS5(double tKL_SAT_ISKMAS5) {
		TKL_SAT_ISKMAS5 = tKL_SAT_ISKMAS5;
	}
	public double getTKL_SAT_ISKMAS6() {
		return TKL_SAT_ISKMAS6;
	}
	public void setTKL_SAT_ISKMAS6(double tKL_SAT_ISKMAS6) {
		TKL_SAT_ISKMAS6 = tKL_SAT_ISKMAS6;
	}
	public double getTKL_SAT_ISKMAS7() {
		return TKL_SAT_ISKMAS7;
	}
	public void setTKL_SAT_ISKMAS7(double tKL_SAT_ISKMAS7) {
		TKL_SAT_ISKMAS7 = tKL_SAT_ISKMAS7;
	}
	public double getTKL_SAT_ISKMAS8() {
		return TKL_SAT_ISKMAS8;
	}
	public void setTKL_SAT_ISKMAS8(double tKL_SAT_ISKMAS8) {
		TKL_SAT_ISKMAS8 = tKL_SAT_ISKMAS8;
	}
	public double getTKL_SAT_ISKMAS9() {
		return TKL_SAT_ISKMAS9;
	}
	public void setTKL_SAT_ISKMAS9(double tKL_SAT_ISKMAS9) {
		TKL_SAT_ISKMAS9 = tKL_SAT_ISKMAS9;
	}
	public double getTKL_SAT_ISKMAS10() {
		return TKL_SAT_ISKMAS10;
	}
	public void setTKL_SAT_ISKMAS10(double tKL_SAT_ISKMAS10) {
		TKL_SAT_ISKMAS10 = tKL_SAT_ISKMAS10;
	}
	public double getTKL_VERGISIZ_FL() {
		return TKL_VERGISIZ_FL;
	}
	public void setTKL_VERGISIZ_FL(double tKL_VERGISIZ_FL) {
		TKL_VERGISIZ_FL = tKL_VERGISIZ_FL;
	}
	public double getTKL_KAPAT_FL() {
		return TKL_KAPAT_FL;
	}
	public void setTKL_KAPAT_FL(double tKL_KAPAT_FL) {
		TKL_KAPAT_FL = tKL_KAPAT_FL;
	}
	public String getTKL_TESLIMTURU() {
		return TKL_TESLIMTURU;
	}
	public void setTKL_TESLIMTURU(String tKL_TESLIMTURU) {
		TKL_TESLIMTURU = tKL_TESLIMTURU;
	}
	public String getTkl_ProjeKodu() {
		return tkl_ProjeKodu;
	}
	public void setTkl_ProjeKodu(String tkl_ProjeKodu) {
		this.tkl_ProjeKodu = tkl_ProjeKodu;
	}
	public String getTkl_Sorumlu_Kod() {
		return tkl_Sorumlu_Kod;
	}
	public void setTkl_Sorumlu_Kod(String tkl_Sorumlu_Kod) {
		this.tkl_Sorumlu_Kod = tkl_Sorumlu_Kod;
	}
	public double getTkl_adres_no() {
		return tkl_adres_no;
	}
	public void setTkl_adres_no(double tkl_adres_no) {
		this.tkl_adres_no = tkl_adres_no;
	}
	public double getTkl_yetkili_recid_dbcno() {
		return tkl_yetkili_recid_dbcno;
	}
	public void setTkl_yetkili_recid_dbcno(double tkl_yetkili_recid_dbcno) {
		this.tkl_yetkili_recid_dbcno = tkl_yetkili_recid_dbcno;
	}
	public double getTkl_yetkili_recid_recno() {
		return tkl_yetkili_recid_recno;
	}
	public void setTkl_yetkili_recid_recno(double tkl_yetkili_recid_recno) {
		this.tkl_yetkili_recid_recno = tkl_yetkili_recid_recno;
	}
	public double getTkl_durumu() {
		return tkl_durumu;
	}
	public void setTkl_durumu(double tkl_durumu) {
		this.tkl_durumu = tkl_durumu;
	}
	public String getTkl_TedarikEdilecekCari() {
		return tkl_TedarikEdilecekCari;
	}
	public void setTkl_TedarikEdilecekCari(String tkl_TedarikEdilecekCari) {
		this.tkl_TedarikEdilecekCari = tkl_TedarikEdilecekCari;
	}
	public double getTkl_fiyat_liste_no() {
		return tkl_fiyat_liste_no;
	}
	public void setTkl_fiyat_liste_no(double tkl_fiyat_liste_no) {
		this.tkl_fiyat_liste_no = tkl_fiyat_liste_no;
	}
	public double getTkl_Birimfiyati() {
		return tkl_Birimfiyati;
	}
	public void setTkl_Birimfiyati(double tkl_Birimfiyati) {
		this.tkl_Birimfiyati = tkl_Birimfiyati;
	}
	public String getTkl_paket_kod() {
		return tkl_paket_kod;
	}
	public void setTkl_paket_kod(String tkl_paket_kod) {
		this.tkl_paket_kod = tkl_paket_kod;
	}
	public double getTkl_teslim_miktar() {
		return tkl_teslim_miktar;
	}
	public void setTkl_teslim_miktar(double tkl_teslim_miktar) {
		this.tkl_teslim_miktar = tkl_teslim_miktar;
	}
	public double getTkl_OnaylayanKulNo() {
		return tkl_OnaylayanKulNo;
	}
	public void setTkl_OnaylayanKulNo(double tkl_OnaylayanKulNo) {
		this.tkl_OnaylayanKulNo = tkl_OnaylayanKulNo;
	}
	public double getTkl_cagrilabilir_fl() {
		return tkl_cagrilabilir_fl;
	}
	public void setTkl_cagrilabilir_fl(double tkl_cagrilabilir_fl) {
		this.tkl_cagrilabilir_fl = tkl_cagrilabilir_fl;
	}
	public int getTkl_harekettipi() {
		return tkl_harekettipi;
	}
	public void setTkl_harekettipi(int tkl_harekettipi) {
		this.tkl_harekettipi = tkl_harekettipi;
	}
	public String getTkl_cari_sormerk() {
		return tkl_cari_sormerk;
	}
	public void setTkl_cari_sormerk(String tkl_cari_sormerk) {
		this.tkl_cari_sormerk = tkl_cari_sormerk;
	}
	public String getTkl_stok_sormerk() {
		return tkl_stok_sormerk;
	}
	public void setTkl_stok_sormerk(String tkl_stok_sormerk) {
		this.tkl_stok_sormerk = tkl_stok_sormerk;
	}
	public String getTkl_kapatmanedenkod() {
		return tkl_kapatmanedenkod;
	}
	public void setTkl_kapatmanedenkod(String tkl_kapatmanedenkod) {
		this.tkl_kapatmanedenkod = tkl_kapatmanedenkod;
	}
	public double getSto_RECno() {
		return sto_RECno;
	}
	public void setSto_RECno(double sto_RECno) {
		this.sto_RECno = sto_RECno;
	}
	public double getSto_RECid_DBCno() {
		return sto_RECid_DBCno;
	}
	public void setSto_RECid_DBCno(double sto_RECid_DBCno) {
		this.sto_RECid_DBCno = sto_RECid_DBCno;
	}
	public double getSto_RECid_RECno() {
		return sto_RECid_RECno;
	}
	public void setSto_RECid_RECno(double sto_RECid_RECno) {
		this.sto_RECid_RECno = sto_RECid_RECno;
	}
	public double getSto_SpecRECno() {
		return sto_SpecRECno;
	}
	public void setSto_SpecRECno(double sto_SpecRECno) {
		this.sto_SpecRECno = sto_SpecRECno;
	}
	public int getSto_iptal() {
		return sto_iptal;
	}
	public void setSto_iptal(int sto_iptal) {
		this.sto_iptal = sto_iptal;
	}
	public double getSto_fileid() {
		return sto_fileid;
	}
	public void setSto_fileid(double sto_fileid) {
		this.sto_fileid = sto_fileid;
	}
	public double getSto_hidden() {
		return sto_hidden;
	}
	public void setSto_hidden(double sto_hidden) {
		this.sto_hidden = sto_hidden;
	}
	public double getSto_kilitli() {
		return sto_kilitli;
	}
	public void setSto_kilitli(double sto_kilitli) {
		this.sto_kilitli = sto_kilitli;
	}
	public double getSto_degisti() {
		return sto_degisti;
	}
	public void setSto_degisti(double sto_degisti) {
		this.sto_degisti = sto_degisti;
	}
	public int getSto_checksum() {
		return sto_checksum;
	}
	public void setSto_checksum(int sto_checksum) {
		this.sto_checksum = sto_checksum;
	}
	public double getSto_create_user() {
		return sto_create_user;
	}
	public void setSto_create_user(double sto_create_user) {
		this.sto_create_user = sto_create_user;
	}
	public TimeAndZoneVO getSto_create_date() {
		return sto_create_date;
	}
	public void setSto_create_date(TimeAndZoneVO sto_create_date) {
		this.sto_create_date = sto_create_date;
	}
	public double getSto_lastup_user() {
		return sto_lastup_user;
	}
	public void setSto_lastup_user(double sto_lastup_user) {
		this.sto_lastup_user = sto_lastup_user;
	}
	public TimeAndZoneVO getSto_lastup_date() {
		return sto_lastup_date;
	}
	public void setSto_lastup_date(TimeAndZoneVO sto_lastup_date) {
		this.sto_lastup_date = sto_lastup_date;
	}
	public String getSto_special1() {
		return sto_special1;
	}
	public void setSto_special1(String sto_special1) {
		this.sto_special1 = sto_special1;
	}
	public String getSto_special2() {
		return sto_special2;
	}
	public void setSto_special2(String sto_special2) {
		this.sto_special2 = sto_special2;
	}
	public String getSto_special3() {
		return sto_special3;
	}
	public void setSto_special3(String sto_special3) {
		this.sto_special3 = sto_special3;
	}
	public String getSto_kod() {
		return sto_kod;
	}
	public void setSto_kod(String sto_kod) {
		this.sto_kod = sto_kod;
	}
	public String getSto_isim() {
		return sto_isim;
	}
	public void setSto_isim(String sto_isim) {
		this.sto_isim = sto_isim;
	}
	public String getSto_kisa_ismi() {
		return sto_kisa_ismi;
	}
	public void setSto_kisa_ismi(String sto_kisa_ismi) {
		this.sto_kisa_ismi = sto_kisa_ismi;
	}
	public String getSto_yabanci_isim() {
		return sto_yabanci_isim;
	}
	public void setSto_yabanci_isim(String sto_yabanci_isim) {
		this.sto_yabanci_isim = sto_yabanci_isim;
	}
	public String getSto_sat_cari_kod() {
		return sto_sat_cari_kod;
	}
	public void setSto_sat_cari_kod(String sto_sat_cari_kod) {
		this.sto_sat_cari_kod = sto_sat_cari_kod;
	}
	public double getSto_cins() {
		return sto_cins;
	}
	public void setSto_cins(double sto_cins) {
		this.sto_cins = sto_cins;
	}
	public double getSto_doviz_cinsi() {
		return sto_doviz_cinsi;
	}
	public void setSto_doviz_cinsi(double sto_doviz_cinsi) {
		this.sto_doviz_cinsi = sto_doviz_cinsi;
	}
	public double getSto_detay_takip() {
		return sto_detay_takip;
	}
	public void setSto_detay_takip(double sto_detay_takip) {
		this.sto_detay_takip = sto_detay_takip;
	}
	public String getSto_birim1_ad() {
		return sto_birim1_ad;
	}
	public void setSto_birim1_ad(String sto_birim1_ad) {
		this.sto_birim1_ad = sto_birim1_ad;
	}
	public double getSto_birim1_katsayi() {
		return sto_birim1_katsayi;
	}
	public void setSto_birim1_katsayi(double sto_birim1_katsayi) {
		this.sto_birim1_katsayi = sto_birim1_katsayi;
	}
	public double getSto_birim1_agirlik() {
		return sto_birim1_agirlik;
	}
	public void setSto_birim1_agirlik(double sto_birim1_agirlik) {
		this.sto_birim1_agirlik = sto_birim1_agirlik;
	}
	public double getSto_birim1_en() {
		return sto_birim1_en;
	}
	public void setSto_birim1_en(double sto_birim1_en) {
		this.sto_birim1_en = sto_birim1_en;
	}
	public double getSto_birim1_boy() {
		return sto_birim1_boy;
	}
	public void setSto_birim1_boy(double sto_birim1_boy) {
		this.sto_birim1_boy = sto_birim1_boy;
	}
	public double getSto_birim1_yukseklik() {
		return sto_birim1_yukseklik;
	}
	public void setSto_birim1_yukseklik(double sto_birim1_yukseklik) {
		this.sto_birim1_yukseklik = sto_birim1_yukseklik;
	}
	public double getSto_birim1_dara() {
		return sto_birim1_dara;
	}
	public void setSto_birim1_dara(double sto_birim1_dara) {
		this.sto_birim1_dara = sto_birim1_dara;
	}
	public String getSto_birim2_ad() {
		return sto_birim2_ad;
	}
	public void setSto_birim2_ad(String sto_birim2_ad) {
		this.sto_birim2_ad = sto_birim2_ad;
	}
	public double getSto_birim2_katsayi() {
		return sto_birim2_katsayi;
	}
	public void setSto_birim2_katsayi(double sto_birim2_katsayi) {
		this.sto_birim2_katsayi = sto_birim2_katsayi;
	}
	public double getSto_birim2_agirlik() {
		return sto_birim2_agirlik;
	}
	public void setSto_birim2_agirlik(double sto_birim2_agirlik) {
		this.sto_birim2_agirlik = sto_birim2_agirlik;
	}
	public double getSto_birim2_en() {
		return sto_birim2_en;
	}
	public void setSto_birim2_en(double sto_birim2_en) {
		this.sto_birim2_en = sto_birim2_en;
	}
	public double getSto_birim2_boy() {
		return sto_birim2_boy;
	}
	public void setSto_birim2_boy(double sto_birim2_boy) {
		this.sto_birim2_boy = sto_birim2_boy;
	}
	public double getSto_birim2_yukseklik() {
		return sto_birim2_yukseklik;
	}
	public void setSto_birim2_yukseklik(double sto_birim2_yukseklik) {
		this.sto_birim2_yukseklik = sto_birim2_yukseklik;
	}
	public double getSto_birim2_dara() {
		return sto_birim2_dara;
	}
	public void setSto_birim2_dara(double sto_birim2_dara) {
		this.sto_birim2_dara = sto_birim2_dara;
	}
	public String getSto_birim3_ad() {
		return sto_birim3_ad;
	}
	public void setSto_birim3_ad(String sto_birim3_ad) {
		this.sto_birim3_ad = sto_birim3_ad;
	}
	public double getSto_birim3_katsayi() {
		return sto_birim3_katsayi;
	}
	public void setSto_birim3_katsayi(double sto_birim3_katsayi) {
		this.sto_birim3_katsayi = sto_birim3_katsayi;
	}
	public double getSto_birim3_agirlik() {
		return sto_birim3_agirlik;
	}
	public void setSto_birim3_agirlik(double sto_birim3_agirlik) {
		this.sto_birim3_agirlik = sto_birim3_agirlik;
	}
	public double getSto_birim3_en() {
		return sto_birim3_en;
	}
	public void setSto_birim3_en(double sto_birim3_en) {
		this.sto_birim3_en = sto_birim3_en;
	}
	public double getSto_birim3_boy() {
		return sto_birim3_boy;
	}
	public void setSto_birim3_boy(double sto_birim3_boy) {
		this.sto_birim3_boy = sto_birim3_boy;
	}
	public double getSto_birim3_yukseklik() {
		return sto_birim3_yukseklik;
	}
	public void setSto_birim3_yukseklik(double sto_birim3_yukseklik) {
		this.sto_birim3_yukseklik = sto_birim3_yukseklik;
	}
	public double getSto_birim3_dara() {
		return sto_birim3_dara;
	}
	public void setSto_birim3_dara(double sto_birim3_dara) {
		this.sto_birim3_dara = sto_birim3_dara;
	}
	public String getSto_birim4_ad() {
		return sto_birim4_ad;
	}
	public void setSto_birim4_ad(String sto_birim4_ad) {
		this.sto_birim4_ad = sto_birim4_ad;
	}
	public double getSto_birim4_katsayi() {
		return sto_birim4_katsayi;
	}
	public void setSto_birim4_katsayi(double sto_birim4_katsayi) {
		this.sto_birim4_katsayi = sto_birim4_katsayi;
	}
	public double getSto_birim4_agirlik() {
		return sto_birim4_agirlik;
	}
	public void setSto_birim4_agirlik(double sto_birim4_agirlik) {
		this.sto_birim4_agirlik = sto_birim4_agirlik;
	}
	public double getSto_birim4_en() {
		return sto_birim4_en;
	}
	public void setSto_birim4_en(double sto_birim4_en) {
		this.sto_birim4_en = sto_birim4_en;
	}
	public double getSto_birim4_boy() {
		return sto_birim4_boy;
	}
	public void setSto_birim4_boy(double sto_birim4_boy) {
		this.sto_birim4_boy = sto_birim4_boy;
	}
	public double getSto_birim4_yukseklik() {
		return sto_birim4_yukseklik;
	}
	public void setSto_birim4_yukseklik(double sto_birim4_yukseklik) {
		this.sto_birim4_yukseklik = sto_birim4_yukseklik;
	}
	public double getSto_birim4_dara() {
		return sto_birim4_dara;
	}
	public void setSto_birim4_dara(double sto_birim4_dara) {
		this.sto_birim4_dara = sto_birim4_dara;
	}
	public String getSto_muh_kod() {
		return sto_muh_kod;
	}
	public void setSto_muh_kod(String sto_muh_kod) {
		this.sto_muh_kod = sto_muh_kod;
	}
	public String getSto_muh_Iade_kod() {
		return sto_muh_Iade_kod;
	}
	public void setSto_muh_Iade_kod(String sto_muh_Iade_kod) {
		this.sto_muh_Iade_kod = sto_muh_Iade_kod;
	}
	public String getSto_muh_sat_muh_kod() {
		return sto_muh_sat_muh_kod;
	}
	public void setSto_muh_sat_muh_kod(String sto_muh_sat_muh_kod) {
		this.sto_muh_sat_muh_kod = sto_muh_sat_muh_kod;
	}
	public String getSto_muh_satIadmuhkod() {
		return sto_muh_satIadmuhkod;
	}
	public void setSto_muh_satIadmuhkod(String sto_muh_satIadmuhkod) {
		this.sto_muh_satIadmuhkod = sto_muh_satIadmuhkod;
	}
	public String getSto_muh_sat_isk_kod() {
		return sto_muh_sat_isk_kod;
	}
	public void setSto_muh_sat_isk_kod(String sto_muh_sat_isk_kod) {
		this.sto_muh_sat_isk_kod = sto_muh_sat_isk_kod;
	}
	public String getSto_muh_aIiskmuhkod() {
		return sto_muh_aIiskmuhkod;
	}
	public void setSto_muh_aIiskmuhkod(String sto_muh_aIiskmuhkod) {
		this.sto_muh_aIiskmuhkod = sto_muh_aIiskmuhkod;
	}
	public String getSto_muh_satmalmuhkod() {
		return sto_muh_satmalmuhkod;
	}
	public void setSto_muh_satmalmuhkod(String sto_muh_satmalmuhkod) {
		this.sto_muh_satmalmuhkod = sto_muh_satmalmuhkod;
	}
	public String getSto_yurtdisi_satmuhk() {
		return sto_yurtdisi_satmuhk;
	}
	public void setSto_yurtdisi_satmuhk(String sto_yurtdisi_satmuhk) {
		this.sto_yurtdisi_satmuhk = sto_yurtdisi_satmuhk;
	}
	public String getSto_ilavemasmuhkod() {
		return sto_ilavemasmuhkod;
	}
	public void setSto_ilavemasmuhkod(String sto_ilavemasmuhkod) {
		this.sto_ilavemasmuhkod = sto_ilavemasmuhkod;
	}
	public String getSto_yatirimtesmuhkod() {
		return sto_yatirimtesmuhkod;
	}
	public void setSto_yatirimtesmuhkod(String sto_yatirimtesmuhkod) {
		this.sto_yatirimtesmuhkod = sto_yatirimtesmuhkod;
	}
	public String getSto_depsatmuhkod() {
		return sto_depsatmuhkod;
	}
	public void setSto_depsatmuhkod(String sto_depsatmuhkod) {
		this.sto_depsatmuhkod = sto_depsatmuhkod;
	}
	public String getSto_depsatmalmuhkod() {
		return sto_depsatmalmuhkod;
	}
	public void setSto_depsatmalmuhkod(String sto_depsatmalmuhkod) {
		this.sto_depsatmalmuhkod = sto_depsatmalmuhkod;
	}
	public String getSto_bagortsatmuhkod() {
		return sto_bagortsatmuhkod;
	}
	public void setSto_bagortsatmuhkod(String sto_bagortsatmuhkod) {
		this.sto_bagortsatmuhkod = sto_bagortsatmuhkod;
	}
	public String getSto_bagortsatIadmuhkod() {
		return sto_bagortsatIadmuhkod;
	}
	public void setSto_bagortsatIadmuhkod(String sto_bagortsatIadmuhkod) {
		this.sto_bagortsatIadmuhkod = sto_bagortsatIadmuhkod;
	}
	public String getSto_bagortsatIskmuhkod() {
		return sto_bagortsatIskmuhkod;
	}
	public void setSto_bagortsatIskmuhkod(String sto_bagortsatIskmuhkod) {
		this.sto_bagortsatIskmuhkod = sto_bagortsatIskmuhkod;
	}
	public String getSto_satfiyfarkmuhkod() {
		return sto_satfiyfarkmuhkod;
	}
	public void setSto_satfiyfarkmuhkod(String sto_satfiyfarkmuhkod) {
		this.sto_satfiyfarkmuhkod = sto_satfiyfarkmuhkod;
	}
	public String getSto_yurtdisisatmalmuhkod() {
		return sto_yurtdisisatmalmuhkod;
	}
	public void setSto_yurtdisisatmalmuhkod(String sto_yurtdisisatmalmuhkod) {
		this.sto_yurtdisisatmalmuhkod = sto_yurtdisisatmalmuhkod;
	}
	public String getSto_bagortsatmalmuhkod() {
		return sto_bagortsatmalmuhkod;
	}
	public void setSto_bagortsatmalmuhkod(String sto_bagortsatmalmuhkod) {
		this.sto_bagortsatmalmuhkod = sto_bagortsatmalmuhkod;
	}
	public double getSto_karorani() {
		return sto_karorani;
	}
	public void setSto_karorani(double sto_karorani) {
		this.sto_karorani = sto_karorani;
	}
	public double getSto_min_stok() {
		return sto_min_stok;
	}
	public void setSto_min_stok(double sto_min_stok) {
		this.sto_min_stok = sto_min_stok;
	}
	public double getSto_siparis_stok() {
		return sto_siparis_stok;
	}
	public void setSto_siparis_stok(double sto_siparis_stok) {
		this.sto_siparis_stok = sto_siparis_stok;
	}
	public double getSto_max_stok() {
		return sto_max_stok;
	}
	public void setSto_max_stok(double sto_max_stok) {
		this.sto_max_stok = sto_max_stok;
	}
	public double getSto_ver_sip_birim() {
		return sto_ver_sip_birim;
	}
	public void setSto_ver_sip_birim(double sto_ver_sip_birim) {
		this.sto_ver_sip_birim = sto_ver_sip_birim;
	}
	public double getSto_al_sip_birim() {
		return sto_al_sip_birim;
	}
	public void setSto_al_sip_birim(double sto_al_sip_birim) {
		this.sto_al_sip_birim = sto_al_sip_birim;
	}
	public double getSto_siparis_sure() {
		return sto_siparis_sure;
	}
	public void setSto_siparis_sure(double sto_siparis_sure) {
		this.sto_siparis_sure = sto_siparis_sure;
	}
	public double getSto_perakende_vergi() {
		return sto_perakende_vergi;
	}
	public void setSto_perakende_vergi(double sto_perakende_vergi) {
		this.sto_perakende_vergi = sto_perakende_vergi;
	}
	public double getSto_toptan_vergi() {
		return sto_toptan_vergi;
	}
	public void setSto_toptan_vergi(double sto_toptan_vergi) {
		this.sto_toptan_vergi = sto_toptan_vergi;
	}
	public String getSto_yer_kod() {
		return sto_yer_kod;
	}
	public void setSto_yer_kod(String sto_yer_kod) {
		this.sto_yer_kod = sto_yer_kod;
	}
	public double getSto_elk_etk_tipi() {
		return sto_elk_etk_tipi;
	}
	public void setSto_elk_etk_tipi(double sto_elk_etk_tipi) {
		this.sto_elk_etk_tipi = sto_elk_etk_tipi;
	}
	public double getSto_raf_etiketli() {
		return sto_raf_etiketli;
	}
	public void setSto_raf_etiketli(double sto_raf_etiketli) {
		this.sto_raf_etiketli = sto_raf_etiketli;
	}
	public double getSto_etiket_bas() {
		return sto_etiket_bas;
	}
	public void setSto_etiket_bas(double sto_etiket_bas) {
		this.sto_etiket_bas = sto_etiket_bas;
	}
	public double getSto_satis_dursun() {
		return sto_satis_dursun;
	}
	public void setSto_satis_dursun(double sto_satis_dursun) {
		this.sto_satis_dursun = sto_satis_dursun;
	}
	public double getSto_siparis_dursun() {
		return sto_siparis_dursun;
	}
	public void setSto_siparis_dursun(double sto_siparis_dursun) {
		this.sto_siparis_dursun = sto_siparis_dursun;
	}
	public double getSto_malkabul_dursun() {
		return sto_malkabul_dursun;
	}
	public void setSto_malkabul_dursun(double sto_malkabul_dursun) {
		this.sto_malkabul_dursun = sto_malkabul_dursun;
	}
	public double getSto_malkabul_gun1() {
		return sto_malkabul_gun1;
	}
	public void setSto_malkabul_gun1(double sto_malkabul_gun1) {
		this.sto_malkabul_gun1 = sto_malkabul_gun1;
	}
	public double getSto_malkabul_gun2() {
		return sto_malkabul_gun2;
	}
	public void setSto_malkabul_gun2(double sto_malkabul_gun2) {
		this.sto_malkabul_gun2 = sto_malkabul_gun2;
	}
	public double getSto_malkabul_gun3() {
		return sto_malkabul_gun3;
	}
	public void setSto_malkabul_gun3(double sto_malkabul_gun3) {
		this.sto_malkabul_gun3 = sto_malkabul_gun3;
	}
	public double getSto_malkabul_gun4() {
		return sto_malkabul_gun4;
	}
	public void setSto_malkabul_gun4(double sto_malkabul_gun4) {
		this.sto_malkabul_gun4 = sto_malkabul_gun4;
	}
	public double getSto_malkabul_gun5() {
		return sto_malkabul_gun5;
	}
	public void setSto_malkabul_gun5(double sto_malkabul_gun5) {
		this.sto_malkabul_gun5 = sto_malkabul_gun5;
	}
	public double getSto_malkabul_gun6() {
		return sto_malkabul_gun6;
	}
	public void setSto_malkabul_gun6(double sto_malkabul_gun6) {
		this.sto_malkabul_gun6 = sto_malkabul_gun6;
	}
	public double getSto_malkabul_gun7() {
		return sto_malkabul_gun7;
	}
	public void setSto_malkabul_gun7(double sto_malkabul_gun7) {
		this.sto_malkabul_gun7 = sto_malkabul_gun7;
	}
	public double getSto_siparis_gun1() {
		return sto_siparis_gun1;
	}
	public void setSto_siparis_gun1(double sto_siparis_gun1) {
		this.sto_siparis_gun1 = sto_siparis_gun1;
	}
	public double getSto_siparis_gun2() {
		return sto_siparis_gun2;
	}
	public void setSto_siparis_gun2(double sto_siparis_gun2) {
		this.sto_siparis_gun2 = sto_siparis_gun2;
	}
	public double getSto_siparis_gun3() {
		return sto_siparis_gun3;
	}
	public void setSto_siparis_gun3(double sto_siparis_gun3) {
		this.sto_siparis_gun3 = sto_siparis_gun3;
	}
	public double getSto_siparis_gun4() {
		return sto_siparis_gun4;
	}
	public void setSto_siparis_gun4(double sto_siparis_gun4) {
		this.sto_siparis_gun4 = sto_siparis_gun4;
	}
	public double getSto_siparis_gun5() {
		return sto_siparis_gun5;
	}
	public void setSto_siparis_gun5(double sto_siparis_gun5) {
		this.sto_siparis_gun5 = sto_siparis_gun5;
	}
	public double getSto_siparis_gun6() {
		return sto_siparis_gun6;
	}
	public void setSto_siparis_gun6(double sto_siparis_gun6) {
		this.sto_siparis_gun6 = sto_siparis_gun6;
	}
	public double getSto_siparis_gun7() {
		return sto_siparis_gun7;
	}
	public void setSto_siparis_gun7(double sto_siparis_gun7) {
		this.sto_siparis_gun7 = sto_siparis_gun7;
	}
	public double getSto_iskon_yapilamaz() {
		return sto_iskon_yapilamaz;
	}
	public void setSto_iskon_yapilamaz(double sto_iskon_yapilamaz) {
		this.sto_iskon_yapilamaz = sto_iskon_yapilamaz;
	}
	public double getSto_tasfiyede() {
		return sto_tasfiyede;
	}
	public void setSto_tasfiyede(double sto_tasfiyede) {
		this.sto_tasfiyede = sto_tasfiyede;
	}
	public double getSto_alt_grup_no() {
		return sto_alt_grup_no;
	}
	public void setSto_alt_grup_no(double sto_alt_grup_no) {
		this.sto_alt_grup_no = sto_alt_grup_no;
	}
	public String getSto_kategori_kodu() {
		return sto_kategori_kodu;
	}
	public void setSto_kategori_kodu(String sto_kategori_kodu) {
		this.sto_kategori_kodu = sto_kategori_kodu;
	}
	public String getSto_urun_sorkod() {
		return sto_urun_sorkod;
	}
	public void setSto_urun_sorkod(String sto_urun_sorkod) {
		this.sto_urun_sorkod = sto_urun_sorkod;
	}
	public String getSto_altgrup_kod() {
		return sto_altgrup_kod;
	}
	public void setSto_altgrup_kod(String sto_altgrup_kod) {
		this.sto_altgrup_kod = sto_altgrup_kod;
	}
	public String getSto_anagrup_kod() {
		return sto_anagrup_kod;
	}
	public void setSto_anagrup_kod(String sto_anagrup_kod) {
		this.sto_anagrup_kod = sto_anagrup_kod;
	}
	public String getSto_uretici_kodu() {
		return sto_uretici_kodu;
	}
	public void setSto_uretici_kodu(String sto_uretici_kodu) {
		this.sto_uretici_kodu = sto_uretici_kodu;
	}
	public String getSto_sektor_kodu() {
		return sto_sektor_kodu;
	}
	public void setSto_sektor_kodu(String sto_sektor_kodu) {
		this.sto_sektor_kodu = sto_sektor_kodu;
	}
	public String getSto_reyon_kodu() {
		return sto_reyon_kodu;
	}
	public void setSto_reyon_kodu(String sto_reyon_kodu) {
		this.sto_reyon_kodu = sto_reyon_kodu;
	}
	public String getSto_muhgrup_kodu() {
		return sto_muhgrup_kodu;
	}
	public void setSto_muhgrup_kodu(String sto_muhgrup_kodu) {
		this.sto_muhgrup_kodu = sto_muhgrup_kodu;
	}
	public String getSto_ambalaj_kodu() {
		return sto_ambalaj_kodu;
	}
	public void setSto_ambalaj_kodu(String sto_ambalaj_kodu) {
		this.sto_ambalaj_kodu = sto_ambalaj_kodu;
	}
	public String getSto_marka_kodu() {
		return sto_marka_kodu;
	}
	public void setSto_marka_kodu(String sto_marka_kodu) {
		this.sto_marka_kodu = sto_marka_kodu;
	}
	public String getSto_beden_kodu() {
		return sto_beden_kodu;
	}
	public void setSto_beden_kodu(String sto_beden_kodu) {
		this.sto_beden_kodu = sto_beden_kodu;
	}
	public String getSto_renk_kodu() {
		return sto_renk_kodu;
	}
	public void setSto_renk_kodu(String sto_renk_kodu) {
		this.sto_renk_kodu = sto_renk_kodu;
	}
	public String getSto_model_kodu() {
		return sto_model_kodu;
	}
	public void setSto_model_kodu(String sto_model_kodu) {
		this.sto_model_kodu = sto_model_kodu;
	}
	public String getSto_sezon_kodu() {
		return sto_sezon_kodu;
	}
	public void setSto_sezon_kodu(String sto_sezon_kodu) {
		this.sto_sezon_kodu = sto_sezon_kodu;
	}
	public String getSto_hammadde_kodu() {
		return sto_hammadde_kodu;
	}
	public void setSto_hammadde_kodu(String sto_hammadde_kodu) {
		this.sto_hammadde_kodu = sto_hammadde_kodu;
	}
	public String getSto_prim_kodu() {
		return sto_prim_kodu;
	}
	public void setSto_prim_kodu(String sto_prim_kodu) {
		this.sto_prim_kodu = sto_prim_kodu;
	}
	public String getSto_kalkon_kodu() {
		return sto_kalkon_kodu;
	}
	public void setSto_kalkon_kodu(String sto_kalkon_kodu) {
		this.sto_kalkon_kodu = sto_kalkon_kodu;
	}
	public String getSto_paket_kodu() {
		return sto_paket_kodu;
	}
	public void setSto_paket_kodu(String sto_paket_kodu) {
		this.sto_paket_kodu = sto_paket_kodu;
	}
	public String getSto_pozisyonbayrak_kodu() {
		return sto_pozisyonbayrak_kodu;
	}
	public void setSto_pozisyonbayrak_kodu(String sto_pozisyonbayrak_kodu) {
		this.sto_pozisyonbayrak_kodu = sto_pozisyonbayrak_kodu;
	}
	public String getSto_mkod_artik() {
		return sto_mkod_artik;
	}
	public void setSto_mkod_artik(String sto_mkod_artik) {
		this.sto_mkod_artik = sto_mkod_artik;
	}
	public double getSto_kasa_tarti_fl() {
		return sto_kasa_tarti_fl;
	}
	public void setSto_kasa_tarti_fl(double sto_kasa_tarti_fl) {
		this.sto_kasa_tarti_fl = sto_kasa_tarti_fl;
	}
	public double getSto_bedenli_takip() {
		return sto_bedenli_takip;
	}
	public void setSto_bedenli_takip(double sto_bedenli_takip) {
		this.sto_bedenli_takip = sto_bedenli_takip;
	}
	public double getSto_renkDetayli() {
		return sto_renkDetayli;
	}
	public void setSto_renkDetayli(double sto_renkDetayli) {
		this.sto_renkDetayli = sto_renkDetayli;
	}
	public double getSto_miktarondalikli_fl() {
		return sto_miktarondalikli_fl;
	}
	public void setSto_miktarondalikli_fl(double sto_miktarondalikli_fl) {
		this.sto_miktarondalikli_fl = sto_miktarondalikli_fl;
	}
	public double getSto_pasif_fl() {
		return sto_pasif_fl;
	}
	public void setSto_pasif_fl(double sto_pasif_fl) {
		this.sto_pasif_fl = sto_pasif_fl;
	}
	public double getSto_eksiyedusebilir_fl() {
		return sto_eksiyedusebilir_fl;
	}
	public void setSto_eksiyedusebilir_fl(double sto_eksiyedusebilir_fl) {
		this.sto_eksiyedusebilir_fl = sto_eksiyedusebilir_fl;
	}
	public String getSto_GtipNo() {
		return sto_GtipNo;
	}
	public void setSto_GtipNo(String sto_GtipNo) {
		this.sto_GtipNo = sto_GtipNo;
	}
	public double getSto_puan() {
		return sto_puan;
	}
	public void setSto_puan(double sto_puan) {
		this.sto_puan = sto_puan;
	}
	public String getSto_komisyon_hzmkodu() {
		return sto_komisyon_hzmkodu;
	}
	public void setSto_komisyon_hzmkodu(String sto_komisyon_hzmkodu) {
		this.sto_komisyon_hzmkodu = sto_komisyon_hzmkodu;
	}
	public double getSto_komisyon_orani() {
		return sto_komisyon_orani;
	}
	public void setSto_komisyon_orani(double sto_komisyon_orani) {
		this.sto_komisyon_orani = sto_komisyon_orani;
	}
	public double getSto_otvuygulama() {
		return sto_otvuygulama;
	}
	public void setSto_otvuygulama(double sto_otvuygulama) {
		this.sto_otvuygulama = sto_otvuygulama;
	}
	public double getSto_otvtutar() {
		return sto_otvtutar;
	}
	public void setSto_otvtutar(double sto_otvtutar) {
		this.sto_otvtutar = sto_otvtutar;
	}
	public double getSto_otvliste() {
		return sto_otvliste;
	}
	public void setSto_otvliste(double sto_otvliste) {
		this.sto_otvliste = sto_otvliste;
	}
	public double getSto_otvbirimi() {
		return sto_otvbirimi;
	}
	public void setSto_otvbirimi(double sto_otvbirimi) {
		this.sto_otvbirimi = sto_otvbirimi;
	}
	public double getSto_prim_orani() {
		return sto_prim_orani;
	}
	public void setSto_prim_orani(double sto_prim_orani) {
		this.sto_prim_orani = sto_prim_orani;
	}
	public double getSto_garanti_sure() {
		return sto_garanti_sure;
	}
	public void setSto_garanti_sure(double sto_garanti_sure) {
		this.sto_garanti_sure = sto_garanti_sure;
	}
	public double getSto_garanti_sure_tipi() {
		return sto_garanti_sure_tipi;
	}
	public void setSto_garanti_sure_tipi(double sto_garanti_sure_tipi) {
		this.sto_garanti_sure_tipi = sto_garanti_sure_tipi;
	}
	public double getSto_iplik_Ne_no() {
		return sto_iplik_Ne_no;
	}
	public void setSto_iplik_Ne_no(double sto_iplik_Ne_no) {
		this.sto_iplik_Ne_no = sto_iplik_Ne_no;
	}
	public double getSto_standartmaliyet() {
		return sto_standartmaliyet;
	}
	public void setSto_standartmaliyet(double sto_standartmaliyet) {
		this.sto_standartmaliyet = sto_standartmaliyet;
	}
	public double getSto_kanban_kasa_miktari() {
		return sto_kanban_kasa_miktari;
	}
	public void setSto_kanban_kasa_miktari(double sto_kanban_kasa_miktari) {
		this.sto_kanban_kasa_miktari = sto_kanban_kasa_miktari;
	}
	public double getSto_oivuygulama() {
		return sto_oivuygulama;
	}
	public void setSto_oivuygulama(double sto_oivuygulama) {
		this.sto_oivuygulama = sto_oivuygulama;
	}
	public double getSto_zraporu_stoku_fl() {
		return sto_zraporu_stoku_fl;
	}
	public void setSto_zraporu_stoku_fl(double sto_zraporu_stoku_fl) {
		this.sto_zraporu_stoku_fl = sto_zraporu_stoku_fl;
	}
	public double getSto_maxiskonto_orani() {
		return sto_maxiskonto_orani;
	}
	public void setSto_maxiskonto_orani(double sto_maxiskonto_orani) {
		this.sto_maxiskonto_orani = sto_maxiskonto_orani;
	}
	public double getSto_detay_takibinde_depo_kontrolu_fl() {
		return sto_detay_takibinde_depo_kontrolu_fl;
	}
	public void setSto_detay_takibinde_depo_kontrolu_fl(
			double sto_detay_takibinde_depo_kontrolu_fl) {
		this.sto_detay_takibinde_depo_kontrolu_fl = sto_detay_takibinde_depo_kontrolu_fl;
	}
	public String getSto_tamamlayici_kodu() {
		return sto_tamamlayici_kodu;
	}
	public void setSto_tamamlayici_kodu(String sto_tamamlayici_kodu) {
		this.sto_tamamlayici_kodu = sto_tamamlayici_kodu;
	}
	public double getSto_oto_barkod_acma_sekli() {
		return sto_oto_barkod_acma_sekli;
	}
	public void setSto_oto_barkod_acma_sekli(double sto_oto_barkod_acma_sekli) {
		this.sto_oto_barkod_acma_sekli = sto_oto_barkod_acma_sekli;
	}
	public String getSto_oto_barkod_kod_yapisi() {
		return sto_oto_barkod_kod_yapisi;
	}
	public void setSto_oto_barkod_kod_yapisi(String sto_oto_barkod_kod_yapisi) {
		this.sto_oto_barkod_kod_yapisi = sto_oto_barkod_kod_yapisi;
	}
	public double getSto_KasaIskontoOrani() {
		return sto_KasaIskontoOrani;
	}
	public void setSto_KasaIskontoOrani(double sto_KasaIskontoOrani) {
		this.sto_KasaIskontoOrani = sto_KasaIskontoOrani;
	}
	public double getSto_KasaIskontoTutari() {
		return sto_KasaIskontoTutari;
	}
	public void setSto_KasaIskontoTutari(double sto_KasaIskontoTutari) {
		this.sto_KasaIskontoTutari = sto_KasaIskontoTutari;
	}
	public double getSto_gelirpayi() {
		return sto_gelirpayi;
	}
	public void setSto_gelirpayi(double sto_gelirpayi) {
		this.sto_gelirpayi = sto_gelirpayi;
	}
	public double getSto_oivtutar() {
		return sto_oivtutar;
	}
	public void setSto_oivtutar(double sto_oivtutar) {
		this.sto_oivtutar = sto_oivtutar;
	}
	public double getSto_oivturu() {
		return sto_oivturu;
	}
	public void setSto_oivturu(double sto_oivturu) {
		this.sto_oivturu = sto_oivturu;
	}
	public String getSto_giderkodu() {
		return sto_giderkodu;
	}
	public void setSto_giderkodu(String sto_giderkodu) {
		this.sto_giderkodu = sto_giderkodu;
	}
	public double getSto_oivvergipntr() {
		return sto_oivvergipntr;
	}
	public void setSto_oivvergipntr(double sto_oivvergipntr) {
		this.sto_oivvergipntr = sto_oivvergipntr;
	}
	public double getSto_Tevkifat_turu() {
		return sto_Tevkifat_turu;
	}
	public void setSto_Tevkifat_turu(double sto_Tevkifat_turu) {
		this.sto_Tevkifat_turu = sto_Tevkifat_turu;
	}
	public double getSto_SKT_fl() {
		return sto_SKT_fl;
	}
	public void setSto_SKT_fl(double sto_SKT_fl) {
		this.sto_SKT_fl = sto_SKT_fl;
	}
	public double getSto_terazi_SKT() {
		return sto_terazi_SKT;
	}
	public void setSto_terazi_SKT(double sto_terazi_SKT) {
		this.sto_terazi_SKT = sto_terazi_SKT;
	}
	public double getSto_RafOmru() {
		return sto_RafOmru;
	}
	public void setSto_RafOmru(double sto_RafOmru) {
		this.sto_RafOmru = sto_RafOmru;
	}
	public double getSto_KasadaTaksitlenebilir_fl() {
		return sto_KasadaTaksitlenebilir_fl;
	}
	public void setSto_KasadaTaksitlenebilir_fl(double sto_KasadaTaksitlenebilir_fl) {
		this.sto_KasadaTaksitlenebilir_fl = sto_KasadaTaksitlenebilir_fl;
	}
	public String getSto_ufrsfark_kod() {
		return sto_ufrsfark_kod;
	}
	public void setSto_ufrsfark_kod(String sto_ufrsfark_kod) {
		this.sto_ufrsfark_kod = sto_ufrsfark_kod;
	}
	public String getSto_iade_ufrsfark_kod() {
		return sto_iade_ufrsfark_kod;
	}
	public void setSto_iade_ufrsfark_kod(String sto_iade_ufrsfark_kod) {
		this.sto_iade_ufrsfark_kod = sto_iade_ufrsfark_kod;
	}
	public String getSto_yurticisat_ufrsfark_kod() {
		return sto_yurticisat_ufrsfark_kod;
	}
	public void setSto_yurticisat_ufrsfark_kod(String sto_yurticisat_ufrsfark_kod) {
		this.sto_yurticisat_ufrsfark_kod = sto_yurticisat_ufrsfark_kod;
	}
	public String getSto_satiade_ufrsfark_kod() {
		return sto_satiade_ufrsfark_kod;
	}
	public void setSto_satiade_ufrsfark_kod(String sto_satiade_ufrsfark_kod) {
		this.sto_satiade_ufrsfark_kod = sto_satiade_ufrsfark_kod;
	}
	public String getSto_satisk_ufrsfark_kod() {
		return sto_satisk_ufrsfark_kod;
	}
	public void setSto_satisk_ufrsfark_kod(String sto_satisk_ufrsfark_kod) {
		this.sto_satisk_ufrsfark_kod = sto_satisk_ufrsfark_kod;
	}
	public String getSto_alisk_ufrsfark_kod() {
		return sto_alisk_ufrsfark_kod;
	}
	public void setSto_alisk_ufrsfark_kod(String sto_alisk_ufrsfark_kod) {
		this.sto_alisk_ufrsfark_kod = sto_alisk_ufrsfark_kod;
	}
	public String getSto_satmal_ufrsfark_kod() {
		return sto_satmal_ufrsfark_kod;
	}
	public void setSto_satmal_ufrsfark_kod(String sto_satmal_ufrsfark_kod) {
		this.sto_satmal_ufrsfark_kod = sto_satmal_ufrsfark_kod;
	}
	public String getSto_yurtdisisat_ufrsfark_kod() {
		return sto_yurtdisisat_ufrsfark_kod;
	}
	public void setSto_yurtdisisat_ufrsfark_kod(String sto_yurtdisisat_ufrsfark_kod) {
		this.sto_yurtdisisat_ufrsfark_kod = sto_yurtdisisat_ufrsfark_kod;
	}
	public String getSto_ilavemas_ufrsfark_kod() {
		return sto_ilavemas_ufrsfark_kod;
	}
	public void setSto_ilavemas_ufrsfark_kod(String sto_ilavemas_ufrsfark_kod) {
		this.sto_ilavemas_ufrsfark_kod = sto_ilavemas_ufrsfark_kod;
	}
	public String getSto_yatirimtes_ufrsfark_kod() {
		return sto_yatirimtes_ufrsfark_kod;
	}
	public void setSto_yatirimtes_ufrsfark_kod(String sto_yatirimtes_ufrsfark_kod) {
		this.sto_yatirimtes_ufrsfark_kod = sto_yatirimtes_ufrsfark_kod;
	}
	public String getSto_depsat_ufrsfark_kod() {
		return sto_depsat_ufrsfark_kod;
	}
	public void setSto_depsat_ufrsfark_kod(String sto_depsat_ufrsfark_kod) {
		this.sto_depsat_ufrsfark_kod = sto_depsat_ufrsfark_kod;
	}
	public String getSto_depsatmal_ufrsfark_kod() {
		return sto_depsatmal_ufrsfark_kod;
	}
	public void setSto_depsatmal_ufrsfark_kod(String sto_depsatmal_ufrsfark_kod) {
		this.sto_depsatmal_ufrsfark_kod = sto_depsatmal_ufrsfark_kod;
	}
	public String getSto_bagortsat_ufrsfark_kod() {
		return sto_bagortsat_ufrsfark_kod;
	}
	public void setSto_bagortsat_ufrsfark_kod(String sto_bagortsat_ufrsfark_kod) {
		this.sto_bagortsat_ufrsfark_kod = sto_bagortsat_ufrsfark_kod;
	}
	public String getSto_bagortsatiade_ufrsfark_kod() {
		return sto_bagortsatiade_ufrsfark_kod;
	}
	public void setSto_bagortsatiade_ufrsfark_kod(
			String sto_bagortsatiade_ufrsfark_kod) {
		this.sto_bagortsatiade_ufrsfark_kod = sto_bagortsatiade_ufrsfark_kod;
	}
	public String getSto_bagortsatisk_ufrsfark_kod() {
		return sto_bagortsatisk_ufrsfark_kod;
	}
	public void setSto_bagortsatisk_ufrsfark_kod(
			String sto_bagortsatisk_ufrsfark_kod) {
		this.sto_bagortsatisk_ufrsfark_kod = sto_bagortsatisk_ufrsfark_kod;
	}
	public String getSto_satfiyfark_ufrsfark_kod() {
		return sto_satfiyfark_ufrsfark_kod;
	}
	public void setSto_satfiyfark_ufrsfark_kod(String sto_satfiyfark_ufrsfark_kod) {
		this.sto_satfiyfark_ufrsfark_kod = sto_satfiyfark_ufrsfark_kod;
	}
	public String getSto_yurtdisisatmal_ufrsfark_kod() {
		return sto_yurtdisisatmal_ufrsfark_kod;
	}
	public void setSto_yurtdisisatmal_ufrsfark_kod(
			String sto_yurtdisisatmal_ufrsfark_kod) {
		this.sto_yurtdisisatmal_ufrsfark_kod = sto_yurtdisisatmal_ufrsfark_kod;
	}
	public String getSto_bagortsatmal_ufrsfark_kod() {
		return sto_bagortsatmal_ufrsfark_kod;
	}
	public void setSto_bagortsatmal_ufrsfark_kod(
			String sto_bagortsatmal_ufrsfark_kod) {
		this.sto_bagortsatmal_ufrsfark_kod = sto_bagortsatmal_ufrsfark_kod;
	}
	public String getSto_uretimmaliyet_ufrsfark_kod() {
		return sto_uretimmaliyet_ufrsfark_kod;
	}
	public void setSto_uretimmaliyet_ufrsfark_kod(
			String sto_uretimmaliyet_ufrsfark_kod) {
		this.sto_uretimmaliyet_ufrsfark_kod = sto_uretimmaliyet_ufrsfark_kod;
	}
	public String getSto_uretimkapasite_ufrsfark_kod() {
		return sto_uretimkapasite_ufrsfark_kod;
	}
	public void setSto_uretimkapasite_ufrsfark_kod(
			String sto_uretimkapasite_ufrsfark_kod) {
		this.sto_uretimkapasite_ufrsfark_kod = sto_uretimkapasite_ufrsfark_kod;
	}
	public String getSto_degerdusuklugu_ufrs_kod() {
		return sto_degerdusuklugu_ufrs_kod;
	}
	public void setSto_degerdusuklugu_ufrs_kod(String sto_degerdusuklugu_ufrs_kod) {
		this.sto_degerdusuklugu_ufrs_kod = sto_degerdusuklugu_ufrs_kod;
	}
	public double getSto_halrusumyudesi() {
		return sto_halrusumyudesi;
	}
	public void setSto_halrusumyudesi(double sto_halrusumyudesi) {
		this.sto_halrusumyudesi = sto_halrusumyudesi;
	}
	public double getSto_webe_gonderilecek_fl() {
		return sto_webe_gonderilecek_fl;
	}
	public void setSto_webe_gonderilecek_fl(double sto_webe_gonderilecek_fl) {
		this.sto_webe_gonderilecek_fl = sto_webe_gonderilecek_fl;
	}
	public double getSto_min_stok_belirleme_gun() {
		return sto_min_stok_belirleme_gun;
	}
	public void setSto_min_stok_belirleme_gun(double sto_min_stok_belirleme_gun) {
		this.sto_min_stok_belirleme_gun = sto_min_stok_belirleme_gun;
	}
	public double getSto_sip_stok_belirleme_gun() {
		return sto_sip_stok_belirleme_gun;
	}
	public void setSto_sip_stok_belirleme_gun(double sto_sip_stok_belirleme_gun) {
		this.sto_sip_stok_belirleme_gun = sto_sip_stok_belirleme_gun;
	}
	public double getSto_max_stok_belirleme_gun() {
		return sto_max_stok_belirleme_gun;
	}
	public void setSto_max_stok_belirleme_gun(double sto_max_stok_belirleme_gun) {
		this.sto_max_stok_belirleme_gun = sto_max_stok_belirleme_gun;
	}
	public double getSto_sev_bel_opr_degerlendime_fl() {
		return sto_sev_bel_opr_degerlendime_fl;
	}
	public void setSto_sev_bel_opr_degerlendime_fl(
			double sto_sev_bel_opr_degerlendime_fl) {
		this.sto_sev_bel_opr_degerlendime_fl = sto_sev_bel_opr_degerlendime_fl;
	}
	public String getCari_unvan1() {
		return cari_unvan1;
	}
	public void setCari_unvan1(String cari_unvan1) {
		this.cari_unvan1 = cari_unvan1;
	}
	public String getTkl_stok_isim() {
		tkl_stok_isim = null;
		return sto_isim;
	}
	
}
