package com.biftekyazilim.hunkargida.vo;

public class SiparisVO {

	private int cari_odemeplan_no;
	private String cari_unvan1;
	private int sip_RECno;
	private int sip_RECid_DBCno;
	private int sip_RECid_RECno;
	private int sip_SpecRECno;
	private int sip_iptal;
	private int sip_fileid;
	private int sip_hidden;
	private int sip_kilitli;
	private int sip_degisti;
	private int sip_checksum;
	private int sip_create_user;
	private TimeAndZoneVO sip_create_date;
	private int sip_lastup_user;
	private TimeAndZoneVO sip_lastup_date;
	private String sip_special1;
	private String sip_special2;
	private String sip_special3;
	private int sip_firmano;
	private int sip_subeno;
	private TimeAndZoneVO sip_tarih;
	private TimeAndZoneVO sip_teslim_tarih;
	private int sip_tip;
	private int sip_cins;
	private String sip_evrakno_seri;
	private int sip_evrakno_sira;
	private int sip_satirno;
	private String sip_belgeno;
	private TimeAndZoneVO sip_belge_tarih;
	private String sip_satici_kod;
	private String sip_musteri_kod;
	private String sip_stok_kod;
	private double sip_b_fiyat;
	private double sip_miktar;
	private int sip_birim_pntr;
	private double sip_teslim_miktar;
	private double sip_tutar;
	private double sip_iskonto_1;
	private double sip_iskonto_2;
	private double sip_iskonto_3;
	private double sip_iskonto_4;
	private double sip_iskonto_5;
	private double sip_iskonto_6;
	private double sip_masraf_1;
	private double sip_masraf_2;
	private double sip_masraf_3;
	private double sip_masraf_4;
	private int sip_vergi_pntr;
	private double sip_vergi;
	private double sip_masvergi_pntr;
	private double sip_masvergi;
	private int sip_opno;
	private String sip_aciklama;
	private String sip_aciklama2;
	private int sip_depono;
	private double sip_OnaylayanKulNo;
	private double sip_vergisiz_fl;
	private double sip_kapat_fl;
	private double sip_promosyon_fl;
	private String sip_cari_sormerk;
	private String sip_stok_sormerk;
	private double sip_cari_grupno;
	private double sip_doviz_cinsi;
	private double sip_doviz_kuru;
	private double sip_alt_doviz_kuru;
	private int sip_adresno;
	private String sip_teslimturu;
	private double sip_cagrilabilir_fl;
	private double sip_prosiprecDbId;
	private double sip_prosiprecrecI;
	private double sip_iskonto1;
	private double sip_iskonto2;
	private double sip_iskonto3;
	private double sip_iskonto4;
	private double sip_iskonto5;
	private double sip_iskonto6;
	private double sip_masraf1;
	private double sip_masraf2;
	private double sip_masraf3;
	private double sip_masraf4;
	private double sip_isk1;
	private double sip_isk2;
	private double sip_isk3;
	private double sip_isk4;
	private double sip_isk5;
	private double sip_isk6;
	private double sip_mas1;
	private double sip_mas2;
	private double sip_mas3;
	private double sip_mas4;
	private String sip_Exp_Imp_Kodu;
	private double sip_kar_orani;
	private double sip_durumu;
	private int sip_stalRecId_DBCno;
	private int sip_stalRecId_RECno;
	private int sip_planlananmiktar;
	private int sip_teklifRecId_DBCno;
	private int sip_teklifRecId_RECno;
	private String sip_parti_kodu;
	private double sip_lot_no;
	private String sip_projekodu;
	private double sip_fiyat_liste_no;
	private double sip_Otv_Pntr;
	private double sip_Otv_Vergi;
	private double sip_otvtutari;
	private double sip_OtvVergisiz_Fl;
	private String sip_paket_kod;
	private double sip_RezRecId_DBCno;
	private double sip_RezRecId_RECno;
	private int sip_harekettipi;
	private double sip_yetkili_recid_dbcno;
	private double sip_yetkili_recid_recno;
	private String sip_kapatmanedenkod;
	private TimeAndZoneVO sip_gecerlilik_tarihi;
	private double sip_onodeme_evrak_tip;
	private String sip_onodeme_evrak_seri;
	private double sip_onodeme_evrak_sira;
	private double sip_rezervasyon_miktari;
	private double sip_rezerveden_teslim_edilen;
	private String sip_stok_isim;
	
	private int sto_RECno;
	private double sto_RECid_DBCno;
	private double sto_RECid_RECno;
	private double sto_SpecRECno;
	private int sto_iptal;
	private double sto_fileid;
	private double sto_hidden;
	private double sto_kilitli;
	private double sto_degisti;
	private int sto_checksum;
	private double sto_create_user;
	private TimeAndZoneVO sto_create_date;
	private double sto_lastup_user;
	private TimeAndZoneVO sto_lastup_date;
	private String sto_special1;
	private String sto_special2;
	private String sto_special3;
	private String sto_kod;
	private String sto_isim;
	private String sto_kisa_ismi;
	private String sto_yabanci_isim;
	private String sto_sat_cari_kod;
	private double sto_cins;
	private double sto_doviz_cinsi;
	private double sto_detay_takip;
	private String sto_birim1_ad;
	private double sto_birim1_katsayi;
	private double sto_birim1_agirlik;
	private double sto_birim1_en;
	private double sto_birim1_boy;
	private double sto_birim1_yukseklik;
	private double sto_birim1_dara;
	private String sto_birim2_ad;
	private double sto_birim2_katsayi;
	private double sto_birim2_agirlik;
	private double sto_birim2_en;
	private double sto_birim2_boy;
	private double sto_birim2_yukseklik;
	private double sto_birim2_dara;
	private String sto_birim3_ad;
	private double sto_birim3_katsayi;
	private double sto_birim3_agirlik;
	private double sto_birim3_en;
	private double sto_birim3_boy;
	private double sto_birim3_yukseklik;
	private double sto_birim3_dara;
	private String sto_birim4_ad;
	private double sto_birim4_katsayi;
	private double sto_birim4_agirlik;
	private double sto_birim4_en;
	private double sto_birim4_boy;
	private double sto_birim4_yukseklik;
	private double sto_birim4_dara;
	private String sto_muh_kod;
	private String sto_muh_Iade_kod;
	private String sto_muh_sat_muh_kod;
	private String sto_muh_satIadmuhkod;
	private String sto_muh_sat_isk_kod;
	private String sto_muh_aIiskmuhkod;
	private String sto_muh_satmalmuhkod;
	private String sto_yurtdisi_satmuhk;
	private String sto_ilavemasmuhkod;
	private String sto_yatirimtesmuhkod;
	private String sto_depsatmuhkod;
	private String sto_depsatmalmuhkod;
	private String sto_bagortsatmuhkod;
	private String sto_bagortsatIadmuhkod;
	private String sto_bagortsatIskmuhkod;
	private String sto_satfiyfarkmuhkod;
	private String sto_yurtdisisatmalmuhkod;
	private String sto_bagortsatmalmuhkod;
	private double sto_karorani;
	private double sto_min_stok;
	private double sto_siparis_stok;
	private double sto_max_stok;
	private double sto_ver_sip_birim;
	private double sto_al_sip_birim;
	private double sto_siparis_sure;
	private double sto_perakende_vergi;
	private double sto_toptan_vergi;
	private String sto_yer_kod;
	private double sto_elk_etk_tipi;
	private double sto_raf_etiketli;
	private double sto_etiket_bas;
	private double sto_satis_dursun;
	private double sto_siparis_dursun;
	private double sto_malkabul_dursun;
	private double sto_malkabul_gun1;
	private double sto_malkabul_gun2;
	private double sto_malkabul_gun3;
	private double sto_malkabul_gun4;
	private double sto_malkabul_gun5;
	private double sto_malkabul_gun6;
	private double sto_malkabul_gun7;
	private double sto_siparis_gun1;
	private double sto_siparis_gun2;
	private double sto_siparis_gun3;
	private double sto_siparis_gun4;
	private double sto_siparis_gun5;
	private double sto_siparis_gun6;
	private double sto_siparis_gun7;
	private double sto_iskon_yapilamaz;
	private double sto_tasfiyede;
	private double sto_alt_grup_no;
	private String sto_kategori_kodu;
	private String sto_urun_sorkod;
	private String sto_altgrup_kod;
	private String sto_anagrup_kod;
	private String sto_uretici_kodu;
	private String sto_sektor_kodu;
	private String sto_reyon_kodu;
	private String sto_muhgrup_kodu;
	private String sto_ambalaj_kodu;
	private String sto_marka_kodu;
	private String sto_beden_kodu;
	private String sto_renk_kodu;
	private String sto_model_kodu;
	private String sto_sezon_kodu;
	private String sto_hammadde_kodu;
	private String sto_prim_kodu;
	private String sto_kalkon_kodu;
	private String sto_paket_kodu;
	private String sto_pozisyonbayrak_kodu;
	private String sto_mkod_artik;
	private double sto_kasa_tarti_fl;
	private double sto_bedenli_takip;
	private double sto_renkDetayli;
	private double sto_miktarondalikli_fl;
	private double sto_pasif_fl;
	private double sto_eksiyedusebilir_fl;
	private String sto_GtipNo;
	private double sto_puan;
	private String sto_komisyon_hzmkodu;
	private double sto_komisyon_orani;
	private double sto_otvuygulama;
	private double sto_otvtutar;
	private double sto_otvliste;
	private double sto_otvbirimi;
	private double sto_prim_orani;
	private double sto_garanti_sure;
	private double sto_garanti_sure_tipi;
	private double sto_iplik_Ne_no;
	private double sto_standartmaliyet;
	private double sto_kanban_kasa_miktari;
	private double sto_oivuygulama;
	private double sto_zraporu_stoku_fl;
	private double sto_maxiskonto_orani;
	private double sto_detay_takibinde_depo_kontrolu_fl;
	private String sto_tamamlayici_kodu;
	private double sto_oto_barkod_acma_sekli;
	private String sto_oto_barkod_kod_yapisi;
	private double sto_KasaIskontoOrani;
	private double sto_KasaIskontoTutari;
	private double sto_gelirpayi;
	private double sto_oivtutar;
	private double sto_oivturu;
	private String sto_giderkodu;
	private double sto_oivvergipntr;
	private double sto_Tevkifat_turu;
	private double sto_SKT_fl;
	private double sto_terazi_SKT;
	private double sto_RafOmru;
	private double sto_KasadaTaksitlenebilir_fl;
	private String sto_ufrsfark_kod;
	private String sto_iade_ufrsfark_kod;
	private String sto_yurticisat_ufrsfark_kod;
	private String sto_satiade_ufrsfark_kod;
	private String sto_satisk_ufrsfark_kod;
	private String sto_alisk_ufrsfark_kod;
	private String sto_satmal_ufrsfark_kod;
	private String sto_yurtdisisat_ufrsfark_kod;
	private String sto_ilavemas_ufrsfark_kod;
	private String sto_yatirimtes_ufrsfark_kod;
	private String sto_depsat_ufrsfark_kod;
	private String sto_depsatmal_ufrsfark_kod;
	private String sto_bagortsat_ufrsfark_kod;
	private String sto_bagortsatiade_ufrsfark_kod;
	private String sto_bagortsatisk_ufrsfark_kod;
	private String sto_satfiyfark_ufrsfark_kod;
	private String sto_yurtdisisatmal_ufrsfark_kod;
	private String sto_bagortsatmal_ufrsfark_kod;
	private String sto_uretimmaliyet_ufrsfark_kod;
	private String sto_uretimkapasite_ufrsfark_kod;
	private String sto_degerdusuklugu_ufrs_kod;
	private double sto_halrusumyudesi;
	private double sto_webe_gonderilecek_fl;
	private double sto_min_stok_belirleme_gun;
	private double sto_sip_stok_belirleme_gun;
	private double sto_max_stok_belirleme_gun;
	private double sto_sev_bel_opr_degerlendime_fl;
	
	
	private String adr_cadde;
	private String adr_sokak;
	private String adr_ilce;
	private String adr_il;
	
	public int getSip_RECno() {
		return sip_RECno;
	}
	public void setSip_RECno(int sip_RECno) {
		this.sip_RECno = sip_RECno;
	}
	public int getSip_RECid_DBCno() {
		return sip_RECid_DBCno;
	}
	public void setSip_RECid_DBCno(int sip_RECid_DBCno) {
		this.sip_RECid_DBCno = sip_RECid_DBCno;
	}
	public int getSip_RECid_RECno() {
		return sip_RECid_RECno;
	}
	public void setSip_RECid_RECno(int sip_RECid_RECno) {
		this.sip_RECid_RECno = sip_RECid_RECno;
	}
	public int getSip_SpecRECno() {
		return sip_SpecRECno;
	}
	public void setSip_SpecRECno(int sip_SpecRECno) {
		this.sip_SpecRECno = sip_SpecRECno;
	}
	public int getSip_iptal() {
		return sip_iptal;
	}
	public void setSip_iptal(int sip_iptal) {
		this.sip_iptal = sip_iptal;
	}
	public int getSip_fileid() {
		return sip_fileid;
	}
	public void setSip_fileid(int sip_fileid) {
		this.sip_fileid = sip_fileid;
	}
	public int getSip_hidden() {
		return sip_hidden;
	}
	public void setSip_hidden(int sip_hidden) {
		this.sip_hidden = sip_hidden;
	}
	public int getSip_kilitli() {
		return sip_kilitli;
	}
	public void setSip_kilitli(int sip_kilitli) {
		this.sip_kilitli = sip_kilitli;
	}
	public int getSip_degisti() {
		return sip_degisti;
	}
	public void setSip_degisti(int sip_degisti) {
		this.sip_degisti = sip_degisti;
	}
	public int getSip_checksum() {
		return sip_checksum;
	}
	public void setSip_checksum(int sip_checksum) {
		this.sip_checksum = sip_checksum;
	}
	public int getSip_create_user() {
		return sip_create_user;
	}
	public void setSip_create_user(int sip_create_user) {
		this.sip_create_user = sip_create_user;
	}
	public TimeAndZoneVO getSip_create_date() {
		return sip_create_date;
	}
	public void setSip_create_date(TimeAndZoneVO sip_create_date) {
		this.sip_create_date = sip_create_date;
	}
	public int getSip_lastup_user() {
		return sip_lastup_user;
	}
	public void setSip_lastup_user(int sip_lastup_user) {
		this.sip_lastup_user = sip_lastup_user;
	}
	public TimeAndZoneVO getSip_lastup_date() {
		return sip_lastup_date;
	}
	public void setSip_lastup_date(TimeAndZoneVO sip_lastup_date) {
		this.sip_lastup_date = sip_lastup_date;
	}
	public String getSip_special1() {
		return sip_special1;
	}
	public void setSip_special1(String sip_special1) {
		this.sip_special1 = sip_special1;
	}
	public String getSip_special2() {
		return sip_special2;
	}
	public void setSip_special2(String sip_special2) {
		this.sip_special2 = sip_special2;
	}
	public String getSip_special3() {
		return sip_special3;
	}
	public void setSip_special3(String sip_special3) {
		this.sip_special3 = sip_special3;
	}
	public int getSip_firmano() {
		return sip_firmano;
	}
	public void setSip_firmano(int sip_firmano) {
		this.sip_firmano = sip_firmano;
	}
	public int getSip_subeno() {
		return sip_subeno;
	}
	public void setSip_subeno(int sip_subeno) {
		this.sip_subeno = sip_subeno;
	}
	public TimeAndZoneVO getSip_tarih() {
		return sip_tarih;
	}
	public void setSip_tarih(TimeAndZoneVO sip_tarih) {
		this.sip_tarih = sip_tarih;
	}
	public TimeAndZoneVO getSip_teslim_tarih() {
		return sip_teslim_tarih;
	}
	public void setSip_teslim_tarih(TimeAndZoneVO sip_teslim_tarih) {
		this.sip_teslim_tarih = sip_teslim_tarih;
	}
	public int getSip_tip() {
		return sip_tip;
	}
	public void setSip_tip(int sip_tip) {
		this.sip_tip = sip_tip;
	}
	public int getSip_cins() {
		return sip_cins;
	}
	public void setSip_cins(int sip_cins) {
		this.sip_cins = sip_cins;
	}
	public String getSip_evrakno_seri() {
		return sip_evrakno_seri;
	}
	public void setSip_evrakno_seri(String sip_evrakno_seri) {
		this.sip_evrakno_seri = sip_evrakno_seri;
	}
	public int getSip_evrakno_sira() {
		return sip_evrakno_sira;
	}
	public void setSip_evrakno_sira(int sip_evrakno_sira) {
		this.sip_evrakno_sira = sip_evrakno_sira;
	}
	public int getSip_satirno() {
		return sip_satirno;
	}
	public void setSip_satirno(int sip_satirno) {
		this.sip_satirno = sip_satirno;
	}
	public String getSip_belgeno() {
		return sip_belgeno;
	}
	public void setSip_belgeno(String sip_belgeno) {
		this.sip_belgeno = sip_belgeno;
	}
	public TimeAndZoneVO getSip_belge_tarih() {
		return sip_belge_tarih;
	}
	public void setSip_belge_tarih(TimeAndZoneVO sip_belge_tarih) {
		this.sip_belge_tarih = sip_belge_tarih;
	}
	public String getSip_satici_kod() {
		return sip_satici_kod;
	}
	public void setSip_satici_kod(String sip_satici_kod) {
		this.sip_satici_kod = sip_satici_kod;
	}
	public String getSip_musteri_kod() {
		return sip_musteri_kod;
	}
	public void setSip_musteri_kod(String sip_musteri_kod) {
		this.sip_musteri_kod = sip_musteri_kod;
	}
	public String getSip_stok_kod() {
		return sip_stok_kod;
	}
	public void setSip_stok_kod(String sip_stok_kod) {
		this.sip_stok_kod = sip_stok_kod;
	}
	public double getSip_b_fiyat() {
		return sip_b_fiyat;
	}
	public void setSip_b_fiyat(double sip_b_fiyat) {
		this.sip_b_fiyat = sip_b_fiyat;
	}
	public double getSip_miktar() {
		return sip_miktar;
	}
	public void setSip_miktar(double sip_miktar) {
		this.sip_miktar = sip_miktar;
	}
	public int getSip_birim_pntr() {
		return sip_birim_pntr;
	}
	public void setSip_birim_pntr(int sip_birim_pntr) {
		this.sip_birim_pntr = sip_birim_pntr;
	}
	public double getSip_teslim_miktar() {
		return sip_teslim_miktar;
	}
	public void setSip_teslim_miktar(double sip_teslim_miktar) {
		this.sip_teslim_miktar = sip_teslim_miktar;
	}
	public double getSip_tutar() {
		return sip_tutar;
	}
	public void setSip_tutar(double sip_tutar) {
		this.sip_tutar = sip_tutar;
	}
	public double getSip_iskonto_1() {
		return sip_iskonto_1;
	}
	public void setSip_iskonto_1(double sip_iskonto_1) {
		this.sip_iskonto_1 = sip_iskonto_1;
	}
	public double getSip_iskonto_2() {
		return sip_iskonto_2;
	}
	public void setSip_iskonto_2(double sip_iskonto_2) {
		this.sip_iskonto_2 = sip_iskonto_2;
	}
	public double getSip_iskonto_3() {
		return sip_iskonto_3;
	}
	public void setSip_iskonto_3(double sip_iskonto_3) {
		this.sip_iskonto_3 = sip_iskonto_3;
	}
	public double getSip_iskonto_4() {
		return sip_iskonto_4;
	}
	public void setSip_iskonto_4(double sip_iskonto_4) {
		this.sip_iskonto_4 = sip_iskonto_4;
	}
	public double getSip_iskonto_5() {
		return sip_iskonto_5;
	}
	public void setSip_iskonto_5(double sip_iskonto_5) {
		this.sip_iskonto_5 = sip_iskonto_5;
	}
	public double getSip_iskonto_6() {
		return sip_iskonto_6;
	}
	public void setSip_iskonto_6(double sip_iskonto_6) {
		this.sip_iskonto_6 = sip_iskonto_6;
	}
	public double getSip_masraf_1() {
		return sip_masraf_1;
	}
	public void setSip_masraf_1(double sip_masraf_1) {
		this.sip_masraf_1 = sip_masraf_1;
	}
	public double getSip_masraf_2() {
		return sip_masraf_2;
	}
	public void setSip_masraf_2(double sip_masraf_2) {
		this.sip_masraf_2 = sip_masraf_2;
	}
	public double getSip_masraf_3() {
		return sip_masraf_3;
	}
	public void setSip_masraf_3(double sip_masraf_3) {
		this.sip_masraf_3 = sip_masraf_3;
	}
	public double getSip_masraf_4() {
		return sip_masraf_4;
	}
	public void setSip_masraf_4(double sip_masraf_4) {
		this.sip_masraf_4 = sip_masraf_4;
	}
	public int getSip_vergi_pntr() {
		return sip_vergi_pntr;
	}
	public void setSip_vergi_pntr(int sip_vergi_pntr) {
		this.sip_vergi_pntr = sip_vergi_pntr;
	}
	public double getSip_vergi() {
		return sip_vergi;
	}
	public void setSip_vergi(double sip_vergi) {
		this.sip_vergi = sip_vergi;
	}
	public double getSip_masvergi_pntr() {
		return sip_masvergi_pntr;
	}
	public void setSip_masvergi_pntr(double sip_masvergi_pntr) {
		this.sip_masvergi_pntr = sip_masvergi_pntr;
	}
	public double getSip_masvergi() {
		return sip_masvergi;
	}
	public void setSip_masvergi(double sip_masvergi) {
		this.sip_masvergi = sip_masvergi;
	}
	public int getSip_opno() {
		return sip_opno;
	}
	public void setSip_opno(int sip_opno) {
		this.sip_opno = sip_opno;
	}
	public String getSip_aciklama() {
		return sip_aciklama;
	}
	public void setSip_aciklama(String sip_aciklama) {
		this.sip_aciklama = sip_aciklama;
	}
	public String getSip_aciklama2() {
		return sip_aciklama2;
	}
	public void setSip_aciklama2(String sip_aciklama2) {
		this.sip_aciklama2 = sip_aciklama2;
	}
	public int getSip_depono() {
		return sip_depono;
	}
	public void setSip_depono(int sip_depono) {
		this.sip_depono = sip_depono;
	}
	public double getSip_OnaylayanKulNo() {
		return sip_OnaylayanKulNo;
	}
	public void setSip_OnaylayanKulNo(double sip_OnaylayanKulNo) {
		this.sip_OnaylayanKulNo = sip_OnaylayanKulNo;
	}
	public double getSip_vergisiz_fl() {
		return sip_vergisiz_fl;
	}
	public void setSip_vergisiz_fl(double sip_vergisiz_fl) {
		this.sip_vergisiz_fl = sip_vergisiz_fl;
	}
	public double getSip_kapat_fl() {
		return sip_kapat_fl;
	}
	public void setSip_kapat_fl(double sip_kapat_fl) {
		this.sip_kapat_fl = sip_kapat_fl;
	}
	public double getSip_promosyon_fl() {
		return sip_promosyon_fl;
	}
	public void setSip_promosyon_fl(double sip_promosyon_fl) {
		this.sip_promosyon_fl = sip_promosyon_fl;
	}
	public String getSip_cari_sormerk() {
		return sip_cari_sormerk;
	}
	public void setSip_cari_sormerk(String sip_cari_sormerk) {
		this.sip_cari_sormerk = sip_cari_sormerk;
	}
	public String getSip_stok_sormerk() {
		return sip_stok_sormerk;
	}
	public void setSip_stok_sormerk(String sip_stok_sormerk) {
		this.sip_stok_sormerk = sip_stok_sormerk;
	}
	public double getSip_cari_grupno() {
		return sip_cari_grupno;
	}
	public void setSip_cari_grupno(double sip_cari_grupno) {
		this.sip_cari_grupno = sip_cari_grupno;
	}
	public double getSip_doviz_cinsi() {
		return sip_doviz_cinsi;
	}
	public void setSip_doviz_cinsi(double sip_doviz_cinsi) {
		this.sip_doviz_cinsi = sip_doviz_cinsi;
	}
	public double getSip_doviz_kuru() {
		return sip_doviz_kuru;
	}
	public void setSip_doviz_kuru(double sip_doviz_kuru) {
		this.sip_doviz_kuru = sip_doviz_kuru;
	}
	public double getSip_alt_doviz_kuru() {
		return sip_alt_doviz_kuru;
	}
	public void setSip_alt_doviz_kuru(double sip_alt_doviz_kuru) {
		this.sip_alt_doviz_kuru = sip_alt_doviz_kuru;
	}
	public int getSip_adresno() {
		return sip_adresno;
	}
	public void setSip_adresno(int sip_adresno) {
		this.sip_adresno = sip_adresno;
	}
	public String getSip_teslimturu() {
		return sip_teslimturu;
	}
	public void setSip_teslimturu(String sip_teslimturu) {
		this.sip_teslimturu = sip_teslimturu;
	}
	public double getSip_cagrilabilir_fl() {
		return sip_cagrilabilir_fl;
	}
	public void setSip_cagrilabilir_fl(double sip_cagrilabilir_fl) {
		this.sip_cagrilabilir_fl = sip_cagrilabilir_fl;
	}
	public double getSip_prosiprecDbId() {
		return sip_prosiprecDbId;
	}
	public void setSip_prosiprecDbId(double sip_prosiprecDbId) {
		this.sip_prosiprecDbId = sip_prosiprecDbId;
	}
	public double getSip_prosiprecrecI() {
		return sip_prosiprecrecI;
	}
	public void setSip_prosiprecrecI(double sip_prosiprecrecI) {
		this.sip_prosiprecrecI = sip_prosiprecrecI;
	}
	public double getSip_iskonto1() {
		return sip_iskonto1;
	}
	public void setSip_iskonto1(double sip_iskonto1) {
		this.sip_iskonto1 = sip_iskonto1;
	}
	public double getSip_iskonto2() {
		return sip_iskonto2;
	}
	public void setSip_iskonto2(double sip_iskonto2) {
		this.sip_iskonto2 = sip_iskonto2;
	}
	public double getSip_iskonto3() {
		return sip_iskonto3;
	}
	public void setSip_iskonto3(double sip_iskonto3) {
		this.sip_iskonto3 = sip_iskonto3;
	}
	public double getSip_iskonto4() {
		return sip_iskonto4;
	}
	public void setSip_iskonto4(double sip_iskonto4) {
		this.sip_iskonto4 = sip_iskonto4;
	}
	public double getSip_iskonto5() {
		return sip_iskonto5;
	}
	public void setSip_iskonto5(double sip_iskonto5) {
		this.sip_iskonto5 = sip_iskonto5;
	}
	public double getSip_iskonto6() {
		return sip_iskonto6;
	}
	public void setSip_iskonto6(double sip_iskonto6) {
		this.sip_iskonto6 = sip_iskonto6;
	}
	public double getSip_masraf1() {
		return sip_masraf1;
	}
	public void setSip_masraf1(double sip_masraf1) {
		this.sip_masraf1 = sip_masraf1;
	}
	public double getSip_masraf2() {
		return sip_masraf2;
	}
	public void setSip_masraf2(double sip_masraf2) {
		this.sip_masraf2 = sip_masraf2;
	}
	public double getSip_masraf3() {
		return sip_masraf3;
	}
	public void setSip_masraf3(double sip_masraf3) {
		this.sip_masraf3 = sip_masraf3;
	}
	public double getSip_masraf4() {
		return sip_masraf4;
	}
	public void setSip_masraf4(double sip_masraf4) {
		this.sip_masraf4 = sip_masraf4;
	}
	public double getSip_isk1() {
		return sip_isk1;
	}
	public void setSip_isk1(double sip_isk1) {
		this.sip_isk1 = sip_isk1;
	}
	public double getSip_isk2() {
		return sip_isk2;
	}
	public void setSip_isk2(double sip_isk2) {
		this.sip_isk2 = sip_isk2;
	}
	public double getSip_isk3() {
		return sip_isk3;
	}
	public void setSip_isk3(double sip_isk3) {
		this.sip_isk3 = sip_isk3;
	}
	public double getSip_isk4() {
		return sip_isk4;
	}
	public void setSip_isk4(double sip_isk4) {
		this.sip_isk4 = sip_isk4;
	}
	public double getSip_isk5() {
		return sip_isk5;
	}
	public void setSip_isk5(double sip_isk5) {
		this.sip_isk5 = sip_isk5;
	}
	public double getSip_isk6() {
		return sip_isk6;
	}
	public void setSip_isk6(double sip_isk6) {
		this.sip_isk6 = sip_isk6;
	}
	public double getSip_mas1() {
		return sip_mas1;
	}
	public void setSip_mas1(double sip_mas1) {
		this.sip_mas1 = sip_mas1;
	}
	public double getSip_mas2() {
		return sip_mas2;
	}
	public void setSip_mas2(double sip_mas2) {
		this.sip_mas2 = sip_mas2;
	}
	public double getSip_mas3() {
		return sip_mas3;
	}
	public void setSip_mas3(double sip_mas3) {
		this.sip_mas3 = sip_mas3;
	}
	public double getSip_mas4() {
		return sip_mas4;
	}
	public void setSip_mas4(double sip_mas4) {
		this.sip_mas4 = sip_mas4;
	}
	public String getSip_Exp_Imp_Kodu() {
		return sip_Exp_Imp_Kodu;
	}
	public void setSip_Exp_Imp_Kodu(String sip_Exp_Imp_Kodu) {
		this.sip_Exp_Imp_Kodu = sip_Exp_Imp_Kodu;
	}
	public double getSip_kar_orani() {
		return sip_kar_orani;
	}
	public void setSip_kar_orani(double sip_kar_orani) {
		this.sip_kar_orani = sip_kar_orani;
	}
	public double getSip_durumu() {
		return sip_durumu;
	}
	public void setSip_durumu(double sip_durumu) {
		this.sip_durumu = sip_durumu;
	}
	public int getSip_stalRecId_DBCno() {
		return sip_stalRecId_DBCno;
	}
	public void setSip_stalRecId_DBCno(int sip_stalRecId_DBCno) {
		this.sip_stalRecId_DBCno = sip_stalRecId_DBCno;
	}
	public int getSip_stalRecId_RECno() {
		return sip_stalRecId_RECno;
	}
	public void setSip_stalRecId_RECno(int sip_stalRecId_RECno) {
		this.sip_stalRecId_RECno = sip_stalRecId_RECno;
	}
	public int getSip_planlananmiktar() {
		return sip_planlananmiktar;
	}
	public void setSip_planlananmiktar(int sip_planlananmiktar) {
		this.sip_planlananmiktar = sip_planlananmiktar;
	}
	public int getSip_teklifRecId_DBCno() {
		return sip_teklifRecId_DBCno;
	}
	public void setSip_teklifRecId_DBCno(int sip_teklifRecId_DBCno) {
		this.sip_teklifRecId_DBCno = sip_teklifRecId_DBCno;
	}
	public int getSip_teklifRecId_RECno() {
		return sip_teklifRecId_RECno;
	}
	public void setSip_teklifRecId_RECno(int sip_teklifRecId_RECno) {
		this.sip_teklifRecId_RECno = sip_teklifRecId_RECno;
	}
	public String getSip_parti_kodu() {
		return sip_parti_kodu;
	}
	public void setSip_parti_kodu(String sip_parti_kodu) {
		this.sip_parti_kodu = sip_parti_kodu;
	}
	public double getSip_lot_no() {
		return sip_lot_no;
	}
	public void setSip_lot_no(double sip_lot_no) {
		this.sip_lot_no = sip_lot_no;
	}
	public String getSip_projekodu() {
		return sip_projekodu;
	}
	public void setSip_projekodu(String sip_projekodu) {
		this.sip_projekodu = sip_projekodu;
	}
	public double getSip_fiyat_liste_no() {
		return sip_fiyat_liste_no;
	}
	public void setSip_fiyat_liste_no(double sip_fiyat_liste_no) {
		this.sip_fiyat_liste_no = sip_fiyat_liste_no;
	}
	public double getSip_Otv_Pntr() {
		return sip_Otv_Pntr;
	}
	public void setSip_Otv_Pntr(double sip_Otv_Pntr) {
		this.sip_Otv_Pntr = sip_Otv_Pntr;
	}
	public double getSip_Otv_Vergi() {
		return sip_Otv_Vergi;
	}
	public void setSip_Otv_Vergi(double sip_Otv_Vergi) {
		this.sip_Otv_Vergi = sip_Otv_Vergi;
	}
	public double getSip_otvtutari() {
		return sip_otvtutari;
	}
	public void setSip_otvtutari(double sip_otvtutari) {
		this.sip_otvtutari = sip_otvtutari;
	}
	public double getSip_OtvVergisiz_Fl() {
		return sip_OtvVergisiz_Fl;
	}
	public void setSip_OtvVergisiz_Fl(double sip_OtvVergisiz_Fl) {
		this.sip_OtvVergisiz_Fl = sip_OtvVergisiz_Fl;
	}
	public String getSip_paket_kod() {
		return sip_paket_kod;
	}
	public void setSip_paket_kod(String sip_paket_kod) {
		this.sip_paket_kod = sip_paket_kod;
	}
	public double getSip_RezRecId_DBCno() {
		return sip_RezRecId_DBCno;
	}
	public void setSip_RezRecId_DBCno(double sip_RezRecId_DBCno) {
		this.sip_RezRecId_DBCno = sip_RezRecId_DBCno;
	}
	public double getSip_RezRecId_RECno() {
		return sip_RezRecId_RECno;
	}
	public void setSip_RezRecId_RECno(double sip_RezRecId_RECno) {
		this.sip_RezRecId_RECno = sip_RezRecId_RECno;
	}
	public int getSip_harekettipi() {
		return sip_harekettipi;
	}
	public void setSip_harekettipi(int sip_harekettipi) {
		this.sip_harekettipi = sip_harekettipi;
	}
	public double getSip_yetkili_recid_dbcno() {
		return sip_yetkili_recid_dbcno;
	}
	public void setSip_yetkili_recid_dbcno(double sip_yetkili_recid_dbcno) {
		this.sip_yetkili_recid_dbcno = sip_yetkili_recid_dbcno;
	}
	public double getSip_yetkili_recid_recno() {
		return sip_yetkili_recid_recno;
	}
	public void setSip_yetkili_recid_recno(double sip_yetkili_recid_recno) {
		this.sip_yetkili_recid_recno = sip_yetkili_recid_recno;
	}
	public String getSip_kapatmanedenkod() {
		return sip_kapatmanedenkod;
	}
	public void setSip_kapatmanedenkod(String sip_kapatmanedenkod) {
		this.sip_kapatmanedenkod = sip_kapatmanedenkod;
	}
	public TimeAndZoneVO getSip_gecerlilik_tarihi() {
		return sip_gecerlilik_tarihi;
	}
	public void setSip_gecerlilik_tarihi(TimeAndZoneVO sip_gecerlilik_tarihi) {
		this.sip_gecerlilik_tarihi = sip_gecerlilik_tarihi;
	}
	public double getSip_onodeme_evrak_tip() {
		return sip_onodeme_evrak_tip;
	}
	public void setSip_onodeme_evrak_tip(double sip_onodeme_evrak_tip) {
		this.sip_onodeme_evrak_tip = sip_onodeme_evrak_tip;
	}
	public String getSip_onodeme_evrak_seri() {
		return sip_onodeme_evrak_seri;
	}
	public void setSip_onodeme_evrak_seri(String sip_onodeme_evrak_seri) {
		this.sip_onodeme_evrak_seri = sip_onodeme_evrak_seri;
	}
	public double getSip_onodeme_evrak_sira() {
		return sip_onodeme_evrak_sira;
	}
	public void setSip_onodeme_evrak_sira(double sip_onodeme_evrak_sira) {
		this.sip_onodeme_evrak_sira = sip_onodeme_evrak_sira;
	}
	public double getSip_rezervasyon_miktari() {
		return sip_rezervasyon_miktari;
	}
	public void setSip_rezervasyon_miktari(double sip_rezervasyon_miktari) {
		this.sip_rezervasyon_miktari = sip_rezervasyon_miktari;
	}
	public double getSip_rezerveden_teslim_edilen() {
		return sip_rezerveden_teslim_edilen;
	}
	public void setSip_rezerveden_teslim_edilen(double sip_rezerveden_teslim_edilen) {
		this.sip_rezerveden_teslim_edilen = sip_rezerveden_teslim_edilen;
	}
	public String getSip_stok_isim() {
		return sip_stok_isim;
	}
	public void setSip_stok_isim(String sip_stok_isim) {
		this.sip_stok_isim = sip_stok_isim;
	}
	public int getSto_RECno() {
		return sto_RECno;
	}
	public void setSto_RECno(int sto_RECno) {
		this.sto_RECno = sto_RECno;
	}
	public double getSto_RECid_DBCno() {
		return sto_RECid_DBCno;
	}
	public void setSto_RECid_DBCno(double sto_RECid_DBCno) {
		this.sto_RECid_DBCno = sto_RECid_DBCno;
	}
	public double getSto_RECid_RECno() {
		return sto_RECid_RECno;
	}
	public void setSto_RECid_RECno(double sto_RECid_RECno) {
		this.sto_RECid_RECno = sto_RECid_RECno;
	}
	public double getSto_SpecRECno() {
		return sto_SpecRECno;
	}
	public void setSto_SpecRECno(double sto_SpecRECno) {
		this.sto_SpecRECno = sto_SpecRECno;
	}
	public int getSto_iptal() {
		return sto_iptal;
	}
	public void setSto_iptal(int sto_iptal) {
		this.sto_iptal = sto_iptal;
	}
	public double getSto_fileid() {
		return sto_fileid;
	}
	public void setSto_fileid(double sto_fileid) {
		this.sto_fileid = sto_fileid;
	}
	public double getSto_hidden() {
		return sto_hidden;
	}
	public void setSto_hidden(double sto_hidden) {
		this.sto_hidden = sto_hidden;
	}
	public double getSto_kilitli() {
		return sto_kilitli;
	}
	public void setSto_kilitli(double sto_kilitli) {
		this.sto_kilitli = sto_kilitli;
	}
	public double getSto_degisti() {
		return sto_degisti;
	}
	public void setSto_degisti(double sto_degisti) {
		this.sto_degisti = sto_degisti;
	}
	public int getSto_checksum() {
		return sto_checksum;
	}
	public void setSto_checksum(int sto_checksum) {
		this.sto_checksum = sto_checksum;
	}
	public double getSto_create_user() {
		return sto_create_user;
	}
	public void setSto_create_user(double sto_create_user) {
		this.sto_create_user = sto_create_user;
	}
	public TimeAndZoneVO getSto_create_date() {
		return sto_create_date;
	}
	public void setSto_create_date(TimeAndZoneVO sto_create_date) {
		this.sto_create_date = sto_create_date;
	}
	public double getSto_lastup_user() {
		return sto_lastup_user;
	}
	public void setSto_lastup_user(double sto_lastup_user) {
		this.sto_lastup_user = sto_lastup_user;
	}
	public TimeAndZoneVO getSto_lastup_date() {
		return sto_lastup_date;
	}
	public void setSto_lastup_date(TimeAndZoneVO sto_lastup_date) {
		this.sto_lastup_date = sto_lastup_date;
	}
	public String getSto_special1() {
		return sto_special1;
	}
	public void setSto_special1(String sto_special1) {
		this.sto_special1 = sto_special1;
	}
	public String getSto_special2() {
		return sto_special2;
	}
	public void setSto_special2(String sto_special2) {
		this.sto_special2 = sto_special2;
	}
	public String getSto_special3() {
		return sto_special3;
	}
	public void setSto_special3(String sto_special3) {
		this.sto_special3 = sto_special3;
	}
	public String getSto_kod() {
		return sto_kod;
	}
	public void setSto_kod(String sto_kod) {
		this.sto_kod = sto_kod;
	}
	public String getSto_isim() {
		return sto_isim;
	}
	public void setSto_isim(String sto_isim) {
		this.sto_isim = sto_isim;
	}
	public String getSto_kisa_ismi() {
		return sto_kisa_ismi;
	}
	public void setSto_kisa_ismi(String sto_kisa_ismi) {
		this.sto_kisa_ismi = sto_kisa_ismi;
	}
	public String getSto_yabanci_isim() {
		return sto_yabanci_isim;
	}
	public void setSto_yabanci_isim(String sto_yabanci_isim) {
		this.sto_yabanci_isim = sto_yabanci_isim;
	}
	public String getSto_sat_cari_kod() {
		return sto_sat_cari_kod;
	}
	public void setSto_sat_cari_kod(String sto_sat_cari_kod) {
		this.sto_sat_cari_kod = sto_sat_cari_kod;
	}
	public double getSto_cins() {
		return sto_cins;
	}
	public void setSto_cins(double sto_cins) {
		this.sto_cins = sto_cins;
	}
	public double getSto_doviz_cinsi() {
		return sto_doviz_cinsi;
	}
	public void setSto_doviz_cinsi(double sto_doviz_cinsi) {
		this.sto_doviz_cinsi = sto_doviz_cinsi;
	}
	public double getSto_detay_takip() {
		return sto_detay_takip;
	}
	public void setSto_detay_takip(double sto_detay_takip) {
		this.sto_detay_takip = sto_detay_takip;
	}
	public String getSto_birim1_ad() {
		return sto_birim1_ad;
	}
	public void setSto_birim1_ad(String sto_birim1_ad) {
		this.sto_birim1_ad = sto_birim1_ad;
	}
	public double getSto_birim1_katsayi() {
		return sto_birim1_katsayi;
	}
	public void setSto_birim1_katsayi(double sto_birim1_katsayi) {
		this.sto_birim1_katsayi = sto_birim1_katsayi;
	}
	public double getSto_birim1_agirlik() {
		return sto_birim1_agirlik;
	}
	public void setSto_birim1_agirlik(double sto_birim1_agirlik) {
		this.sto_birim1_agirlik = sto_birim1_agirlik;
	}
	public double getSto_birim1_en() {
		return sto_birim1_en;
	}
	public void setSto_birim1_en(double sto_birim1_en) {
		this.sto_birim1_en = sto_birim1_en;
	}
	public double getSto_birim1_boy() {
		return sto_birim1_boy;
	}
	public void setSto_birim1_boy(double sto_birim1_boy) {
		this.sto_birim1_boy = sto_birim1_boy;
	}
	public double getSto_birim1_yukseklik() {
		return sto_birim1_yukseklik;
	}
	public void setSto_birim1_yukseklik(double sto_birim1_yukseklik) {
		this.sto_birim1_yukseklik = sto_birim1_yukseklik;
	}
	public double getSto_birim1_dara() {
		return sto_birim1_dara;
	}
	public void setSto_birim1_dara(double sto_birim1_dara) {
		this.sto_birim1_dara = sto_birim1_dara;
	}
	public String getSto_birim2_ad() {
		return sto_birim2_ad;
	}
	public void setSto_birim2_ad(String sto_birim2_ad) {
		this.sto_birim2_ad = sto_birim2_ad;
	}
	public double getSto_birim2_katsayi() {
		return sto_birim2_katsayi;
	}
	public void setSto_birim2_katsayi(double sto_birim2_katsayi) {
		this.sto_birim2_katsayi = sto_birim2_katsayi;
	}
	public double getSto_birim2_agirlik() {
		return sto_birim2_agirlik;
	}
	public void setSto_birim2_agirlik(double sto_birim2_agirlik) {
		this.sto_birim2_agirlik = sto_birim2_agirlik;
	}
	public double getSto_birim2_en() {
		return sto_birim2_en;
	}
	public void setSto_birim2_en(double sto_birim2_en) {
		this.sto_birim2_en = sto_birim2_en;
	}
	public double getSto_birim2_boy() {
		return sto_birim2_boy;
	}
	public void setSto_birim2_boy(double sto_birim2_boy) {
		this.sto_birim2_boy = sto_birim2_boy;
	}
	public double getSto_birim2_yukseklik() {
		return sto_birim2_yukseklik;
	}
	public void setSto_birim2_yukseklik(double sto_birim2_yukseklik) {
		this.sto_birim2_yukseklik = sto_birim2_yukseklik;
	}
	public double getSto_birim2_dara() {
		return sto_birim2_dara;
	}
	public void setSto_birim2_dara(double sto_birim2_dara) {
		this.sto_birim2_dara = sto_birim2_dara;
	}
	public String getSto_birim3_ad() {
		return sto_birim3_ad;
	}
	public void setSto_birim3_ad(String sto_birim3_ad) {
		this.sto_birim3_ad = sto_birim3_ad;
	}
	public double getSto_birim3_katsayi() {
		return sto_birim3_katsayi;
	}
	public void setSto_birim3_katsayi(double sto_birim3_katsayi) {
		this.sto_birim3_katsayi = sto_birim3_katsayi;
	}
	public double getSto_birim3_agirlik() {
		return sto_birim3_agirlik;
	}
	public void setSto_birim3_agirlik(double sto_birim3_agirlik) {
		this.sto_birim3_agirlik = sto_birim3_agirlik;
	}
	public double getSto_birim3_en() {
		return sto_birim3_en;
	}
	public void setSto_birim3_en(double sto_birim3_en) {
		this.sto_birim3_en = sto_birim3_en;
	}
	public double getSto_birim3_boy() {
		return sto_birim3_boy;
	}
	public void setSto_birim3_boy(double sto_birim3_boy) {
		this.sto_birim3_boy = sto_birim3_boy;
	}
	public double getSto_birim3_yukseklik() {
		return sto_birim3_yukseklik;
	}
	public void setSto_birim3_yukseklik(double sto_birim3_yukseklik) {
		this.sto_birim3_yukseklik = sto_birim3_yukseklik;
	}
	public double getSto_birim3_dara() {
		return sto_birim3_dara;
	}
	public void setSto_birim3_dara(double sto_birim3_dara) {
		this.sto_birim3_dara = sto_birim3_dara;
	}
	public String getSto_birim4_ad() {
		return sto_birim4_ad;
	}
	public void setSto_birim4_ad(String sto_birim4_ad) {
		this.sto_birim4_ad = sto_birim4_ad;
	}
	public double getSto_birim4_katsayi() {
		return sto_birim4_katsayi;
	}
	public void setSto_birim4_katsayi(double sto_birim4_katsayi) {
		this.sto_birim4_katsayi = sto_birim4_katsayi;
	}
	public double getSto_birim4_agirlik() {
		return sto_birim4_agirlik;
	}
	public void setSto_birim4_agirlik(double sto_birim4_agirlik) {
		this.sto_birim4_agirlik = sto_birim4_agirlik;
	}
	public double getSto_birim4_en() {
		return sto_birim4_en;
	}
	public void setSto_birim4_en(double sto_birim4_en) {
		this.sto_birim4_en = sto_birim4_en;
	}
	public double getSto_birim4_boy() {
		return sto_birim4_boy;
	}
	public void setSto_birim4_boy(double sto_birim4_boy) {
		this.sto_birim4_boy = sto_birim4_boy;
	}
	public double getSto_birim4_yukseklik() {
		return sto_birim4_yukseklik;
	}
	public void setSto_birim4_yukseklik(double sto_birim4_yukseklik) {
		this.sto_birim4_yukseklik = sto_birim4_yukseklik;
	}
	public double getSto_birim4_dara() {
		return sto_birim4_dara;
	}
	public void setSto_birim4_dara(double sto_birim4_dara) {
		this.sto_birim4_dara = sto_birim4_dara;
	}
	public String getSto_muh_kod() {
		return sto_muh_kod;
	}
	public void setSto_muh_kod(String sto_muh_kod) {
		this.sto_muh_kod = sto_muh_kod;
	}
	public String getSto_muh_Iade_kod() {
		return sto_muh_Iade_kod;
	}
	public void setSto_muh_Iade_kod(String sto_muh_Iade_kod) {
		this.sto_muh_Iade_kod = sto_muh_Iade_kod;
	}
	public String getSto_muh_sat_muh_kod() {
		return sto_muh_sat_muh_kod;
	}
	public void setSto_muh_sat_muh_kod(String sto_muh_sat_muh_kod) {
		this.sto_muh_sat_muh_kod = sto_muh_sat_muh_kod;
	}
	public String getSto_muh_satIadmuhkod() {
		return sto_muh_satIadmuhkod;
	}
	public void setSto_muh_satIadmuhkod(String sto_muh_satIadmuhkod) {
		this.sto_muh_satIadmuhkod = sto_muh_satIadmuhkod;
	}
	public String getSto_muh_sat_isk_kod() {
		return sto_muh_sat_isk_kod;
	}
	public void setSto_muh_sat_isk_kod(String sto_muh_sat_isk_kod) {
		this.sto_muh_sat_isk_kod = sto_muh_sat_isk_kod;
	}
	public String getSto_muh_aIiskmuhkod() {
		return sto_muh_aIiskmuhkod;
	}
	public void setSto_muh_aIiskmuhkod(String sto_muh_aIiskmuhkod) {
		this.sto_muh_aIiskmuhkod = sto_muh_aIiskmuhkod;
	}
	public String getSto_muh_satmalmuhkod() {
		return sto_muh_satmalmuhkod;
	}
	public void setSto_muh_satmalmuhkod(String sto_muh_satmalmuhkod) {
		this.sto_muh_satmalmuhkod = sto_muh_satmalmuhkod;
	}
	public String getSto_yurtdisi_satmuhk() {
		return sto_yurtdisi_satmuhk;
	}
	public void setSto_yurtdisi_satmuhk(String sto_yurtdisi_satmuhk) {
		this.sto_yurtdisi_satmuhk = sto_yurtdisi_satmuhk;
	}
	public String getSto_ilavemasmuhkod() {
		return sto_ilavemasmuhkod;
	}
	public void setSto_ilavemasmuhkod(String sto_ilavemasmuhkod) {
		this.sto_ilavemasmuhkod = sto_ilavemasmuhkod;
	}
	public String getSto_yatirimtesmuhkod() {
		return sto_yatirimtesmuhkod;
	}
	public void setSto_yatirimtesmuhkod(String sto_yatirimtesmuhkod) {
		this.sto_yatirimtesmuhkod = sto_yatirimtesmuhkod;
	}
	public String getSto_depsatmuhkod() {
		return sto_depsatmuhkod;
	}
	public void setSto_depsatmuhkod(String sto_depsatmuhkod) {
		this.sto_depsatmuhkod = sto_depsatmuhkod;
	}
	public String getSto_depsatmalmuhkod() {
		return sto_depsatmalmuhkod;
	}
	public void setSto_depsatmalmuhkod(String sto_depsatmalmuhkod) {
		this.sto_depsatmalmuhkod = sto_depsatmalmuhkod;
	}
	public String getSto_bagortsatmuhkod() {
		return sto_bagortsatmuhkod;
	}
	public void setSto_bagortsatmuhkod(String sto_bagortsatmuhkod) {
		this.sto_bagortsatmuhkod = sto_bagortsatmuhkod;
	}
	public String getSto_bagortsatIadmuhkod() {
		return sto_bagortsatIadmuhkod;
	}
	public void setSto_bagortsatIadmuhkod(String sto_bagortsatIadmuhkod) {
		this.sto_bagortsatIadmuhkod = sto_bagortsatIadmuhkod;
	}
	public String getSto_bagortsatIskmuhkod() {
		return sto_bagortsatIskmuhkod;
	}
	public void setSto_bagortsatIskmuhkod(String sto_bagortsatIskmuhkod) {
		this.sto_bagortsatIskmuhkod = sto_bagortsatIskmuhkod;
	}
	public String getSto_satfiyfarkmuhkod() {
		return sto_satfiyfarkmuhkod;
	}
	public void setSto_satfiyfarkmuhkod(String sto_satfiyfarkmuhkod) {
		this.sto_satfiyfarkmuhkod = sto_satfiyfarkmuhkod;
	}
	public String getSto_yurtdisisatmalmuhkod() {
		return sto_yurtdisisatmalmuhkod;
	}
	public void setSto_yurtdisisatmalmuhkod(String sto_yurtdisisatmalmuhkod) {
		this.sto_yurtdisisatmalmuhkod = sto_yurtdisisatmalmuhkod;
	}
	public String getSto_bagortsatmalmuhkod() {
		return sto_bagortsatmalmuhkod;
	}
	public void setSto_bagortsatmalmuhkod(String sto_bagortsatmalmuhkod) {
		this.sto_bagortsatmalmuhkod = sto_bagortsatmalmuhkod;
	}
	public double getSto_karorani() {
		return sto_karorani;
	}
	public void setSto_karorani(double sto_karorani) {
		this.sto_karorani = sto_karorani;
	}
	public double getSto_min_stok() {
		return sto_min_stok;
	}
	public void setSto_min_stok(double sto_min_stok) {
		this.sto_min_stok = sto_min_stok;
	}
	public double getSto_siparis_stok() {
		return sto_siparis_stok;
	}
	public void setSto_siparis_stok(double sto_siparis_stok) {
		this.sto_siparis_stok = sto_siparis_stok;
	}
	public double getSto_max_stok() {
		return sto_max_stok;
	}
	public void setSto_max_stok(double sto_max_stok) {
		this.sto_max_stok = sto_max_stok;
	}
	public double getSto_ver_sip_birim() {
		return sto_ver_sip_birim;
	}
	public void setSto_ver_sip_birim(double sto_ver_sip_birim) {
		this.sto_ver_sip_birim = sto_ver_sip_birim;
	}
	public double getSto_al_sip_birim() {
		return sto_al_sip_birim;
	}
	public void setSto_al_sip_birim(double sto_al_sip_birim) {
		this.sto_al_sip_birim = sto_al_sip_birim;
	}
	public double getSto_siparis_sure() {
		return sto_siparis_sure;
	}
	public void setSto_siparis_sure(double sto_siparis_sure) {
		this.sto_siparis_sure = sto_siparis_sure;
	}
	public double getSto_perakende_vergi() {
		return sto_perakende_vergi;
	}
	public void setSto_perakende_vergi(double sto_perakende_vergi) {
		this.sto_perakende_vergi = sto_perakende_vergi;
	}
	public double getSto_toptan_vergi() {
		return sto_toptan_vergi;
	}
	public void setSto_toptan_vergi(double sto_toptan_vergi) {
		this.sto_toptan_vergi = sto_toptan_vergi;
	}
	public String getSto_yer_kod() {
		return sto_yer_kod;
	}
	public void setSto_yer_kod(String sto_yer_kod) {
		this.sto_yer_kod = sto_yer_kod;
	}
	public double getSto_elk_etk_tipi() {
		return sto_elk_etk_tipi;
	}
	public void setSto_elk_etk_tipi(double sto_elk_etk_tipi) {
		this.sto_elk_etk_tipi = sto_elk_etk_tipi;
	}
	public double getSto_raf_etiketli() {
		return sto_raf_etiketli;
	}
	public void setSto_raf_etiketli(double sto_raf_etiketli) {
		this.sto_raf_etiketli = sto_raf_etiketli;
	}
	public double getSto_etiket_bas() {
		return sto_etiket_bas;
	}
	public void setSto_etiket_bas(double sto_etiket_bas) {
		this.sto_etiket_bas = sto_etiket_bas;
	}
	public double getSto_satis_dursun() {
		return sto_satis_dursun;
	}
	public void setSto_satis_dursun(double sto_satis_dursun) {
		this.sto_satis_dursun = sto_satis_dursun;
	}
	public double getSto_siparis_dursun() {
		return sto_siparis_dursun;
	}
	public void setSto_siparis_dursun(double sto_siparis_dursun) {
		this.sto_siparis_dursun = sto_siparis_dursun;
	}
	public double getSto_malkabul_dursun() {
		return sto_malkabul_dursun;
	}
	public void setSto_malkabul_dursun(double sto_malkabul_dursun) {
		this.sto_malkabul_dursun = sto_malkabul_dursun;
	}
	public double getSto_malkabul_gun1() {
		return sto_malkabul_gun1;
	}
	public void setSto_malkabul_gun1(double sto_malkabul_gun1) {
		this.sto_malkabul_gun1 = sto_malkabul_gun1;
	}
	public double getSto_malkabul_gun2() {
		return sto_malkabul_gun2;
	}
	public void setSto_malkabul_gun2(double sto_malkabul_gun2) {
		this.sto_malkabul_gun2 = sto_malkabul_gun2;
	}
	public double getSto_malkabul_gun3() {
		return sto_malkabul_gun3;
	}
	public void setSto_malkabul_gun3(double sto_malkabul_gun3) {
		this.sto_malkabul_gun3 = sto_malkabul_gun3;
	}
	public double getSto_malkabul_gun4() {
		return sto_malkabul_gun4;
	}
	public void setSto_malkabul_gun4(double sto_malkabul_gun4) {
		this.sto_malkabul_gun4 = sto_malkabul_gun4;
	}
	public double getSto_malkabul_gun5() {
		return sto_malkabul_gun5;
	}
	public void setSto_malkabul_gun5(double sto_malkabul_gun5) {
		this.sto_malkabul_gun5 = sto_malkabul_gun5;
	}
	public double getSto_malkabul_gun6() {
		return sto_malkabul_gun6;
	}
	public void setSto_malkabul_gun6(double sto_malkabul_gun6) {
		this.sto_malkabul_gun6 = sto_malkabul_gun6;
	}
	public double getSto_malkabul_gun7() {
		return sto_malkabul_gun7;
	}
	public void setSto_malkabul_gun7(double sto_malkabul_gun7) {
		this.sto_malkabul_gun7 = sto_malkabul_gun7;
	}
	public double getSto_siparis_gun1() {
		return sto_siparis_gun1;
	}
	public void setSto_siparis_gun1(double sto_siparis_gun1) {
		this.sto_siparis_gun1 = sto_siparis_gun1;
	}
	public double getSto_siparis_gun2() {
		return sto_siparis_gun2;
	}
	public void setSto_siparis_gun2(double sto_siparis_gun2) {
		this.sto_siparis_gun2 = sto_siparis_gun2;
	}
	public double getSto_siparis_gun3() {
		return sto_siparis_gun3;
	}
	public void setSto_siparis_gun3(double sto_siparis_gun3) {
		this.sto_siparis_gun3 = sto_siparis_gun3;
	}
	public double getSto_siparis_gun4() {
		return sto_siparis_gun4;
	}
	public void setSto_siparis_gun4(double sto_siparis_gun4) {
		this.sto_siparis_gun4 = sto_siparis_gun4;
	}
	public double getSto_siparis_gun5() {
		return sto_siparis_gun5;
	}
	public void setSto_siparis_gun5(double sto_siparis_gun5) {
		this.sto_siparis_gun5 = sto_siparis_gun5;
	}
	public double getSto_siparis_gun6() {
		return sto_siparis_gun6;
	}
	public void setSto_siparis_gun6(double sto_siparis_gun6) {
		this.sto_siparis_gun6 = sto_siparis_gun6;
	}
	public double getSto_siparis_gun7() {
		return sto_siparis_gun7;
	}
	public void setSto_siparis_gun7(double sto_siparis_gun7) {
		this.sto_siparis_gun7 = sto_siparis_gun7;
	}
	public double getSto_iskon_yapilamaz() {
		return sto_iskon_yapilamaz;
	}
	public void setSto_iskon_yapilamaz(double sto_iskon_yapilamaz) {
		this.sto_iskon_yapilamaz = sto_iskon_yapilamaz;
	}
	public double getSto_tasfiyede() {
		return sto_tasfiyede;
	}
	public void setSto_tasfiyede(double sto_tasfiyede) {
		this.sto_tasfiyede = sto_tasfiyede;
	}
	public double getSto_alt_grup_no() {
		return sto_alt_grup_no;
	}
	public void setSto_alt_grup_no(double sto_alt_grup_no) {
		this.sto_alt_grup_no = sto_alt_grup_no;
	}
	public String getSto_kategori_kodu() {
		return sto_kategori_kodu;
	}
	public void setSto_kategori_kodu(String sto_kategori_kodu) {
		this.sto_kategori_kodu = sto_kategori_kodu;
	}
	public String getSto_urun_sorkod() {
		return sto_urun_sorkod;
	}
	public void setSto_urun_sorkod(String sto_urun_sorkod) {
		this.sto_urun_sorkod = sto_urun_sorkod;
	}
	public String getSto_altgrup_kod() {
		return sto_altgrup_kod;
	}
	public void setSto_altgrup_kod(String sto_altgrup_kod) {
		this.sto_altgrup_kod = sto_altgrup_kod;
	}
	public String getSto_anagrup_kod() {
		return sto_anagrup_kod;
	}
	public void setSto_anagrup_kod(String sto_anagrup_kod) {
		this.sto_anagrup_kod = sto_anagrup_kod;
	}
	public String getSto_uretici_kodu() {
		return sto_uretici_kodu;
	}
	public void setSto_uretici_kodu(String sto_uretici_kodu) {
		this.sto_uretici_kodu = sto_uretici_kodu;
	}
	public String getSto_sektor_kodu() {
		return sto_sektor_kodu;
	}
	public void setSto_sektor_kodu(String sto_sektor_kodu) {
		this.sto_sektor_kodu = sto_sektor_kodu;
	}
	public String getSto_reyon_kodu() {
		return sto_reyon_kodu;
	}
	public void setSto_reyon_kodu(String sto_reyon_kodu) {
		this.sto_reyon_kodu = sto_reyon_kodu;
	}
	public String getSto_muhgrup_kodu() {
		return sto_muhgrup_kodu;
	}
	public void setSto_muhgrup_kodu(String sto_muhgrup_kodu) {
		this.sto_muhgrup_kodu = sto_muhgrup_kodu;
	}
	public String getSto_ambalaj_kodu() {
		return sto_ambalaj_kodu;
	}
	public void setSto_ambalaj_kodu(String sto_ambalaj_kodu) {
		this.sto_ambalaj_kodu = sto_ambalaj_kodu;
	}
	public String getSto_marka_kodu() {
		return sto_marka_kodu;
	}
	public void setSto_marka_kodu(String sto_marka_kodu) {
		this.sto_marka_kodu = sto_marka_kodu;
	}
	public String getSto_beden_kodu() {
		return sto_beden_kodu;
	}
	public void setSto_beden_kodu(String sto_beden_kodu) {
		this.sto_beden_kodu = sto_beden_kodu;
	}
	public String getSto_renk_kodu() {
		return sto_renk_kodu;
	}
	public void setSto_renk_kodu(String sto_renk_kodu) {
		this.sto_renk_kodu = sto_renk_kodu;
	}
	public String getSto_model_kodu() {
		return sto_model_kodu;
	}
	public void setSto_model_kodu(String sto_model_kodu) {
		this.sto_model_kodu = sto_model_kodu;
	}
	public String getSto_sezon_kodu() {
		return sto_sezon_kodu;
	}
	public void setSto_sezon_kodu(String sto_sezon_kodu) {
		this.sto_sezon_kodu = sto_sezon_kodu;
	}
	public String getSto_hammadde_kodu() {
		return sto_hammadde_kodu;
	}
	public void setSto_hammadde_kodu(String sto_hammadde_kodu) {
		this.sto_hammadde_kodu = sto_hammadde_kodu;
	}
	public String getSto_prim_kodu() {
		return sto_prim_kodu;
	}
	public void setSto_prim_kodu(String sto_prim_kodu) {
		this.sto_prim_kodu = sto_prim_kodu;
	}
	public String getSto_kalkon_kodu() {
		return sto_kalkon_kodu;
	}
	public void setSto_kalkon_kodu(String sto_kalkon_kodu) {
		this.sto_kalkon_kodu = sto_kalkon_kodu;
	}
	public String getSto_paket_kodu() {
		return sto_paket_kodu;
	}
	public void setSto_paket_kodu(String sto_paket_kodu) {
		this.sto_paket_kodu = sto_paket_kodu;
	}
	public String getSto_pozisyonbayrak_kodu() {
		return sto_pozisyonbayrak_kodu;
	}
	public void setSto_pozisyonbayrak_kodu(String sto_pozisyonbayrak_kodu) {
		this.sto_pozisyonbayrak_kodu = sto_pozisyonbayrak_kodu;
	}
	public String getSto_mkod_artik() {
		return sto_mkod_artik;
	}
	public void setSto_mkod_artik(String sto_mkod_artik) {
		this.sto_mkod_artik = sto_mkod_artik;
	}
	public double getSto_kasa_tarti_fl() {
		return sto_kasa_tarti_fl;
	}
	public void setSto_kasa_tarti_fl(double sto_kasa_tarti_fl) {
		this.sto_kasa_tarti_fl = sto_kasa_tarti_fl;
	}
	public double getSto_bedenli_takip() {
		return sto_bedenli_takip;
	}
	public void setSto_bedenli_takip(double sto_bedenli_takip) {
		this.sto_bedenli_takip = sto_bedenli_takip;
	}
	public double getSto_renkDetayli() {
		return sto_renkDetayli;
	}
	public void setSto_renkDetayli(double sto_renkDetayli) {
		this.sto_renkDetayli = sto_renkDetayli;
	}
	public double getSto_miktarondalikli_fl() {
		return sto_miktarondalikli_fl;
	}
	public void setSto_miktarondalikli_fl(double sto_miktarondalikli_fl) {
		this.sto_miktarondalikli_fl = sto_miktarondalikli_fl;
	}
	public double getSto_pasif_fl() {
		return sto_pasif_fl;
	}
	public void setSto_pasif_fl(double sto_pasif_fl) {
		this.sto_pasif_fl = sto_pasif_fl;
	}
	public double getSto_eksiyedusebilir_fl() {
		return sto_eksiyedusebilir_fl;
	}
	public void setSto_eksiyedusebilir_fl(double sto_eksiyedusebilir_fl) {
		this.sto_eksiyedusebilir_fl = sto_eksiyedusebilir_fl;
	}
	public String getSto_GtipNo() {
		return sto_GtipNo;
	}
	public void setSto_GtipNo(String sto_GtipNo) {
		this.sto_GtipNo = sto_GtipNo;
	}
	public double getSto_puan() {
		return sto_puan;
	}
	public void setSto_puan(double sto_puan) {
		this.sto_puan = sto_puan;
	}
	public String getSto_komisyon_hzmkodu() {
		return sto_komisyon_hzmkodu;
	}
	public void setSto_komisyon_hzmkodu(String sto_komisyon_hzmkodu) {
		this.sto_komisyon_hzmkodu = sto_komisyon_hzmkodu;
	}
	public double getSto_komisyon_orani() {
		return sto_komisyon_orani;
	}
	public void setSto_komisyon_orani(double sto_komisyon_orani) {
		this.sto_komisyon_orani = sto_komisyon_orani;
	}
	public double getSto_otvuygulama() {
		return sto_otvuygulama;
	}
	public void setSto_otvuygulama(double sto_otvuygulama) {
		this.sto_otvuygulama = sto_otvuygulama;
	}
	public double getSto_otvtutar() {
		return sto_otvtutar;
	}
	public void setSto_otvtutar(double sto_otvtutar) {
		this.sto_otvtutar = sto_otvtutar;
	}
	public double getSto_otvliste() {
		return sto_otvliste;
	}
	public void setSto_otvliste(double sto_otvliste) {
		this.sto_otvliste = sto_otvliste;
	}
	public double getSto_otvbirimi() {
		return sto_otvbirimi;
	}
	public void setSto_otvbirimi(double sto_otvbirimi) {
		this.sto_otvbirimi = sto_otvbirimi;
	}
	public double getSto_prim_orani() {
		return sto_prim_orani;
	}
	public void setSto_prim_orani(double sto_prim_orani) {
		this.sto_prim_orani = sto_prim_orani;
	}
	public double getSto_garanti_sure() {
		return sto_garanti_sure;
	}
	public void setSto_garanti_sure(double sto_garanti_sure) {
		this.sto_garanti_sure = sto_garanti_sure;
	}
	public double getSto_garanti_sure_tipi() {
		return sto_garanti_sure_tipi;
	}
	public void setSto_garanti_sure_tipi(double sto_garanti_sure_tipi) {
		this.sto_garanti_sure_tipi = sto_garanti_sure_tipi;
	}
	public double getSto_iplik_Ne_no() {
		return sto_iplik_Ne_no;
	}
	public void setSto_iplik_Ne_no(double sto_iplik_Ne_no) {
		this.sto_iplik_Ne_no = sto_iplik_Ne_no;
	}
	public double getSto_standartmaliyet() {
		return sto_standartmaliyet;
	}
	public void setSto_standartmaliyet(double sto_standartmaliyet) {
		this.sto_standartmaliyet = sto_standartmaliyet;
	}
	public double getSto_kanban_kasa_miktari() {
		return sto_kanban_kasa_miktari;
	}
	public void setSto_kanban_kasa_miktari(double sto_kanban_kasa_miktari) {
		this.sto_kanban_kasa_miktari = sto_kanban_kasa_miktari;
	}
	public double getSto_oivuygulama() {
		return sto_oivuygulama;
	}
	public void setSto_oivuygulama(double sto_oivuygulama) {
		this.sto_oivuygulama = sto_oivuygulama;
	}
	public double getSto_zraporu_stoku_fl() {
		return sto_zraporu_stoku_fl;
	}
	public void setSto_zraporu_stoku_fl(double sto_zraporu_stoku_fl) {
		this.sto_zraporu_stoku_fl = sto_zraporu_stoku_fl;
	}
	public double getSto_maxiskonto_orani() {
		return sto_maxiskonto_orani;
	}
	public void setSto_maxiskonto_orani(double sto_maxiskonto_orani) {
		this.sto_maxiskonto_orani = sto_maxiskonto_orani;
	}
	public double getSto_detay_takibinde_depo_kontrolu_fl() {
		return sto_detay_takibinde_depo_kontrolu_fl;
	}
	public void setSto_detay_takibinde_depo_kontrolu_fl(
			double sto_detay_takibinde_depo_kontrolu_fl) {
		this.sto_detay_takibinde_depo_kontrolu_fl = sto_detay_takibinde_depo_kontrolu_fl;
	}
	public String getSto_tamamlayici_kodu() {
		return sto_tamamlayici_kodu;
	}
	public void setSto_tamamlayici_kodu(String sto_tamamlayici_kodu) {
		this.sto_tamamlayici_kodu = sto_tamamlayici_kodu;
	}
	public double getSto_oto_barkod_acma_sekli() {
		return sto_oto_barkod_acma_sekli;
	}
	public void setSto_oto_barkod_acma_sekli(double sto_oto_barkod_acma_sekli) {
		this.sto_oto_barkod_acma_sekli = sto_oto_barkod_acma_sekli;
	}
	public String getSto_oto_barkod_kod_yapisi() {
		return sto_oto_barkod_kod_yapisi;
	}
	public void setSto_oto_barkod_kod_yapisi(String sto_oto_barkod_kod_yapisi) {
		this.sto_oto_barkod_kod_yapisi = sto_oto_barkod_kod_yapisi;
	}
	public double getSto_KasaIskontoOrani() {
		return sto_KasaIskontoOrani;
	}
	public void setSto_KasaIskontoOrani(double sto_KasaIskontoOrani) {
		this.sto_KasaIskontoOrani = sto_KasaIskontoOrani;
	}
	public double getSto_KasaIskontoTutari() {
		return sto_KasaIskontoTutari;
	}
	public void setSto_KasaIskontoTutari(double sto_KasaIskontoTutari) {
		this.sto_KasaIskontoTutari = sto_KasaIskontoTutari;
	}
	public double getSto_gelirpayi() {
		return sto_gelirpayi;
	}
	public void setSto_gelirpayi(double sto_gelirpayi) {
		this.sto_gelirpayi = sto_gelirpayi;
	}
	public double getSto_oivtutar() {
		return sto_oivtutar;
	}
	public void setSto_oivtutar(double sto_oivtutar) {
		this.sto_oivtutar = sto_oivtutar;
	}
	public double getSto_oivturu() {
		return sto_oivturu;
	}
	public void setSto_oivturu(double sto_oivturu) {
		this.sto_oivturu = sto_oivturu;
	}
	public String getSto_giderkodu() {
		return sto_giderkodu;
	}
	public void setSto_giderkodu(String sto_giderkodu) {
		this.sto_giderkodu = sto_giderkodu;
	}
	public double getSto_oivvergipntr() {
		return sto_oivvergipntr;
	}
	public void setSto_oivvergipntr(double sto_oivvergipntr) {
		this.sto_oivvergipntr = sto_oivvergipntr;
	}
	public double getSto_Tevkifat_turu() {
		return sto_Tevkifat_turu;
	}
	public void setSto_Tevkifat_turu(double sto_Tevkifat_turu) {
		this.sto_Tevkifat_turu = sto_Tevkifat_turu;
	}
	public double getSto_SKT_fl() {
		return sto_SKT_fl;
	}
	public void setSto_SKT_fl(double sto_SKT_fl) {
		this.sto_SKT_fl = sto_SKT_fl;
	}
	public double getSto_terazi_SKT() {
		return sto_terazi_SKT;
	}
	public void setSto_terazi_SKT(double sto_terazi_SKT) {
		this.sto_terazi_SKT = sto_terazi_SKT;
	}
	public double getSto_RafOmru() {
		return sto_RafOmru;
	}
	public void setSto_RafOmru(double sto_RafOmru) {
		this.sto_RafOmru = sto_RafOmru;
	}
	public double getSto_KasadaTaksitlenebilir_fl() {
		return sto_KasadaTaksitlenebilir_fl;
	}
	public void setSto_KasadaTaksitlenebilir_fl(double sto_KasadaTaksitlenebilir_fl) {
		this.sto_KasadaTaksitlenebilir_fl = sto_KasadaTaksitlenebilir_fl;
	}
	public String getSto_ufrsfark_kod() {
		return sto_ufrsfark_kod;
	}
	public void setSto_ufrsfark_kod(String sto_ufrsfark_kod) {
		this.sto_ufrsfark_kod = sto_ufrsfark_kod;
	}
	public String getSto_iade_ufrsfark_kod() {
		return sto_iade_ufrsfark_kod;
	}
	public void setSto_iade_ufrsfark_kod(String sto_iade_ufrsfark_kod) {
		this.sto_iade_ufrsfark_kod = sto_iade_ufrsfark_kod;
	}
	public String getSto_yurticisat_ufrsfark_kod() {
		return sto_yurticisat_ufrsfark_kod;
	}
	public void setSto_yurticisat_ufrsfark_kod(String sto_yurticisat_ufrsfark_kod) {
		this.sto_yurticisat_ufrsfark_kod = sto_yurticisat_ufrsfark_kod;
	}
	public String getSto_satiade_ufrsfark_kod() {
		return sto_satiade_ufrsfark_kod;
	}
	public void setSto_satiade_ufrsfark_kod(String sto_satiade_ufrsfark_kod) {
		this.sto_satiade_ufrsfark_kod = sto_satiade_ufrsfark_kod;
	}
	public String getSto_satisk_ufrsfark_kod() {
		return sto_satisk_ufrsfark_kod;
	}
	public void setSto_satisk_ufrsfark_kod(String sto_satisk_ufrsfark_kod) {
		this.sto_satisk_ufrsfark_kod = sto_satisk_ufrsfark_kod;
	}
	public String getSto_alisk_ufrsfark_kod() {
		return sto_alisk_ufrsfark_kod;
	}
	public void setSto_alisk_ufrsfark_kod(String sto_alisk_ufrsfark_kod) {
		this.sto_alisk_ufrsfark_kod = sto_alisk_ufrsfark_kod;
	}
	public String getSto_satmal_ufrsfark_kod() {
		return sto_satmal_ufrsfark_kod;
	}
	public void setSto_satmal_ufrsfark_kod(String sto_satmal_ufrsfark_kod) {
		this.sto_satmal_ufrsfark_kod = sto_satmal_ufrsfark_kod;
	}
	public String getSto_yurtdisisat_ufrsfark_kod() {
		return sto_yurtdisisat_ufrsfark_kod;
	}
	public void setSto_yurtdisisat_ufrsfark_kod(String sto_yurtdisisat_ufrsfark_kod) {
		this.sto_yurtdisisat_ufrsfark_kod = sto_yurtdisisat_ufrsfark_kod;
	}
	public String getSto_ilavemas_ufrsfark_kod() {
		return sto_ilavemas_ufrsfark_kod;
	}
	public void setSto_ilavemas_ufrsfark_kod(String sto_ilavemas_ufrsfark_kod) {
		this.sto_ilavemas_ufrsfark_kod = sto_ilavemas_ufrsfark_kod;
	}
	public String getSto_yatirimtes_ufrsfark_kod() {
		return sto_yatirimtes_ufrsfark_kod;
	}
	public void setSto_yatirimtes_ufrsfark_kod(String sto_yatirimtes_ufrsfark_kod) {
		this.sto_yatirimtes_ufrsfark_kod = sto_yatirimtes_ufrsfark_kod;
	}
	public String getSto_depsat_ufrsfark_kod() {
		return sto_depsat_ufrsfark_kod;
	}
	public void setSto_depsat_ufrsfark_kod(String sto_depsat_ufrsfark_kod) {
		this.sto_depsat_ufrsfark_kod = sto_depsat_ufrsfark_kod;
	}
	public String getSto_depsatmal_ufrsfark_kod() {
		return sto_depsatmal_ufrsfark_kod;
	}
	public void setSto_depsatmal_ufrsfark_kod(String sto_depsatmal_ufrsfark_kod) {
		this.sto_depsatmal_ufrsfark_kod = sto_depsatmal_ufrsfark_kod;
	}
	public String getSto_bagortsat_ufrsfark_kod() {
		return sto_bagortsat_ufrsfark_kod;
	}
	public void setSto_bagortsat_ufrsfark_kod(String sto_bagortsat_ufrsfark_kod) {
		this.sto_bagortsat_ufrsfark_kod = sto_bagortsat_ufrsfark_kod;
	}
	public String getSto_bagortsatiade_ufrsfark_kod() {
		return sto_bagortsatiade_ufrsfark_kod;
	}
	public void setSto_bagortsatiade_ufrsfark_kod(
			String sto_bagortsatiade_ufrsfark_kod) {
		this.sto_bagortsatiade_ufrsfark_kod = sto_bagortsatiade_ufrsfark_kod;
	}
	public String getSto_bagortsatisk_ufrsfark_kod() {
		return sto_bagortsatisk_ufrsfark_kod;
	}
	public void setSto_bagortsatisk_ufrsfark_kod(
			String sto_bagortsatisk_ufrsfark_kod) {
		this.sto_bagortsatisk_ufrsfark_kod = sto_bagortsatisk_ufrsfark_kod;
	}
	public String getSto_satfiyfark_ufrsfark_kod() {
		return sto_satfiyfark_ufrsfark_kod;
	}
	public void setSto_satfiyfark_ufrsfark_kod(String sto_satfiyfark_ufrsfark_kod) {
		this.sto_satfiyfark_ufrsfark_kod = sto_satfiyfark_ufrsfark_kod;
	}
	public String getSto_yurtdisisatmal_ufrsfark_kod() {
		return sto_yurtdisisatmal_ufrsfark_kod;
	}
	public void setSto_yurtdisisatmal_ufrsfark_kod(
			String sto_yurtdisisatmal_ufrsfark_kod) {
		this.sto_yurtdisisatmal_ufrsfark_kod = sto_yurtdisisatmal_ufrsfark_kod;
	}
	public String getSto_bagortsatmal_ufrsfark_kod() {
		return sto_bagortsatmal_ufrsfark_kod;
	}
	public void setSto_bagortsatmal_ufrsfark_kod(
			String sto_bagortsatmal_ufrsfark_kod) {
		this.sto_bagortsatmal_ufrsfark_kod = sto_bagortsatmal_ufrsfark_kod;
	}
	public String getSto_uretimmaliyet_ufrsfark_kod() {
		return sto_uretimmaliyet_ufrsfark_kod;
	}
	public void setSto_uretimmaliyet_ufrsfark_kod(
			String sto_uretimmaliyet_ufrsfark_kod) {
		this.sto_uretimmaliyet_ufrsfark_kod = sto_uretimmaliyet_ufrsfark_kod;
	}
	public String getSto_uretimkapasite_ufrsfark_kod() {
		return sto_uretimkapasite_ufrsfark_kod;
	}
	public void setSto_uretimkapasite_ufrsfark_kod(
			String sto_uretimkapasite_ufrsfark_kod) {
		this.sto_uretimkapasite_ufrsfark_kod = sto_uretimkapasite_ufrsfark_kod;
	}
	public String getSto_degerdusuklugu_ufrs_kod() {
		return sto_degerdusuklugu_ufrs_kod;
	}
	public void setSto_degerdusuklugu_ufrs_kod(String sto_degerdusuklugu_ufrs_kod) {
		this.sto_degerdusuklugu_ufrs_kod = sto_degerdusuklugu_ufrs_kod;
	}
	public double getSto_halrusumyudesi() {
		return sto_halrusumyudesi;
	}
	public void setSto_halrusumyudesi(double sto_halrusumyudesi) {
		this.sto_halrusumyudesi = sto_halrusumyudesi;
	}
	public double getSto_webe_gonderilecek_fl() {
		return sto_webe_gonderilecek_fl;
	}
	public void setSto_webe_gonderilecek_fl(double sto_webe_gonderilecek_fl) {
		this.sto_webe_gonderilecek_fl = sto_webe_gonderilecek_fl;
	}
	public double getSto_min_stok_belirleme_gun() {
		return sto_min_stok_belirleme_gun;
	}
	public void setSto_min_stok_belirleme_gun(double sto_min_stok_belirleme_gun) {
		this.sto_min_stok_belirleme_gun = sto_min_stok_belirleme_gun;
	}
	public double getSto_sip_stok_belirleme_gun() {
		return sto_sip_stok_belirleme_gun;
	}
	public void setSto_sip_stok_belirleme_gun(double sto_sip_stok_belirleme_gun) {
		this.sto_sip_stok_belirleme_gun = sto_sip_stok_belirleme_gun;
	}
	public double getSto_max_stok_belirleme_gun() {
		return sto_max_stok_belirleme_gun;
	}
	public void setSto_max_stok_belirleme_gun(double sto_max_stok_belirleme_gun) {
		this.sto_max_stok_belirleme_gun = sto_max_stok_belirleme_gun;
	}
	public double getSto_sev_bel_opr_degerlendime_fl() {
		return sto_sev_bel_opr_degerlendime_fl;
	}
	public void setSto_sev_bel_opr_degerlendime_fl(
			double sto_sev_bel_opr_degerlendime_fl) {
		this.sto_sev_bel_opr_degerlendime_fl = sto_sev_bel_opr_degerlendime_fl;
	}
	public String getCari_unvan1() {
		return cari_unvan1;
	}
	public void setCari_unvan1(String cari_unvan1) {
		this.cari_unvan1 = cari_unvan1;
	}
	public int getCari_odemeplan_no() {
		return cari_odemeplan_no;
	}
	public void setCari_odemeplan_no(int cari_odemeplan_no) {
		this.cari_odemeplan_no = cari_odemeplan_no;
	}
	public String getAdr_cadde() {
		return adr_cadde;
	}
	public void setAdr_cadde(String adr_cadde) {
		this.adr_cadde = adr_cadde;
	}
	public String getAdr_sokak() {
		return adr_sokak;
	}
	public void setAdr_sokak(String adr_sokak) {
		this.adr_sokak = adr_sokak;
	}
	public String getAdr_ilce() {
		return adr_ilce;
	}
	public void setAdr_ilce(String adr_ilce) {
		this.adr_ilce = adr_ilce;
	}
	public String getAdr_il() {
		return adr_il;
	}
	public void setAdr_il(String adr_il) {
		this.adr_il = adr_il;
	}
	
	
}
