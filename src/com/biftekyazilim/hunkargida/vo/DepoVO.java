package com.biftekyazilim.hunkargida.vo;

public class DepoVO {

	private String dep_Adres_kodu;
   	private String dep_Apt_No;
   	private int dep_BagliOrtakliklaraSatisUygFiyat;
   	private String dep_Daire_No;
   	private int dep_DepoSevkOtoFiyat;
   	private int dep_DepoSevkUygFiyat;
   	private int dep_EksiyeDusurenStkHar;
   	private String dep_Il;
   	private String dep_Ilce;
   	private TimeAndZoneVO dep_KilitTarihi;
   	private int dep_RECid_DBCno;
   	private int dep_RECid_RECno;
   	private int dep_RECno;
   	private String dep_Semt;
   	private int dep_SpecRECno;
   	private String dep_Ulke;
   	private String dep_adi;
   	private int dep_alani;
   	private String dep_barkod_yazici_yolu;
   	private String dep_cadde;
   	private int dep_checksum;
   	private TimeAndZoneVO dep_create_date;
   	private int dep_create_user;
   	private int dep_degisti;
   	private int dep_detay_takibi;
   	private String dep_dizin_adi;
   	private int dep_envanter_harici_fl;
   	private String dep_fason_sor_mer_kodu;
   	private int dep_fileid;
   	private int dep_firmano;
   	private int dep_gps_boylam;
   	private int dep_gps_enlem;
   	private String dep_grup_kodu;
   	private int dep_hareket_tipi;
   	private int dep_hidden;
   	private int dep_iptal;
   	private int dep_kamyon_istiab_haddi;
   	private int dep_kamyon_kasa_hacmi;
   	private int dep_kasa_sayisi;
   	private int dep_kilitli;
   	private TimeAndZoneVO dep_lastup_date;
   	private int dep_lastup_user;
   	private String dep_mahalle;
   	private String dep_muh_kodu;
   	private int dep_no;
   	private int dep_otopark_alani;
   	private int dep_otopark_kapasite;
   	private String dep_posta_Kodu;
   	private String dep_proje_kodu;
   	private int dep_rafhacmi;
   	private int dep_satis_alani;
   	private int dep_sergi_alani;
   	private String dep_sokak;
   	private String dep_sor_mer_kodu;
   	private String dep_special1;
   	private String dep_special2;
   	private String dep_special3;
   	private int dep_subeno;
   	private String dep_tel_bolge_kodu;
   	private String dep_tel_faxno;
   	private String dep_tel_modem;
   	private String dep_tel_no1;
   	private String dep_tel_no2;
   	private String dep_tel_ulke_kodu;
   	private int dep_tipi;
   	private String dep_yetkili_email;
	public String getDep_Adres_kodu() {
		return dep_Adres_kodu;
	}
	public void setDep_Adres_kodu(String dep_Adres_kodu) {
		this.dep_Adres_kodu = dep_Adres_kodu;
	}
	public String getDep_Apt_No() {
		return dep_Apt_No;
	}
	public void setDep_Apt_No(String dep_Apt_No) {
		this.dep_Apt_No = dep_Apt_No;
	}
	public int getDep_BagliOrtakliklaraSatisUygFiyat() {
		return dep_BagliOrtakliklaraSatisUygFiyat;
	}
	public void setDep_BagliOrtakliklaraSatisUygFiyat(
			int dep_BagliOrtakliklaraSatisUygFiyat) {
		this.dep_BagliOrtakliklaraSatisUygFiyat = dep_BagliOrtakliklaraSatisUygFiyat;
	}
	public String getDep_Daire_No() {
		return dep_Daire_No;
	}
	public void setDep_Daire_No(String dep_Daire_No) {
		this.dep_Daire_No = dep_Daire_No;
	}
	public int getDep_DepoSevkOtoFiyat() {
		return dep_DepoSevkOtoFiyat;
	}
	public void setDep_DepoSevkOtoFiyat(int dep_DepoSevkOtoFiyat) {
		this.dep_DepoSevkOtoFiyat = dep_DepoSevkOtoFiyat;
	}
	public int getDep_DepoSevkUygFiyat() {
		return dep_DepoSevkUygFiyat;
	}
	public void setDep_DepoSevkUygFiyat(int dep_DepoSevkUygFiyat) {
		this.dep_DepoSevkUygFiyat = dep_DepoSevkUygFiyat;
	}
	public int getDep_EksiyeDusurenStkHar() {
		return dep_EksiyeDusurenStkHar;
	}
	public void setDep_EksiyeDusurenStkHar(int dep_EksiyeDusurenStkHar) {
		this.dep_EksiyeDusurenStkHar = dep_EksiyeDusurenStkHar;
	}
	public String getDep_Il() {
		return dep_Il;
	}
	public void setDep_Il(String dep_Il) {
		this.dep_Il = dep_Il;
	}
	public String getDep_Ilce() {
		return dep_Ilce;
	}
	public void setDep_Ilce(String dep_Ilce) {
		this.dep_Ilce = dep_Ilce;
	}
	public TimeAndZoneVO getDep_KilitTarihi() {
		return dep_KilitTarihi;
	}
	public void setDep_KilitTarihi(TimeAndZoneVO dep_KilitTarihi) {
		this.dep_KilitTarihi = dep_KilitTarihi;
	}
	public int getDep_RECid_DBCno() {
		return dep_RECid_DBCno;
	}
	public void setDep_RECid_DBCno(int dep_RECid_DBCno) {
		this.dep_RECid_DBCno = dep_RECid_DBCno;
	}
	public int getDep_RECid_RECno() {
		return dep_RECid_RECno;
	}
	public void setDep_RECid_RECno(int dep_RECid_RECno) {
		this.dep_RECid_RECno = dep_RECid_RECno;
	}
	public int getDep_RECno() {
		return dep_RECno;
	}
	public void setDep_RECno(int dep_RECno) {
		this.dep_RECno = dep_RECno;
	}
	public String getDep_Semt() {
		return dep_Semt;
	}
	public void setDep_Semt(String dep_Semt) {
		this.dep_Semt = dep_Semt;
	}
	public int getDep_SpecRECno() {
		return dep_SpecRECno;
	}
	public void setDep_SpecRECno(int dep_SpecRECno) {
		this.dep_SpecRECno = dep_SpecRECno;
	}
	public String getDep_Ulke() {
		return dep_Ulke;
	}
	public void setDep_Ulke(String dep_Ulke) {
		this.dep_Ulke = dep_Ulke;
	}
	public String getDep_adi() {
		return dep_adi;
	}
	public void setDep_adi(String dep_adi) {
		this.dep_adi = dep_adi;
	}
	public int getDep_alani() {
		return dep_alani;
	}
	public void setDep_alani(int dep_alani) {
		this.dep_alani = dep_alani;
	}
	public String getDep_barkod_yazici_yolu() {
		return dep_barkod_yazici_yolu;
	}
	public void setDep_barkod_yazici_yolu(String dep_barkod_yazici_yolu) {
		this.dep_barkod_yazici_yolu = dep_barkod_yazici_yolu;
	}
	public String getDep_cadde() {
		return dep_cadde;
	}
	public void setDep_cadde(String dep_cadde) {
		this.dep_cadde = dep_cadde;
	}
	public int getDep_checksum() {
		return dep_checksum;
	}
	public void setDep_checksum(int dep_checksum) {
		this.dep_checksum = dep_checksum;
	}
	public TimeAndZoneVO getDep_create_date() {
		return dep_create_date;
	}
	public void setDep_create_date(TimeAndZoneVO dep_create_date) {
		this.dep_create_date = dep_create_date;
	}
	public int getDep_create_user() {
		return dep_create_user;
	}
	public void setDep_create_user(int dep_create_user) {
		this.dep_create_user = dep_create_user;
	}
	public int getDep_degisti() {
		return dep_degisti;
	}
	public void setDep_degisti(int dep_degisti) {
		this.dep_degisti = dep_degisti;
	}
	public int getDep_detay_takibi() {
		return dep_detay_takibi;
	}
	public void setDep_detay_takibi(int dep_detay_takibi) {
		this.dep_detay_takibi = dep_detay_takibi;
	}
	public String getDep_dizin_adi() {
		return dep_dizin_adi;
	}
	public void setDep_dizin_adi(String dep_dizin_adi) {
		this.dep_dizin_adi = dep_dizin_adi;
	}
	public int getDep_envanter_harici_fl() {
		return dep_envanter_harici_fl;
	}
	public void setDep_envanter_harici_fl(int dep_envanter_harici_fl) {
		this.dep_envanter_harici_fl = dep_envanter_harici_fl;
	}
	public String getDep_fason_sor_mer_kodu() {
		return dep_fason_sor_mer_kodu;
	}
	public void setDep_fason_sor_mer_kodu(String dep_fason_sor_mer_kodu) {
		this.dep_fason_sor_mer_kodu = dep_fason_sor_mer_kodu;
	}
	public int getDep_fileid() {
		return dep_fileid;
	}
	public void setDep_fileid(int dep_fileid) {
		this.dep_fileid = dep_fileid;
	}
	public int getDep_firmano() {
		return dep_firmano;
	}
	public void setDep_firmano(int dep_firmano) {
		this.dep_firmano = dep_firmano;
	}
	public int getDep_gps_boylam() {
		return dep_gps_boylam;
	}
	public void setDep_gps_boylam(int dep_gps_boylam) {
		this.dep_gps_boylam = dep_gps_boylam;
	}
	public int getDep_gps_enlem() {
		return dep_gps_enlem;
	}
	public void setDep_gps_enlem(int dep_gps_enlem) {
		this.dep_gps_enlem = dep_gps_enlem;
	}
	public String getDep_grup_kodu() {
		return dep_grup_kodu;
	}
	public void setDep_grup_kodu(String dep_grup_kodu) {
		this.dep_grup_kodu = dep_grup_kodu;
	}
	public int getDep_hareket_tipi() {
		return dep_hareket_tipi;
	}
	public void setDep_hareket_tipi(int dep_hareket_tipi) {
		this.dep_hareket_tipi = dep_hareket_tipi;
	}
	public int getDep_hidden() {
		return dep_hidden;
	}
	public void setDep_hidden(int dep_hidden) {
		this.dep_hidden = dep_hidden;
	}
	public int getDep_iptal() {
		return dep_iptal;
	}
	public void setDep_iptal(int dep_iptal) {
		this.dep_iptal = dep_iptal;
	}
	public int getDep_kamyon_istiab_haddi() {
		return dep_kamyon_istiab_haddi;
	}
	public void setDep_kamyon_istiab_haddi(int dep_kamyon_istiab_haddi) {
		this.dep_kamyon_istiab_haddi = dep_kamyon_istiab_haddi;
	}
	public int getDep_kamyon_kasa_hacmi() {
		return dep_kamyon_kasa_hacmi;
	}
	public void setDep_kamyon_kasa_hacmi(int dep_kamyon_kasa_hacmi) {
		this.dep_kamyon_kasa_hacmi = dep_kamyon_kasa_hacmi;
	}
	public int getDep_kasa_sayisi() {
		return dep_kasa_sayisi;
	}
	public void setDep_kasa_sayisi(int dep_kasa_sayisi) {
		this.dep_kasa_sayisi = dep_kasa_sayisi;
	}
	public int getDep_kilitli() {
		return dep_kilitli;
	}
	public void setDep_kilitli(int dep_kilitli) {
		this.dep_kilitli = dep_kilitli;
	}
	public TimeAndZoneVO getDep_lastup_date() {
		return dep_lastup_date;
	}
	public void setDep_lastup_date(TimeAndZoneVO dep_lastup_date) {
		this.dep_lastup_date = dep_lastup_date;
	}
	public int getDep_lastup_user() {
		return dep_lastup_user;
	}
	public void setDep_lastup_user(int dep_lastup_user) {
		this.dep_lastup_user = dep_lastup_user;
	}
	public String getDep_mahalle() {
		return dep_mahalle;
	}
	public void setDep_mahalle(String dep_mahalle) {
		this.dep_mahalle = dep_mahalle;
	}
	public String getDep_muh_kodu() {
		return dep_muh_kodu;
	}
	public void setDep_muh_kodu(String dep_muh_kodu) {
		this.dep_muh_kodu = dep_muh_kodu;
	}
	public int getDep_no() {
		return dep_no;
	}
	public void setDep_no(int dep_no) {
		this.dep_no = dep_no;
	}
	public int getDep_otopark_alani() {
		return dep_otopark_alani;
	}
	public void setDep_otopark_alani(int dep_otopark_alani) {
		this.dep_otopark_alani = dep_otopark_alani;
	}
	public int getDep_otopark_kapasite() {
		return dep_otopark_kapasite;
	}
	public void setDep_otopark_kapasite(int dep_otopark_kapasite) {
		this.dep_otopark_kapasite = dep_otopark_kapasite;
	}
	public String getDep_posta_Kodu() {
		return dep_posta_Kodu;
	}
	public void setDep_posta_Kodu(String dep_posta_Kodu) {
		this.dep_posta_Kodu = dep_posta_Kodu;
	}
	public String getDep_proje_kodu() {
		return dep_proje_kodu;
	}
	public void setDep_proje_kodu(String dep_proje_kodu) {
		this.dep_proje_kodu = dep_proje_kodu;
	}
	public int getDep_rafhacmi() {
		return dep_rafhacmi;
	}
	public void setDep_rafhacmi(int dep_rafhacmi) {
		this.dep_rafhacmi = dep_rafhacmi;
	}
	public int getDep_satis_alani() {
		return dep_satis_alani;
	}
	public void setDep_satis_alani(int dep_satis_alani) {
		this.dep_satis_alani = dep_satis_alani;
	}
	public int getDep_sergi_alani() {
		return dep_sergi_alani;
	}
	public void setDep_sergi_alani(int dep_sergi_alani) {
		this.dep_sergi_alani = dep_sergi_alani;
	}
	public String getDep_sokak() {
		return dep_sokak;
	}
	public void setDep_sokak(String dep_sokak) {
		this.dep_sokak = dep_sokak;
	}
	public String getDep_sor_mer_kodu() {
		return dep_sor_mer_kodu;
	}
	public void setDep_sor_mer_kodu(String dep_sor_mer_kodu) {
		this.dep_sor_mer_kodu = dep_sor_mer_kodu;
	}
	public String getDep_special1() {
		return dep_special1;
	}
	public void setDep_special1(String dep_special1) {
		this.dep_special1 = dep_special1;
	}
	public String getDep_special2() {
		return dep_special2;
	}
	public void setDep_special2(String dep_special2) {
		this.dep_special2 = dep_special2;
	}
	public String getDep_special3() {
		return dep_special3;
	}
	public void setDep_special3(String dep_special3) {
		this.dep_special3 = dep_special3;
	}
	public int getDep_subeno() {
		return dep_subeno;
	}
	public void setDep_subeno(int dep_subeno) {
		this.dep_subeno = dep_subeno;
	}
	public String getDep_tel_bolge_kodu() {
		return dep_tel_bolge_kodu;
	}
	public void setDep_tel_bolge_kodu(String dep_tel_bolge_kodu) {
		this.dep_tel_bolge_kodu = dep_tel_bolge_kodu;
	}
	public String getDep_tel_faxno() {
		return dep_tel_faxno;
	}
	public void setDep_tel_faxno(String dep_tel_faxno) {
		this.dep_tel_faxno = dep_tel_faxno;
	}
	public String getDep_tel_modem() {
		return dep_tel_modem;
	}
	public void setDep_tel_modem(String dep_tel_modem) {
		this.dep_tel_modem = dep_tel_modem;
	}
	public String getDep_tel_no1() {
		return dep_tel_no1;
	}
	public void setDep_tel_no1(String dep_tel_no1) {
		this.dep_tel_no1 = dep_tel_no1;
	}
	public String getDep_tel_no2() {
		return dep_tel_no2;
	}
	public void setDep_tel_no2(String dep_tel_no2) {
		this.dep_tel_no2 = dep_tel_no2;
	}
	public String getDep_tel_ulke_kodu() {
		return dep_tel_ulke_kodu;
	}
	public void setDep_tel_ulke_kodu(String dep_tel_ulke_kodu) {
		this.dep_tel_ulke_kodu = dep_tel_ulke_kodu;
	}
	public int getDep_tipi() {
		return dep_tipi;
	}
	public void setDep_tipi(int dep_tipi) {
		this.dep_tipi = dep_tipi;
	}
	public String getDep_yetkili_email() {
		return dep_yetkili_email;
	}
	public void setDep_yetkili_email(String dep_yetkili_email) {
		this.dep_yetkili_email = dep_yetkili_email;
	}
   	
   	
}
