package com.biftekyazilim.hunkargida.vo;

public class CariStokVO {

	
	private int kayit_no;
	private String  stok_kodu;
	private String  stok_adi;
	private String  parti;
	private int lot;
	private TimeAndZoneVO tarih;
	private String  seri;
	private int sira_no;
	private String  belge_no;
	private TimeAndZoneVO belge_tarihi;
	private String  evrak_tipi;
	private String  hareket_cinsi;
	private String  tipi;
	private String  ni;
	private String  depo;
	private double  giren_miktar;
	private double  cikan_miktar;
	private String  birim_adi;
	private double  giren_miktar_2birim;
	private double  cikan_miktar_2birim;
	private double  miktar_2birim;
	//private String  2_birim_adi;
	private double  giren_miktar_3birim;
	private double  cikan_miktar_3birim;
	private double  miktar_3birim;
	//private String  3_birim_adi;
	private double  giren_miktar_4birim;
	private double  cikan_miktar_4birim;
	private double  miktar_4birim;
	//private String  4_birim_adi;
	private double  ana_doviz_brut_birim_fiyati;
	private double  ana_doviz_net_birim_fiyati;
	private double  ana_doviz_brut_tutar;
	private double  ana_doviz_iskonto_1;
	private double  ana_doviz_iskonto_2;
	private double  ana_doviz_iskonto_3;
	private double  ana_doviz_iskonto_4;
	private double  ana_doviz_iskonto_5;
	private double  ana_doviz_iskonto_6;
	private double  ana_doviz_toplam_iskonto;
	private double  ana_doviz_masraf_1;
	private double  ana_doviz_masraf_2;
	private double  ana_doviz_masraf_3;
	private double  ana_doviz_masraf_4;
	private double  ana_doviz_toplam_masraf;
	private double  ana_doviz_net_tutar;
	private double  vergili_tutar_yerel_doviz;
	private double  vergili_tutar_alternatif_doviz;
	private double  alternatif_doviz_brut_birim_fiyati;
	private double  alternatif_doviz_net_birim_fiyati;
	private double  alternatif_doviz_brut_tutar;
	private double  alternatif_doviz_iskonto_1;
	private double  alternatif_doviz_iskonto_2;
	private double  alternatif_doviz_iskonto_3;
	private double  alternatif_doviz_iskonto_4;
	private double  alternatif_doviz_iskonto_5;
	private double  alternatif_doviz_iskonto_6;
	private double  alternatif_doviz_toplam_iskonto;
	private double  alternatif_doviz_masraf_1;
	private double  alternatif_doviz_masraf_2;
	private double  alternatif_doviz_masraf_3;
	private double  alternatif_doviz_masraf_4;
	private double  alternatif_doviz_toplam_masraf;
	private double  alternatif_doviz_net_tutar;
	private double  orjinal_doviz_brut_birim_fiyati;
	private double  orjinal_doviz_net_birim_fiyati;
	private double  orjinal_doviz_brut_tutar;
	private double  orjinal_doviz_iskonto_1;
	private double  orjinal_doviz_iskonto_2;
	private double  orjinal_doviz_iskonto_3;
	private double  orjinal_doviz_iskonto_4;
	private double  orjinal_doviz_iskonto_5;
	private double  orjinal_doviz_iskonto_6;
	private double  orjinal_doviz_toplam_iskonto;
	private double  orjinal_doviz_masraf_1;
	private double  orjinal_doviz_masraf_2;
	private double  orjinal_doviz_masraf_3;
	private double  orjinal_doviz_masraf_4;
	private double  orjinal_doviz_toplam_masraf;
	private double  orjinal_doviz_net_tutar;
	private double  evrak_dovizi_brut_birim_fiyati;
	private double  evrak_dovizi_net_birim_fiyati;
	private double  evrak_dovizi_brut_tutar;
	private double  evrak_dovizi_iskonto_1;
	private double  evrak_dovizi_iskonto_2;
	private double  evrak_dovizi_iskonto_3;
	private double  evrak_dovizi_iskonto_4;
	private double  evrak_dovizi_iskonto_5;
	private double  evrak_dovizi_iskonto_6;
	private double  evrak_dovizi_toplam_iskonto;
	private double  evrak_dovizi_masraf_1;
	private double  evrak_dovizi_masraf_2;
	private double  evrak_dovizi_masraf_3;
	private double  evrak_dovizi_masraf_4;
	private double  evrak_dovizi_toplam_masraf;
	private double  evrak_dovizi_net_tutar;
	private String  aciklama;
	private String  fatseri;
	private int fatsira_no;
	private String  srmmrkkodu;
	private String  srmmrkadi;
	private String  stok_sorumluluk_merkezi;
	private String  stok_sorumluluk_merkezi_adi;
	private String  proje_kodu;
	private String  proje_adi;
	private double  ana_doviz_giren_maliyet_deger;
	private double  ana_doviz_cikan_maliyet_deger;
	private double  ana_doviz_kar_veya_maliyet_farki;
	private double  alternatif_doviz_giren_maliyet_deger;
	private double  alternatif_doviz_cikan_maliyet_deger;
	private double  alternatif_doviz_kar_veya_maliyet_farki;
	private double  orjinal_doviz_giren_maliyet_deger;
	private double  orjinal_doviz_cikan_maliyet_deger;
	private double  orjinal_doviz_kar_veya_maliyet_farki;
	public int getKayit_no() {
		return kayit_no;
	}
	public void setKayit_no(int kayit_no) {
		this.kayit_no = kayit_no;
	}
	public String getStok_kodu() {
		return stok_kodu;
	}
	public void setStok_kodu(String stok_kodu) {
		this.stok_kodu = stok_kodu;
	}
	public String getStok_adi() {
		return stok_adi;
	}
	public void setStok_adi(String stok_adi) {
		this.stok_adi = stok_adi;
	}
	public String getParti() {
		return parti;
	}
	public void setParti(String parti) {
		this.parti = parti;
	}
	public int getLot() {
		return lot;
	}
	public void setLot(int lot) {
		this.lot = lot;
	}
	public TimeAndZoneVO getTarih() {
		return tarih;
	}
	public void setTarih(TimeAndZoneVO tarih) {
		this.tarih = tarih;
	}
	public String getSeri() {
		return seri;
	}
	public void setSeri(String seri) {
		this.seri = seri;
	}
	public int getSira_no() {
		return sira_no;
	}
	public void setSira_no(int sira_no) {
		this.sira_no = sira_no;
	}
	public String getBelge_no() {
		return belge_no;
	}
	public void setBelge_no(String belge_no) {
		this.belge_no = belge_no;
	}
	public TimeAndZoneVO getBelge_tarihi() {
		return belge_tarihi;
	}
	public void setBelge_tarihi(TimeAndZoneVO belge_tarihi) {
		this.belge_tarihi = belge_tarihi;
	}
	public String getEvrak_tipi() {
		return evrak_tipi;
	}
	public void setEvrak_tipi(String evrak_tipi) {
		this.evrak_tipi = evrak_tipi;
	}
	public String getHareket_cinsi() {
		return hareket_cinsi;
	}
	public void setHareket_cinsi(String hareket_cinsi) {
		this.hareket_cinsi = hareket_cinsi;
	}
	public String getTipi() {
		return tipi;
	}
	public void setTipi(String tipi) {
		this.tipi = tipi;
	}
	public String getNi() {
		return ni;
	}
	public void setNi(String ni) {
		this.ni = ni;
	}
	public String getDepo() {
		return depo;
	}
	public void setDepo(String depo) {
		this.depo = depo;
	}
	public double getGiren_miktar() {
		return giren_miktar;
	}
	public void setGiren_miktar(double giren_miktar) {
		this.giren_miktar = giren_miktar;
	}
	public double getCikan_miktar() {
		return cikan_miktar;
	}
	public void setCikan_miktar(double cikan_miktar) {
		this.cikan_miktar = cikan_miktar;
	}
	public String getBirim_adi() {
		return birim_adi;
	}
	public void setBirim_adi(String birim_adi) {
		this.birim_adi = birim_adi;
	}
	public double getGiren_miktar_2birim() {
		return giren_miktar_2birim;
	}
	public void setGiren_miktar_2birim(double giren_miktar_2birim) {
		this.giren_miktar_2birim = giren_miktar_2birim;
	}
	public double getCikan_miktar_2birim() {
		return cikan_miktar_2birim;
	}
	public void setCikan_miktar_2birim(double cikan_miktar_2birim) {
		this.cikan_miktar_2birim = cikan_miktar_2birim;
	}
	public double getMiktar_2birim() {
		return miktar_2birim;
	}
	public void setMiktar_2birim(double miktar_2birim) {
		this.miktar_2birim = miktar_2birim;
	}
	public double getGiren_miktar_3birim() {
		return giren_miktar_3birim;
	}
	public void setGiren_miktar_3birim(double giren_miktar_3birim) {
		this.giren_miktar_3birim = giren_miktar_3birim;
	}
	public double getCikan_miktar_3birim() {
		return cikan_miktar_3birim;
	}
	public void setCikan_miktar_3birim(double cikan_miktar_3birim) {
		this.cikan_miktar_3birim = cikan_miktar_3birim;
	}
	public double getMiktar_3birim() {
		return miktar_3birim;
	}
	public void setMiktar_3birim(double miktar_3birim) {
		this.miktar_3birim = miktar_3birim;
	}
	public double getGiren_miktar_4birim() {
		return giren_miktar_4birim;
	}
	public void setGiren_miktar_4birim(double giren_miktar_4birim) {
		this.giren_miktar_4birim = giren_miktar_4birim;
	}
	public double getCikan_miktar_4birim() {
		return cikan_miktar_4birim;
	}
	public void setCikan_miktar_4birim(double cikan_miktar_4birim) {
		this.cikan_miktar_4birim = cikan_miktar_4birim;
	}
	public double getMiktar_4birim() {
		return miktar_4birim;
	}
	public void setMiktar_4birim(double miktar_4birim) {
		this.miktar_4birim = miktar_4birim;
	}
	public double getAna_doviz_brut_birim_fiyati() {
		return ana_doviz_brut_birim_fiyati;
	}
	public void setAna_doviz_brut_birim_fiyati(double ana_doviz_brut_birim_fiyati) {
		this.ana_doviz_brut_birim_fiyati = ana_doviz_brut_birim_fiyati;
	}
	public double getAna_doviz_net_birim_fiyati() {
		return ana_doviz_net_birim_fiyati;
	}
	public void setAna_doviz_net_birim_fiyati(double ana_doviz_net_birim_fiyati) {
		this.ana_doviz_net_birim_fiyati = ana_doviz_net_birim_fiyati;
	}
	public double getAna_doviz_brut_tutar() {
		return ana_doviz_brut_tutar;
	}
	public void setAna_doviz_brut_tutar(double ana_doviz_brut_tutar) {
		this.ana_doviz_brut_tutar = ana_doviz_brut_tutar;
	}
	public double getAna_doviz_iskonto_1() {
		return ana_doviz_iskonto_1;
	}
	public void setAna_doviz_iskonto_1(double ana_doviz_iskonto_1) {
		this.ana_doviz_iskonto_1 = ana_doviz_iskonto_1;
	}
	public double getAna_doviz_iskonto_2() {
		return ana_doviz_iskonto_2;
	}
	public void setAna_doviz_iskonto_2(double ana_doviz_iskonto_2) {
		this.ana_doviz_iskonto_2 = ana_doviz_iskonto_2;
	}
	public double getAna_doviz_iskonto_3() {
		return ana_doviz_iskonto_3;
	}
	public void setAna_doviz_iskonto_3(double ana_doviz_iskonto_3) {
		this.ana_doviz_iskonto_3 = ana_doviz_iskonto_3;
	}
	public double getAna_doviz_iskonto_4() {
		return ana_doviz_iskonto_4;
	}
	public void setAna_doviz_iskonto_4(double ana_doviz_iskonto_4) {
		this.ana_doviz_iskonto_4 = ana_doviz_iskonto_4;
	}
	public double getAna_doviz_iskonto_5() {
		return ana_doviz_iskonto_5;
	}
	public void setAna_doviz_iskonto_5(double ana_doviz_iskonto_5) {
		this.ana_doviz_iskonto_5 = ana_doviz_iskonto_5;
	}
	public double getAna_doviz_iskonto_6() {
		return ana_doviz_iskonto_6;
	}
	public void setAna_doviz_iskonto_6(double ana_doviz_iskonto_6) {
		this.ana_doviz_iskonto_6 = ana_doviz_iskonto_6;
	}
	public double getAna_doviz_toplam_iskonto() {
		return ana_doviz_toplam_iskonto;
	}
	public void setAna_doviz_toplam_iskonto(double ana_doviz_toplam_iskonto) {
		this.ana_doviz_toplam_iskonto = ana_doviz_toplam_iskonto;
	}
	public double getAna_doviz_masraf_1() {
		return ana_doviz_masraf_1;
	}
	public void setAna_doviz_masraf_1(double ana_doviz_masraf_1) {
		this.ana_doviz_masraf_1 = ana_doviz_masraf_1;
	}
	public double getAna_doviz_masraf_2() {
		return ana_doviz_masraf_2;
	}
	public void setAna_doviz_masraf_2(double ana_doviz_masraf_2) {
		this.ana_doviz_masraf_2 = ana_doviz_masraf_2;
	}
	public double getAna_doviz_masraf_3() {
		return ana_doviz_masraf_3;
	}
	public void setAna_doviz_masraf_3(double ana_doviz_masraf_3) {
		this.ana_doviz_masraf_3 = ana_doviz_masraf_3;
	}
	public double getAna_doviz_masraf_4() {
		return ana_doviz_masraf_4;
	}
	public void setAna_doviz_masraf_4(double ana_doviz_masraf_4) {
		this.ana_doviz_masraf_4 = ana_doviz_masraf_4;
	}
	public double getAna_doviz_toplam_masraf() {
		return ana_doviz_toplam_masraf;
	}
	public void setAna_doviz_toplam_masraf(double ana_doviz_toplam_masraf) {
		this.ana_doviz_toplam_masraf = ana_doviz_toplam_masraf;
	}
	public double getAna_doviz_net_tutar() {
		return ana_doviz_net_tutar;
	}
	public void setAna_doviz_net_tutar(double ana_doviz_net_tutar) {
		this.ana_doviz_net_tutar = ana_doviz_net_tutar;
	}
	public double getVergili_tutar_yerel_doviz() {
		return vergili_tutar_yerel_doviz;
	}
	public void setVergili_tutar_yerel_doviz(double vergili_tutar_yerel_doviz) {
		this.vergili_tutar_yerel_doviz = vergili_tutar_yerel_doviz;
	}
	public double getVergili_tutar_alternatif_doviz() {
		return vergili_tutar_alternatif_doviz;
	}
	public void setVergili_tutar_alternatif_doviz(
			double vergili_tutar_alternatif_doviz) {
		this.vergili_tutar_alternatif_doviz = vergili_tutar_alternatif_doviz;
	}
	public double getAlternatif_doviz_brut_birim_fiyati() {
		return alternatif_doviz_brut_birim_fiyati;
	}
	public void setAlternatif_doviz_brut_birim_fiyati(
			double alternatif_doviz_brut_birim_fiyati) {
		this.alternatif_doviz_brut_birim_fiyati = alternatif_doviz_brut_birim_fiyati;
	}
	public double getAlternatif_doviz_net_birim_fiyati() {
		return alternatif_doviz_net_birim_fiyati;
	}
	public void setAlternatif_doviz_net_birim_fiyati(
			double alternatif_doviz_net_birim_fiyati) {
		this.alternatif_doviz_net_birim_fiyati = alternatif_doviz_net_birim_fiyati;
	}
	public double getAlternatif_doviz_brut_tutar() {
		return alternatif_doviz_brut_tutar;
	}
	public void setAlternatif_doviz_brut_tutar(double alternatif_doviz_brut_tutar) {
		this.alternatif_doviz_brut_tutar = alternatif_doviz_brut_tutar;
	}
	public double getAlternatif_doviz_iskonto_1() {
		return alternatif_doviz_iskonto_1;
	}
	public void setAlternatif_doviz_iskonto_1(double alternatif_doviz_iskonto_1) {
		this.alternatif_doviz_iskonto_1 = alternatif_doviz_iskonto_1;
	}
	public double getAlternatif_doviz_iskonto_2() {
		return alternatif_doviz_iskonto_2;
	}
	public void setAlternatif_doviz_iskonto_2(double alternatif_doviz_iskonto_2) {
		this.alternatif_doviz_iskonto_2 = alternatif_doviz_iskonto_2;
	}
	public double getAlternatif_doviz_iskonto_3() {
		return alternatif_doviz_iskonto_3;
	}
	public void setAlternatif_doviz_iskonto_3(double alternatif_doviz_iskonto_3) {
		this.alternatif_doviz_iskonto_3 = alternatif_doviz_iskonto_3;
	}
	public double getAlternatif_doviz_iskonto_4() {
		return alternatif_doviz_iskonto_4;
	}
	public void setAlternatif_doviz_iskonto_4(double alternatif_doviz_iskonto_4) {
		this.alternatif_doviz_iskonto_4 = alternatif_doviz_iskonto_4;
	}
	public double getAlternatif_doviz_iskonto_5() {
		return alternatif_doviz_iskonto_5;
	}
	public void setAlternatif_doviz_iskonto_5(double alternatif_doviz_iskonto_5) {
		this.alternatif_doviz_iskonto_5 = alternatif_doviz_iskonto_5;
	}
	public double getAlternatif_doviz_iskonto_6() {
		return alternatif_doviz_iskonto_6;
	}
	public void setAlternatif_doviz_iskonto_6(double alternatif_doviz_iskonto_6) {
		this.alternatif_doviz_iskonto_6 = alternatif_doviz_iskonto_6;
	}
	public double getAlternatif_doviz_toplam_iskonto() {
		return alternatif_doviz_toplam_iskonto;
	}
	public void setAlternatif_doviz_toplam_iskonto(
			double alternatif_doviz_toplam_iskonto) {
		this.alternatif_doviz_toplam_iskonto = alternatif_doviz_toplam_iskonto;
	}
	public double getAlternatif_doviz_masraf_1() {
		return alternatif_doviz_masraf_1;
	}
	public void setAlternatif_doviz_masraf_1(double alternatif_doviz_masraf_1) {
		this.alternatif_doviz_masraf_1 = alternatif_doviz_masraf_1;
	}
	public double getAlternatif_doviz_masraf_2() {
		return alternatif_doviz_masraf_2;
	}
	public void setAlternatif_doviz_masraf_2(double alternatif_doviz_masraf_2) {
		this.alternatif_doviz_masraf_2 = alternatif_doviz_masraf_2;
	}
	public double getAlternatif_doviz_masraf_3() {
		return alternatif_doviz_masraf_3;
	}
	public void setAlternatif_doviz_masraf_3(double alternatif_doviz_masraf_3) {
		this.alternatif_doviz_masraf_3 = alternatif_doviz_masraf_3;
	}
	public double getAlternatif_doviz_masraf_4() {
		return alternatif_doviz_masraf_4;
	}
	public void setAlternatif_doviz_masraf_4(double alternatif_doviz_masraf_4) {
		this.alternatif_doviz_masraf_4 = alternatif_doviz_masraf_4;
	}
	public double getAlternatif_doviz_toplam_masraf() {
		return alternatif_doviz_toplam_masraf;
	}
	public void setAlternatif_doviz_toplam_masraf(
			double alternatif_doviz_toplam_masraf) {
		this.alternatif_doviz_toplam_masraf = alternatif_doviz_toplam_masraf;
	}
	public double getAlternatif_doviz_net_tutar() {
		return alternatif_doviz_net_tutar;
	}
	public void setAlternatif_doviz_net_tutar(double alternatif_doviz_net_tutar) {
		this.alternatif_doviz_net_tutar = alternatif_doviz_net_tutar;
	}
	public double getOrjinal_doviz_brut_birim_fiyati() {
		return orjinal_doviz_brut_birim_fiyati;
	}
	public void setOrjinal_doviz_brut_birim_fiyati(
			double orjinal_doviz_brut_birim_fiyati) {
		this.orjinal_doviz_brut_birim_fiyati = orjinal_doviz_brut_birim_fiyati;
	}
	public double getOrjinal_doviz_net_birim_fiyati() {
		return orjinal_doviz_net_birim_fiyati;
	}
	public void setOrjinal_doviz_net_birim_fiyati(
			double orjinal_doviz_net_birim_fiyati) {
		this.orjinal_doviz_net_birim_fiyati = orjinal_doviz_net_birim_fiyati;
	}
	public double getOrjinal_doviz_brut_tutar() {
		return orjinal_doviz_brut_tutar;
	}
	public void setOrjinal_doviz_brut_tutar(double orjinal_doviz_brut_tutar) {
		this.orjinal_doviz_brut_tutar = orjinal_doviz_brut_tutar;
	}
	public double getOrjinal_doviz_iskonto_1() {
		return orjinal_doviz_iskonto_1;
	}
	public void setOrjinal_doviz_iskonto_1(double orjinal_doviz_iskonto_1) {
		this.orjinal_doviz_iskonto_1 = orjinal_doviz_iskonto_1;
	}
	public double getOrjinal_doviz_iskonto_2() {
		return orjinal_doviz_iskonto_2;
	}
	public void setOrjinal_doviz_iskonto_2(double orjinal_doviz_iskonto_2) {
		this.orjinal_doviz_iskonto_2 = orjinal_doviz_iskonto_2;
	}
	public double getOrjinal_doviz_iskonto_3() {
		return orjinal_doviz_iskonto_3;
	}
	public void setOrjinal_doviz_iskonto_3(double orjinal_doviz_iskonto_3) {
		this.orjinal_doviz_iskonto_3 = orjinal_doviz_iskonto_3;
	}
	public double getOrjinal_doviz_iskonto_4() {
		return orjinal_doviz_iskonto_4;
	}
	public void setOrjinal_doviz_iskonto_4(double orjinal_doviz_iskonto_4) {
		this.orjinal_doviz_iskonto_4 = orjinal_doviz_iskonto_4;
	}
	public double getOrjinal_doviz_iskonto_5() {
		return orjinal_doviz_iskonto_5;
	}
	public void setOrjinal_doviz_iskonto_5(double orjinal_doviz_iskonto_5) {
		this.orjinal_doviz_iskonto_5 = orjinal_doviz_iskonto_5;
	}
	public double getOrjinal_doviz_iskonto_6() {
		return orjinal_doviz_iskonto_6;
	}
	public void setOrjinal_doviz_iskonto_6(double orjinal_doviz_iskonto_6) {
		this.orjinal_doviz_iskonto_6 = orjinal_doviz_iskonto_6;
	}
	public double getOrjinal_doviz_toplam_iskonto() {
		return orjinal_doviz_toplam_iskonto;
	}
	public void setOrjinal_doviz_toplam_iskonto(double orjinal_doviz_toplam_iskonto) {
		this.orjinal_doviz_toplam_iskonto = orjinal_doviz_toplam_iskonto;
	}
	public double getOrjinal_doviz_masraf_1() {
		return orjinal_doviz_masraf_1;
	}
	public void setOrjinal_doviz_masraf_1(double orjinal_doviz_masraf_1) {
		this.orjinal_doviz_masraf_1 = orjinal_doviz_masraf_1;
	}
	public double getOrjinal_doviz_masraf_2() {
		return orjinal_doviz_masraf_2;
	}
	public void setOrjinal_doviz_masraf_2(double orjinal_doviz_masraf_2) {
		this.orjinal_doviz_masraf_2 = orjinal_doviz_masraf_2;
	}
	public double getOrjinal_doviz_masraf_3() {
		return orjinal_doviz_masraf_3;
	}
	public void setOrjinal_doviz_masraf_3(double orjinal_doviz_masraf_3) {
		this.orjinal_doviz_masraf_3 = orjinal_doviz_masraf_3;
	}
	public double getOrjinal_doviz_masraf_4() {
		return orjinal_doviz_masraf_4;
	}
	public void setOrjinal_doviz_masraf_4(double orjinal_doviz_masraf_4) {
		this.orjinal_doviz_masraf_4 = orjinal_doviz_masraf_4;
	}
	public double getOrjinal_doviz_toplam_masraf() {
		return orjinal_doviz_toplam_masraf;
	}
	public void setOrjinal_doviz_toplam_masraf(double orjinal_doviz_toplam_masraf) {
		this.orjinal_doviz_toplam_masraf = orjinal_doviz_toplam_masraf;
	}
	public double getOrjinal_doviz_net_tutar() {
		return orjinal_doviz_net_tutar;
	}
	public void setOrjinal_doviz_net_tutar(double orjinal_doviz_net_tutar) {
		this.orjinal_doviz_net_tutar = orjinal_doviz_net_tutar;
	}
	public double getEvrak_dovizi_brut_birim_fiyati() {
		return evrak_dovizi_brut_birim_fiyati;
	}
	public void setEvrak_dovizi_brut_birim_fiyati(
			double evrak_dovizi_brut_birim_fiyati) {
		this.evrak_dovizi_brut_birim_fiyati = evrak_dovizi_brut_birim_fiyati;
	}
	public double getEvrak_dovizi_net_birim_fiyati() {
		return evrak_dovizi_net_birim_fiyati;
	}
	public void setEvrak_dovizi_net_birim_fiyati(double evrak_dovizi_net_birim_fiyati) {
		this.evrak_dovizi_net_birim_fiyati = evrak_dovizi_net_birim_fiyati;
	}
	public double getEvrak_dovizi_brut_tutar() {
		return evrak_dovizi_brut_tutar;
	}
	public void setEvrak_dovizi_brut_tutar(double evrak_dovizi_brut_tutar) {
		this.evrak_dovizi_brut_tutar = evrak_dovizi_brut_tutar;
	}
	public double getEvrak_dovizi_iskonto_1() {
		return evrak_dovizi_iskonto_1;
	}
	public void setEvrak_dovizi_iskonto_1(double evrak_dovizi_iskonto_1) {
		this.evrak_dovizi_iskonto_1 = evrak_dovizi_iskonto_1;
	}
	public double getEvrak_dovizi_iskonto_2() {
		return evrak_dovizi_iskonto_2;
	}
	public void setEvrak_dovizi_iskonto_2(double evrak_dovizi_iskonto_2) {
		this.evrak_dovizi_iskonto_2 = evrak_dovizi_iskonto_2;
	}
	public double getEvrak_dovizi_iskonto_3() {
		return evrak_dovizi_iskonto_3;
	}
	public void setEvrak_dovizi_iskonto_3(double evrak_dovizi_iskonto_3) {
		this.evrak_dovizi_iskonto_3 = evrak_dovizi_iskonto_3;
	}
	public double getEvrak_dovizi_iskonto_4() {
		return evrak_dovizi_iskonto_4;
	}
	public void setEvrak_dovizi_iskonto_4(double evrak_dovizi_iskonto_4) {
		this.evrak_dovizi_iskonto_4 = evrak_dovizi_iskonto_4;
	}
	public double getEvrak_dovizi_iskonto_5() {
		return evrak_dovizi_iskonto_5;
	}
	public void setEvrak_dovizi_iskonto_5(double evrak_dovizi_iskonto_5) {
		this.evrak_dovizi_iskonto_5 = evrak_dovizi_iskonto_5;
	}
	public double getEvrak_dovizi_iskonto_6() {
		return evrak_dovizi_iskonto_6;
	}
	public void setEvrak_dovizi_iskonto_6(double evrak_dovizi_iskonto_6) {
		this.evrak_dovizi_iskonto_6 = evrak_dovizi_iskonto_6;
	}
	public double getEvrak_dovizi_toplam_iskonto() {
		return evrak_dovizi_toplam_iskonto;
	}
	public void setEvrak_dovizi_toplam_iskonto(double evrak_dovizi_toplam_iskonto) {
		this.evrak_dovizi_toplam_iskonto = evrak_dovizi_toplam_iskonto;
	}
	public double getEvrak_dovizi_masraf_1() {
		return evrak_dovizi_masraf_1;
	}
	public void setEvrak_dovizi_masraf_1(double evrak_dovizi_masraf_1) {
		this.evrak_dovizi_masraf_1 = evrak_dovizi_masraf_1;
	}
	public double getEvrak_dovizi_masraf_2() {
		return evrak_dovizi_masraf_2;
	}
	public void setEvrak_dovizi_masraf_2(double evrak_dovizi_masraf_2) {
		this.evrak_dovizi_masraf_2 = evrak_dovizi_masraf_2;
	}
	public double getEvrak_dovizi_masraf_3() {
		return evrak_dovizi_masraf_3;
	}
	public void setEvrak_dovizi_masraf_3(double evrak_dovizi_masraf_3) {
		this.evrak_dovizi_masraf_3 = evrak_dovizi_masraf_3;
	}
	public double getEvrak_dovizi_masraf_4() {
		return evrak_dovizi_masraf_4;
	}
	public void setEvrak_dovizi_masraf_4(double evrak_dovizi_masraf_4) {
		this.evrak_dovizi_masraf_4 = evrak_dovizi_masraf_4;
	}
	public double getEvrak_dovizi_toplam_masraf() {
		return evrak_dovizi_toplam_masraf;
	}
	public void setEvrak_dovizi_toplam_masraf(double evrak_dovizi_toplam_masraf) {
		this.evrak_dovizi_toplam_masraf = evrak_dovizi_toplam_masraf;
	}
	public double getEvrak_dovizi_net_tutar() {
		return evrak_dovizi_net_tutar;
	}
	public void setEvrak_dovizi_net_tutar(double evrak_dovizi_net_tutar) {
		this.evrak_dovizi_net_tutar = evrak_dovizi_net_tutar;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	public String getFatseri() {
		return fatseri;
	}
	public void setFatseri(String fatseri) {
		this.fatseri = fatseri;
	}
	public int getFatsira_no() {
		return fatsira_no;
	}
	public void setFatsira_no(int fatsira_no) {
		this.fatsira_no = fatsira_no;
	}
	public String getFatsira_seri() {
		return fatseri + "-" + fatsira_no;
	}
	public String getSrmmrkkodu() {
		return srmmrkkodu;
	}
	public void setSrmmrkkodu(String srmmrkkodu) {
		this.srmmrkkodu = srmmrkkodu;
	}
	public String getSrmmrkadi() {
		return srmmrkadi;
	}
	public void setSrmmrkadi(String srmmrkadi) {
		this.srmmrkadi = srmmrkadi;
	}
	public String getStok_sorumluluk_merkezi() {
		return stok_sorumluluk_merkezi;
	}
	public void setStok_sorumluluk_merkezi(String stok_sorumluluk_merkezi) {
		this.stok_sorumluluk_merkezi = stok_sorumluluk_merkezi;
	}
	public String getStok_sorumluluk_merkezi_adi() {
		return stok_sorumluluk_merkezi_adi;
	}
	public void setStok_sorumluluk_merkezi_adi(String stok_sorumluluk_merkezi_adi) {
		this.stok_sorumluluk_merkezi_adi = stok_sorumluluk_merkezi_adi;
	}
	public String getProje_kodu() {
		return proje_kodu;
	}
	public void setProje_kodu(String proje_kodu) {
		this.proje_kodu = proje_kodu;
	}
	public String getProje_adi() {
		return proje_adi;
	}
	public void setProje_adi(String proje_adi) {
		this.proje_adi = proje_adi;
	}
	public double getAna_doviz_giren_maliyet_deger() {
		return ana_doviz_giren_maliyet_deger;
	}
	public void setAna_doviz_giren_maliyet_deger(double ana_doviz_giren_maliyet_deger) {
		this.ana_doviz_giren_maliyet_deger = ana_doviz_giren_maliyet_deger;
	}
	public double getAna_doviz_cikan_maliyet_deger() {
		return ana_doviz_cikan_maliyet_deger;
	}
	public void setAna_doviz_cikan_maliyet_deger(double ana_doviz_cikan_maliyet_deger) {
		this.ana_doviz_cikan_maliyet_deger = ana_doviz_cikan_maliyet_deger;
	}
	public double getAna_doviz_kar_veya_maliyet_farki() {
		return ana_doviz_kar_veya_maliyet_farki;
	}
	public void setAna_doviz_kar_veya_maliyet_farki(
			double ana_doviz_kar_veya_maliyet_farki) {
		this.ana_doviz_kar_veya_maliyet_farki = ana_doviz_kar_veya_maliyet_farki;
	}
	public double getAlternatif_doviz_giren_maliyet_deger() {
		return alternatif_doviz_giren_maliyet_deger;
	}
	public void setAlternatif_doviz_giren_maliyet_deger(
			double alternatif_doviz_giren_maliyet_deger) {
		this.alternatif_doviz_giren_maliyet_deger = alternatif_doviz_giren_maliyet_deger;
	}
	public double getAlternatif_doviz_cikan_maliyet_deger() {
		return alternatif_doviz_cikan_maliyet_deger;
	}
	public void setAlternatif_doviz_cikan_maliyet_deger(
			double alternatif_doviz_cikan_maliyet_deger) {
		this.alternatif_doviz_cikan_maliyet_deger = alternatif_doviz_cikan_maliyet_deger;
	}
	public double getAlternatif_doviz_kar_veya_maliyet_farki() {
		return alternatif_doviz_kar_veya_maliyet_farki;
	}
	public void setAlternatif_doviz_kar_veya_maliyet_farki(
			double alternatif_doviz_kar_veya_maliyet_farki) {
		this.alternatif_doviz_kar_veya_maliyet_farki = alternatif_doviz_kar_veya_maliyet_farki;
	}
	public double getOrjinal_doviz_giren_maliyet_deger() {
		return orjinal_doviz_giren_maliyet_deger;
	}
	public void setOrjinal_doviz_giren_maliyet_deger(
			double orjinal_doviz_giren_maliyet_deger) {
		this.orjinal_doviz_giren_maliyet_deger = orjinal_doviz_giren_maliyet_deger;
	}
	public double getOrjinal_doviz_cikan_maliyet_deger() {
		return orjinal_doviz_cikan_maliyet_deger;
	}
	public void setOrjinal_doviz_cikan_maliyet_deger(
			double orjinal_doviz_cikan_maliyet_deger) {
		this.orjinal_doviz_cikan_maliyet_deger = orjinal_doviz_cikan_maliyet_deger;
	}
	public double getOrjinal_doviz_kar_veya_maliyet_farki() {
		return orjinal_doviz_kar_veya_maliyet_farki;
	}
	public void setOrjinal_doviz_kar_veya_maliyet_farki(
			double orjinal_doviz_kar_veya_maliyet_farki) {
		this.orjinal_doviz_kar_veya_maliyet_farki = orjinal_doviz_kar_veya_maliyet_farki;
	}
	
	
}
