package com.biftekyazilim.hunkargida.vo;

public class StokVO {

	private int sto_RECno;
	private String sto_birim1_ad;
	
	private String sto_special3;
	private String sto_kod;
	private String sto_isim;
	private int sto_perakende_vergi;
	private int sto_toptan_vergi;
	
	public String getSto_birim1_ad() {
		return sto_birim1_ad;
	}

	public void setSto_birim1_ad(String sto_birim1_ad) {
		this.sto_birim1_ad = sto_birim1_ad;
	}

	public int getSto_RECno() {
		return sto_RECno;
	}

	public void setSto_RECno(int sto_RECno) {
		this.sto_RECno = sto_RECno;
	}

	public String getSto_special3() {
		return sto_special3;
	}

	public void setSto_special3(String sto_special3) {
		this.sto_special3 = sto_special3;
	}

	public String getSto_kod() {
		return sto_kod;
	}

	public void setSto_kod(String sto_kod) {
		this.sto_kod = sto_kod;
	}

	public String getSto_isim() {
		return sto_isim;
	}

	public void setSto_isim(String sto_isim) {
		this.sto_isim = sto_isim;
	}

	public int getSto_perakende_vergi() {
		return sto_perakende_vergi;
	}

	public void setSto_perakende_vergi(int sto_perakende_vergi) {
		this.sto_perakende_vergi = sto_perakende_vergi;
	}

	public int getSto_toptan_vergi() {
		return sto_toptan_vergi;
	}

	public void setSto_toptan_vergi(int sto_toptan_vergi) {
		this.sto_toptan_vergi = sto_toptan_vergi;
	}

	
	
}
