package com.biftekyazilim.hunkargida.vo;

import com.biftekyazilim.hunkargida.enm.FragmentType;

public class NavItemVO {

	private FragmentType fragmentType;
	private String count = "0";
	private boolean enabled;
	// boolean to set visiblity of the counter
	private boolean isCounterVisible = false;

	public NavItemVO() {
	}

	public NavItemVO(FragmentType fragmentType) {
		this.setFragmentType(fragmentType);
	}

	public NavItemVO(FragmentType fragmentType, boolean isCounterVisible,
			String count) {
		this.setFragmentType(fragmentType);
		this.isCounterVisible = isCounterVisible;
		this.count = count;
	}


	public String getCount() {
		return this.count;
	}

	public boolean getCounterVisibility() {
		return this.isCounterVisible;
	}

	public void setCount(String count) {
		this.count = count;
	}

	public void setCounterVisibility(boolean isCounterVisible) {
		this.isCounterVisible = isCounterVisible;
	}

	public FragmentType getFragmentType() {
		return fragmentType;
	}

	public void setFragmentType(FragmentType fragmentType) {
		this.fragmentType = fragmentType;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}