package com.biftekyazilim.hunkargida.vo;

public class SorumlulukMerkeziVO {

	private int msg_S_0088;
	private String msg_S_0078;
	private String msg_S_0870;
	
	public int getMsg_S_0088() {
		return msg_S_0088;
	}
	public void setMsg_S_0088(int msg_S_0088) {
		this.msg_S_0088 = msg_S_0088;
	}
	public String getMsg_S_0078() {
		return msg_S_0078;
	}
	public void setMsg_S_0078(String msg_S_0078) {
		this.msg_S_0078 = msg_S_0078;
	}
	public String getMsg_S_0870() {
		return msg_S_0870;
	}
	public void setMsg_S_0870(String msg_S_0870) {
		this.msg_S_0870 = msg_S_0870;
	}
	
	
}
