package com.biftekyazilim.hunkargida.vo;

public class SiparisSearchVO {

	private int ilk_kayit_no;
	private String seri;
	private int sira;
	private TimeAndZoneVO siparis_tarihi;
	private TimeAndZoneVO teslim_tarihi;
	private String tipi;
	private String siparis_cinsi;
	private String cari_kodu;
	private String cari_ismi;
	private double miktar;
	private double toplam;
	private double kalanmiktar;
	private int satir_sayisi;
	public int getIlk_kayit_no() {
		return ilk_kayit_no;
	}
	public void setIlk_kayit_no(int ilk_kayit_no) {
		this.ilk_kayit_no = ilk_kayit_no;
	}
	public String getSeri() {
		return seri;
	}
	public void setSeri(String seri) {
		this.seri = seri;
	}
	public int getSira() {
		return sira;
	}
	public void setSira(int sira) {
		this.sira = sira;
	}
	public TimeAndZoneVO getSiparis_tarihi() {
		return siparis_tarihi;
	}
	public void setSiparis_tarihi(TimeAndZoneVO siparis_tarihi) {
		this.siparis_tarihi = siparis_tarihi;
	}
	public TimeAndZoneVO getTeslim_tarihi() {
		return teslim_tarihi;
	}
	public void setTeslim_tarihi(TimeAndZoneVO teslim_tarihi) {
		this.teslim_tarihi = teslim_tarihi;
	}
	public String getTipi() {
		return tipi;
	}
	public void setTipi(String tipi) {
		this.tipi = tipi;
	}
	public String getSiparis_cinsi() {
		return siparis_cinsi;
	}
	public void setSiparis_cinsi(String siparis_cinsi) {
		this.siparis_cinsi = siparis_cinsi;
	}
	public String getCari_kodu() {
		return cari_kodu;
	}
	public void setCari_kodu(String cari_kodu) {
		this.cari_kodu = cari_kodu;
	}
	public String getCari_ismi() {
		return cari_ismi;
	}
	public void setCari_ismi(String cari_ismi) {
		this.cari_ismi = cari_ismi;
	}
	public double getMiktar() {
		return miktar;
	}
	public void setMiktar(double miktar) {
		this.miktar = miktar;
	}
	public double getToplam() {
		return toplam;
	}
	public void setToplam(double toplam) {
		this.toplam = toplam;
	}
	public double getKalanmiktar() {
		return kalanmiktar;
	}
	public void setKalanmiktar(double kalanmiktar) {
		this.kalanmiktar = kalanmiktar;
	}
	public int getSatir_sayisi() {
		return satir_sayisi;
	}
	public void setSatir_sayisi(int satir_sayisi) {
		this.satir_sayisi = satir_sayisi;
	}
	
	
	
	
	
}
