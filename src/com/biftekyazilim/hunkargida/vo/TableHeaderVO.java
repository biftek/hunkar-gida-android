package com.biftekyazilim.hunkargida.vo;

public class TableHeaderVO {

	private String title;
	private String objectPropertyName;
	private int textSize;
	private int weight; //weightSum=1000
	
	public TableHeaderVO(String title, int weight) {
		super();
		this.setTitle(title);
		this.setWeight(weight);
		this.setTextSize(-1);
		this.setObjectPropertyName(null);
	}
	
	public TableHeaderVO(String title, String objectPropertyName, int weight) {
		super();
		this.setTitle(title);
		this.setWeight(weight);
		this.setTextSize(-1);
		this.setObjectPropertyName(objectPropertyName);
	}
	
	public TableHeaderVO(String title, int weight, int textSize) {
		super();
		this.setTitle(title);
		this.setWeight(weight);
		this.setTextSize(textSize);
		this.setObjectPropertyName(null);
	}
	

	
	public TableHeaderVO(String title, String objectPropertyName, int weight, int textSize) {
		super();
		this.setTitle(title);
		this.setWeight(weight);
		this.setTextSize(textSize);
		this.setObjectPropertyName(objectPropertyName);
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}

	public int getTextSize() {
		return textSize;
	}

	public void setTextSize(int textSize) {
		this.textSize = textSize;
	}

	public String getObjectPropertyName() {
		return objectPropertyName;
	}

	public void setObjectPropertyName(String objectPropertyName) {
		this.objectPropertyName = objectPropertyName;
	}


	
}
