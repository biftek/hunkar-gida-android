package com.biftekyazilim.hunkargida.vo;

public class UserVO {

	private String userID;
	private String username;
	private String password;
	
	public UserVO() {
		// TODO Auto-generated constructor stub
	}

	public UserVO(String userID, String username, String password) {
		super();
		this.setUsername(username);
		this.setPassword(password);
		this.setUserID(userID);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}
	
}
