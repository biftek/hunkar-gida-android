package com.biftekyazilim.hunkargida.vo;

public class CariSummaryVO {

	private int cari_RECno;
	private String cari_kod;
	private String cari_unvan1;
	private String cari_unvan2;
	private int cari_efatura_fl;
	private String cari_temsilci_kodu;
	private int cari_odemeplan_no;
	private double bakiye;
	private double kk1;
	private double kk2;
	private double kk3;
	private String baglanti_tipi;
	private String hareket_tipi;
	
	
	public int getCari_RECno() {
		return cari_RECno;
	}
	public void setCari_RECno(int cari_RECno) {
		this.cari_RECno = cari_RECno;
	}
	public String getCari_kod() {
		return cari_kod;
	}
	public void setCari_kod(String cari_kod) {
		this.cari_kod = cari_kod;
	}
	public String getCari_unvan1() {
		return cari_unvan1;
	}
	public void setCari_unvan1(String cari_unvan1) {
		this.cari_unvan1 = cari_unvan1;
	}
	public String getCari_unvan2() {
		return cari_unvan2;
	}
	public void setCari_unvan2(String cari_unvan2) {
		this.cari_unvan2 = cari_unvan2;
	}
	public int getCari_efatura_fl() {
		return cari_efatura_fl;
	}
	public void setCari_efatura_fl(int cari_efatura_fl) {
		this.cari_efatura_fl = cari_efatura_fl;
	}
	public String getCari_temsilci_kodu() {
		return cari_temsilci_kodu;
	}
	public void setCari_temsilci_kodu(String cari_temsilci_kodu) {
		this.cari_temsilci_kodu = cari_temsilci_kodu;
	}
	public double getBakiye() {
		return bakiye;
	}
	public void setBakiye(double bakiye) {
		this.bakiye = bakiye;
	}
	public double getKk1() {
		return kk1;
	}
	public void setKk1(double kk1) {
		this.kk1 = kk1;
	}
	public double getKk2() {
		return kk2;
	}
	public void setKk2(double kk2) {
		this.kk2 = kk2;
	}
	public double getKk3() {
		return kk3;
	}
	public void setKk3(double kk3) {
		this.kk3 = kk3;
	}
	public String getBaglanti_tipi() {
		return baglanti_tipi;
	}
	public void setBaglanti_tipi(String baglanti_tipi) {
		this.baglanti_tipi = baglanti_tipi;
	}
	public String getHareket_tipi() {
		return hareket_tipi;
	}
	public void setHareket_tipi(String hareket_tipi) {
		this.hareket_tipi = hareket_tipi;
	}
	public int getCari_odemeplan_no() {
		return cari_odemeplan_no;
	}
	public void setCari_odemeplan_no(int cari_odemeplan_no) {
		this.cari_odemeplan_no = cari_odemeplan_no;
	}
	
	
	
}
