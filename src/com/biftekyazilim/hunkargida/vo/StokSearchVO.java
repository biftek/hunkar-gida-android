package com.biftekyazilim.hunkargida.vo;

public class StokSearchVO {

	private int msg_S_0088; //recno
	private String msg_S_0870;
	private String msg_S_0078;
	private double msg_S_0006;
	private String msg_S_1173;
	private double msg_S_0165;
	
	
	public int getMsg_S_0088() {
		return msg_S_0088;
	}
	public void setMsg_S_0088(int msg_S_0088) {
		this.msg_S_0088 = msg_S_0088;
	}
	public String getMsg_S_0870() {
		return msg_S_0870;
	}
	public void setMsg_S_0870(String msg_S_0870) {
		this.msg_S_0870 = msg_S_0870;
	}
	public String getMsg_S_0078() {
		return msg_S_0078;
	}
	public void setMsg_S_0078(String msg_S_0078) {
		this.msg_S_0078 = msg_S_0078;
	}
	public double getMsg_S_0006() {
		return msg_S_0006;
	}
	public void setMsg_S_0006(double msg_S_0006) {
		this.msg_S_0006 = msg_S_0006;
	}
	public String getMsg_S_1173() {
		return msg_S_1173;
	}
	public void setMsg_S_1173(String msg_S_1173) {
		this.msg_S_1173 = msg_S_1173;
	}
	public double getMsg_S_0165() {
		return msg_S_0165;
	}
	public void setMsg_S_0165(double msg_S_0165) {
		this.msg_S_0165 = msg_S_0165;
	}
	
	
	
}
