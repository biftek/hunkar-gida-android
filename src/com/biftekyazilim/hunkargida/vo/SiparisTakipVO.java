package com.biftekyazilim.hunkargida.vo;

public class SiparisTakipVO {

	private String msg_S_0078;
	private String msg_S_0070;
	private String msg_S_0084;
	private String msg_S_0201;
	private String msg_S_0200;
	private TimeAndZoneVO msg_S_0241;
	private String msg_S_0243;
	private int msg_S_0157;
	private String msg_S_0159;
	private double msg_S_0463;
	private double msg_S_0248;
	private double msg_S_0249;
	private double msg_S_0252;
	private double msg_S_0253;
	private String msg_S_0262;
	
	public String getMsg_S_0078() {
		return msg_S_0078;
	}
	public void setMsg_S_0078(String msg_S_0078) {
		this.msg_S_0078 = msg_S_0078;
	}
	public String getMsg_S_0070() {
		return msg_S_0070;
	}
	public void setMsg_S_0070(String msg_S_0070) {
		this.msg_S_0070 = msg_S_0070;
	}
	public String getMsg_S_0084() {
		return msg_S_0084;
	}
	public void setMsg_S_0084(String msg_S_0084) {
		this.msg_S_0084 = msg_S_0084;
	}
	public String getMsg_S_0201() {
		return msg_S_0201;
	}
	public void setMsg_S_0201(String msg_S_0201) {
		this.msg_S_0201 = msg_S_0201;
	}
	public String getMsg_S_0200() {
		return msg_S_0200;
	}
	public void setMsg_S_0200(String msg_S_0200) {
		this.msg_S_0200 = msg_S_0200;
	}
	public TimeAndZoneVO getMsg_S_0241() {
		return msg_S_0241;
	}
	public void setMsg_S_0241(TimeAndZoneVO msg_S_0241) {
		this.msg_S_0241 = msg_S_0241;
	}
	public String getMsg_S_0243() {
		return msg_S_0243;
	}
	public void setMsg_S_0243(String msg_S_0243) {
		this.msg_S_0243 = msg_S_0243;
	}
	public int getMsg_S_0157() {
		return msg_S_0157;
	}
	public void setMsg_S_0157(int msg_S_0157) {
		this.msg_S_0157 = msg_S_0157;
	}
	public String getMsg_S_0159() {
		return msg_S_0159;
	}
	public void setMsg_S_0159(String msg_S_0159) {
		this.msg_S_0159 = msg_S_0159;
	}
	public double getMsg_S_0463() {
		return msg_S_0463;
	}
	public void setMsg_S_0463(double msg_S_0463) {
		this.msg_S_0463 = msg_S_0463;
	}
	public double getMsg_S_0248() {
		return msg_S_0248;
	}
	public void setMsg_S_0248(double msg_S_0248) {
		this.msg_S_0248 = msg_S_0248;
	}
	public double getMsg_S_0249() {
		return msg_S_0249;
	}
	public void setMsg_S_0249(double msg_S_0249) {
		this.msg_S_0249 = msg_S_0249;
	}
	public double getMsg_S_0252() {
		return msg_S_0252;
	}
	public void setMsg_S_0252(double msg_S_0252) {
		this.msg_S_0252 = msg_S_0252;
	}
	public double getMsg_S_0253() {
		return msg_S_0253;
	}
	public void setMsg_S_0253(double msg_S_0253) {
		this.msg_S_0253 = msg_S_0253;
	}
	public String getMsg_S_0262() {
		return msg_S_0262;
	}
	public void setMsg_S_0262(String msg_S_0262) {
		this.msg_S_0262 = msg_S_0262;
	}
	public String getTeslim_Tarihi() {
		return msg_S_0241.getFormattedDate();
	}
	
}
