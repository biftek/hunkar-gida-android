package com.biftekyazilim.hunkargida.vo;

/**
 * 
 * @author biftek
 *
 *"msg_S_0088": 1,
       "msg_S_0263":
       {
           "date": "2014-04-29 00:00:00",
           "timezone_type": 3,
           "timezone": "Europe/Istanbul"
       },
       "msg_S_0960": "ENSP",
       "msg_S_0213": 1,
       "msg_S_0200": "120.1.01.00001",
       "msg_S_1086":
       {
           "date": "2014-04-29 00:00:00",
           "timezone_type": 3,
           "timezone": "Europe/Istanbul"
       },
       "msg_S_1087":
       {
           "date": "2014-04-29 00:00:00",
           "timezone_type": 3,
           "timezone": "Europe/Istanbul"
       },
       "msg_S_0293": 24.6,
       "msg_S_0165": 5,
       "msg_S_0456": 0,
       "msg_S_0247": 5,
       "msg_S_0262": "Açık"
       
       
      SELECT        TOP (100) PERCENT MIN(tkl_RECno) AS msg_S_0088,
	tkl_evrak_tarihi AS msg_S_0263,
	tkl_evrakno_seri AS msg_S_0960,
	tkl_evrakno_sira AS msg_S_0213,
	
	tkl_cari_kod AS msg_S_0200,
	MIN(tkl_baslangic_tarihi) AS msg_S_1086,
	MIN(tkl_Gecerlilik_Sures) AS msg_S_1087,
	
	SUM((tkl_Brut_fiyat + (tkl_masraf1 + tkl_masraf2 + tkl_masraf3 + tkl_masraf4)) - (tkl_iskonto1 + tkl_iskonto2 + tkl_iskonto3 + tkl_iskonto4 + tkl_iskonto5 + tkl_iskonto6))
	 AS msg_S_0293,
	SUM(tkl_miktar) AS msg_S_0165,
	SUM(tkl_teslim_miktar) AS msg_S_0456,
	CASE WHEN TKL_KAPAT_FL = 1 THEN 0 ELSE (SUM(tkl_miktar) 
	- SUM(tkl_teslim_miktar)) END AS msg_S_0247,
	dbo.fn_SiparisAcikKapali(TKL_KAPAT_FL,
	SUM(tkl_miktar),
	SUM(tkl_teslim_miktar)) AS msg_S_0262
	FROM            dbo.VERILEN_TEKLIFLER WITH (NOLOCK)
	GROUP BY TKL_KAPAT_FL,
	tkl_evrakno_seri,
	tkl_evrakno_sira,
	tkl_evrak_tarihi,
	tkl_cari_kod
	ORDER BY TKL_KAPAT_FL,
	msg_S_0960,
	msg_S_0213,
	msg_S_0263,
	msg_S_0200
 */
public class TeklifSummaryVO {

	private int msg_S_0088; //tkl_RECno
	private TimeAndZoneVO msg_S_0263; //tkl_evrak_tarihi
	private String msg_S_0960; //tkl_evrakno_seri
	private int msg_S_0213; //tkl_evrakno_sira
	private String msg_S_0200; //tkl_cari_kod
	private TimeAndZoneVO msg_S_1086; //tkl_baslangic_tarihi
	private TimeAndZoneVO msg_S_1087; //tkl_Gecerlilik_Sures
	private double msg_S_0293; //SUM((tkl_Brut_fiyat + (tkl_masraf1 + tkl_masraf2 + tkl_masraf3 + tkl_masraf4)) - (tkl_iskonto1 + tkl_iskonto2 + tkl_iskonto3 + tkl_iskonto4 + tkl_iskonto5 + tkl_iskonto6))
	private int msg_S_0165; //SUM(tkl_miktar)
	private int msg_S_0456; //SUM(tkl_teslim_miktar)
	private int msg_S_0247; //TKL_KAPAT_FL == 1 ? 0 : SUM(tkl_miktar) - SUM(tkl_teslim_miktar)
	private String msg_S_0262; //fn_SiparisAcikKapali
	
	public int getMsg_S_0088() {
		return msg_S_0088;
	}
	public void setMsg_S_0088(int msg_S_0088) {
		this.msg_S_0088 = msg_S_0088;
	}
	public TimeAndZoneVO getMsg_S_0263() {
		return msg_S_0263;
	}
	public void setMsg_S_0263(TimeAndZoneVO msg_S_0263) {
		this.msg_S_0263 = msg_S_0263;
	}
	public String getMsg_S_0960() {
		return msg_S_0960;
	}
	public void setMsg_S_0960(String msg_S_0960) {
		this.msg_S_0960 = msg_S_0960;
	}
	public int getMsg_S_0213() {
		return msg_S_0213;
	}
	public void setMsg_S_0213(int msg_S_0213) {
		this.msg_S_0213 = msg_S_0213;
	}
	public String getMsg_S_0200() {
		return msg_S_0200;
	}
	public void setMsg_S_0200(String msg_S_0200) {
		this.msg_S_0200 = msg_S_0200;
	}
	public TimeAndZoneVO getMsg_S_1086() {
		return msg_S_1086;
	}
	public void setMsg_S_1086(TimeAndZoneVO msg_S_1086) {
		this.msg_S_1086 = msg_S_1086;
	}
	public TimeAndZoneVO getMsg_S_1087() {
		return msg_S_1087;
	}
	public void setMsg_S_1087(TimeAndZoneVO msg_S_1087) {
		this.msg_S_1087 = msg_S_1087;
	}
	public double getMsg_S_0293() {
		return msg_S_0293;
	}
	public void setMsg_S_0293(double msg_S_0293) {
		this.msg_S_0293 = msg_S_0293;
	}
	public int getMsg_S_0165() {
		return msg_S_0165;
	}
	public void setMsg_S_0165(int msg_S_0165) {
		this.msg_S_0165 = msg_S_0165;
	}
	public int getMsg_S_0456() {
		return msg_S_0456;
	}
	public void setMsg_S_0456(int msg_S_0456) {
		this.msg_S_0456 = msg_S_0456;
	}
	public int getMsg_S_0247() {
		return msg_S_0247;
	}
	public void setMsg_S_0247(int msg_S_0247) {
		this.msg_S_0247 = msg_S_0247;
	}
	public String getMsg_S_0262() {
		return msg_S_0262;
	}
	public void setMsg_S_0262(String msg_S_0262) {
		this.msg_S_0262 = msg_S_0262;
	}
	
}
