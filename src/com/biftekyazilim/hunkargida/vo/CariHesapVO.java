package com.biftekyazilim.hunkargida.vo;

public class CariHesapVO {

	private int kayit_no;
	private String  cari_kodu;
	private String  cari_ismi;
	private String  firma_unvani;
	private TimeAndZoneVO  tarih;
	private String  seri;
	private int sira;
	private TimeAndZoneVO  belge_tarihi;
	private String belge_no;
	private String  evrak_tipi;
	private String  cinsi;
	private int hareket_cinsi;
	private String  grubu;
	private int cari_hesap_grup_no;
	private String  srmmrkkodu;
	private String  srmmrkismi;
	private String  n_i;
	private String  aciklama;
	private String  sorumlu_kodu;
	private String  sorumlu_ismi;
	private TimeAndZoneVO  vade_tarih;
	private int vade_gun;
	private String  depo;
	private double  miktar;
	private String  b_a;
	private double  ana_doviz_borc;
	private double  ana_doviz_alacak;
	private double  ana_doviz_tutar;
	private double  ana_doviz_borc_bakiye;
	private double  ana_doviz_alacak_bakiye;
	private double  ana_doviz_bakiye;
	private double  altdoviz_kur;
	private double  alt_doviz_borc;
	private double  alt_doviz_alacak;
	private double  alt_doviz_tutar;
	private double  alternatif_doviz_borc_bakiye;
	private double  alternatif_doviz_alacak_bakiye;
	private double  alternatif_doviz_bakiye;
	private double  orjdoviz_kur;
	private double  orj_doviz_borc;
	private double  orj_doviz_alacak;
	private double  orj_doviz_tutar;
	private double  orjinal_doviz_borc_bakiye;
	private double  orjinal_doviz_alacak_bakiye;
	private double  orjinal_doviz_bakiye;
	private String  orjdoviz;
	private String  karsi_hesap_cinsi;
	private String  karsi_hesap_kodu;
	private String  karsi_hesap_ismi;
	private String  proje_kodu;
	private String  proje_adi;
	private String referans_no;
	private String  kilitli;
	private String  parti;
	private int lot;
	private String  ciro_cari_kodu;
	private String  ciro_cari_ismi;
	public int getKayit_no() {
		return kayit_no;
	}
	public void setKayit_no(int kayit_no) {
		this.kayit_no = kayit_no;
	}
	public String getCari_kodu() {
		return cari_kodu;
	}
	public void setCari_kodu(String cari_kodu) {
		this.cari_kodu = cari_kodu;
	}
	public String getCari_ismi() {
		return cari_ismi;
	}
	public void setCari_ismi(String cari_ismi) {
		this.cari_ismi = cari_ismi;
	}
	public String getFirma_unvani() {
		return firma_unvani;
	}
	public void setFirma_unvani(String firma_unvani) {
		this.firma_unvani = firma_unvani;
	}
	public TimeAndZoneVO getTarih() {
		return tarih;
	}
	public void setTarih(TimeAndZoneVO tarih) {
		this.tarih = tarih;
	}
	public String getSeri() {
		return seri;
	}
	public void setSeri(String seri) {
		this.seri = seri;
	}
	public int getSira() {
		return sira;
	}
	public void setSira(int sira) {
		this.sira = sira;
	}
	public String getSeri_Sira() {
		return seri+"-"+sira;
	}
	public TimeAndZoneVO getBelge_tarihi() {
		return belge_tarihi;
	}
	public void setBelge_tarihi(TimeAndZoneVO belge_tarihi) {
		this.belge_tarihi = belge_tarihi;
	}
	public String getBelge_no() {
		return belge_no;
	}
	public void setBelge_no(String belge_no) {
		this.belge_no = belge_no;
	}
	public String getEvrak_tipi() {
		return evrak_tipi;
	}
	public void setEvrak_tipi(String evrak_tipi) {
		this.evrak_tipi = evrak_tipi;
	}
	public String getCinsi() {
		return cinsi;
	}
	public void setCinsi(String cinsi) {
		this.cinsi = cinsi;
	}
	public int getHareket_cinsi() {
		return hareket_cinsi;
	}
	public void setHareket_cinsi(int hareket_cinsi) {
		this.hareket_cinsi = hareket_cinsi;
	}
	public String getGrubu() {
		return grubu;
	}
	public void setGrubu(String grubu) {
		this.grubu = grubu;
	}
	public int getCari_hesap_grup_no() {
		return cari_hesap_grup_no;
	}
	public void setCari_hesap_grup_no(int cari_hesap_grup_no) {
		this.cari_hesap_grup_no = cari_hesap_grup_no;
	}
	public String getSrmmrkkodu() {
		return srmmrkkodu;
	}
	public void setSrmmrkkodu(String srmmrkkodu) {
		this.srmmrkkodu = srmmrkkodu;
	}
	public String getSrmmrkismi() {
		return srmmrkismi;
	}
	public void setSrmmrkismi(String srmmrkismi) {
		this.srmmrkismi = srmmrkismi;
	}
	public String getN_i() {
		return n_i;
	}
	public void setN_i(String n_i) {
		this.n_i = n_i;
	}
	public String getAciklama() {
		return aciklama;
	}
	public void setAciklama(String aciklama) {
		this.aciklama = aciklama;
	}
	public String getSorumlu_kodu() {
		return sorumlu_kodu;
	}
	public void setSorumlu_kodu(String sorumlu_kodu) {
		this.sorumlu_kodu = sorumlu_kodu;
	}
	public String getSorumlu_ismi() {
		return sorumlu_ismi;
	}
	public void setSorumlu_ismi(String sorumlu_ismi) {
		this.sorumlu_ismi = sorumlu_ismi;
	}
	public TimeAndZoneVO getVade_tarih() {
		return vade_tarih;
	}
	public void setVade_tarih(TimeAndZoneVO vade_tarih) {
		this.vade_tarih = vade_tarih;
	}
	public int getVade_gun() {
		return vade_gun;
	}
	public void setVade_gun(int vade_gun) {
		this.vade_gun = vade_gun;
	}
	public String getVade() {
		if(getSeri().equals("HNK")) {
			return getVade_gun() + "";
		} else {
			return getVade_tarih() == null ? "" : getVade_tarih().getFormattedDate();
		}
	}
	public String getDepo() {
		return depo;
	}
	public void setDepo(String depo) {
		this.depo = depo;
	}
	public double getMiktar() {
		return miktar;
	}
	public void setMiktar(double miktar) {
		this.miktar = miktar;
	}
	public String getB_a() {
		return b_a;
	}
	public void setB_a(String b_a) {
		this.b_a = b_a;
	}
	public double getAna_doviz_borc() {
		return ana_doviz_borc;
	}
	public void setAna_doviz_borc(double ana_doviz_borc) {
		this.ana_doviz_borc = ana_doviz_borc;
	}
	public double getAna_doviz_alacak() {
		return ana_doviz_alacak;
	}
	public void setAna_doviz_alacak(double ana_doviz_alacak) {
		this.ana_doviz_alacak = ana_doviz_alacak;
	}
	public double getAna_doviz_tutar() {
		return ana_doviz_tutar;
	}
	public void setAna_doviz_tutar(double ana_doviz_tutar) {
		this.ana_doviz_tutar = ana_doviz_tutar;
	}
	public double getAna_doviz_borc_bakiye() {
		return ana_doviz_borc_bakiye;
	}
	public void setAna_doviz_borc_bakiye(double ana_doviz_borc_bakiye) {
		this.ana_doviz_borc_bakiye = ana_doviz_borc_bakiye;
	}
	public double getAna_doviz_alacak_bakiye() {
		return ana_doviz_alacak_bakiye;
	}
	public void setAna_doviz_alacak_bakiye(double ana_doviz_alacak_bakiye) {
		this.ana_doviz_alacak_bakiye = ana_doviz_alacak_bakiye;
	}
	public double getAna_doviz_bakiye() {
		return ana_doviz_bakiye;
	}
	public void setAna_doviz_bakiye(double ana_doviz_bakiye) {
		this.ana_doviz_bakiye = ana_doviz_bakiye;
	}
	public double getAltdoviz_kur() {
		return altdoviz_kur;
	}
	public void setAltdoviz_kur(double altdoviz_kur) {
		this.altdoviz_kur = altdoviz_kur;
	}
	public double getAlt_doviz_borc() {
		return alt_doviz_borc;
	}
	public void setAlt_doviz_borc(double alt_doviz_borc) {
		this.alt_doviz_borc = alt_doviz_borc;
	}
	public double getAlt_doviz_alacak() {
		return alt_doviz_alacak;
	}
	public void setAlt_doviz_alacak(double alt_doviz_alacak) {
		this.alt_doviz_alacak = alt_doviz_alacak;
	}
	public double getAlt_doviz_tutar() {
		return alt_doviz_tutar;
	}
	public void setAlt_doviz_tutar(double alt_doviz_tutar) {
		this.alt_doviz_tutar = alt_doviz_tutar;
	}
	public double getAlternatif_doviz_borc_bakiye() {
		return alternatif_doviz_borc_bakiye;
	}
	public void setAlternatif_doviz_borc_bakiye(double alternatif_doviz_borc_bakiye) {
		this.alternatif_doviz_borc_bakiye = alternatif_doviz_borc_bakiye;
	}
	public double getAlternatif_doviz_alacak_bakiye() {
		return alternatif_doviz_alacak_bakiye;
	}
	public void setAlternatif_doviz_alacak_bakiye(
			double alternatif_doviz_alacak_bakiye) {
		this.alternatif_doviz_alacak_bakiye = alternatif_doviz_alacak_bakiye;
	}
	public double getAlternatif_doviz_bakiye() {
		return alternatif_doviz_bakiye;
	}
	public void setAlternatif_doviz_bakiye(double alternatif_doviz_bakiye) {
		this.alternatif_doviz_bakiye = alternatif_doviz_bakiye;
	}
	public double getOrjdoviz_kur() {
		return orjdoviz_kur;
	}
	public void setOrjdoviz_kur(double orjdoviz_kur) {
		this.orjdoviz_kur = orjdoviz_kur;
	}
	public double getOrj_doviz_borc() {
		return orj_doviz_borc;
	}
	public void setOrj_doviz_borc(double orj_doviz_borc) {
		this.orj_doviz_borc = orj_doviz_borc;
	}
	public double getOrj_doviz_alacak() {
		return orj_doviz_alacak;
	}
	public void setOrj_doviz_alacak(double orj_doviz_alacak) {
		this.orj_doviz_alacak = orj_doviz_alacak;
	}
	public double getOrj_doviz_tutar() {
		return orj_doviz_tutar;
	}
	public void setOrj_doviz_tutar(double orj_doviz_tutar) {
		this.orj_doviz_tutar = orj_doviz_tutar;
	}
	public double getOrjinal_doviz_borc_bakiye() {
		return orjinal_doviz_borc_bakiye;
	}
	public void setOrjinal_doviz_borc_bakiye(double orjinal_doviz_borc_bakiye) {
		this.orjinal_doviz_borc_bakiye = orjinal_doviz_borc_bakiye;
	}
	public double getOrjinal_doviz_alacak_bakiye() {
		return orjinal_doviz_alacak_bakiye;
	}
	public void setOrjinal_doviz_alacak_bakiye(double orjinal_doviz_alacak_bakiye) {
		this.orjinal_doviz_alacak_bakiye = orjinal_doviz_alacak_bakiye;
	}
	public double getOrjinal_doviz_bakiye() {
		return orjinal_doviz_bakiye;
	}
	public void setOrjinal_doviz_bakiye(double orjinal_doviz_bakiye) {
		this.orjinal_doviz_bakiye = orjinal_doviz_bakiye;
	}
	public String getOrjdoviz() {
		return orjdoviz;
	}
	public void setOrjdoviz(String orjdoviz) {
		this.orjdoviz = orjdoviz;
	}
	public String getKarsi_hesap_cinsi() {
		return karsi_hesap_cinsi;
	}
	public void setKarsi_hesap_cinsi(String karsi_hesap_cinsi) {
		this.karsi_hesap_cinsi = karsi_hesap_cinsi;
	}
	public String getKarsi_hesap_kodu() {
		return karsi_hesap_kodu;
	}
	public void setKarsi_hesap_kodu(String karsi_hesap_kodu) {
		this.karsi_hesap_kodu = karsi_hesap_kodu;
	}
	public String getKarsi_hesap_ismi() {
		return karsi_hesap_ismi;
	}
	public void setKarsi_hesap_ismi(String karsi_hesap_ismi) {
		this.karsi_hesap_ismi = karsi_hesap_ismi;
	}
	public String getProje_kodu() {
		return proje_kodu;
	}
	public void setProje_kodu(String proje_kodu) {
		this.proje_kodu = proje_kodu;
	}
	public String getProje_adi() {
		return proje_adi;
	}
	public void setProje_adi(String proje_adi) {
		this.proje_adi = proje_adi;
	}
	public String getReferans_no() {
		return referans_no;
	}
	public void setReferans_no(String referans_no) {
		this.referans_no = referans_no;
	}
	public String getKilitli() {
		return kilitli;
	}
	public void setKilitli(String kilitli) {
		this.kilitli = kilitli;
	}
	public String getParti() {
		return parti;
	}
	public void setParti(String parti) {
		this.parti = parti;
	}
	public int getLot() {
		return lot;
	}
	public void setLot(int lot) {
		this.lot = lot;
	}
	public String getCiro_cari_kodu() {
		return ciro_cari_kodu;
	}
	public void setCiro_cari_kodu(String ciro_cari_kodu) {
		this.ciro_cari_kodu = ciro_cari_kodu;
	}
	public String getCiro_cari_ismi() {
		return ciro_cari_ismi;
	}
	public void setCiro_cari_ismi(String ciro_cari_ismi) {
		this.ciro_cari_ismi = ciro_cari_ismi;
	}
	

	
}
