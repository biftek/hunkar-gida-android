package com.biftekyazilim.hunkargida.vo;

public class CariVO {
	
	private int cari_RECno;
	private int cari_RECid_DBCno;
	private int cari_RECid_RECno;
	private int cari_SpecRECno;
	private int cari_iptal;
	private int cari_fileid;
	private int cari_hidden;
	private int cari_kilitli;
	private int cari_degisti;
	private int cari_checksum;
	private int cari_create_user;
	private TimeAndZoneVO  cari_create_date;
	private int cari_lastup_user;
	private TimeAndZoneVO  cari_lastup_date;
	private String cari_special1;
	private String cari_special2;
	private String cari_special3;
	private String cari_kod;
	private String cari_unvan1;
	private String cari_unvan2;
	private int cari_hareket_tipi;
	private int cari_baglanti_tipi;
	private int cari_stok_alim_cinsi;
	private int cari_stok_satim_cinsi;
	private String cari_muh_kod;
	private String cari_muh_kod1;
	private String cari_muh_kod2;
	private int cari_doviz_cinsi;
	private int cari_doviz_cinsi1;
	private int cari_doviz_cinsi2;
	private int cari_vade_fark_yuz;
	private int cari_vade_fark_yuz1;
	private int cari_vade_fark_yuz2;
	private int cari_KurHesapSekli;
	private String cari_vdaire_adi;
	private String cari_vdaire_no;
	private String cari_sicil_no;
	private String cari_VergiKimlikNo;
	private int cari_satis_fk;
	private int cari_odeme_cinsi;
	private int cari_odeme_gunu;
	private int cari_odemeplan_no;
	private int cari_opsiyon_gun;
	private int cari_cariodemetercihi;
	private int cari_fatura_adres_no;
	private int cari_sevk_adres_no;
	private String cari_banka_tcmb_kod1;
	private String cari_banka_tcmb_subekod1;
	private String cari_banka_tcmb_ilkod1;
	private String cari_banka_hesapno1;
	private String cari_banka_tcmb_kod2;
	private String cari_banka_tcmb_subekod2;
	private String cari_banka_tcmb_ilkod2;
	private String cari_banka_hesapno2;
	private String cari_banka_tcmb_kod3;
	private String cari_banka_tcmb_subekod3;
	private String cari_banka_tcmb_ilkod3;
	private String cari_banka_hesapno3;
	private int cari_EftHesapNum;
	private String cari_Ana_cari_kodu;
	private String cari_satis_isk_kod;
	private String cari_sektor_kodu;
	private String cari_bolge_kodu;
	private String cari_grup_kodu;
	private String cari_temsilci_kodu;
	private String cari_muhartikeli;
	private int cari_firma_acik_kapal;
	private int cari_BUV_tabi_fl;
	private int cari_cari_kilitli_flg;
	private int cari_etiket_bas_fl;
	private int cari_Detay_incele_flg;
	private int cari_efatura_fl;
	private int cari_POS_ongpesyuzde;
	private int cari_POS_ongtaksayi;
	private int cari_POS_ongIskOran;
	private TimeAndZoneVO  cari_kaydagiristarihi;
	private int cari_KabEdFCekTutar;
	private int cari_hal_caritip;
	private int cari_HalKomYuzdesi;
	private int cari_TeslimSuresi;
	private String cari_wwwadresi;
	private String cari_EMail;
	private String cari_CepTel;
	private int cari_VarsayilanGirisDepo;
	private int cari_VarsayilanCikisDepo;
	private int cari_Portal_Enabled;
	private String cari_Portal_PW;
	private int cari_BagliOrtaklisa_Firma;
	private String cari_kampanyakodu;
	private int cari_b_bakiye_degerlendirilmesin_fl;
	private int cari_a_bakiye_degerlendirilmesin_fl;
	private int cari_b_irsbakiye_degerlendirilmesin_fl;
	private int cari_a_irsbakiye_degerlendirilmesin_fl;
	private int cari_b_sipbakiye_degerlendirilmesin_fl;
	private int cari_a_sipbakiye_degerlendirilmesin_fl;
	private String cari_AvmBilgileri1KiraKodu;
	private int cari_AvmBilgileri1TebligatSekli;
	private String cari_AvmBilgileri2KiraKodu;
	private int cari_AvmBilgileri2TebligatSekli;
	private String cari_AvmBilgileri3KiraKodu;
	private int cari_AvmBilgileri3TebligatSekli;
	private String cari_AvmBilgileri4KiraKodu;
	private int cari_AvmBilgileri4TebligatSekli;
	private String cari_AvmBilgileri5KiraKodu;
	private int cari_AvmBilgileri5TebligatSekli;
	private String cari_AvmBilgileri6KiraKodu;
	private int cari_AvmBilgileri6TebligatSekli;
	private String cari_AvmBilgileri7KiraKodu;
	private int cari_AvmBilgileri7TebligatSekli;
	private String cari_AvmBilgileri8KiraKodu;
	private int cari_AvmBilgileri8TebligatSekli;
	private String cari_AvmBilgileri9KiraKodu;
	private int cari_AvmBilgileri9TebligatSekli;
	private String cari_AvmBilgileri10KiraKodu;
	private int cari_AvmBilgileri10TebligatSekli;
	private int cari_KrediRiskTakibiVar_flg;
	private String cari_ufrs_fark_muh_kod;
	private String cari_ufrs_fark_muh_kod1;
	private String cari_ufrs_fark_muh_kod2;
	private int cari_odeme_sekli;
	private String cari_TeminatMekAlacakMuhKodu;
	private String cari_TeminatMekAlacakMuhKodu1;
	private String cari_TeminatMekAlacakMuhKodu2;
	private String cari_TeminatMekBorcMuhKodu;
	private String cari_TeminatMekBorcMuhKodu1;
	private String cari_TeminatMekBorcMuhKodu2;
	private String cari_VerilenDepozitoTeminatMuhKodu;
	private String cari_VerilenDepozitoTeminatMuhKodu1;
	private String cari_VerilenDepozitoTeminatMuhKodu2;
	private String cari_AlinanDepozitoTeminatMuhKodu;
	private String cari_AlinanDepozitoTeminatMuhKodu1;
	private String cari_AlinanDepozitoTeminatMuhKodu2;
	private int cari_def_efatura_cinsi;
	public int getCari_RECno() {
		return cari_RECno;
	}
	public void setCari_RECno(int cari_RECno) {
		this.cari_RECno = cari_RECno;
	}
	public int getCari_RECid_DBCno() {
		return cari_RECid_DBCno;
	}
	public void setCari_RECid_DBCno(int cari_RECid_DBCno) {
		this.cari_RECid_DBCno = cari_RECid_DBCno;
	}
	public int getCari_RECid_RECno() {
		return cari_RECid_RECno;
	}
	public void setCari_RECid_RECno(int cari_RECid_RECno) {
		this.cari_RECid_RECno = cari_RECid_RECno;
	}
	public int getCari_SpecRECno() {
		return cari_SpecRECno;
	}
	public void setCari_SpecRECno(int cari_SpecRECno) {
		this.cari_SpecRECno = cari_SpecRECno;
	}
	public int getCari_iptal() {
		return cari_iptal;
	}
	public void setCari_iptal(int cari_iptal) {
		this.cari_iptal = cari_iptal;
	}
	public int getCari_fileid() {
		return cari_fileid;
	}
	public void setCari_fileid(int cari_fileid) {
		this.cari_fileid = cari_fileid;
	}
	public int getCari_hidden() {
		return cari_hidden;
	}
	public void setCari_hidden(int cari_hidden) {
		this.cari_hidden = cari_hidden;
	}
	public int getCari_kilitli() {
		return cari_kilitli;
	}
	public void setCari_kilitli(int cari_kilitli) {
		this.cari_kilitli = cari_kilitli;
	}
	public int getCari_degisti() {
		return cari_degisti;
	}
	public void setCari_degisti(int cari_degisti) {
		this.cari_degisti = cari_degisti;
	}
	public int getCari_checksum() {
		return cari_checksum;
	}
	public void setCari_checksum(int cari_checksum) {
		this.cari_checksum = cari_checksum;
	}
	public int getCari_create_user() {
		return cari_create_user;
	}
	public void setCari_create_user(int cari_create_user) {
		this.cari_create_user = cari_create_user;
	}
	public TimeAndZoneVO getCari_create_date() {
		return cari_create_date;
	}
	public void setCari_create_date(TimeAndZoneVO cari_create_date) {
		this.cari_create_date = cari_create_date;
	}
	public int getCari_lastup_user() {
		return cari_lastup_user;
	}
	public void setCari_lastup_user(int cari_lastup_user) {
		this.cari_lastup_user = cari_lastup_user;
	}
	public TimeAndZoneVO getCari_lastup_date() {
		return cari_lastup_date;
	}
	public void setCari_lastup_date(TimeAndZoneVO cari_lastup_date) {
		this.cari_lastup_date = cari_lastup_date;
	}
	public String getCari_special1() {
		return cari_special1;
	}
	public void setCari_special1(String cari_special1) {
		this.cari_special1 = cari_special1;
	}
	public String getCari_special2() {
		return cari_special2;
	}
	public void setCari_special2(String cari_special2) {
		this.cari_special2 = cari_special2;
	}
	public String getCari_special3() {
		return cari_special3;
	}
	public void setCari_special3(String cari_special3) {
		this.cari_special3 = cari_special3;
	}
	public String getCari_kod() {
		return cari_kod;
	}
	public void setCari_kod(String cari_kod) {
		this.cari_kod = cari_kod;
	}
	public String getCari_unvan1() {
		return cari_unvan1;
	}
	public void setCari_unvan1(String cari_unvan1) {
		this.cari_unvan1 = cari_unvan1;
	}
	public String getCari_unvan2() {
		return cari_unvan2;
	}
	public void setCari_unvan2(String cari_unvan2) {
		this.cari_unvan2 = cari_unvan2;
	}
	public int getCari_hareket_tipi() {
		return cari_hareket_tipi;
	}
	public void setCari_hareket_tipi(int cari_hareket_tipi) {
		this.cari_hareket_tipi = cari_hareket_tipi;
	}
	public int getCari_baglanti_tipi() {
		return cari_baglanti_tipi;
	}
	public void setCari_baglanti_tipi(int cari_baglanti_tipi) {
		this.cari_baglanti_tipi = cari_baglanti_tipi;
	}
	public int getCari_stok_alim_cinsi() {
		return cari_stok_alim_cinsi;
	}
	public void setCari_stok_alim_cinsi(int cari_stok_alim_cinsi) {
		this.cari_stok_alim_cinsi = cari_stok_alim_cinsi;
	}
	public int getCari_stok_satim_cinsi() {
		return cari_stok_satim_cinsi;
	}
	public void setCari_stok_satim_cinsi(int cari_stok_satim_cinsi) {
		this.cari_stok_satim_cinsi = cari_stok_satim_cinsi;
	}
	public String getCari_muh_kod() {
		return cari_muh_kod;
	}
	public void setCari_muh_kod(String cari_muh_kod) {
		this.cari_muh_kod = cari_muh_kod;
	}
	public String getCari_muh_kod1() {
		return cari_muh_kod1;
	}
	public void setCari_muh_kod1(String cari_muh_kod1) {
		this.cari_muh_kod1 = cari_muh_kod1;
	}
	public String getCari_muh_kod2() {
		return cari_muh_kod2;
	}
	public void setCari_muh_kod2(String cari_muh_kod2) {
		this.cari_muh_kod2 = cari_muh_kod2;
	}
	public int getCari_doviz_cinsi() {
		return cari_doviz_cinsi;
	}
	public void setCari_doviz_cinsi(int cari_doviz_cinsi) {
		this.cari_doviz_cinsi = cari_doviz_cinsi;
	}
	public int getCari_doviz_cinsi1() {
		return cari_doviz_cinsi1;
	}
	public void setCari_doviz_cinsi1(int cari_doviz_cinsi1) {
		this.cari_doviz_cinsi1 = cari_doviz_cinsi1;
	}
	public int getCari_doviz_cinsi2() {
		return cari_doviz_cinsi2;
	}
	public void setCari_doviz_cinsi2(int cari_doviz_cinsi2) {
		this.cari_doviz_cinsi2 = cari_doviz_cinsi2;
	}
	public int getCari_vade_fark_yuz() {
		return cari_vade_fark_yuz;
	}
	public void setCari_vade_fark_yuz(int cari_vade_fark_yuz) {
		this.cari_vade_fark_yuz = cari_vade_fark_yuz;
	}
	public int getCari_vade_fark_yuz1() {
		return cari_vade_fark_yuz1;
	}
	public void setCari_vade_fark_yuz1(int cari_vade_fark_yuz1) {
		this.cari_vade_fark_yuz1 = cari_vade_fark_yuz1;
	}
	public int getCari_vade_fark_yuz2() {
		return cari_vade_fark_yuz2;
	}
	public void setCari_vade_fark_yuz2(int cari_vade_fark_yuz2) {
		this.cari_vade_fark_yuz2 = cari_vade_fark_yuz2;
	}
	public int getCari_KurHesapSekli() {
		return cari_KurHesapSekli;
	}
	public void setCari_KurHesapSekli(int cari_KurHesapSekli) {
		this.cari_KurHesapSekli = cari_KurHesapSekli;
	}
	public String getCari_vdaire_adi() {
		return cari_vdaire_adi;
	}
	public void setCari_vdaire_adi(String cari_vdaire_adi) {
		this.cari_vdaire_adi = cari_vdaire_adi;
	}
	public String getCari_vdaire_no() {
		return cari_vdaire_no;
	}
	public void setCari_vdaire_no(String cari_vdaire_no) {
		this.cari_vdaire_no = cari_vdaire_no;
	}
	public String getCari_sicil_no() {
		return cari_sicil_no;
	}
	public void setCari_sicil_no(String cari_sicil_no) {
		this.cari_sicil_no = cari_sicil_no;
	}
	public String getCari_VergiKimlikNo() {
		return cari_VergiKimlikNo;
	}
	public void setCari_VergiKimlikNo(String cari_VergiKimlikNo) {
		this.cari_VergiKimlikNo = cari_VergiKimlikNo;
	}
	public int getCari_satis_fk() {
		return cari_satis_fk;
	}
	public void setCari_satis_fk(int cari_satis_fk) {
		this.cari_satis_fk = cari_satis_fk;
	}
	public int getCari_odeme_cinsi() {
		return cari_odeme_cinsi;
	}
	public void setCari_odeme_cinsi(int cari_odeme_cinsi) {
		this.cari_odeme_cinsi = cari_odeme_cinsi;
	}
	public int getCari_odeme_gunu() {
		return cari_odeme_gunu;
	}
	public void setCari_odeme_gunu(int cari_odeme_gunu) {
		this.cari_odeme_gunu = cari_odeme_gunu;
	}
	public int getCari_odemeplan_no() {
		return cari_odemeplan_no;
	}
	public void setCari_odemeplan_no(int cari_odemeplan_no) {
		this.cari_odemeplan_no = cari_odemeplan_no;
	}
	public int getCari_opsiyon_gun() {
		return cari_opsiyon_gun;
	}
	public void setCari_opsiyon_gun(int cari_opsiyon_gun) {
		this.cari_opsiyon_gun = cari_opsiyon_gun;
	}
	public int getCari_cariodemetercihi() {
		return cari_cariodemetercihi;
	}
	public void setCari_cariodemetercihi(int cari_cariodemetercihi) {
		this.cari_cariodemetercihi = cari_cariodemetercihi;
	}
	public int getCari_fatura_adres_no() {
		return cari_fatura_adres_no;
	}
	public void setCari_fatura_adres_no(int cari_fatura_adres_no) {
		this.cari_fatura_adres_no = cari_fatura_adres_no;
	}
	public int getCari_sevk_adres_no() {
		return cari_sevk_adres_no;
	}
	public void setCari_sevk_adres_no(int cari_sevk_adres_no) {
		this.cari_sevk_adres_no = cari_sevk_adres_no;
	}
	public String getCari_banka_tcmb_kod1() {
		return cari_banka_tcmb_kod1;
	}
	public void setCari_banka_tcmb_kod1(String cari_banka_tcmb_kod1) {
		this.cari_banka_tcmb_kod1 = cari_banka_tcmb_kod1;
	}
	public String getCari_banka_tcmb_subekod1() {
		return cari_banka_tcmb_subekod1;
	}
	public void setCari_banka_tcmb_subekod1(String cari_banka_tcmb_subekod1) {
		this.cari_banka_tcmb_subekod1 = cari_banka_tcmb_subekod1;
	}
	public String getCari_banka_tcmb_ilkod1() {
		return cari_banka_tcmb_ilkod1;
	}
	public void setCari_banka_tcmb_ilkod1(String cari_banka_tcmb_ilkod1) {
		this.cari_banka_tcmb_ilkod1 = cari_banka_tcmb_ilkod1;
	}
	public String getCari_banka_hesapno1() {
		return cari_banka_hesapno1;
	}
	public void setCari_banka_hesapno1(String cari_banka_hesapno1) {
		this.cari_banka_hesapno1 = cari_banka_hesapno1;
	}
	public String getCari_banka_tcmb_kod2() {
		return cari_banka_tcmb_kod2;
	}
	public void setCari_banka_tcmb_kod2(String cari_banka_tcmb_kod2) {
		this.cari_banka_tcmb_kod2 = cari_banka_tcmb_kod2;
	}
	public String getCari_banka_tcmb_subekod2() {
		return cari_banka_tcmb_subekod2;
	}
	public void setCari_banka_tcmb_subekod2(String cari_banka_tcmb_subekod2) {
		this.cari_banka_tcmb_subekod2 = cari_banka_tcmb_subekod2;
	}
	public String getCari_banka_tcmb_ilkod2() {
		return cari_banka_tcmb_ilkod2;
	}
	public void setCari_banka_tcmb_ilkod2(String cari_banka_tcmb_ilkod2) {
		this.cari_banka_tcmb_ilkod2 = cari_banka_tcmb_ilkod2;
	}
	public String getCari_banka_hesapno2() {
		return cari_banka_hesapno2;
	}
	public void setCari_banka_hesapno2(String cari_banka_hesapno2) {
		this.cari_banka_hesapno2 = cari_banka_hesapno2;
	}
	public String getCari_banka_tcmb_kod3() {
		return cari_banka_tcmb_kod3;
	}
	public void setCari_banka_tcmb_kod3(String cari_banka_tcmb_kod3) {
		this.cari_banka_tcmb_kod3 = cari_banka_tcmb_kod3;
	}
	public String getCari_banka_tcmb_subekod3() {
		return cari_banka_tcmb_subekod3;
	}
	public void setCari_banka_tcmb_subekod3(String cari_banka_tcmb_subekod3) {
		this.cari_banka_tcmb_subekod3 = cari_banka_tcmb_subekod3;
	}
	public String getCari_banka_tcmb_ilkod3() {
		return cari_banka_tcmb_ilkod3;
	}
	public void setCari_banka_tcmb_ilkod3(String cari_banka_tcmb_ilkod3) {
		this.cari_banka_tcmb_ilkod3 = cari_banka_tcmb_ilkod3;
	}
	public String getCari_banka_hesapno3() {
		return cari_banka_hesapno3;
	}
	public void setCari_banka_hesapno3(String cari_banka_hesapno3) {
		this.cari_banka_hesapno3 = cari_banka_hesapno3;
	}
	public int getCari_EftHesapNum() {
		return cari_EftHesapNum;
	}
	public void setCari_EftHesapNum(int cari_EftHesapNum) {
		this.cari_EftHesapNum = cari_EftHesapNum;
	}
	public String getCari_Ana_cari_kodu() {
		return cari_Ana_cari_kodu;
	}
	public void setCari_Ana_cari_kodu(String cari_Ana_cari_kodu) {
		this.cari_Ana_cari_kodu = cari_Ana_cari_kodu;
	}
	public String getCari_satis_isk_kod() {
		return cari_satis_isk_kod;
	}
	public void setCari_satis_isk_kod(String cari_satis_isk_kod) {
		this.cari_satis_isk_kod = cari_satis_isk_kod;
	}
	public String getCari_sektor_kodu() {
		return cari_sektor_kodu;
	}
	public void setCari_sektor_kodu(String cari_sektor_kodu) {
		this.cari_sektor_kodu = cari_sektor_kodu;
	}
	public String getCari_bolge_kodu() {
		return cari_bolge_kodu;
	}
	public void setCari_bolge_kodu(String cari_bolge_kodu) {
		this.cari_bolge_kodu = cari_bolge_kodu;
	}
	public String getCari_grup_kodu() {
		return cari_grup_kodu;
	}
	public void setCari_grup_kodu(String cari_grup_kodu) {
		this.cari_grup_kodu = cari_grup_kodu;
	}
	public String getCari_temsilci_kodu() {
		return cari_temsilci_kodu;
	}
	public void setCari_temsilci_kodu(String cari_temsilci_kodu) {
		this.cari_temsilci_kodu = cari_temsilci_kodu;
	}
	public String getCari_muhartikeli() {
		return cari_muhartikeli;
	}
	public void setCari_muhartikeli(String cari_muhartikeli) {
		this.cari_muhartikeli = cari_muhartikeli;
	}
	public int getCari_firma_acik_kapal() {
		return cari_firma_acik_kapal;
	}
	public void setCari_firma_acik_kapal(int cari_firma_acik_kapal) {
		this.cari_firma_acik_kapal = cari_firma_acik_kapal;
	}
	public int getCari_BUV_tabi_fl() {
		return cari_BUV_tabi_fl;
	}
	public void setCari_BUV_tabi_fl(int cari_BUV_tabi_fl) {
		this.cari_BUV_tabi_fl = cari_BUV_tabi_fl;
	}
	public int getCari_cari_kilitli_flg() {
		return cari_cari_kilitli_flg;
	}
	public void setCari_cari_kilitli_flg(int cari_cari_kilitli_flg) {
		this.cari_cari_kilitli_flg = cari_cari_kilitli_flg;
	}
	public int getCari_etiket_bas_fl() {
		return cari_etiket_bas_fl;
	}
	public void setCari_etiket_bas_fl(int cari_etiket_bas_fl) {
		this.cari_etiket_bas_fl = cari_etiket_bas_fl;
	}
	public int getCari_Detay_incele_flg() {
		return cari_Detay_incele_flg;
	}
	public void setCari_Detay_incele_flg(int cari_Detay_incele_flg) {
		this.cari_Detay_incele_flg = cari_Detay_incele_flg;
	}
	public int getCari_efatura_fl() {
		return cari_efatura_fl;
	}
	public void setCari_efatura_fl(int cari_efatura_fl) {
		this.cari_efatura_fl = cari_efatura_fl;
	}
	public int getCari_POS_ongpesyuzde() {
		return cari_POS_ongpesyuzde;
	}
	public void setCari_POS_ongpesyuzde(int cari_POS_ongpesyuzde) {
		this.cari_POS_ongpesyuzde = cari_POS_ongpesyuzde;
	}
	public int getCari_POS_ongtaksayi() {
		return cari_POS_ongtaksayi;
	}
	public void setCari_POS_ongtaksayi(int cari_POS_ongtaksayi) {
		this.cari_POS_ongtaksayi = cari_POS_ongtaksayi;
	}
	public int getCari_POS_ongIskOran() {
		return cari_POS_ongIskOran;
	}
	public void setCari_POS_ongIskOran(int cari_POS_ongIskOran) {
		this.cari_POS_ongIskOran = cari_POS_ongIskOran;
	}
	public TimeAndZoneVO getCari_kaydagiristarihi() {
		return cari_kaydagiristarihi;
	}
	public void setCari_kaydagiristarihi(TimeAndZoneVO cari_kaydagiristarihi) {
		this.cari_kaydagiristarihi = cari_kaydagiristarihi;
	}
	public int getCari_KabEdFCekTutar() {
		return cari_KabEdFCekTutar;
	}
	public void setCari_KabEdFCekTutar(int cari_KabEdFCekTutar) {
		this.cari_KabEdFCekTutar = cari_KabEdFCekTutar;
	}
	public int getCari_hal_caritip() {
		return cari_hal_caritip;
	}
	public void setCari_hal_caritip(int cari_hal_caritip) {
		this.cari_hal_caritip = cari_hal_caritip;
	}
	public int getCari_HalKomYuzdesi() {
		return cari_HalKomYuzdesi;
	}
	public void setCari_HalKomYuzdesi(int cari_HalKomYuzdesi) {
		this.cari_HalKomYuzdesi = cari_HalKomYuzdesi;
	}
	public int getCari_TeslimSuresi() {
		return cari_TeslimSuresi;
	}
	public void setCari_TeslimSuresi(int cari_TeslimSuresi) {
		this.cari_TeslimSuresi = cari_TeslimSuresi;
	}
	public String getCari_wwwadresi() {
		return cari_wwwadresi;
	}
	public void setCari_wwwadresi(String cari_wwwadresi) {
		this.cari_wwwadresi = cari_wwwadresi;
	}
	public String getCari_EMail() {
		return cari_EMail;
	}
	public void setCari_EMail(String cari_EMail) {
		this.cari_EMail = cari_EMail;
	}
	public String getCari_CepTel() {
		return cari_CepTel;
	}
	public void setCari_CepTel(String cari_CepTel) {
		this.cari_CepTel = cari_CepTel;
	}
	public int getCari_VarsayilanGirisDepo() {
		return cari_VarsayilanGirisDepo;
	}
	public void setCari_VarsayilanGirisDepo(int cari_VarsayilanGirisDepo) {
		this.cari_VarsayilanGirisDepo = cari_VarsayilanGirisDepo;
	}
	public int getCari_VarsayilanCikisDepo() {
		return cari_VarsayilanCikisDepo;
	}
	public void setCari_VarsayilanCikisDepo(int cari_VarsayilanCikisDepo) {
		this.cari_VarsayilanCikisDepo = cari_VarsayilanCikisDepo;
	}
	public int getCari_Portal_Enabled() {
		return cari_Portal_Enabled;
	}
	public void setCari_Portal_Enabled(int cari_Portal_Enabled) {
		this.cari_Portal_Enabled = cari_Portal_Enabled;
	}
	public String getCari_Portal_PW() {
		return cari_Portal_PW;
	}
	public void setCari_Portal_PW(String cari_Portal_PW) {
		this.cari_Portal_PW = cari_Portal_PW;
	}
	public int getCari_BagliOrtaklisa_Firma() {
		return cari_BagliOrtaklisa_Firma;
	}
	public void setCari_BagliOrtaklisa_Firma(int cari_BagliOrtaklisa_Firma) {
		this.cari_BagliOrtaklisa_Firma = cari_BagliOrtaklisa_Firma;
	}
	public String getCari_kampanyakodu() {
		return cari_kampanyakodu;
	}
	public void setCari_kampanyakodu(String cari_kampanyakodu) {
		this.cari_kampanyakodu = cari_kampanyakodu;
	}
	public int getCari_b_bakiye_degerlendirilmesin_fl() {
		return cari_b_bakiye_degerlendirilmesin_fl;
	}
	public void setCari_b_bakiye_degerlendirilmesin_fl(
			int cari_b_bakiye_degerlendirilmesin_fl) {
		this.cari_b_bakiye_degerlendirilmesin_fl = cari_b_bakiye_degerlendirilmesin_fl;
	}
	public int getCari_a_bakiye_degerlendirilmesin_fl() {
		return cari_a_bakiye_degerlendirilmesin_fl;
	}
	public void setCari_a_bakiye_degerlendirilmesin_fl(
			int cari_a_bakiye_degerlendirilmesin_fl) {
		this.cari_a_bakiye_degerlendirilmesin_fl = cari_a_bakiye_degerlendirilmesin_fl;
	}
	public int getCari_b_irsbakiye_degerlendirilmesin_fl() {
		return cari_b_irsbakiye_degerlendirilmesin_fl;
	}
	public void setCari_b_irsbakiye_degerlendirilmesin_fl(
			int cari_b_irsbakiye_degerlendirilmesin_fl) {
		this.cari_b_irsbakiye_degerlendirilmesin_fl = cari_b_irsbakiye_degerlendirilmesin_fl;
	}
	public int getCari_a_irsbakiye_degerlendirilmesin_fl() {
		return cari_a_irsbakiye_degerlendirilmesin_fl;
	}
	public void setCari_a_irsbakiye_degerlendirilmesin_fl(
			int cari_a_irsbakiye_degerlendirilmesin_fl) {
		this.cari_a_irsbakiye_degerlendirilmesin_fl = cari_a_irsbakiye_degerlendirilmesin_fl;
	}
	public int getCari_b_sipbakiye_degerlendirilmesin_fl() {
		return cari_b_sipbakiye_degerlendirilmesin_fl;
	}
	public void setCari_b_sipbakiye_degerlendirilmesin_fl(
			int cari_b_sipbakiye_degerlendirilmesin_fl) {
		this.cari_b_sipbakiye_degerlendirilmesin_fl = cari_b_sipbakiye_degerlendirilmesin_fl;
	}
	public int getCari_a_sipbakiye_degerlendirilmesin_fl() {
		return cari_a_sipbakiye_degerlendirilmesin_fl;
	}
	public void setCari_a_sipbakiye_degerlendirilmesin_fl(
			int cari_a_sipbakiye_degerlendirilmesin_fl) {
		this.cari_a_sipbakiye_degerlendirilmesin_fl = cari_a_sipbakiye_degerlendirilmesin_fl;
	}
	public String getCari_AvmBilgileri1KiraKodu() {
		return cari_AvmBilgileri1KiraKodu;
	}
	public void setCari_AvmBilgileri1KiraKodu(String cari_AvmBilgileri1KiraKodu) {
		this.cari_AvmBilgileri1KiraKodu = cari_AvmBilgileri1KiraKodu;
	}
	public int getCari_AvmBilgileri1TebligatSekli() {
		return cari_AvmBilgileri1TebligatSekli;
	}
	public void setCari_AvmBilgileri1TebligatSekli(
			int cari_AvmBilgileri1TebligatSekli) {
		this.cari_AvmBilgileri1TebligatSekli = cari_AvmBilgileri1TebligatSekli;
	}
	public String getCari_AvmBilgileri2KiraKodu() {
		return cari_AvmBilgileri2KiraKodu;
	}
	public void setCari_AvmBilgileri2KiraKodu(String cari_AvmBilgileri2KiraKodu) {
		this.cari_AvmBilgileri2KiraKodu = cari_AvmBilgileri2KiraKodu;
	}
	public int getCari_AvmBilgileri2TebligatSekli() {
		return cari_AvmBilgileri2TebligatSekli;
	}
	public void setCari_AvmBilgileri2TebligatSekli(
			int cari_AvmBilgileri2TebligatSekli) {
		this.cari_AvmBilgileri2TebligatSekli = cari_AvmBilgileri2TebligatSekli;
	}
	public String getCari_AvmBilgileri3KiraKodu() {
		return cari_AvmBilgileri3KiraKodu;
	}
	public void setCari_AvmBilgileri3KiraKodu(String cari_AvmBilgileri3KiraKodu) {
		this.cari_AvmBilgileri3KiraKodu = cari_AvmBilgileri3KiraKodu;
	}
	public int getCari_AvmBilgileri3TebligatSekli() {
		return cari_AvmBilgileri3TebligatSekli;
	}
	public void setCari_AvmBilgileri3TebligatSekli(
			int cari_AvmBilgileri3TebligatSekli) {
		this.cari_AvmBilgileri3TebligatSekli = cari_AvmBilgileri3TebligatSekli;
	}
	public String getCari_AvmBilgileri4KiraKodu() {
		return cari_AvmBilgileri4KiraKodu;
	}
	public void setCari_AvmBilgileri4KiraKodu(String cari_AvmBilgileri4KiraKodu) {
		this.cari_AvmBilgileri4KiraKodu = cari_AvmBilgileri4KiraKodu;
	}
	public int getCari_AvmBilgileri4TebligatSekli() {
		return cari_AvmBilgileri4TebligatSekli;
	}
	public void setCari_AvmBilgileri4TebligatSekli(
			int cari_AvmBilgileri4TebligatSekli) {
		this.cari_AvmBilgileri4TebligatSekli = cari_AvmBilgileri4TebligatSekli;
	}
	public String getCari_AvmBilgileri5KiraKodu() {
		return cari_AvmBilgileri5KiraKodu;
	}
	public void setCari_AvmBilgileri5KiraKodu(String cari_AvmBilgileri5KiraKodu) {
		this.cari_AvmBilgileri5KiraKodu = cari_AvmBilgileri5KiraKodu;
	}
	public int getCari_AvmBilgileri5TebligatSekli() {
		return cari_AvmBilgileri5TebligatSekli;
	}
	public void setCari_AvmBilgileri5TebligatSekli(
			int cari_AvmBilgileri5TebligatSekli) {
		this.cari_AvmBilgileri5TebligatSekli = cari_AvmBilgileri5TebligatSekli;
	}
	public String getCari_AvmBilgileri6KiraKodu() {
		return cari_AvmBilgileri6KiraKodu;
	}
	public void setCari_AvmBilgileri6KiraKodu(String cari_AvmBilgileri6KiraKodu) {
		this.cari_AvmBilgileri6KiraKodu = cari_AvmBilgileri6KiraKodu;
	}
	public int getCari_AvmBilgileri6TebligatSekli() {
		return cari_AvmBilgileri6TebligatSekli;
	}
	public void setCari_AvmBilgileri6TebligatSekli(
			int cari_AvmBilgileri6TebligatSekli) {
		this.cari_AvmBilgileri6TebligatSekli = cari_AvmBilgileri6TebligatSekli;
	}
	public String getCari_AvmBilgileri7KiraKodu() {
		return cari_AvmBilgileri7KiraKodu;
	}
	public void setCari_AvmBilgileri7KiraKodu(String cari_AvmBilgileri7KiraKodu) {
		this.cari_AvmBilgileri7KiraKodu = cari_AvmBilgileri7KiraKodu;
	}
	public int getCari_AvmBilgileri7TebligatSekli() {
		return cari_AvmBilgileri7TebligatSekli;
	}
	public void setCari_AvmBilgileri7TebligatSekli(
			int cari_AvmBilgileri7TebligatSekli) {
		this.cari_AvmBilgileri7TebligatSekli = cari_AvmBilgileri7TebligatSekli;
	}
	public String getCari_AvmBilgileri8KiraKodu() {
		return cari_AvmBilgileri8KiraKodu;
	}
	public void setCari_AvmBilgileri8KiraKodu(String cari_AvmBilgileri8KiraKodu) {
		this.cari_AvmBilgileri8KiraKodu = cari_AvmBilgileri8KiraKodu;
	}
	public int getCari_AvmBilgileri8TebligatSekli() {
		return cari_AvmBilgileri8TebligatSekli;
	}
	public void setCari_AvmBilgileri8TebligatSekli(
			int cari_AvmBilgileri8TebligatSekli) {
		this.cari_AvmBilgileri8TebligatSekli = cari_AvmBilgileri8TebligatSekli;
	}
	public String getCari_AvmBilgileri9KiraKodu() {
		return cari_AvmBilgileri9KiraKodu;
	}
	public void setCari_AvmBilgileri9KiraKodu(String cari_AvmBilgileri9KiraKodu) {
		this.cari_AvmBilgileri9KiraKodu = cari_AvmBilgileri9KiraKodu;
	}
	public int getCari_AvmBilgileri9TebligatSekli() {
		return cari_AvmBilgileri9TebligatSekli;
	}
	public void setCari_AvmBilgileri9TebligatSekli(
			int cari_AvmBilgileri9TebligatSekli) {
		this.cari_AvmBilgileri9TebligatSekli = cari_AvmBilgileri9TebligatSekli;
	}
	public String getCari_AvmBilgileri10KiraKodu() {
		return cari_AvmBilgileri10KiraKodu;
	}
	public void setCari_AvmBilgileri10KiraKodu(String cari_AvmBilgileri10KiraKodu) {
		this.cari_AvmBilgileri10KiraKodu = cari_AvmBilgileri10KiraKodu;
	}
	public int getCari_AvmBilgileri10TebligatSekli() {
		return cari_AvmBilgileri10TebligatSekli;
	}
	public void setCari_AvmBilgileri10TebligatSekli(
			int cari_AvmBilgileri10TebligatSekli) {
		this.cari_AvmBilgileri10TebligatSekli = cari_AvmBilgileri10TebligatSekli;
	}
	public int getCari_KrediRiskTakibiVar_flg() {
		return cari_KrediRiskTakibiVar_flg;
	}
	public void setCari_KrediRiskTakibiVar_flg(int cari_KrediRiskTakibiVar_flg) {
		this.cari_KrediRiskTakibiVar_flg = cari_KrediRiskTakibiVar_flg;
	}
	public String getCari_ufrs_fark_muh_kod() {
		return cari_ufrs_fark_muh_kod;
	}
	public void setCari_ufrs_fark_muh_kod(String cari_ufrs_fark_muh_kod) {
		this.cari_ufrs_fark_muh_kod = cari_ufrs_fark_muh_kod;
	}
	public String getCari_ufrs_fark_muh_kod1() {
		return cari_ufrs_fark_muh_kod1;
	}
	public void setCari_ufrs_fark_muh_kod1(String cari_ufrs_fark_muh_kod1) {
		this.cari_ufrs_fark_muh_kod1 = cari_ufrs_fark_muh_kod1;
	}
	public String getCari_ufrs_fark_muh_kod2() {
		return cari_ufrs_fark_muh_kod2;
	}
	public void setCari_ufrs_fark_muh_kod2(String cari_ufrs_fark_muh_kod2) {
		this.cari_ufrs_fark_muh_kod2 = cari_ufrs_fark_muh_kod2;
	}
	public int getCari_odeme_sekli() {
		return cari_odeme_sekli;
	}
	public void setCari_odeme_sekli(int cari_odeme_sekli) {
		this.cari_odeme_sekli = cari_odeme_sekli;
	}
	public String getCari_TeminatMekAlacakMuhKodu() {
		return cari_TeminatMekAlacakMuhKodu;
	}
	public void setCari_TeminatMekAlacakMuhKodu(String cari_TeminatMekAlacakMuhKodu) {
		this.cari_TeminatMekAlacakMuhKodu = cari_TeminatMekAlacakMuhKodu;
	}
	public String getCari_TeminatMekAlacakMuhKodu1() {
		return cari_TeminatMekAlacakMuhKodu1;
	}
	public void setCari_TeminatMekAlacakMuhKodu1(
			String cari_TeminatMekAlacakMuhKodu1) {
		this.cari_TeminatMekAlacakMuhKodu1 = cari_TeminatMekAlacakMuhKodu1;
	}
	public String getCari_TeminatMekAlacakMuhKodu2() {
		return cari_TeminatMekAlacakMuhKodu2;
	}
	public void setCari_TeminatMekAlacakMuhKodu2(
			String cari_TeminatMekAlacakMuhKodu2) {
		this.cari_TeminatMekAlacakMuhKodu2 = cari_TeminatMekAlacakMuhKodu2;
	}
	public String getCari_TeminatMekBorcMuhKodu() {
		return cari_TeminatMekBorcMuhKodu;
	}
	public void setCari_TeminatMekBorcMuhKodu(String cari_TeminatMekBorcMuhKodu) {
		this.cari_TeminatMekBorcMuhKodu = cari_TeminatMekBorcMuhKodu;
	}
	public String getCari_TeminatMekBorcMuhKodu1() {
		return cari_TeminatMekBorcMuhKodu1;
	}
	public void setCari_TeminatMekBorcMuhKodu1(String cari_TeminatMekBorcMuhKodu1) {
		this.cari_TeminatMekBorcMuhKodu1 = cari_TeminatMekBorcMuhKodu1;
	}
	public String getCari_TeminatMekBorcMuhKodu2() {
		return cari_TeminatMekBorcMuhKodu2;
	}
	public void setCari_TeminatMekBorcMuhKodu2(String cari_TeminatMekBorcMuhKodu2) {
		this.cari_TeminatMekBorcMuhKodu2 = cari_TeminatMekBorcMuhKodu2;
	}
	public String getCari_VerilenDepozitoTeminatMuhKodu() {
		return cari_VerilenDepozitoTeminatMuhKodu;
	}
	public void setCari_VerilenDepozitoTeminatMuhKodu(
			String cari_VerilenDepozitoTeminatMuhKodu) {
		this.cari_VerilenDepozitoTeminatMuhKodu = cari_VerilenDepozitoTeminatMuhKodu;
	}
	public String getCari_VerilenDepozitoTeminatMuhKodu1() {
		return cari_VerilenDepozitoTeminatMuhKodu1;
	}
	public void setCari_VerilenDepozitoTeminatMuhKodu1(
			String cari_VerilenDepozitoTeminatMuhKodu1) {
		this.cari_VerilenDepozitoTeminatMuhKodu1 = cari_VerilenDepozitoTeminatMuhKodu1;
	}
	public String getCari_VerilenDepozitoTeminatMuhKodu2() {
		return cari_VerilenDepozitoTeminatMuhKodu2;
	}
	public void setCari_VerilenDepozitoTeminatMuhKodu2(
			String cari_VerilenDepozitoTeminatMuhKodu2) {
		this.cari_VerilenDepozitoTeminatMuhKodu2 = cari_VerilenDepozitoTeminatMuhKodu2;
	}
	public String getCari_AlinanDepozitoTeminatMuhKodu() {
		return cari_AlinanDepozitoTeminatMuhKodu;
	}
	public void setCari_AlinanDepozitoTeminatMuhKodu(
			String cari_AlinanDepozitoTeminatMuhKodu) {
		this.cari_AlinanDepozitoTeminatMuhKodu = cari_AlinanDepozitoTeminatMuhKodu;
	}
	public String getCari_AlinanDepozitoTeminatMuhKodu1() {
		return cari_AlinanDepozitoTeminatMuhKodu1;
	}
	public void setCari_AlinanDepozitoTeminatMuhKodu1(
			String cari_AlinanDepozitoTeminatMuhKodu1) {
		this.cari_AlinanDepozitoTeminatMuhKodu1 = cari_AlinanDepozitoTeminatMuhKodu1;
	}
	public String getCari_AlinanDepozitoTeminatMuhKodu2() {
		return cari_AlinanDepozitoTeminatMuhKodu2;
	}
	public void setCari_AlinanDepozitoTeminatMuhKodu2(
			String cari_AlinanDepozitoTeminatMuhKodu2) {
		this.cari_AlinanDepozitoTeminatMuhKodu2 = cari_AlinanDepozitoTeminatMuhKodu2;
	}
	public int getCari_def_efatura_cinsi() {
		return cari_def_efatura_cinsi;
	}
	public void setCari_def_efatura_cinsi(int cari_def_efatura_cinsi) {
		this.cari_def_efatura_cinsi = cari_def_efatura_cinsi;
	}
	
	
	

}
