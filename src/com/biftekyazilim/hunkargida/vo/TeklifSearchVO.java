package com.biftekyazilim.hunkargida.vo;

public class TeklifSearchVO {
	
	private String cari_unvan1;
    private String tkl_evrakno_seri;
    private int tkl_evrakno_sira;
    
	public String getCari_unvan1() {
		return cari_unvan1;
	}
	public void setCari_unvan1(String cari_unvan1) {
		this.cari_unvan1 = cari_unvan1;
	}
	public String getTkl_evrakno_seri() {
		return tkl_evrakno_seri;
	}
	public void setTkl_evrakno_seri(String tkl_evrakno_seri) {
		this.tkl_evrakno_seri = tkl_evrakno_seri;
	}
	public int getTkl_evrakno_sira() {
		return tkl_evrakno_sira;
	}
	public void setTkl_evrakno_sira(int tkl_evrakno_sira) {
		this.tkl_evrakno_sira = tkl_evrakno_sira;
	}

    
}
