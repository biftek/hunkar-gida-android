package com.biftekyazilim.hunkargida.vo;

public class CariAdresVO {

	private int adr_RECno;
	private String adr_cari_kod;
	private String cari_ismi;
	private int adr_adres_no;
	private String adr_cadde;
	private String adr_sokak;
	private String adr_posta_kodu;
	private String adr_ilce;
	private String adr_il;
	private String adr_ulke;
	private String tel1;
	private String tel2;
	private String fax;
	private String modem;
	private String adr_yon_kodu;
	private int adr_uzaklik_kodu;
	private String adr_temsilci_kodu;
	private String temsilci_kodu;
	private String adr_ozel_not;
	private long adr_gps_enlem;
	private long adr_gps_boylam;
	public int getAdr_RECno() {
		return adr_RECno;
	}
	public void setAdr_RECno(int adr_RECno) {
		this.adr_RECno = adr_RECno;
	}
	public String getAdr_cari_kod() {
		return adr_cari_kod;
	}
	public void setAdr_cari_kod(String adr_cari_kod) {
		this.adr_cari_kod = adr_cari_kod;
	}
	public String getCari_ismi() {
		return cari_ismi;
	}
	public void setCari_ismi(String cari_ismi) {
		this.cari_ismi = cari_ismi;
	}
	public int getAdr_adres_no() {
		return adr_adres_no;
	}
	public void setAdr_adres_no(int adr_adres_no) {
		this.adr_adres_no = adr_adres_no;
	}
	public String getAdr_cadde() {
		return adr_cadde;
	}
	public void setAdr_cadde(String adr_cadde) {
		this.adr_cadde = adr_cadde;
	}
	public String getAdr_sokak() {
		return adr_sokak;
	}
	public void setAdr_sokak(String adr_sokak) {
		this.adr_sokak = adr_sokak;
	}
	public String getAdr_posta_kodu() {
		return adr_posta_kodu;
	}
	public void setAdr_posta_kodu(String adr_posta_kodu) {
		this.adr_posta_kodu = adr_posta_kodu;
	}
	public String getAdr_ilce() {
		return adr_ilce;
	}
	public void setAdr_ilce(String adr_ilce) {
		this.adr_ilce = adr_ilce;
	}
	public String getAdr_il() {
		return adr_il;
	}
	public void setAdr_il(String adr_il) {
		this.adr_il = adr_il;
	}
	public String getAdr_ulke() {
		return adr_ulke;
	}
	public void setAdr_ulke(String adr_ulke) {
		this.adr_ulke = adr_ulke;
	}
	public String getTel1() {
		return tel1;
	}
	public void setTel1(String tel1) {
		this.tel1 = tel1;
	}
	public String getTel2() {
		return tel2;
	}
	public void setTel2(String tel2) {
		this.tel2 = tel2;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getModem() {
		return modem;
	}
	public void setModem(String modem) {
		this.modem = modem;
	}
	public String getAdr_yon_kodu() {
		return adr_yon_kodu;
	}
	public void setAdr_yon_kodu(String adr_yon_kodu) {
		this.adr_yon_kodu = adr_yon_kodu;
	}
	public int getAdr_uzaklik_kodu() {
		return adr_uzaklik_kodu;
	}
	public void setAdr_uzaklik_kodu(int adr_uzaklik_kodu) {
		this.adr_uzaklik_kodu = adr_uzaklik_kodu;
	}
	public String getAdr_temsilci_kodu() {
		return adr_temsilci_kodu;
	}
	public void setAdr_temsilci_kodu(String adr_temsilci_kodu) {
		this.adr_temsilci_kodu = adr_temsilci_kodu;
	}
	public String getTemsilci_kodu() {
		return temsilci_kodu;
	}
	public void setTemsilci_kodu(String temsilci_kodu) {
		this.temsilci_kodu = temsilci_kodu;
	}
	public String getAdr_ozel_not() {
		return adr_ozel_not;
	}
	public void setAdr_ozel_not(String adr_ozel_not) {
		this.adr_ozel_not = adr_ozel_not;
	}
	public long getAdr_gps_enlem() {
		return adr_gps_enlem;
	}
	public void setAdr_gps_enlem(long adr_gps_enlem) {
		this.adr_gps_enlem = adr_gps_enlem;
	}
	public long getAdr_gps_boylam() {
		return adr_gps_boylam;
	}
	public void setAdr_gps_boylam(long adr_gps_boylam) {
		this.adr_gps_boylam = adr_gps_boylam;
	}
	
	
}
