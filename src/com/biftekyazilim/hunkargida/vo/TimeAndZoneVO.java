package com.biftekyazilim.hunkargida.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

public class TimeAndZoneVO {

	private String date;
	private int timezone_type;
	private String timezone;
	private String formattedDate;
	
	public TimeAndZoneVO() {
		// TODO Auto-generated constructor stub
	}
	public TimeAndZoneVO(String date, int timezone_type, String timezone) {
		super();
		this.date = date;
		this.timezone_type = timezone_type;
		this.timezone = timezone;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTimezone_type() {
		return timezone_type;
	}
	public void setTimezone_type(int timezone_type) {
		this.timezone_type = timezone_type;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	public String getFormattedDate() {
		if(formattedDate == null) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("tr"));
				sdf.setTimeZone(TimeZone.getTimeZone(timezone));
				Date dt = sdf.parse(date);
				
				SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy", new Locale("tr"));
				formattedDate = sdf1.format(dt);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return formattedDate;
		
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return date;
	}
	
}
