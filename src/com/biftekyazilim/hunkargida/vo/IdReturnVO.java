package com.biftekyazilim.hunkargida.vo;

public class IdReturnVO {

	private int id;
	private int error;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getError() {
		return error;
	}

	public void setError(int error) {
		this.error = error;
	}
	
}
