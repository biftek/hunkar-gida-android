package com.biftekyazilim.hunkargida;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.adapter.NavListAdapter;
import com.biftekyazilim.hunkargida.bo.LoginBO;
import com.biftekyazilim.hunkargida.bo.StaticDataBO;
import com.biftekyazilim.hunkargida.enm.FragmentType;
import com.biftekyazilim.hunkargida.fragment.ArgumentableFragment;
import com.biftekyazilim.hunkargida.util.AuthLockUtil;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.vo.NavItemVO;
 
public class MainActivity extends Activity {
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
 
    // nav drawer title
    private CharSequence mDrawerTitle;
 
    // used to store app title
    private CharSequence mTitle;
 
    // slide menu items
    private FragmentType[] fragmentTypeList;
 
    private ArrayList<NavItemVO> navItemList;
    private NavListAdapter adapter;
    private Map<FragmentType, ArgumentableFragment> fragmentMap;
 
    private static boolean isFirstOpen = true;
    public static boolean FRAGMENT_CHANGE = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        
        setContentView(R.layout.activity_main);
 
        mTitle = mDrawerTitle = getTitle();
 
        // load slide menu items
        fragmentTypeList = new FragmentType[]{
        		FragmentType.CARI_ARAMA,
        		FragmentType.SIPARIS_VER,
        		FragmentType.TEKLIF_VER,
        		FragmentType.SIPARISLERIM
        };
 
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
 
        navItemList = new ArrayList<NavItemVO>();
 
        // adding nav drawer items to array
        // Home
        for (FragmentType fragmentType : fragmentTypeList) {
        	NavItemVO nav = new NavItemVO(fragmentType);
        	navItemList.add(nav);
        	nav.setEnabled(true);
		}
        
         
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
 
        // setting the nav drawer list adapter
        adapter = new NavListAdapter(getApplicationContext(),
                navItemList);
        mDrawerList.setAdapter(adapter);
 
        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setHomeButtonEnabled(true);
 
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }
 
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        getFragmentManager().addOnBackStackChangedListener(new OnBackStackChangedListener() {

        	  @Override
        	  public void onBackStackChanged() {
        	    Fragment f = getFragmentManager().findFragmentById(R.id.frame_container);
        	    if (f != null){
        	      updateTitleAndDrawer(f);
        	    }

        	  }
        	});
        
        //final ProgressDialog dialog = ProgressDialog.show(this, "",  "Yükleniyor. Bekleyiniz...", true);
        StaticDataBO.getInstance(this).loadStaticData(new Callback<Boolean>() {
			
			@Override
			public void success(Boolean arg0, Response arg1) {
				//dialog.dismiss();
			}
			
			@Override
			public void failure(RetrofitError arg0) {
				new AlertDialog.Builder(MainActivity.this)
			    .setTitle(R.string.app_name)
			    .setMessage("Uygulama servislerinde hata meydana geldi. Lütfen uygulamayı kapatıp açmayı deneyiniz.")
			    .setPositiveButton(android.R.string.ok, null)
			    .setIcon(android.R.drawable.ic_dialog_alert)
			     .show();
				//dialog.dismiss();
			}
		});
        
        if(isFirstOpen) {
        	mDrawerLayout.openDrawer(Gravity.LEFT);
        	isFirstOpen = false;
        }
    }
 
    /**
     * Slide menu item click listener
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
            // display view for selected nav drawer item
            displayView(position);
        }
    }
 
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
 
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action bar actions click
        switch (item.getItemId()) {
        case R.id.action_logout:
        	LoginBO.getInstance(this).deleteLogin();
        	Intent i= new Intent(this, LoginActivity.class);//homescreen of your app.
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(i);
            finish(); 
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
 
    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_logout).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }
 
    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
    	
    	
    	NavItemVO nav = navItemList.get(position);
    	if(!nav.isEnabled()) {
    		return;
    	}
    	FRAGMENT_CHANGE = true;
        // update the main content by replacing fragments
        Fragment fragment = null;
        try { 
        	fragment = getFragment(nav.getFragmentType());
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "Sistemde bir hata oluştu. Lütfen geliştirici ile iletişime geçiniz. (" + navItemList.get(position).getFragmentType().title + ", " + e.getClass().getCanonicalName() + ")", Toast.LENGTH_LONG).show();
			return;
		}
 
        if (fragment != null) {
            replaceFragment(fragment);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }
 
    
    private void updateTitleAndDrawer(Fragment f) {
    	
    	int position = -1;
    	FragmentType fragmentType = null;
    	for (Entry<FragmentType, ArgumentableFragment> fragmentEntry : fragmentMap.entrySet()) {
			if(fragmentEntry.getValue().equals(f)) {
				fragmentType = fragmentEntry.getKey();
				break;
			}
		}
    	if(fragmentType == null)
    		return;
    	for (int i = 0; i < fragmentTypeList.length; i++) {
			if(fragmentTypeList[i] == fragmentType) {
				position = i;
				break;
			}
		}
    	if(position == -1)
    		return;
    	// update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        setTitle(fragmentType.title);
        mDrawerLayout.closeDrawer(mDrawerList);
        
        
    }
    
	private void replaceFragment(Fragment fragment) {
		String backStateName = fragment.getClass().getName();
		String fragmentTag = backStateName;

		FragmentManager manager = getFragmentManager();
		boolean fragmentPopped = manager
				.popBackStackImmediate(backStateName, 0);

		if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
			FragmentTransaction ft = manager.beginTransaction();
			ft.replace(R.id.frame_container, fragment, fragmentTag);
			ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			ft.addToBackStack(backStateName);
			ft.commit();
		}
	}
    
    public void popBackFragment() {
    	UIUtil.hideKeyboard(this);
    	getFragmentManager().popBackStackImmediate();
    	MainActivity.FRAGMENT_CHANGE = false;
    }
    
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
 
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
 
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
 
    
    
    private ArgumentableFragment getFragment(FragmentType fragmentType) throws Exception {
    	if(fragmentMap == null) {
    		fragmentMap = new HashMap<FragmentType, ArgumentableFragment>();
    	}
    	
    	ArgumentableFragment frgmnt = null;
    	
    	if(fragmentMap.containsKey(fragmentType)) {
    		frgmnt = fragmentMap.get(fragmentType);
    	} else {
        	frgmnt = (ArgumentableFragment) fragmentType.clazz.newInstance();
        	fragmentMap.put(fragmentType, frgmnt);
    	}
    	
    	return frgmnt;
    }
    
    
    public void displayFragment(FragmentType fragmentType, Bundle arguments) {
        // update the main content by replacing fragments
        ArgumentableFragment fragment = null;
        try { 
        	fragment = getFragment(fragmentType);
        	
        	if(arguments != null) {
	        	for(String key : arguments.keySet()) {
	        		fragment.getArguments().putSerializable(key, arguments.getSerializable(key));
	        	}
        	}
        	
		} catch (Exception e) {
			e.printStackTrace();
			Toast.makeText(this, "Sistemde bir hata oluştu. Lütfen geliştirici ile iletişime geçiniz. (" + fragmentType.clazz.getCanonicalName() + ", " + e.getClass().getCanonicalName() + ")", Toast.LENGTH_LONG).show();
			return;
		}
 
        if (fragment != null) {
            replaceFragment(fragment);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
    }
    
}