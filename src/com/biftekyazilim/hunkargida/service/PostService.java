package com.biftekyazilim.hunkargida.service;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.Headers;
import retrofit.http.POST;

import com.biftekyazilim.hunkargida.vo.IdReturnVO;

public interface PostService {

	/*
	 * tkl_create_user=100 // kullanıcı id (sisteme giriş yapan kullanıcı id)
	 * tkl_create_date=yyyyMMdd HH:mm:ss.S // oluşturma tarihi
	 * tkl_lastup_user=100 // update eden kullanıcı id tkl_lastup_date=yyyyMMdd
	 * HH:mm:ss.S // son update tarihi tkl_stok_kod=010.01.000021 // stok
	 * numarası (stok listesinden alınacak) tkl_cari_kod=120.1.01.00011 // cari
	 * kod (cari listeden alınaca) tkl_evrakno_seri=ENSP // Evrak Seri no
	 * tkl_evrakno_sira=3 // Evrak Sıra no tkl_evrak_tarihi=yyyyMMdd // Evrak
	 * Tarihi tkl_satirno=0 // Izgaradaki satır numarası (her bir satır
	 * teklifteki bir ürün) tkl_belge_no=4 // Belge no tkl_belge_tarih=yyyyMMdd
	 * // Belge Tarihi tkl_baslangic_tarihi=yyyyMMdd // Teklif başlangıç tarihi
	 * tkl_Gecerlilik_Sures=yyyyMMdd // Teklif geçerlilik süresi
	 * tkl_Odeme_Plani=-60 // Ödeme Planı (cari kartta dönen değer)
	 * tkl_Alisfiyati=3 // Ürünün alış fiyatı tkl_karorani=3 // Ürüne % bazında
	 * eklenecek kar tkl_miktar=4 // Miktar tkl_alt_doviz_kur=2.13 // Döviz kuru
	 * (döviz servisinden alınacak) tkl_vergi_pntr=1 // Vergi kodu (Stok
	 * kartından alınacak ) örn tkl_Sorumlu_Kod=0.MURATE // Sorumlu kodu
	 * (sisteme giriş yapan kullanıcı) tkl_cari_sormerk=1 // Sorumluluk merkezi
	 * (servisten alınacak) tkl_stok_sormerk=1 // Sorumluluk merkezi (servisten
	 * alınacak)
	 */

	@Headers({ "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" })
	@FormUrlEncoded
	@POST("/teklif-kaydet")
	void saveTeklif(@Field("tkl_create_user") int tkl_create_user,
			@Field("tkl_create_date") String tkl_create_date,
			@Field("tkl_lastup_user") int tkl_lastup_user,
			@Field("tkl_lastup_date") String tkl_lastup_date,
			@Field("tkl_stok_kod") String tkl_stok_kod,
			@Field("tkl_cari_kod") String tkl_cari_kod,
			@Field("tkl_evrakno_seri") String tkl_evrakno_seri,
			@Field("tkl_evrakno_sira") int tkl_evrakno_sira,
			@Field("tkl_evrak_tarihi") String tkl_evrak_tarihi,
			@Field("tkl_satirno") int tkl_satirno,
			@Field("tkl_belge_no") String tkl_belge_no,
			@Field("tkl_belge_tarih") String tkl_belge_tarih,
			@Field("tkl_baslangic_tarihi") String tkl_baslangic_tarihi,
			@Field("tkl_Gecerlilik_Sures") String tkl_Gecerlilik_Sures,
			@Field("tkl_Odeme_Plani") int tkl_Odeme_Plani,
			@Field("tkl_Alisfiyati") double tkl_Alisfiyati,
			@Field("tkl_karorani") double tkl_karorani,
			@Field("tkl_miktar") double tkl_miktar,
			@Field("tkl_alt_doviz_kur") double tkl_alt_doviz_kur,
			@Field("tkl_vergi_pntr") int tkl_vergi_pntr,
			@Field("tkl_Sorumlu_Kod") String tkl_Sorumlu_Kod,
			@Field("tkl_cari_sormerk") String tkl_cari_sormerk,
			@Field("tkl_stok_sormerk") String tkl_stok_sormerk,
			Callback<IdReturnVO> callback

	);

	
	/*  sip_create_user = 100;        
        sip_lastup_user = 100;        
        sip_tarih = yyyyMMdd;
        sip_teslim_tarih = yyyyMMdd;
        sip_evrakno_seri = "ENSP";
        sip_evrakno_sira = "4";
        sip_belgeno = "5";
        sip_belge_tarih = yyyyMMdd;
        sip_satici_kod = "0.MURATE";
        sip_musteri_kod = "120.1.01.00001";
        sip_stok_kod = "010.01.000021";
        sip_b_fiyat = "4";                      birim fiyat
        sip_miktar = "2";
        sip_birim_pntr = "1";               stok tan alınan birim pntr
        sip_vergi_pntr = "3";
        sip_opno = "-60";
        sip_depono = "1";
        sip_cari_sormerk = "1";
        sip_stok_sormerk = "1";
        sip_alt_doviz_kuru = "2.133900000000";
        sip_adresno = "1";
*/
	@Headers({ "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" })
	@FormUrlEncoded
	@POST("/siparis-kaydet")
	void saveSiparis(
			@Field("sip_satirno") int sip_satirno,
			@Field("sip_create_user") int sip_create_user,
			@Field("sip_lastup_user") int sip_lastup_user,
			@Field("sip_tarih") String sip_tarih,
			@Field("sip_teslim_tarih") String sip_teslim_tarih,
			@Field("sip_evrakno_seri") String sip_evrakno_seri,
			@Field("sip_evrakno_sira") int sip_evrakno_sira,
			@Field("sip_belgeno") String sip_belgeno,
			@Field("sip_belge_tarih") String sip_belge_tarih,
			@Field("sip_satici_kod") String sip_satici_kod,
			@Field("sip_musteri_kod") String sip_musteri_kod,
			@Field("sip_stok_kod") String sip_stok_kod,
			@Field("sip_b_fiyat") double sip_b_fiyat,
			@Field("sip_miktar") double sip_miktar,
			@Field("sip_birim_pntr") int sip_birim_pntr,
			@Field("sip_vergi_pntr") int sip_vergi_pntr,
			@Field("sip_opno") int sip_opno,
			@Field("sip_depono") int sip_depono,
			@Field("sip_cari_sormerk") String sip_cari_sormerk,
			@Field("sip_stok_sormerk") String sip_stok_sormerk,
			@Field("sip_alt_doviz_kuru") double sip_alt_doviz_kuru,
			@Field("sip_adresno") int sip_adresno,
			Callback<IdReturnVO> callback);
	
	@Headers({ "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" })
	@FormUrlEncoded
	@POST("/siparis-sil")
	void deleteSiparis(
			@Field("sip_evrakno_seri") String serino,
			@Field("sip_evrakno_sira") String sirano,
			Callback<IdReturnVO> callback);
	
	@Headers({ "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" })
	@FormUrlEncoded
	@POST("/teklif-sil")
	void deleteTeklif(
			@Field("sip_evrakno_seri") String serino,
			@Field("sip_evrakno_sira") String sirano,
			Callback<IdReturnVO> callback);
}
