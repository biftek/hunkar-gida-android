package com.biftekyazilim.hunkargida.service;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

import com.biftekyazilim.hunkargida.vo.CariAdresVO;
import com.biftekyazilim.hunkargida.vo.CariHesapVO;
import com.biftekyazilim.hunkargida.vo.CariStokVO;
import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.CariVO;
import com.biftekyazilim.hunkargida.vo.DepoVO;
import com.biftekyazilim.hunkargida.vo.SiparisSearchVO;
import com.biftekyazilim.hunkargida.vo.SiparisTakipVO;
import com.biftekyazilim.hunkargida.vo.SiparisVO;
import com.biftekyazilim.hunkargida.vo.SiraNoVO;
import com.biftekyazilim.hunkargida.vo.SorumlulukMerkeziVO;
import com.biftekyazilim.hunkargida.vo.StokFiyatVO;
import com.biftekyazilim.hunkargida.vo.StokSearchVO;
import com.biftekyazilim.hunkargida.vo.StokVO;
import com.biftekyazilim.hunkargida.vo.TeklifSearchVO;
import com.biftekyazilim.hunkargida.vo.TeklifSummaryVO;
import com.biftekyazilim.hunkargida.vo.TeklifVO;

public interface GetService {

	
	//http://78.186.185.146/hunkar/api/index/teklif-listesi
	@GET("/siparis-takip")
	void getSiparisTakipList(Callback<List<SiparisTakipVO>> callback);
	
	@GET("/siparis-takip/cariKodu/{cari_kod}")
	void getCariSiparisTakipList(@Path("cari_kod") String cariKod, Callback<List<SiparisTakipVO>> callback);
	
	@GET("/teklif-listesi")
	void getTeklifList(Callback<List<TeklifSummaryVO>> callback);
	
	@GET("/teklif/tkl_evrakno_seri/{seri_no}/tkl_evrakno_sira/{sira_no}")
	void getTeklif(@Path("seri_no") String seriNo, @Path("sira_no") int siraNo, Callback<List<TeklifVO>> callback);
	
	@GET("/teklif-listesi/tkl_evrakno_seri/{seri_no}/tkl_evrakno_sira/{sira_no}")
	void searchTeklif(@Path("seri_no") String seriNo, @Path("sira_no") String siraNo, Callback<List<TeklifSearchVO>> callback);
	
	@GET("/teklif-listesi/tkl_evrakno_seri/{seri_no}")
	void searchTeklif(@Path("seri_no") String seriNo, Callback<List<TeklifSearchVO>> callback);
	
	@GET("/sorumluluk-merkezleri")
	void getSorumlulukMerkeziList(Callback<List<SorumlulukMerkeziVO>> callback);
	
	@GET("/depo-listesi")
	void getDepoList(Callback<List<DepoVO>> callback);
	
	
	@GET("/cari/recno/{cari_rec_no}")
	void getCari(@Path("cari_rec_no") int cariRecNo, Callback<CariVO> callback);
	
	
	@GET("/cari-liste/{search_by}/{search_key}")
	void searchCari(@Path("search_by") String searchBy,@Path("search_key") String searchKey, Callback<List<CariSummaryVO>> callback); //searchBy: cariIsmi, cariKodu
	
	@GET("/siparis/sip_evrakno_seri/{seri_no}/sip_evrakno_sira/{sira_no}")
	void getSiparis(@Path("seri_no") String seriNo, @Path("sira_no") int siraNo, Callback<List<SiparisVO>> callback);
	
	@GET("/siparis-listesi/sip_evrakno_seri/{seri_no}/sip_evrakno_sira/{sira_no}")
	void searchSiparis(@Path("seri_no") String seriNo, @Path("sira_no") String siraNo, Callback<List<SiparisSearchVO>> callback);
	
	@GET("/siparis-listesi/sip_evrakno_seri/{seri_no}")
	void searchSiparis(@Path("seri_no") String seriNo, Callback<List<SiparisSearchVO>> callback);
	
	@GET("/cari-hareket/cariKodu/{cari_kod}")
	void getCariHesapHareket(@Path("cari_kod") String cariKod, Callback<List<CariHesapVO>> callback);
	
	@GET("/stok-hareket/cariKodu/{cari_kod}")
	void getCariStokHareket(@Path("cari_kod") String cariKod, Callback<List<CariStokVO>> callback);
	
	@GET("/stok-listesi/{search_by}/{search_key}")
	void searchStok(@Path("search_by") String searchBy,@Path("search_key") String searchKey, Callback<List<StokSearchVO>> callback);
	
	@GET("/cari-adresler/cariKodu/{cari_kod}")
	void getCariAdresler(@Path("cari_kod") String cariKodu, Callback<List<CariAdresVO>> callback);
	
	@GET("/stok/recno/{rec_no}")
	void getStok(@Path("rec_no") int recNo, Callback<List<StokVO>> callback);
	
	@GET("/stok-fiyat/stokKodu/{stok_kodu}/cariKodu/{cari_kodu}")
	void getStokFiyat(@Path("stok_kodu") String stokKodu, @Path("cari_kodu") String cariKodu, Callback<StokFiyatVO> callback);
	
	@GET("/teklif-evrak-sira-no/serino/{evrak_seri_no}")
	void getTeklifSiraNo(@Path("evrak_seri_no") String evrakSeriNo, Callback<SiraNoVO> callback);
	
	@GET("/siparis-evrak-sira-no/serino/{evrak_seri_no}")
	void getSiparisSiraNo(@Path("evrak_seri_no") String evrakSeriNo, Callback<SiraNoVO> callback);
}
