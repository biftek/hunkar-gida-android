package com.biftekyazilim.hunkargida;

import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.biftekyazilim.hunkargida.bo.LoginBO;
import com.biftekyazilim.hunkargida.bo.RestBO;
import com.biftekyazilim.hunkargida.service.LoginService;
import com.biftekyazilim.hunkargida.util.AuthLockUtil;
import com.biftekyazilim.hunkargida.util.UIUtil;
import com.biftekyazilim.hunkargida.vo.UserVO;

/**
 * Activity which displays a login screen to the user, offering registration as
 * well.
 */
public class LoginActivity extends Activity {

	// Values for email and password at the time of the login attempt.
	private String strUsername;
	private String strPassword;

	// UI references.
	private EditText txtEmail;
	private EditText txtPassword;
	private View vwLoginForm;
	private View vwLoginStatus;
	private TextView vwLoginStatusMessage;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		AuthLockUtil.checkAuth(new Callback<Boolean>() {
			
			@Override
			public void success(Boolean arg0, Response arg1) {
				if(arg0 == false) {
					Toast.makeText(getApplication(), "Servislerde belirlenemeyen bir hata oluştu.", Toast.LENGTH_LONG).show();
					finish();
				} else {
					initAuthorizedApp();
				}
			}
			
			@Override
			public void failure(RetrofitError arg0) {
				initAuthorizedApp();
			}
		});
		
		
	}

	private void initAuthorizedApp() {
		if(LoginBO.getInstance(this).isLoggedIn()) {
			
			doLogin();
			
		} else {
			setContentView(R.layout.activity_login);
	
			txtEmail = (EditText) findViewById(R.id.email);
			txtPassword = (EditText) findViewById(R.id.password);
			txtPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
						@Override
						public boolean onEditorAction(TextView textView, int id,
								KeyEvent keyEvent) {
							if (id == R.id.login || id == EditorInfo.IME_NULL) {
								attemptLogin();
								return true;
							}
							return false;
						}
					});
	
			vwLoginForm = findViewById(R.id.login_form);
			vwLoginStatus = findViewById(R.id.login_status);
			vwLoginStatusMessage = (TextView) findViewById(R.id.login_status_message);
	
			findViewById(R.id.sign_in_button).setOnClickListener(
					new View.OnClickListener() {
						@Override
						public void onClick(View view) {
							UIUtil.hideKeyboard(LoginActivity.this);
							attemptLogin();
						}
					});
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		//getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	/**
	 * Attempts to sign in or register the account specified by the login form.
	 * If there are form errors (invalid email, missing fields, etc.), the
	 * errors are presented and no actual login attempt is made.
	 */
	public void attemptLogin() {

		// Reset errors.
		txtEmail.setError(null);
		txtPassword.setError(null);

		// Store values at the time of the login attempt.
		strUsername = txtEmail.getText().toString();
		strPassword = txtPassword.getText().toString();

		boolean cancel = false;
		View focusView = null;

		// Check for a valid password.
		if (TextUtils.isEmpty(strPassword)) {
			txtPassword.setError(getString(R.string.error_field_required));
			focusView = txtPassword;
			cancel = true;
		} else if (strPassword.length() < 4) {
			txtPassword.setError(getString(R.string.error_invalid_password));
			focusView = txtPassword;
			cancel = true;
		}

		// Check for a valid email address.
		if (TextUtils.isEmpty(strUsername)) {
			txtEmail.setError(getString(R.string.error_field_required));
			focusView = txtEmail;
			cancel = true;
		}

		if (cancel) {
			// There was an error; don't attempt login and focus the first
			// form field with an error.
			focusView.requestFocus();
		} else {
			// Show a progress spinner, and kick off a background task to
			// perform the user login attempt.
			vwLoginStatusMessage.setText(R.string.login_progress_signing_in);
			showProgress(true);
			
			RestBO.getInstance(this).getService(LoginService.class).loginUser(strUsername, strPassword, new Callback<Map<String, String>>() {
				
				@Override
				public void success(Map<String, String> inf, Response arg1) {
					showProgress(false);

					if (inf != null && inf.containsKey("result") && inf.get("result").equals("success")) {
						
						UserVO user = new UserVO(inf.get("userID"), strUsername, strPassword);
						LoginBO.getInstance(LoginActivity.this).saveLogin(user);
						doLogin();
						
					} else {
						txtPassword.setError(getString(R.string.error_incorrect_credentials));
						txtPassword.requestFocus();
					}
				}
				
				@Override
				public void failure(RetrofitError err) {
					showProgress(false);
					Toast.makeText(LoginActivity.this, "Kullanıcı adı yada şifreniz hatalı.", Toast.LENGTH_LONG).show();
					err.printStackTrace();
					
				}
			});
			
		}
	}

	/**
	 * Shows the progress UI and hides the login form.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	private void showProgress(final boolean show) {
		// On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
		// for very easy animations. If available, use these APIs to fade-in
		// the progress spinner.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
			int shortAnimTime = getResources().getInteger(
					android.R.integer.config_shortAnimTime);

			vwLoginStatus.setVisibility(View.VISIBLE);
			vwLoginStatus.animate().setDuration(shortAnimTime)
					.alpha(show ? 1 : 0)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							vwLoginStatus.setVisibility(show ? View.VISIBLE
									: View.GONE);
						}
					});

			vwLoginForm.setVisibility(View.VISIBLE);
			vwLoginForm.animate().setDuration(shortAnimTime)
					.alpha(show ? 0 : 1)
					.setListener(new AnimatorListenerAdapter() {
						@Override
						public void onAnimationEnd(Animator animation) {
							vwLoginForm.setVisibility(show ? View.GONE
									: View.VISIBLE);
						}
					});
		} else {
			// The ViewPropertyAnimator APIs are not available, so simply show
			// and hide the relevant UI components.
			vwLoginStatus.setVisibility(show ? View.VISIBLE : View.GONE);
			vwLoginForm.setVisibility(show ? View.GONE : View.VISIBLE);
		}
	}

	private void doLogin() {
		
		if(txtEmail != null) {
			txtEmail.setText("");
			txtPassword.setText("");
		}
		Intent intent = new Intent(this, MainActivity.class);
		startActivity(intent);
	}
	
	public boolean isOnline() {
	    ConnectivityManager cm =
	        (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo netInfo = cm.getActiveNetworkInfo();
	    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
	        return true;
	    }
	    return false;
	}
}
