package com.biftekyazilim.hunkargida.enm;

import com.biftekyazilim.hunkargida.vo.CariSummaryVO;
import com.biftekyazilim.hunkargida.vo.SiparisVO;
import com.biftekyazilim.hunkargida.vo.StokSearchVO;
import com.biftekyazilim.hunkargida.vo.TeklifVO;

public enum SearchService {

	STOK (StokSearchVO.class),
	CARI (CariSummaryVO.class),
	DOVIZ (null),
	ODEME_PLANI (null),
	TEKLIF(TeklifVO.class),
	SIPARIS(SiparisVO.class);
	
	public final Class classType;
	private SearchService(Class classType) {
		this.classType = classType;
		// TODO Auto-generated constructor stub
	}
}
