package com.biftekyazilim.hunkargida.enm;

import com.biftekyazilim.hunkargida.fragment.CariHesapFragment;
import com.biftekyazilim.hunkargida.fragment.CariKartFragment;
import com.biftekyazilim.hunkargida.fragment.CariStokFragment;
import com.biftekyazilim.hunkargida.fragment.SiparisListesiFragment;
import com.biftekyazilim.hunkargida.fragment.SiparisVerFragment;
import com.biftekyazilim.hunkargida.fragment.TeklifListesiFragment;
import com.biftekyazilim.hunkargida.fragment.TeklifVerFragment;

public enum FragmentType {
	
	CARI_ARAMA("Cari Arama", CariKartFragment.class),
	TEKLIF_VER("Verilen Teklif Fişi", TeklifVerFragment.class),
	SIPARIS_VER("Alınan Sipariş Fişi", SiparisVerFragment.class),
	TEKLIF_LISTESI("Teklif Listesi", TeklifListesiFragment.class),
	SIPARISLERIM("Siparişlerim", SiparisListesiFragment.class),
	CARI_KART("Cari Kart", CariKartFragment.class),
	CARI_HESAP("Cari Hesap", CariHesapFragment.class),
	CARI_STOK("Cari Stok", CariStokFragment.class);
	
	public final String title;
	public final Class<?> clazz;
	private FragmentType(String title, Class<?> clazz) {
		this.title = title;
		this.clazz = clazz;
	}

}
