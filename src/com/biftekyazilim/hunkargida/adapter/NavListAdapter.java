package com.biftekyazilim.hunkargida.adapter;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.biftekyazilim.hunkargida.R;
import com.biftekyazilim.hunkargida.vo.NavItemVO;
 
public class NavListAdapter extends BaseAdapter {
     
    private Context context;
    private ArrayList<NavItemVO> navItemList;
     
    public NavListAdapter(Context context, ArrayList<NavItemVO> navItemList){
        this.context = context;
        this.navItemList = navItemList;
    }
 
    @Override
    public int getCount() {
        return navItemList.size();
    }
 
    @Override
    public Object getItem(int position) {      
        return navItemList.get(position);
    }
 
    @Override
    public long getItemId(int position) {
        return position;
    }
 
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.nav_item, null);
        }
          
        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        TextView txtCount = (TextView) convertView.findViewById(R.id.counter);
          
        txtTitle.setText(navItemList.get(position).getFragmentType().title);
         
        // displaying count
        // check whether it set visible or not
        if(navItemList.get(position).getCounterVisibility()){
            txtCount.setText(navItemList.get(position).getCount());
        }else{
            // hide the counter view
            txtCount.setVisibility(View.GONE);
        }
         
        return convertView;
    }
 
}